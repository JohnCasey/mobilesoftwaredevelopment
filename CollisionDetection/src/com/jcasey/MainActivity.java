package com.jcasey;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class MainActivity extends Activity {
    
	  int x[]= {0,200,190,218,260,275,298,309,327,336,368,382,448,462,476,498,527,600,600,0,0};
	  int y[]={616,540,550,605,605,594,530,520,520,527,626,636,636,623,535,504,481,481,750,750,616};

	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        class Test extends View implements OnTouchListener
        {

			public Test(Context context) {
				super(context);
				
				this.setOnTouchListener(this);
				// TODO Auto-generated constructor stub
			}


			
			
			@Override
			protected void onDraw(Canvas canvas) {
				Paint paint = new Paint();
				
				paint.setColor(Color.RED);
				
				Path path = new Path();
				
				path.moveTo(x[0], y[0]);
				
				for(int i = 1; i< x.length ; i++)
				{
					path.lineTo(x[i], y[i]);
				}
				
				canvas.drawPath(path,paint);

				paint.setColor(Color.BLACK);
				
				for(int i = 0; i< x.length ; i++)
				{
					canvas.drawText(""+x[i]+","+y[i],x[i], y[i], paint);
				}
				
				if(contains)
				{
					Paint paint2 = new Paint();
					paint2.setColor(Color.GREEN);
					
					canvas.drawCircle(cx, cy, 15, paint2);
				}
				else
				{
					Paint paint2 = new Paint();
					paint2.setColor(Color.BLUE);
					
					canvas.drawCircle(cx, cy, 15, paint2);
				}
			}
			
			float cx;
			float cy;
			
			
			public boolean contains(int[] xcor, int[] ycor, double x0, double y0) {
				int crossings = 0;

				for (int i = 0; i < xcor.length - 1; i++)
				{
					int x1 = xcor[i];
					int x2 = xcor[i + 1];

					int y1 = ycor[i];
					int y2 = ycor[i + 1];

					int dy = y2 - y1;
					int dx = x2 - x1;

					double slope = 0;
					if (dx != 0) {
						slope = (double)dy / dx;
						Log.e("slope",Double.toString(slope)+"["+i+"]");
					}

					boolean cond1 = (x1 <= x0) && (x0 < x2); // is it in the range?
					boolean cond2 = (x2 <= x0) && (x0 < x1); // is it in the reverse
																// range?
					boolean above = (y0 < slope * (x0 - x1) + y1); // point slope y - y1
																	// = m(x -x1)
																	// -(check if y0
																	// below line of
																	// closed polygon)

					if ((cond1 || cond2) && above) {
						crossings++;
					}
				}
				return (crossings % 2 != 0); // even or odd
			}
			
			boolean contains;
			
		    public boolean onTouch(View v, MotionEvent event) {

				cx = event.getX();
				cy = event.getY();
		    	
				contains = contains(x, y, event.getX(), event.getY());
				
				invalidate();
				return true;
			}
        }
        
        setContentView(new Test(this.getApplicationContext()));    
    }


    

}