package com.jcasey;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);		
		
		final PaintView paintView = (PaintView) findViewById(R.id.paintView);
		final Button btnDraw = (Button)findViewById(R.id.btnDraw);
		final Button btnZoom = (Button)findViewById(R.id.btnZoom);
		final TextView txtViewScaleFactor = (TextView)findViewById(R.id.txtViewScaleFactor);
		
		OnClickListener btnListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(btnDraw.getId() == v.getId())
				{
					btnDraw.setEnabled(false);
					btnZoom.setEnabled(true);
					
					paintView.setDrawMode(true);
				}
				else if(btnZoom.getId() == v.getId())
				{
					btnZoom.setEnabled(false);
					btnDraw.setEnabled(true);
					
					paintView.setDrawMode(false);
				}
			}
		};
		
		btnZoom.setOnClickListener(btnListener);
		btnDraw.setOnClickListener(btnListener);
		
		paintView.setScaleFactor(txtViewScaleFactor);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
