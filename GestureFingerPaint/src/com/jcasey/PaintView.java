package com.jcasey;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

public class PaintView extends View implements OnTouchListener
{
	public static final float MAX_SCALE = 0.5f;
	private boolean drawMode = true;
	private TextView txtViewScaleFactor;
	
	/**
	 * @author John Casey
	 *  
	 *  PaintView class developed by John Casey 06/03/2014 
	 *  
	 *  Modified 15/5/2014 to work with ScaleListener
	 *  
	*/

	class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector)
        {                            	
        	scaleFactor = scaleFactor * detector.getScaleFactor();
        	
        	scaleFactor = Math.max(MAX_SCALE, scaleFactor);
            
        	if(txtViewScaleFactor != null)
        	{
        		txtViewScaleFactor.setText((String.format("%.2f", scaleFactor)));
        	}
        	
            invalidate(); // trigger a screen refresh
            
            return true;
        }        
    }
	
    private float scaleFactor = 1.0f; // default scale factor
	
	private final Paint paint = new Paint();
	
	private Bitmap offScreenBitmap;
	private Canvas offScreenCanvas;

	private ScaleGestureDetector scaleDetector;

	// define additional constructors so that PaintView will work with out layout file
	
	public PaintView(Context context) {
		super(context);
		
		setup(context);
	}

	public PaintView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		
		setup(context);
	}

	public PaintView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		setup(context);
	}

	public void setup(Context context)
	{
		scaleDetector = new ScaleGestureDetector(context, new ScaleListener());

		setOnTouchListener(this); // define event listener and start intercepting events 
	}
	
	@Override
	protected void onDraw(Canvas canvas)
	{
		// scale the canvas
		canvas.scale(scaleFactor, scaleFactor);
		
		// draw into the off screen bitmap
		canvas.drawBitmap(offScreenBitmap, 0, 0, paint);
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		if(!drawMode)
		{
			scaleDetector.onTouchEvent(event); // delegate control of the onTouch event to our gesture listener
		}
		
		// if user is not doing a scale gesture resume normal operation
		if(!scaleDetector.isInProgress()) 
		{
			if(event.getAction() == MotionEvent.ACTION_MOVE)
			{				
				if(drawMode) // only draw if draw mode is enabled
				{
					{
						// get the x,y coordinates of the MotionEvent.ACTION_MOVE event
						
						//scale the input
						float x = event.getX() / scaleFactor; 
						float y = event.getY() / scaleFactor;
					
						paint.setColor(Color.RED);
						offScreenCanvas.drawCircle(x, y, 20, paint); // draw a red circle at the x,y coordinates specified by the user
						
						invalidate(); // force a screen re-draw
					}
				}
			}
		}
		
		
		
		return true; // actually consume the event	
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
		// create / re-create the off screen bitmap to capture the state of our drawing
		// this operation will reset the user's drawing
		
		// create an offscreen bitmap that is twice as big as our screen
		
		float maxWidth = w / MAX_SCALE;
		float maxHeight = h / MAX_SCALE;
		
		offScreenBitmap = Bitmap.createBitmap((int)maxWidth, (int)maxHeight, Bitmap.Config.ARGB_8888);
		offScreenCanvas = new Canvas(offScreenBitmap);
	}

	
	public void setDrawMode(boolean drawMode)
	{
		this.drawMode = drawMode;
	}

	public void setScaleFactor(TextView txtViewScaleFactor)
	{
		this.txtViewScaleFactor = txtViewScaleFactor;
	}	
}
