package com.jcasey.verlet.controller;

import android.graphics.Canvas;
import android.os.Handler;
import android.os.SystemClock;
import android.view.SurfaceHolder;

import com.jcasey.verlet.model.GameWorld;
import com.jcasey.verlet.view.GameView;

public class GameLoop extends Thread
{
	private boolean running = true;

	SurfaceHolder surfaceHolder;
	GameView gameView;
	GameWorld world;
	Handler handler = null;
		
	public GameLoop(GameWorld world, SurfaceHolder surfaceHolder,GameView gameView)
	{		
		super("GameLoop");
		
		this.world = world;
		this.surfaceHolder = surfaceHolder;
		this.gameView = gameView;
	}
	
	public void run()
	{
		long initialTime = SystemClock.elapsedRealtime();
		Canvas canvas = null;
		while(running)
		{	
			long currentTime = SystemClock.elapsedRealtime();
			long deltaTime = currentTime - initialTime;
			initialTime = currentTime;
			
			if((deltaTime /1000f) >0f)
			{
				world.step(deltaTime / 1000f);
			}
						
			try
			{
				canvas = surfaceHolder.lockCanvas();
				synchronized (surfaceHolder)
				{
					if(canvas != null)
					{
						gameView.render(canvas);
					}
				}
			}
			finally
			{
				if (canvas != null)
				{
					surfaceHolder.unlockCanvasAndPost(canvas);
				}
			}
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
}
