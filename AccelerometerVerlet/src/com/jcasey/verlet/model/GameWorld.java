package com.jcasey.verlet.model;

public class GameWorld
{
	public class Vec2
	{
		public float x;
		public float y;
		
		public Vec2(float x, float y)
		{
			super();
			this.x = x;
			this.y = y;
		}
		
		public Vec2 multiply(float scalar)
		{
			return new Vec2(x * scalar, y * scalar);
		}

		public Vec2 subtract(Vec2 vector) {
			
			return new Vec2(x - vector.x, y - vector.y);
		}

		public Vec2 add(Vec2 vector)
		{
			return new Vec2(x + vector.x, y + vector.y);
		}

		@Override
		public String toString() {
			return "Vec2 [x=" + x + ", y=" + y + "]";
		}
		
		public final Vec2 worldToCanvas()
		{
			return new Vec2(worldXToCanvas(),worldYToCanvas());
		}
		
		private final float worldYToCanvas() {
			return (scaledHeight - (y + (scaledHeight / 2f))) / worldScale;
		}
		
		private final float worldXToCanvas() {
			return ((x + (scaledWidth / 2f))) / worldScale;
		}
		
	}
	
	public Vec2 current[];
	Vec2 previous[];
	Vec2 acceleration;
	
	int particles;
	private float scaledWidth;
	private float scaledHeight;
	private float worldScale;
	
	public GameWorld(float worldScale, final float scaledWidth, final float scaledHeight, final int particles)
	{
		this.worldScale = worldScale;
		this.scaledWidth = scaledWidth;
		this.scaledHeight = scaledHeight;
		
		current = new Vec2[particles];
		previous = new Vec2[particles];
		
		this.particles = particles;
		
		acceleration = new Vec2(0,-9f * worldScale);
		
		//TODO work out how to initialize this properly		
		for(int i = 0 ;i<this.particles; i++)
		{
			current[i] = new Vec2(0,scaledHeight / 2f + 0.001f); 
			previous[i] = new Vec2(0,scaledHeight / 2f);
		}
	}

	public void step(float timeStep)
	{
		verlet(timeStep);
		satisfyConstraints();
	}
	
	public void verlet(float timeStep) {
		for(int i = 0 ;i<particles; i++)
		{
			Vec2 currentPosition = current[i];
			Vec2 previousPosition = previous[i];

			Vec2 tempPosition = currentPosition; // temporary swap variable			
			
			Vec2 deltaPosition = currentPosition.subtract(previousPosition); // current - previous
			
			current[i] = currentPosition.add(deltaPosition).add(acceleration.multiply(timeStep * timeStep));
			previous[i] = tempPosition;
		}
	}
	
	private void satisfyConstraints()
	{
		for(int i = 0 ;i<particles; i++)
		{
			current[i].x = Math.min(current[i].x, scaledWidth /2);
			current[i].x = Math.max(current[i].x, -scaledWidth /2);
			
			current[i].y = Math.min(current[i].y,scaledHeight /2);
			current[i].y = Math.max(current[i].y, -scaledHeight /2);
		}
	}

	public void setGravity(float sensorX, float sensorY) {
		this.acceleration = new Vec2(sensorX, sensorY);
	}
}
