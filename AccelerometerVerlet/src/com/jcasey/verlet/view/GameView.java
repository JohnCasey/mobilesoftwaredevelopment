package com.jcasey.verlet.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import com.jcasey.verlet.controller.GameLoop;
import com.jcasey.verlet.model.GameWorld;
import com.jcasey.verlet.model.GameWorld.Vec2;

public class GameView extends SurfaceView implements SurfaceHolder.Callback, SensorEventListener
{
	public static final int SCREEN_DIAGONAL_METRES = 12;

	GameLoop gameLoop;
	GameWorld world;

	private float scale = 0.01f;
	float scaledWidth = 0;
	float width = 0;
	float height = 0;
	float scaledHeight = 0;

	public GameView(Context context) {
		super(context);

		setup();
	}

	public GameView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		setup();
	}

	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);

		setup();
	}

	public void setup() {
		getHolder().addCallback(this);
		paint.setAntiAlias(true);
	}

	final Paint paint = new Paint();
	private SensorManager mgr;
	private WindowManager windowManager;
	private Display display;

	public void render(final Canvas canvas) {
		// clear the screen
		canvas.drawColor(Color.BLACK);

		paint.setColor(Color.RED);

		for(int i =0 ; i<world.current.length; i++)
		{
			Vec2 particleCanvas = world.current[i].worldToCanvas();
			canvas.drawCircle(particleCanvas.x, particleCanvas.y, 40, paint);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		//TODO 
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Rect surfaceFrame = holder.getSurfaceFrame();

		width = surfaceFrame.width();
		height = surfaceFrame.height();

		float hyp = (float) Math.hypot(width, height); // get screen diagonal

		scale = SCREEN_DIAGONAL_METRES / hyp;

		scaledWidth = width * scale;
		scaledHeight = height * scale;

		world = new GameWorld(scale,scaledWidth, scaledHeight, 1);

		gameLoop = new GameLoop(world, holder, this);
		gameLoop.start();

		windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
		display = windowManager.getDefaultDisplay();

		mgr  = (SensorManager)getContext().getSystemService(Context.SENSOR_SERVICE);
		mgr.registerListener(this, mgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;

		gameLoop.setRunning(false);
		while(retry)
		{
			try
			{
				gameLoop.join();
				retry = false;
			}
			catch(InterruptedException e)
			{

			}
		}
		mgr.unregisterListener(this);
	}

	public final float canvasYToBox2D(float y) {
		float box2d = ((scaledHeight / 2) - (y * scale));
		return box2d;
	}

	public final float canvasXToBox2D(float x) {
		float box2d = (x * scale) - (scaledWidth / 2);
		return box2d;
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
		{
			float sensorX=0;
			float sensorY=0;

			switch (display.getRotation()) {
			case Surface.ROTATION_0:
				sensorX = event.values[0];
				sensorY = event.values[1];
				break;
			case Surface.ROTATION_90:
				sensorX = -event.values[1];
				sensorY = event.values[0];
				break;
			case Surface.ROTATION_180:
				sensorX = -event.values[0];
				sensorY = -event.values[1];
				break;
			case Surface.ROTATION_270:
				sensorX = event.values[1];
				sensorY = -event.values[0];
				break;
			}

			world.setGravity(-sensorX,-sensorY);
		}		
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}
}
