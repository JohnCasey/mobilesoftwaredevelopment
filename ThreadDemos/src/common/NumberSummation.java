package common;

public interface NumberSummation {
	public int getNumber();
	public void setNumber(int number);
}
