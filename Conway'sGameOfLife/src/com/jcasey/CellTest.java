package com.jcasey;

import static org.junit.Assert.*;

import org.junit.Test;


public class CellTest {

	/*
	 * Any live cell with fewer than two live neighbours dies, as if caused by under-population.
	 */
	@Test
	public void testAliveOneNeighbourAlive()
	{
		Cell neighbours[] = new Cell[9];

		neighbours[0] = new Cell(State.DEAD,0,0);
		neighbours[1] = new Cell(State.DEAD,0,1);
		neighbours[2] = new Cell(State.DEAD,0,2);

		neighbours[3] = new Cell(State.DEAD,1,0);
		
		neighbours[4] = new Cell(State.ALIVE,1,1);
		
		neighbours[5] = new Cell(State.DEAD,1,2);
		
		neighbours[6] = new Cell(State.DEAD,2,0);
		neighbours[7] = new Cell(State.DEAD,2,1);
		neighbours[8] = new Cell(State.DEAD,2,2);

		Cell cellTest = neighbours[4];
		cellTest.neighbours = neighbours;
		cellTest.calculateNextState();
		
		assertTrue("Cell should be dead.",cellTest.getState().equals(State.DEAD));		
	}
	
	/*
	 * Any live cell with two or three live neighbours lives on to the next generation.
	 */
	
	@Test
	public void testAliveThreeNeighbourAlive()
	{
		Cell neighbours[] = new Cell[9];

		neighbours[0] = new Cell(State.ALIVE,0,0);
		neighbours[1] = new Cell(State.ALIVE,0,1);
		neighbours[2] = new Cell(State.ALIVE,0,2);

		neighbours[3] = new Cell(State.DEAD,1,0);
		neighbours[4] = new Cell(State.ALIVE,1,1);
		neighbours[5] = new Cell(State.DEAD,1,2);
		
		neighbours[6] = new Cell(State.DEAD,2,0);
		neighbours[7] = new Cell(State.DEAD,2,1);
		neighbours[8] = new Cell(State.DEAD,2,2);

		Cell cellTest = neighbours[4];
		cellTest.neighbours = neighbours;
		cellTest.calculateNextState();

		assertTrue("Cell should stay alive.",cellTest.getState().equals(State.ALIVE));
	}
	
	/*
	 * Any live cell with more than three live neighbours dies, as if by overcrowding.
	 */
	
	@Test
	public void testAliveFourNeighboursDead()
	{
		Cell neighbours[] = new Cell[9];

		neighbours[0] = new Cell(State.ALIVE,0,0);
		neighbours[1] = new Cell(State.ALIVE,0,1);
		neighbours[2] = new Cell(State.ALIVE,0,2);

		neighbours[3] = new Cell(State.DEAD,1,0);
		neighbours[4] = new Cell(State.ALIVE,1,1);
		neighbours[5] = new Cell(State.DEAD,1,2);
		
		neighbours[6] = new Cell(State.DEAD,2,0);
		neighbours[7] = new Cell(State.DEAD,2,1);
		neighbours[8] = new Cell(State.ALIVE,2,2);

		Cell cellTest = neighbours[4];
		cellTest.neighbours = neighbours;
		cellTest.calculateNextState();
		
		assertTrue("Cell should go dead.",cellTest.getState().equals(State.DEAD));
	}
//	
//	public void testDeadThreeNeighboursAlive()
//	{
//		Set <Cell> neighbours = new HashSet <Cell> ();
//		
//		neighbours.add(new Cell(State.ALIVE));
//		neighbours.add(new Cell(State.ALIVE));
//		neighbours.add(new Cell(State.ALIVE));
//		
//		Cell cellTest = new Cell(State.DEAD, neighbours);
//		cellTest.calculateNextState();
//		
//		assertTrue("Cell should go alive.",cellTest.getState().equals(State.ALIVE));
//		
//	}
	
}
