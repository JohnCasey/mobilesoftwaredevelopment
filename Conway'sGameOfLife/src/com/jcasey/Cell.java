package com.jcasey;


public class Cell {
	int alive = 0;
	int x,y;
	State state;
	
	Cell[] neighbours;
	
	public Cell(State state, int x, int y)
	{
		this.state = state;
		this.x = x;
		this.y = y;
	}
	
	public void calculateNextState()
	{
		if(state.equals(State.ALIVE))
		{
			alive = 0;
			for(Cell otherCell: neighbours)
			{
				if(!otherCell.equals(this))
				{
					if(otherCell.state.equals(State.ALIVE))
					{
						alive ++;
					}
				}
			}
			
			if(alive <2)
			{
				state = State.DEAD;
			}
			else if ( alive <4)
			{
				state = State.ALIVE; // unnecessary?
			}
			else
			{
				state = State.DEAD;
			}
		}
		else
		{
			alive = 0;
			for(Cell otherCell: neighbours)
			{
				if(!otherCell.equals(this))
				{
					if(otherCell.state.equals(State.ALIVE))
					{
						alive ++;
					}
				}
			}
			
			if(alive == 3)
			{
				state = State.ALIVE;
			}
		}
	}
	
	public State getState() {
		return state;
	}
//	public Set<Cell> getNeighbours() {
//		return neighbours;
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cell other = (Cell) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cell [x=" + x + ", y=" + y + "] is "+state;
	}
	
}
