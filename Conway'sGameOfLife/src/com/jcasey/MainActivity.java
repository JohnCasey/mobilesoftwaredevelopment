package com.jcasey;

import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Bundle;
import android.util.DisplayMetrics;

import android.view.Menu;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

		class GameView extends SurfaceView implements Runnable, SurfaceHolder.Callback, OnTouchListener
		{
			private Thread game;
			private Cell[][] grid;
			private Paint paint = new Paint();
			DisplayMetrics metrics = new DisplayMetrics();

			int columns;
			int rows;

			float width;
			float height;

			public GameView(Context context) {
				super(context);


				getWindowManager().getDefaultDisplay().getMetrics(metrics);

				
				
				getHolder().addCallback(this);
			}

			@Override
			public boolean onTouch(View view, MotionEvent event) {
				if((event.getAction() == MotionEvent.ACTION_MOVE) || (event.getAction() == MotionEvent.ACTION_DOWN))
				{
					int row = (int) Math.floor((event.getX() / width) * columns);
					int col = (int) Math.floor((event.getY() / height) * rows);
					
					Cell cell = grid[col][row];
					
					if(cell.state.equals(State.ALIVE))
					{
						cell.state = State.DEAD;
					}
					else if(cell.state.equals(State.DEAD))
					{
						cell.state = State.ALIVE;
					}
				}
				return true;
			}

			@Override
			public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			protected void onSizeChanged(int w, int h, int oldw, int oldh) {
				super.onSizeChanged(w, h, oldw, oldh);

				width = w;
				height = h;

				float xInches = w / metrics.xdpi;
				float yInches = h / metrics.ydpi;

				rows = (int) Math.ceil(yInches * 15);
				columns = (int) Math.ceil(xInches * 20);

				grid = new Cell[rows][columns];

				Random rand = new Random();

				int randElements = 0;
				
				for (int i = 0; i<grid.length;i++)
				{
					for (int j = 0; j<grid[i].length;j++)
					{
						Cell cell = new Cell(State.DEAD,i,j);
						grid[i][j]= cell;

						if(rand.nextInt(10) == 1)
						{
							cell.state = State.ALIVE;
						}
					}	
					if(randElements == 10)
					{
						break;
					}
				}


				
				for (int i = 0; i<grid.length;i++)
				{
					for (int j = 0; j<grid[i].length;j++)
					{						
						grid[i][j].neighbours = getNeighbours(grid,i-1 /* left */ ,j-1 /* top */);
					}
				}

				this.setOnTouchListener(this);
				
			}

			@Override
			public void surfaceCreated(SurfaceHolder arg0) {
				game=new Thread(this,"Animation Loop");
				game.start();
			}

			@Override
			public void surfaceDestroyed(SurfaceHolder arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public  void run()
			{
				while(true)
				{
					Canvas canvas = null;

					SurfaceHolder holder = getHolder();
					synchronized(holder)
					{
						canvas = holder.lockCanvas(null);

						for(int i=0;i<grid.length;i++)
						{
							for(int j=0;j<grid[i].length;j++)
							{
								
								int cellWidth = (int) (width / columns);
								int cellHeight = (int) (height / rows);
								
								int top = j * cellWidth;
								int left = i * cellHeight;
								int right = top + cellWidth;
								int bottom = left  + cellHeight;
								
								Cell cell = grid[i][j];
								if(cell.getState().equals(State.ALIVE))
								{
									//paint.setColor(Color.rgb(150, 44, 67));
									if (cell.alive > 2)
									{
										paint.setColor(Color.rgb(230, 236, 240));
									}
									else
									{
										paint.setColor(Color.rgb(238, 244, 255));
									}
									paint.setStyle(Style.FILL_AND_STROKE);
									canvas.drawRect(top , left, right, bottom, paint);
									
									
								}
								else
								{
									paint.setColor(Color.WHITE);
									paint.setStyle(Style.FILL_AND_STROKE);
									canvas.drawRect(top , left, right, bottom, paint);
								}

								paint.setColor(Color.rgb(219, 234, 247));
								paint.setStyle(Style.STROKE);
								canvas.drawRect(top , left, right, bottom, paint);
								
								cell.calculateNextState();
							}
						}



						holder.unlockCanvasAndPost(canvas);
					}
					try {
						Thread.sleep(5);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}



		setContentView(new GameView(getApplicationContext()));
	}

	public Cell[] getNeighbours(final Cell[][] grid, final int x, final int y)
	{
		Set<Cell> neighbours = new LinkedHashSet<Cell>();
		for(int i=y;i<(y+3);i++)
		{
			slice:
			for(int j=x;j<(x+3);j++)
			{
				if((i>=0 && j>=0) && (i < grid.length) && j< grid[i].length)
				{
					
					
					neighbours.add(grid[i][j]);
				}
				else
				{
				
				}
			}
		}
		Cell[] cells = new Cell[neighbours.size()];
		neighbours.toArray(cells);
		return cells;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
