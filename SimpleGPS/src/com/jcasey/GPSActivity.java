package com.jcasey;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class GPSActivity extends Activity
{
    private static final String LOCATION_FORMAT = "%.5f";

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        final TextView longitude = (TextView)findViewById(R.id.longitude);
        final TextView latitude = (TextView)findViewById(R.id.latitude);
        
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
               
        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener()
        {
            public void onLocationChanged(Location location)
            {
              // Called when a new location is found by the network location provider.
              Log.e(SENSOR_SERVICE, location.toString());
              Log.e(SENSOR_SERVICE, Double.toString(location.getLatitude()));
              Log.e(SENSOR_SERVICE, Double.toString(location.getLongitude()));
              
              latitude.setText(String.format(LOCATION_FORMAT, location.getLatitude()));
              longitude.setText(String.format(LOCATION_FORMAT, location.getLongitude()));
            }

            public void onStatusChanged(String provider, int status, Bundle extras)
            {
            	if(status == android.location.LocationProvider.AVAILABLE)
            	{
            		Log.e(SENSOR_SERVICE, "AVAILABLE");
            	}
            	else if(status == android.location.LocationProvider.TEMPORARILY_UNAVAILABLE)
            	{
            		Log.e(SENSOR_SERVICE, "TEMPORARILY_UNAVAILABLE");
            	}
            	else if(status == android.location.LocationProvider.OUT_OF_SERVICE)
            	{
            		Log.e(SENSOR_SERVICE, "OUT_OF_SERVICE");
            	}            	
            }

            public void onProviderEnabled(String provider)
            {
            	Log.e("enabled:",provider);
            }

            public void onProviderDisabled(String provider)
            {
            	Log.e("disabled:",provider);
            }
          };

		  // register both types of location provider
        
          locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,locationListener);          
          locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0 /* milliseconds */, 0 /* metres */, locationListener);

          
          
          
          // Remove the listener you previously added
          // locationManager.removeUpdates(locationListener);
          
    }
}