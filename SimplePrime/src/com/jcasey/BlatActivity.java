package com.jcasey;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Bundle;
import android.view.View;
import android.widget.ScrollView;

public class BlatActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        class DrawView extends View
        {
        	class Node
    		{
    			int data;
    			Node primeFactor;
    			Node right;

    			public Node(int data)
    			{
    				this(data,null,null);
    			}
    			
    			public Node(int data, Node left, Node right)
    			{
    				super();
    				this.data = data;
    				this.primeFactor = left;
    				this.right = right;
    			}

    			public int levels()
    			{
    				int count =0;
    				
    				Node child = right;
    				while (child !=null)
    				{
    					count++;
    					child = child.right;
    				}
    				
    				return count;
    			}
    			
    			public String toString()
    			{
    				return ""+data;
    			}
    		}
        	
        	Node parent = null;

            public DrawView(Context context)
            {
                super(context);

                parent = new Node(1024);
    			Node nodePtr = parent;

    			while(true)
    			{
    				nodePtr.primeFactor = nodePtr;
    				
    				for(int i = 2; i<nodePtr.data;i++)
    				{
    					if(nodePtr.data % i == 0)
    					{
    						nodePtr.primeFactor = new Node(i);
    						break;
    					}
    				}

    				if(nodePtr.data == nodePtr.primeFactor.data)
    				{
    					break;
    				}
    				
    				nodePtr.right = new Node(nodePtr.data / nodePtr.primeFactor.data);
    				
    				nodePtr = nodePtr.right;
    			}
            }

            @Override
            protected void onDraw(Canvas canvas)
            {
                int width = getWidth();
                
                Paint paint = new Paint();

                paint.setAntiAlias(true);
                paint.setStrokeWidth(0);
                
                final int height = 60;
                final int angle = 30;
                final float oppositeWidth = (float) (height * Math.tan(Math.toRadians(angle)));
                
                int levels = parent.levels();
                
                float totalWidth = (oppositeWidth * levels);
                
                if(totalWidth > getWidth())
                {
                	setMinimumWidth((int)totalWidth);
                }
                
                paint.setColor(Color.WHITE);
                canvas.drawRect(0, 0, getWidth(), getHeight(), paint);
                
				drawNode(paint,canvas,(width/2)-(totalWidth/2)+(oppositeWidth/2), 50,oppositeWidth,height, levels,parent, 10);
            }

            public void drawNode(Paint paint,Canvas canvas,float baseXPosition, float baseYPosition, float oppositeWidth, final float height, int levels, Node node, int nodeSz) {
                
                if(levels == 0)
                {
                    return;
                }
                else
                {
                    float rightXCoord = baseXPosition+oppositeWidth;
					float rightYCoord = baseYPosition+height;
					float leftXCoord = baseXPosition-oppositeWidth;
										
                    paint.setStyle(Style.STROKE);
                    paint.setColor(Color.BLACK);
					
					canvas.drawLine(baseXPosition, baseYPosition , rightXCoord, rightYCoord, paint);
                    canvas.drawLine(baseXPosition, baseYPosition , leftXCoord, rightYCoord, paint);     
                    
					paint.setStyle(Style.FILL);
					paint.setColor(Color.WHITE);
					
                    canvas.drawCircle(baseXPosition, baseYPosition, nodeSz, paint);
                    canvas.drawCircle(rightXCoord, rightYCoord, nodeSz, paint);
                    canvas.drawCircle( leftXCoord, rightYCoord, nodeSz, paint);
                    
                    paint.setStyle(Style.STROKE);
                    paint.setColor(Color.BLACK);
                    
                    canvas.drawCircle(baseXPosition, baseYPosition, nodeSz, paint);
                    canvas.drawCircle(rightXCoord, rightYCoord, nodeSz, paint);
                    canvas.drawCircle( leftXCoord, rightYCoord, nodeSz, paint);
                    
                    canvas.drawText(node.data+"", baseXPosition, baseYPosition, paint);
                    canvas.drawText(node.right.data+"", rightXCoord, rightYCoord, paint);
                    canvas.drawText(node.primeFactor.data+"", leftXCoord, rightYCoord, paint);
                    
                    drawNode(paint,canvas,rightXCoord, rightYCoord, oppositeWidth, height, (levels -1),node.right, nodeSz);
                }
            }
        };
        
        setContentView(new DrawView(getApplicationContext()));
    }
}