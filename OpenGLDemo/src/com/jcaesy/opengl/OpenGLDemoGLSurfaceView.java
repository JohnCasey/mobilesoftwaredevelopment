package com.jcaesy.opengl;

import android.content.Context;
import android.opengl.GLSurfaceView;

public class OpenGLDemoGLSurfaceView extends GLSurfaceView {

	GLRenderer renderer;
	
	public OpenGLDemoGLSurfaceView(Context context) {
		super(context);

		setEGLContextClientVersion(2);
		
		renderer = new GLRenderer();
		
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		setRenderer(renderer);
	}

}
