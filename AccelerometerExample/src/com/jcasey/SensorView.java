package com.jcasey;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.view.Display;
import android.view.Surface;
import android.view.View;

public class SensorView extends View implements SensorEventListener
{
	/** 
	 * @author John Casey (john.casey@gmail.com)
	 * 
	 * SensorView class - concise accelerometer example
	 * 
	 */
	
	public static final int RADIUS = 40;
	Paint paint = new Paint();
	Display display;

	int MOVEMENT = 2; // movement constant

    int width = 0;
    int height = 0;    
    
    float x = 0;
    float y = 0;
    
    float sensorX = 0;
	float sensorY = 0;
	float sensorZ = 0;
    
	float magnitude = 0;
	
    public SensorView(Context context, Display display) {
        super(context);
        
        this.display = display;
    }
    
    boolean init = false; // flag to toggle whether we respond to sensor events
    
    @Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
		this.width = w;
		this.height = h;
		
		this.x = width / 2;
		this.y = height / 2;

		init = true; // only respond to sensor events once the width / height are known
		
		paint.setColor(Color.RED);
		
		paint.setTextSize(14);
	}

	@Override
    protected void onDraw(Canvas canvas) {        
		
		// display acceleration
		canvas.drawText("x:"+String.format("%.5f", sensorX), 10, 10, paint);
		canvas.drawText("y:"+String.format("%.5f", sensorY), 10, 25, paint);
		canvas.drawText("z:"+String.format("%.5f", sensorZ), 10, 40, paint);
		canvas.drawText("mag:"+String.format("%.5f", magnitude), 10, 55, paint);
		
		// draw the circle at the new x,y point
		canvas.drawCircle(x,y , RADIUS, paint);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        
    }

    
    @Override
    public void onSensorChanged(SensorEvent event)
    {
     
    	if(init)
    	{
	        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
	        {                   
	        	sensorZ = event.values[2];
	        	
				// translate the accelerometer (x,y) values based on screen orientation
				switch (display.getRotation()) {
	            case Surface.ROTATION_0:
	                sensorX = event.values[0];
	                sensorY = event.values[1];
	                break;
	            case Surface.ROTATION_90:
	                sensorX = -event.values[1];
	                sensorY = event.values[0];
	                break;
	            case Surface.ROTATION_180:
	                sensorX = -event.values[0];
	                sensorY = -event.values[1];
	                break;
	            case Surface.ROTATION_270:
	                sensorX = event.values[1];
	                sensorY = -event.values[0];
	                break;
	        	}
	
				magnitude = (sensorX * sensorX + sensorY * sensorY + sensorZ * sensorZ);
				
				// work out the x,y co-ordinate based on direction of acceleration + movement constant
	            if(sensorX < 0f)
	            {
	            	x = x + MOVEMENT;	                	                
	            }
	            else if(sensorX > 0f)
	            {
	                x = x - MOVEMENT;
	            }
	            
	            if(sensorY < 0f)
	            {
	                y = y - MOVEMENT;
	            }
	            else if(sensorY >0f)
	            {
	                y = y + MOVEMENT;
	            }
	            
	            // boundary conditions for x,y point taking radius of the circle into consideration
	            if(x - RADIUS < 0f)
	            {
	                x = RADIUS;
	            }
	            if(y - RADIUS < 0f)
	            {
	                y = RADIUS;
	            }
	            if(x + RADIUS > width)
	            {
	                x = width - RADIUS;
	            }
	            if(y + RADIUS > height)
	            {
	                y = height - RADIUS;
	            }
	
	            invalidate(); // force screen redraw
	        }
    	}
    }
}
