package com.jcasey;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Display;
import android.view.Window;

public class AccelerometerActivity extends Activity {

	/**
	 * @author John Casey (john.casey@gmail.com)
	 *  
	 *  AccelerometerActivity class developed by John Casey 16/05/2014 
	*/
	
	SensorView sensorView;
	SensorManager mgr;	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                
        requestWindowFeature(Window.FEATURE_NO_TITLE); // remove the title bar
        
        Display display = getWindowManager().getDefaultDisplay();
        
        final Context ctx = getApplicationContext(); // application context
        
		sensorView = new SensorView(ctx,display);
        setContentView(sensorView); // bypass the layout file

        mgr = (SensorManager)ctx.getSystemService(Context.SENSOR_SERVICE);
        mgr.registerListener(sensorView, mgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
    }

	@Override
	protected void onResume() {
		super.onResume();
		
		mgr.registerListener(sensorView, mgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		mgr.unregisterListener(sensorView); // disable the listener onPause
	}
}