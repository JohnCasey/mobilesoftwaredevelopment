package com.jcasey.box2d.circle;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.collision.shapes.ShapeType;
import org.jbox2d.common.Transform;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.Fixture;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
public class WorldView extends SurfaceView implements Runnable, SurfaceHolder.Callback, OnTouchListener
{
	private World world;
	private Thread game;
	
	private float radius = 25f;
	private float scale=0.01f;
	private float scaledHeight;
	private float scaledWidth;
	
	private float WIDTH = 0;
	private float HEIGHT = 0;
	private float timeStep = 1f/30f;
	private int velocityIterations = 4;
	private int positionIterations = 2;
	
	private final Vec2 gravity = new Vec2(0.0f, -7.0f);
	
	public class PolygonObject
	{
		Body body = null;
		Shape shape = null;
	}
	
	public WorldView(Context context) {
		super(context);
		
		setup(context);
	}

	public WorldView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		setup(context);
	}

	public WorldView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		setup(context);
	}

	public void setup(Context context) {
		world = new World(gravity);
		
		getHolder().addCallback(this);
	}

	@Override
	public void run()
	{
		final Path polygon = new Path();
		final Paint paint = new Paint();
		final Transform transform = new Transform();
		final Vec2 origin = new Vec2(0,0);

		paint.setAntiAlias(true);
		
		while(true)
		{
			Canvas canvas = null;

			SurfaceHolder holder = getHolder();
			synchronized(holder)
			{
				canvas = holder.lockCanvas();

				// clear the screen
				canvas.drawColor(Color.BLACK);

				// sync on the world objects make sure we do not objects while stepping the engine
				synchronized(world)
				{
					world.step(timeStep, velocityIterations, positionIterations);
					
					for ( Body b = world.getBodyList(); b!=null; b = b.getNext() )
					{
						Fixture fixture = b.getFixtureList();
				        while(fixture != null){
				            ShapeType type = fixture.getType();
				            if(type == ShapeType.POLYGON){
				                PolygonShape polygonShape = (PolygonShape)fixture.getShape();
				                
								Vec2 position = b.getPosition();

								
								Vec2[] vertices = polygonShape.getVertices();

								// setup the vector transform
								transform.set(origin, b.getAngle());
								
								// reset our polygon
								polygon.reset();
								for(int i = 0; i< polygonShape.getVertexCount(); i++)
								{
									// multiply the shape by the body's angle
									Vec2 src = Transform.mul(transform, vertices[i]);

									// offset the shape vector by the body's position
									src = src.add(position);

									float x = box2DXToCanvas(src.x);
									float y = box2DYToCanvas(src.y);				
									
									if(i == 0)
									{
										polygon.moveTo(x, y);
									}
									
									polygon.lineTo(x, y);
								}
								
								polygon.close();
								
								paint.setColor(Color.RED);
								paint.setAlpha(130);
								paint.setStyle(Style.FILL);
								canvas.drawPath(polygon, paint);
								
								paint.setStyle(Style.STROKE);
								paint.setColor(Color.RED);
								paint.setAlpha(255);
								canvas.drawPath(polygon, paint);

				                
				            }else if(type == ShapeType.CIRCLE){
				                CircleShape shape = (CircleShape)fixture.getShape();
				                
				                float x = box2DXToCanvas(b.getPosition().x);
								float y = box2DYToCanvas(b.getPosition().y);
				                
								paint.setColor(Color.RED);
								paint.setAlpha(130);
								paint.setStyle(Style.FILL);
								canvas.drawCircle(x, y, shape.getRadius() / scale, paint);
								
								paint.setStyle(Style.STROKE);
								paint.setColor(Color.RED);
								paint.setAlpha(255);
								canvas.drawCircle(x, y, shape.getRadius() / scale, paint);				                
				            }
				            fixture = fixture.getNext();
				        }
					}
				}

				holder.unlockCanvasAndPost(canvas);
			}
			try
			{
				Thread.sleep(15);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		setupGameWorld(holder);
	}
	
	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		setupGameWorld(holder);
	}

	public void setupGameWorld(SurfaceHolder holder) {
		setOnTouchListener(this);
		
		Rect surfaceFrame = holder.getSurfaceFrame();
		
		HEIGHT = surfaceFrame.height();
		WIDTH = surfaceFrame.width();

		scaledWidth = scale * (WIDTH);
		scaledHeight = scale * (HEIGHT);
		
		world.setSleepingAllowed(true);
		
	    final float top = scaledHeight/2f;
		final float bottom = -top;
	    
		final float right = scaledWidth/2f;
		final float left = -right;
		
		createWall(right, 0.01f /* this is the height of the wall */, 0f, bottom);
	    createWall(right, 0.01f /* this is the height of the wall */, 0f, top);
		createWall(0.001f /* this is the width of the wall */, scaledHeight,left, top);
	    createWall(0.001f /* this is the width of the wall */, scaledHeight,right, top);
	    
	    createWall(right * 0.6f, 0.1f, 0f-(150f * scale), 0f+(150f * scale), (float)Math.toRadians(145f));
	    createWall(right * 0.6f, 0.1f, 0f+(150f * scale), 0f-(150f * scale), (float)Math.toRadians(185f));
	    
	    game = new Thread(this,"Animation Loop");
	    game.start();
	}

	public void createWall(float width, float height, float x, float y) {
		createWall(width, height, x, y, 0f);
	}

	public void createWall(float width, float height, float x, float y, float angle) {
		PolygonShape groundBox = new PolygonShape();
		groundBox.setAsBox(width,height);
	    
		FixtureDef groundFixture = new FixtureDef();
	    groundFixture.shape = groundBox;
	    groundFixture.density = 1.0f;
	    groundFixture.friction = 0.8f;
	    
	    BodyDef bd = new BodyDef();
	    bd.type = BodyType.STATIC;
	    bd.angle = angle;
	    bd.position.set(x,y);
	    
	    Body body = world.createBody(bd);
		body.createFixture(groundFixture);
	    
	    PolygonObject shape = new PolygonObject();
        shape.body = body;
        shape.shape = groundBox;
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		
		
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int type = event.getAction() & MotionEvent.ACTION_MASK;
		
		if(type == MotionEvent.ACTION_DOWN)
		{
			BodyDef crate = new BodyDef();
	        crate.type = BodyType.DYNAMIC;
	        crate.position.set(canvasXToBox2D(event.getX()), canvasYToBox2D(event.getY()));
	        
	        CircleShape ball = new CircleShape();
	        ball.setRadius(radius * scale);
	        
	        FixtureDef ballFd = new FixtureDef();
	        ballFd.shape = ball;
	        ballFd.density = 1f;
	        ballFd.friction = 0.4f;        
	        ballFd.restitution = 0.2f;
	        
	        // make sure we do not add objects while stepping the engine
	        synchronized(world)
	        {
	        	Body body = world.createBody(crate);
		        body.createFixture(ballFd);
	        }
			return true;
		}
		else
		{
			return false;
		}
	}

	public final float canvasYToBox2D(float y)
	{
		float box2d = ((scaledHeight/2) - (y * scale));
		return box2d;
	}

	public final float canvasXToBox2D(float x)
	{
		float box2d = (x * scale) - (scaledWidth /2);
		return box2d;
	}
	public final float box2DYToCanvas(float y) {
		return (scaledHeight - (y + (scaledHeight /2f))) / scale;
	}
	
	public final float box2DXToCanvas(float x) {
		return (x + (scaledWidth/2f)) / scale;
	}
}
