package com.jcasey.box2d.circle;

import com.jcasey.box2d.circle.R;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == R.id.action_about)
		{
			final Dialog dialog = new Dialog(this);
			dialog.setContentView(R.layout.simpleabout);
			dialog.setTitle(R.string.action_about);
			
			dialog.show();
			
			return true;
		}
		return true;
	}

}
