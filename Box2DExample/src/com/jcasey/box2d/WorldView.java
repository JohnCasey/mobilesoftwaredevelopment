package com.jcasey.box2d;

import java.util.Iterator;
import java.util.Vector;

import android.content.Context;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.Transform;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.AttributeSet;
import android.view.Display;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.WindowManager;
public class WorldView extends SurfaceView implements Runnable, SurfaceHolder.Callback, OnTouchListener, SensorEventListener
{
	private World world;
	private Thread game;
	
	private float crateWidth = 80;
	private float scale=0.01f;
	private float scaledHeight;
	private float scaledWidth;
	
	private SensorManager mgr = null;
	
	private float WIDTH = 0;
	private float HEIGHT = 0;
	private float timeStep = 1f/30f;
	private int velocityIterations = 8;
	private int positionIterations = 8;
	
	private final Vec2 gravity = new Vec2(0.0f, -10.0f);
	private final Vector <PolygonObject> objects = new Vector <PolygonObject>();
	private WindowManager windowManager;
	private Display mDisplay;
	
	public class PolygonObject
	{
		Body body = null;
		Shape shape = null;
	}
	
	public WorldView(Context context) {
		super(context);
		
		setup(context);
	}

	public WorldView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		setup(context);
	}

	public WorldView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		setup(context);
	}

	public void setup(Context context) {
		world = new World(gravity);
		
		getHolder().addCallback(this);
		
		windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        mDisplay = windowManager.getDefaultDisplay();
		
		mgr  = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
		mgr.registerListener(this, mgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
	}

	@Override
	public void run()
	{
		final Path polygon = new Path();
		final Paint paint = new Paint();
		final Transform transform = new Transform();
		final Vec2 origin = new Vec2(0,0);
		Iterator <PolygonObject>iterator;
		PolygonObject polygonObject;
		paint.setAntiAlias(true);
		
		while(true)
		{
			Canvas canvas = null;

			SurfaceHolder holder = getHolder();
			synchronized(holder)
			{
				canvas = holder.lockCanvas();

				// clear the screen
				canvas.drawColor(Color.BLACK);

				// sync on the world objects make sure we do not objects while stepping the engine
				synchronized(objects)
				{
					world.step(timeStep, velocityIterations, positionIterations);
					
					iterator = objects.iterator();
					while(iterator.hasNext())
					{
						polygonObject = iterator.next();
						Vec2 position = polygonObject.body.getPosition();

						PolygonShape polygonShape = (PolygonShape)polygonObject.shape;
						Vec2[] vertices = polygonShape.getVertices();

						// setup the vector transform
						transform.set(origin, polygonObject.body.getAngle());
						
						// reset our polygon
						polygon.reset();
						for(int i = 0; i< polygonShape.getVertexCount(); i++)
						{
							// multiply the shape by the body's angle
							Vec2 src = Transform.mul(transform, vertices[i]);

							// offset the shape vector by the body's position
							src = src.add(position);

							float x = box2DXToCanvas(src.x);
							float y = box2DYToCanvas(src.y);				
							
							if(i == 0)
							{
								polygon.moveTo(x, y);
							}
							
							polygon.lineTo(x, y);
						}
						
						polygon.close();
						
						paint.setColor(Color.RED);
						paint.setAlpha(130);
						paint.setStyle(Style.FILL);
						canvas.drawPath(polygon, paint);
						
						paint.setStyle(Style.STROKE);
						paint.setColor(Color.RED);
						paint.setAlpha(255);
						canvas.drawPath(polygon, paint);
					}
				}

				holder.unlockCanvasAndPost(canvas);
			}
			try
			{
				Thread.sleep(15);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			
		}
	}


	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		setupGameWorld(holder);
	}

	
	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		setupGameWorld(holder);
	}

	public void setupGameWorld(SurfaceHolder holder) {
		setOnTouchListener(this);
		
		Rect surfaceFrame = holder.getSurfaceFrame();
		
		HEIGHT = surfaceFrame.height();
		WIDTH = surfaceFrame.width();

		crateWidth = (float) Math.sqrt((WIDTH * HEIGHT) / 200f); 
		
		scaledWidth = scale * (WIDTH);
		scaledHeight = scale * (HEIGHT);
		
		world.setSleepingAllowed(false);
		
	    createWall(scaledWidth/2f, 0.01f /* this is the height of the wall */, 0f, -(scaledHeight/2f));
	    createWall(scaledWidth/2f, 0.01f /* this is the height of the wall */, 0f, (scaledHeight/2f));
	    createWall(0.001f /* this is the width of the wall */, scaledHeight,-(scaledWidth/2f), (scaledHeight/2f));
	    createWall(0.001f /* this is the width of the wall */, scaledHeight,(scaledWidth/2f), (scaledHeight/2f));
	    
	    game = new Thread(this,"Animation Loop");
	    game.start();
	}

	public void createWall(float width, float height, float x, float y) {
		PolygonShape groundBox = new PolygonShape();
		groundBox.setAsBox(width,height);
	    
		FixtureDef groundFixture = new FixtureDef();
	    groundFixture.shape = groundBox;
	    groundFixture.density = 1.0f;
	    groundFixture.friction = 0.4f;
	 
	    BodyDef bd = new BodyDef();
	    bd.type = BodyType.STATIC;
	    
	    bd.position.set(x,y);
	         
	    world.createBody(bd).createFixture(groundFixture);
	}

	
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		
		
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int type = event.getAction() & MotionEvent.ACTION_MASK;
		
		if(type == MotionEvent.ACTION_DOWN)
		{
			BodyDef crate = new BodyDef();
	        crate.type = BodyType.DYNAMIC;
	        crate.position.set(canvasXToBox2D(event.getX()), canvasYToBox2D(event.getY()));
	        
	        PolygonShape crateShape = new PolygonShape();
	        crateShape.setAsBox((crateWidth * scale)/2f, (crateWidth * scale)/2f);
	        
	        FixtureDef crateFd = new FixtureDef();
	        crateFd.shape = crateShape;
	        crateFd.density = 1f;
	        crateFd.friction = 0.3f;        
	        crateFd.restitution = 0.2f;
	        
	        // make sure we do not add objects while stepping the engine
	        synchronized(objects)
	        {
	        	Body body = world.createBody(crate);
		        body.createFixture(crateFd);

		        PolygonObject shape = new PolygonObject();
		        shape.body = body;
		        shape.shape = crateShape;
		        
		        objects.add(shape);
	        }
			return true;
		}
		else
		{
			return false;
		}
	}

	public final float canvasYToBox2D(float y)
	{
		float box2d = ((scaledHeight/2) - (y * scale));
		return box2d;
	}

	public final float canvasXToBox2D(float x)
	{
		float box2d = (x * scale) - (scaledWidth /2);
		return box2d;
	}
	public final float box2DYToCanvas(float y) {
		return (scaledHeight - (y + (scaledHeight /2f))) / scale;
	}
	
	public final float box2DXToCanvas(float x) {
		return (x + (scaledWidth/2f)) / scale;
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
        {
            float mSensorX=0;
			float mSensorY=0;
			switch (mDisplay.getRotation()) {
            case Surface.ROTATION_0:
                mSensorX = event.values[0];
                mSensorY = event.values[1];
                break;
            case Surface.ROTATION_90:
                mSensorX = -event.values[1];
                mSensorY = event.values[0];
                break;
            case Surface.ROTATION_180:
                mSensorX = -event.values[0];
                mSensorY = -event.values[1];
                break;
            case Surface.ROTATION_270:
                mSensorX = event.values[1];
                mSensorY = -event.values[0];
                break;
        	}
        	
            gravity.set(mSensorX*-2.5f, mSensorY*-2.5f);
            world.setGravity(gravity);
        }		
	}
}
