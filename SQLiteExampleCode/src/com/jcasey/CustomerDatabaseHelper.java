package com.jcasey;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CustomerDatabaseHelper extends SQLiteOpenHelper
{
    public final static int VERSION = 1;
    public final static String DATAFILE_NAME = "mobile.db";
    
    public final static String TABLE = "CLIENT";
    public final static String CID ="CID";
    public final static String FIRSTNAME="FIRSTNAME";
    public final static String LASTNAME="LASTNAME";
    public final static String NOTES="NOTES";

    public CustomerDatabaseHelper(Context context)
    {
        super(context, DATAFILE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {        
        String sql = "create table " +TABLE +" ("+CID+" integer primary key, "+FIRSTNAME+" text, "+LASTNAME+" text, "+NOTES+" text);";
    
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("drop table if exists "+TABLE);
        
        onCreate(db);
    }

}
