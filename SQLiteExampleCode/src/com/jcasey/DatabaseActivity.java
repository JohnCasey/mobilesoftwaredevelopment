package com.jcasey;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class DatabaseActivity extends Activity
{
    SQLiteDatabase dbHandler = null;
    TextView dbResultsView = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        // Open database handler using our own specialized CustomerDatabaseHelper
        CustomerDatabaseHelper helper = new CustomerDatabaseHelper(getApplicationContext());
        dbHandler = helper.getWritableDatabase();
        
        dbResultsView = (TextView) findViewById(R.id.dbResultsView);
        
        Button addRecord = (Button)findViewById(R.id.btnAddRecord);
        addRecord.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
            	// build SQL statement manually
            	
//                String sqlInsert = "insert into "+W5DBHelper.TABLE+" (CID,FIRSTNAME, LASTNAME, NOTES) values(null,'John','Casey','blah blah blah');";
//                dbHandler.execSQL(sqlInsert);
                
            	// use content values map to insert data.
                ContentValues values = new ContentValues();
                
                values.putNull(CustomerDatabaseHelper.CID);
                values.put(CustomerDatabaseHelper.FIRSTNAME,"John");
                values.put(CustomerDatabaseHelper.LASTNAME,"Casey");
                values.put(CustomerDatabaseHelper.NOTES,"This is the notes field");
                
                dbHandler.insert(CustomerDatabaseHelper.TABLE, null,values);

                // Cursor dbCursor = dbHandler.rawQuery("select * from "+W5DBHelper.TABLE,null);
                Cursor dbCursor = dbHandler.query(CustomerDatabaseHelper.TABLE, null, null, null, null, null, null);
                
                dbResultsView.setText("");
                
                while(dbCursor.moveToNext())
                {            
                    int cid = dbCursor.getInt(0);
                    String firstName = dbCursor.getString(1);
                    String lastName = dbCursor.getString(2);
                    String notes = dbCursor.getString(3);

                    dbResultsView.append(cid+firstName+lastName+notes+"\r\n"); // append everything to our TextView component
                    
                    System.err.println(firstName);
                }
                
            }
        });
        
        Button deleteRecord = (Button)findViewById(R.id.btnDeleteRecord);
        deleteRecord.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                dbHandler.delete(CustomerDatabaseHelper.TABLE, "CID=(select max(CID) from CLIENT)",null);
                
                Cursor dbCursor = dbHandler.query(CustomerDatabaseHelper.TABLE, null, null, null, null, null, null);
                
                dbResultsView.setText(""); // clear existing results and rebuild
                
                while(dbCursor.moveToNext())
                {            
                    int cid = dbCursor.getInt(0);
                    String firstName = dbCursor.getString(1);
                    String lastName = dbCursor.getString(2);
                    String notes = dbCursor.getString(3);

                    dbResultsView.append(cid+firstName+lastName+notes+"\r\n");                    
                }
                
            }
        });

        // event listener to save file, illustrates use of permissions as defined in the manifest
        Button btnFile = (Button)findViewById(R.id.btnFile);
        btnFile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				File file = new File(Environment.getExternalStorageDirectory(),"test");
				
				try {
					FileOutputStream fos = new FileOutputStream(file);
					
					fos.write("test".getBytes());
					fos.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

        // display results if no buttons have been pressed
        getAllCustomersRawQuery();
    }

	public void getAllCustomersRawQuery() {
		// build SQL dynamically
        Cursor dbCursor = dbHandler.rawQuery("select * from "+CustomerDatabaseHelper.TABLE,null);
        
        while(dbCursor.moveToNext())
        {            
            int cid = dbCursor.getInt(0);
            String firstName = dbCursor.getString(1);
            String lastName = dbCursor.getString(2);
            String notes = dbCursor.getString(3);

            dbResultsView.append(cid+firstName+lastName+notes+"\r\n");
            
            System.err.println(firstName);
        }
	}
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        
        if(dbHandler !=null)
        {
            dbHandler.close();
        }
    }
    
}