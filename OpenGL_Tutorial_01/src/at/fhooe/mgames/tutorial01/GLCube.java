package at.fhooe.mgames.tutorial01;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLUtils;
import at.fhooe.mgames.utils.GLMatrix;
import at.fhooe.mgames.utils.Utils;

public class GLCube
{

	/** A constant to multiply with the real angle to allow object spinning. */
	private static final float ANGLE_FACTOR = 0.35f;

	/** The initial vertex definition */
	private static final float[] VERTICES = new float[] {
			// The front face.
			-1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, -1.0f, -1.0f,
			1.0f,
			1.0f,
			-1.0f,
			1.0f,

			// The left face.
			1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f, 1.0f,
			1.0f,
			-1.0f,
			-1.0f,

			// The back face.
			1.0f, 1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f,
			-1.0f,
			-1.0f,

			// The right face.
			-1.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,
			1.0f,

			// The top face.
			-1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,

			// The bottom face.
			-1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f };

	private static final float[] NORMALS = new float[] {
			// The front face.
			0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f,
			0.0f,
			0.0f,
			1.0f,

			// The left face.
			1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			1.0f,
			0.0f,
			0.0f,

			// The back face.
			0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f,
			0.0f,
			-1.0f,

			// The right face.
			-1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f,
			0.0f,

			// The top face.
			0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

			// The bottom face.
			0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f };

	private static final float[] TEX_COORDS = new float[] {
			// The front face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
			0.0f,
			0.0f,
			1.0f,
			1.0f,
			1.0f,

			// The left face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,
			0.0f,
			1.0f,
			1.0f,
			1.0f,

			// The back face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f,
			1.0f,
			1.0f,
			1.0f,

			// The right face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
			1.0f,
			1.0f,

			// The top face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
			1.0f,

			// The bottom face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
			1.0f };

	private static final byte[] FACES = new byte[] { 0, 1, 2, 2, 1, 5, 6, 7, 8,
			8, 7, 11, 12, 13, 14, 14, 13, 17, 18, 19, 20, 20, 19, 23, 24, 25,
			26, 26, 25, 29, 30, 31, 32, 32, 31, 35 };

	/** The buffer holding the vertices */
	private static final FloatBuffer vertexBuffer;
	/** The buffer holding the texture coordinates */
	private static final FloatBuffer textureBuffer;
	/** The buffer holding the indices */
	private static final ByteBuffer indexBuffer;
	/** The buffer holding the normals */
	private static final FloatBuffer normalBuffer;

	static
	{
		vertexBuffer = Utils.wrapDirect(VERTICES);
		textureBuffer = Utils.wrapDirect(TEX_COORDS);
		normalBuffer = Utils.wrapDirect(NORMALS);

		indexBuffer = ByteBuffer.allocateDirect(FACES.length);
		indexBuffer.put(FACES);
		indexBuffer.position(0);
	}

	/** Rotational distance for x- and y-axis. */
	public float dx, dy;

	/** Rotational speed for x- and y-axis. */
	public float dxSpeed, dySpeed;

	/** A 4x4 matrix used to calculate rotation. */
	private GLMatrix mBaseMatrix;

	/** Rotation angle. */
	private float rotation;

	/** Textures buffer containing pointers to the textures. */
	private IntBuffer mTexturesBuffer;

	/**
	 * The Cube constructor.
	 * 
	 * <p>
	 * Initiates the buffers with the arrays from above.
	 * </p>
	 */
	public GLCube()
	{
		mBaseMatrix = new GLMatrix();
	}

	/**
	 * Updates the model.
	 * 
	 * @param dt
	 *            The time difference between the last and the current update.
	 */
	public void update(long dt)
	{

		// Calculate the rotation
		rotation = (float) Math.sqrt(dx * dx + dy * dy);

		dx += dxSpeed * dt;
		dy += dySpeed * dt;
		dampenSpeed(dt);
	}

	/**
	 * The object own drawing function. Called from the renderer to redraw this
	 * instance with possible changes in values.
	 * 
	 * @param gl
	 *            - The GL Context
	 * @param filter
	 *            - Which texture filter to be used
	 */
	public void draw(GL10 gl, int filter)
	{
		// Save original modelview matrix to only rotate object and not entire
		// model space
		gl.glPushMatrix();

		// Rotate the cube based on the information from touch input
		if (rotation != 0)
		{
			gl.glRotatef(rotation * ANGLE_FACTOR, dy / rotation, dx / rotation,
					0);
		}

		// Switch back to view coordinates to look at cube
		gl.glMultMatrixf(mBaseMatrix.data, 0);

		/*
		 * STEP 10: Load the desired texture to display the cube. Use the filter
		 * variable as index for the texturesBuffer.
		 */
		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexturesBuffer.get(filter));

		/*
		 * STEP 11: Enable the vertex, texture and normal state
		 */
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);

		// Set the face rotation (counter clock wise)
		gl.glFrontFace(GL10.GL_CCW);

		/*
		 * STEP 12: Set the vertex pointer, texcoord pointer and normal pointer.
		 */
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);
		gl.glNormalPointer(GL10.GL_FLOAT, 0, normalBuffer);

		/*
		 * STEP 13: Draw the vertices as triangles, based on the Index Buffer
		 * information.
		 */
		gl.glDrawElements(GL10.GL_TRIANGLES, FACES.length,
				GL10.GL_UNSIGNED_BYTE, indexBuffer);

		/*
		 * STEP 14: Disable the client state for vertex, texture and normals
		 * array before leaving
		 * 
		 * Run your application. You should see a white cube you already can
		 * spin!
		 */
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glDisableClientState(GL10.GL_NORMAL_ARRAY);

		// Restore original modelview matrix
		gl.glPopMatrix();
	}

	public void loadTextures(GL10 gl, Context context)
	{
		/*
		 * STEP 15a: Enable texturing mode, allocate memory in your
		 * texturesBuffer and generate 3 textures.
		 */
		mTexturesBuffer = IntBuffer.allocate(3);
		gl.glGenTextures(3, mTexturesBuffer);

		// Load texture file from resources folder
		Bitmap texture = Utils.getTextureFromBitmapResource(context,R.drawable.glass);

		/*
		 * STEP 16: Set up your first texture from the texturesBuffer using
		 * nearest neighbor filtering for magnification as well as minification
		 * filtering. Set texture wrapping for s and t coordinates to
		 * clamp-to-edge and apply the bitmap.
		 * 
		 * Run the application and see what you have!
		 */
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexturesBuffer.get(0));

		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
				GL10.GL_NEAREST);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
				GL10.GL_NEAREST);

		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
				GL10.GL_REPEAT);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
				GL10.GL_REPEAT);

		GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, texture, 0);

		/*
		 * STEP 17: Set your second texture from the texturesBuffer using linear
		 * filtering for magnification as well as minification filtering. Set
		 * texture wrapping for s and t coordinates to clamp-to-edge and apply
		 * the bitmap.
		 * 
		 * Run the application and switch the filtering mode using switch
		 * filtering button from the options menu!
		 */
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexturesBuffer.get(1));

		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
				GL10.GL_LINEAR);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
				GL10.GL_LINEAR);

		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
				GL10.GL_REPEAT);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
				GL10.GL_REPEAT);

		GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, texture, 0);

		/*
		 * STEP 18: Set your second texture from the texturesBuffer using linear
		 * filtering for magnification and mipmapping for minification
		 * filtering. Set texture wrapping for s and t coordinates to
		 * clamp-to-edge and generate mipmaps.
		 */
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexturesBuffer.get(2));
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
				GL10.GL_LINEAR);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
				GL10.GL_LINEAR);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
				GL10.GL_CLAMP_TO_EDGE);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
				GL10.GL_CLAMP_TO_EDGE);

		if (gl instanceof GL11)
		{
			gl.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_GENERATE_MIPMAP,
					GL11.GL_TRUE);
			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, texture, 0);
		}
		else
		{
			Utils.generateMipmapsForBoundTexture(texture);
		}

		// free bitmap resources as everything needed is now stored in the
		// texture buffers
		texture.recycle();
	}

	/**
	 * Saves the cube rotation in a rotation matrix.
	 */
	public void saveRotation()
	{
		float r = (float) Math.sqrt(dx * dx + dy * dy);
		if (r != 0)
		{
			GLMatrix rotation = new GLMatrix();
			rotation.rotate(r * ANGLE_FACTOR, dy / r, dx / r, 0);
			mBaseMatrix.premultiply(rotation);
		}

		dx = 0.0f;
		dy = 0.0f;
	}

	/**
	 * Dampens the rotation speed.
	 * 
	 * @param dt
	 *            The time difference between two updates.
	 */
	public void dampenSpeed(long dt)
	{
		if (dxSpeed != 0.0f)
		{
			dxSpeed *= (1.0f - 0.001f * dt);
			if (Math.abs(dxSpeed) < 0.001f)
				dxSpeed = 0.0f;
		}

		if (dySpeed != 0.0f)
		{
			dySpeed *= (1.0f - 0.001f * dt);
			if (Math.abs(dySpeed) < 0.001f)
				dySpeed = 0.0f;
		}
	}
}
