package at.fhooe.mgames.tutorial01;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Implementation of an OpenGL ES drawing area.
 * 
 * <p>
 * The drawing surface can be set as content views by activities and can handle
 * touch gestures as well as touch dragging.
 * </p>
 * 
 * @author Florian Lettner
 * @see GLSurfaceView
 * @see Runnable
 */
public class GameSurfaceView extends GLSurfaceView
{

	/** The game loop to update and render the model. */
	private GameLoop mGameLoop;

	/** The model to be drawn. */
	private GLCube mModel;

	/** The renderer used for drawing OpenGL content. */
	private GLRenderer mRenderer;

	/** A gesture detector to detect touch gestures. */
	private GestureDetector mGestureDetector;

	/** A temporary variable to save initial x-touch coordinate. */
	private float mStartX;

	/** A temporary variable to save initial y-touch coordinate. */
	private float mStartY;

	/** The time measurement from the previous update. */
	private long mLastMillis;

	/** A boolean that indicates to quit the game. */
	private volatile boolean mQuit;

	/**
	 * Instantiates this instance of GameSurfaceView.
	 * 
	 * @param context
	 *            The application context.
	 */
	public GameSurfaceView(Context context)
	{
		super(context);

		/*
		 * STEP 06: Create a new renderer and assign it to the surface view
		 */
		mRenderer = new GLRenderer(context);
		setRenderer(mRenderer);

		// Disable active rendering so we can use our own game loop
		setRenderMode(RENDERMODE_WHEN_DIRTY);

		// Create a gesture detector to catch touch gestures.
		mGestureDetector = new GestureDetector(context,
				new GameSurfaceGestureListener());

		mModel = new GLCube();

		// Create and start our game loop
		mGameLoop = new GameLoop();
		mGameLoop.start();
	}

	public void onDestroy()
	{
		mQuit = true;
	}

	/**
	 * Handle touch events.
	 * 
	 * <p>
	 * Reacts to screen touches and drags. A touch resets the screen to 0.0f and
	 * sets the initial touch points. Dragging on the surface calculates the
	 * distance vector from the start point and assigns the distance to the
	 * scene point.
	 * </p>
	 * <p>
	 * <strong>Touching and dragging is not a touch gesture.</strong>
	 * </p>
	 * 
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{

		// Check if touch is a gesture
		if (mGestureDetector.onTouchEvent(event))
		{
			return true;
		}

		switch (event.getAction())
		{
		case MotionEvent.ACTION_DOWN:
			mModel.dxSpeed = 0.0f;
			mModel.dySpeed = 0.0f;
			mModel.saveRotation();
			mStartX = event.getX();
			mStartY = event.getY();
			break;
		case MotionEvent.ACTION_MOVE:
			mModel.dx = event.getX() - mStartX;
			mModel.dy = event.getY() - mStartY;
			break;
		}

		return true;
	}

	private void render(GLCube cube)
	{
		mRenderer.setModel(cube);
		requestRender();
	}

	/**
	 * A gesture listener to detect flings.
	 * 
	 * @author Florian Lettner
	 * @see GestureDetector.SimpleOnGestureListener
	 */
	private class GameSurfaceGestureListener extends
			GestureDetector.SimpleOnGestureListener
	{

		/**
		 * Modifies the speed variable in the game state.
		 * 
		 * @param e1
		 *            The touch down event.
		 * @param e2
		 *            The touch up event.
		 * @param velocityX
		 *            The velocity on the x-axis.
		 * @param velocityY
		 *            The velocity on the y-axis.
		 * 
		 * @return <code>true</code> if the event has been handled;
		 *         <code>false</code> otherwise.
		 */
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY)
		{

			// Measure speed in milliseconds
			mModel.dxSpeed = velocityX / 1000;
			mModel.dySpeed = velocityY / 1000;

			return super.onFling(e1, e2, velocityX, velocityY);
		}
	}

	/**
	 * The game loop.
	 * 
	 * <p>
	 * The game loop calculates the difference between the single updates and
	 * uses this time to update the model, which is afterwards rendered by the
	 * renderer.
	 * <p>
	 * 
	 * @author Florian Lettner
	 * @see Thread
	 */
	private class GameLoop extends Thread
	{

		@Override
		public void run()
		{
			mLastMillis = System.currentTimeMillis();

			while (!mQuit)
			{
				/*
				 * STEP 07: Get the system time in ms and calculate the time
				 * difference since the last update.
				 */
				long curMillis = System.currentTimeMillis();
				long difference = curMillis - mLastMillis;

				/*
				 * STEP 08a: Update the model
				 */
				mModel.update(difference);

				/*
				 * STEP 08b: Render the model
				 */
				render(mModel);

				/*
				 * STEP 09: Set the last time measured to the current time.
				 */
				mLastMillis = curMillis;

				/*
				 * Short sleep to allow context switch to the OpenGL thread.
				 * This is important because the Androi OpenGL implementation
				 * uses event handling when RENDERMODE_WHEN_DIRTY is used and so
				 * update would be called more often than render.
				 */
				try
				{
					sleep(10);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
}
