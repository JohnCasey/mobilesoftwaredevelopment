package at.fhooe.mgames.tutorial01;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class GlApp extends Activity
{

	/** The drawing area. */
	private GameSurfaceView mDrawingSurface;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		/*
		 * STEP 01: Create a new GLSurfaceView, which will be the OpenGL
		 * drawing area
		 */
		mDrawingSurface = new GameSurfaceView(getApplicationContext());

		/*
		 * STEP 02: Set the GLSurfaceView as the activity's content view
		 */
		setContentView(mDrawingSurface);
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		/*
		 * STEP 03: Pause the drawing view to stop the game loop.
		 */
		mDrawingSurface.onPause();
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		/*
		 * STEP 04: Resume the drawing view to continue the game loop.
		 */
		mDrawingSurface.onResume();
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();

		/*
		 * STEP 05: Destroy the drawing view to stop the game loop.
		 */
		mDrawingSurface.onDestroy();
	}

	/**
	 * Creates the options.
	 * 
	 * <p>
	 * Adds menu items to toggle scene lighting, blending and switching texture
	 * filter modes.
	 * </p>
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(0, 1, 0, "Toggle Lighting");
		menu.add(0, 2, 0, "Toggle Blending");
		menu.add(0, 3, 0, "Switch Filtering");
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * Handles clicks on option menu items.
	 * 
	 * <p>
	 * Identifies the menu item and changes the scene state.
	 * </p>
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		if (item.getTitle().toString().contains("Lighting"))
			SceneState.getInstance().toggleLighting();

		if (item.getTitle().toString().contains("Blending"))
			SceneState.getInstance().toggleBlending();

		if (item.getTitle().toString().contains("Filtering"))
			SceneState.getInstance().switchToNextFilter();

		return super.onOptionsItemSelected(item);
	}
}