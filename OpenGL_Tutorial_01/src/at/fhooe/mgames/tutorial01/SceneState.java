package at.fhooe.mgames.tutorial01;

public class SceneState
{

	private static SceneState mInstance;

	int filter = 2;

	private boolean isLightingEnabled;
	private boolean isBlendingEnabled;

	public static SceneState getInstance()
	{
		if (mInstance == null)
		{
			mInstance = new SceneState();
		}

		return mInstance;
	}

	private SceneState()
	{

	}

	public void toggleLighting()
	{
		isLightingEnabled = !isLightingEnabled;
	}

	public void switchToNextFilter()
	{
		filter = (filter + 1) % 3;

	}

	public void toggleBlending()
	{
		isBlendingEnabled = !isBlendingEnabled;
	}

	public boolean isLightingEnabled()
	{
		return isLightingEnabled;
	}

	public boolean isBlendingEnabled()
	{
		return isBlendingEnabled;
	}
}
