package at.fhooe.mgames.tutorial01;

import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;

public class GLRenderer implements Renderer
{

	/*
	 * STEP 19: Define ambient color according to glColor4f format.
	 */
	private final static float AMBIENT_COLOR[] = { 0.25f, 0.25f, 0.25f, 1.0f };

	/*
	 * STEP 20: Define diffuse color according to glColor4f format.
	 */
	private final static float DIFFUSE_COLOR[] = { 0.01f, 0.01f, 0.01f, 1.0f };

	/*
	 * STEP 21: Define light position according to glColor4f format.
	 */
	private final static float LIGHT_POSITION[] = { 0.5f, 0.5f, 0.5f, 1.0f };

	/** The ambient light buffer. */
	private FloatBuffer mAmbientLightBuffer;

	/** The diffuse light buffer. */
	private FloatBuffer mDiffuseLightBuffer;

	/** The light position buffer. */
	private FloatBuffer mLightPositionBuffer;

	/** The application context required to load resources. */
	private Context mContext;

	/** The model to be rendered. */
	private GLCube mModel;

	/** The scene settings. */
	private SceneState mState;

	/**
	 * Instantiate a new instance of GLRenderer
	 * 
	 * <p>
	 * Puts the light arrays into float buffers.
	 * </p>
	 * 
	 * @param context
	 *            The application context required to load textures;
	 */
	public GLRenderer(Context context)
	{
		this.mContext = context;

		mAmbientLightBuffer = FloatBuffer.wrap(AMBIENT_COLOR);
		mDiffuseLightBuffer = FloatBuffer.wrap(DIFFUSE_COLOR);
		mLightPositionBuffer = FloatBuffer.wrap(LIGHT_POSITION);

		mState = SceneState.getInstance();
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// Use gouraud shading for smooth lights and corners
		gl.glShadeModel(GL10.GL_SMOOTH);

		// Reset the surface color
		gl.glClearColor(0, 0, 0, 0);

		// Reset depth buffer
		gl.glClearDepthf(1.0f);

		// Set depth function
		gl.glDepthFunc(GL10.GL_LEQUAL);

		// Improving the rendering quality by setting a perspective correction
		// hint
		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);

		/*
		 * STEP 22a: Create and enable GL_LIGHT0 with ambient and diffuse light.
		 */
		gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_AMBIENT, mAmbientLightBuffer);
		gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_DIFFUSE, mDiffuseLightBuffer);
		gl.glEnable(GL10.GL_LIGHT0);

		/*
		 * STEP 23: Change glColor to white with 50% transparency as blending
		 * color and set the blend function. Start with GL_SRC_ALPHA and
		 * GL_ONE_MINUS_SRC_ALPHA which is default for OpenGL. Try also GL_ONE
		 * and GL_ONE and GL_SRC_ALPHA and GL_ONE. Enable and disable
		 */
		gl.glEnable(GL10.GL_BLEND);
		gl.glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE);
		gl.glDisable(GL10.GL_BLEND);

	}

	@Override
	public void onDrawFrame(GL10 gl)
	{
		// Clear color buffer
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

		// Load model view matrix
		gl.glMatrixMode(GL10.GL_MODELVIEW);

		// Reset model view matrix
		gl.glLoadIdentity();

		// Move camera away from center
		gl.glTranslatef(0.0f, 0.0f, -6.0f);

		if (mState.isLightingEnabled())
		{
			/*
			 * STEP 24a: Enable lighting
			 */
			gl.glEnable(GL10.GL_LIGHTING);
		}
		else
		{
			/*
			 * STEP 24b: Disable lighting
			 */
			gl.glDisable(GL10.GL_LIGHTING);
		}

		if (mState.isBlendingEnabled())
		{
			/*
			 * STEP 25a: Enable blending
			 */
			gl.glEnable(GL10.GL_BLEND);

			// Disable cull facing to also draw back faces of objects
			gl.glDisable(GL10.GL_CULL_FACE);

			// Disable depth test otherwise back sides of model won't be drawn
			gl.glDisable(GL10.GL_DEPTH_TEST);
		}
		else
		{
			/*
			 * STEP 25: Disable blending
			 */
			gl.glDisable(GL10.GL_BLEND);

			// Enable cull facing to not draw back faces of objects
			gl.glEnable(GL10.GL_CULL_FACE);

			// Enable depth test
			gl.glEnable(GL10.GL_DEPTH_TEST);
		}

		// Draw cube
		if (mModel != null)
		{
			mModel.draw(gl, mState.filter);
		}

		/*
		 * STEP 22b: Set the light position after the model has been drawn. The
		 * light position has to be set each time the scene is rendered,
		 * otherwise the light will rotate when the camera rotates.
		 */
		gl.glLightfv(GL10.GL_POSITION, GL10.GL_LIGHT0, mLightPositionBuffer);
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height)
	{
		/*
		 * STEP 15: Load the textures from model
		 */
		mModel.loadTextures(gl, mContext);

		// avoid division by zero
		if (height == 0)
		{
			height = 1;
		}

		// Define the viewport (fullscreen in this case)
		gl.glViewport(0, 0, width, height);

		// Load projection matrix
		gl.glMatrixMode(GL10.GL_PROJECTION);

		// Load identity matrix and reset projection matrix
		gl.glLoadIdentity();

		// Setup the perspective (field of view 45� - human high)
		GLU.gluPerspective(gl, 45.0f, (float) width / (float) height, 1.0f,
				100.0f);
	}

	public void setModel(GLCube cube)
	{
		this.mModel = cube;
	}
}
