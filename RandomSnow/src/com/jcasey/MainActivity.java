package com.jcasey;

import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.Menu;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MainActivity extends Activity {

	public static final int REFRESH_RATE=20;
	
	public final class RandomView extends SurfaceView implements Runnable, SurfaceHolder.Callback {
		private final Random random;
		Thread main;
		
		public RandomView(Context context, Random random) {
			super(context);
			this.random = random;
			
			getHolder().addCallback(this);
		}


		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
			
		}
		public void surfaceCreated(SurfaceHolder holder) {
		  	main=new Thread(this);
		  	if(main!=null)
		  		main.start();
			
		}
		public void surfaceDestroyed(SurfaceHolder holder) {
			
		}
		
		public void run()
		{
			while(true)
			{
				Canvas canvas = null;
				SurfaceHolder holder = getHolder();
				Paint paint = new Paint();
				paint.setAntiAlias(true);
				synchronized (holder) {
					canvas = holder.lockCanvas();
	
					for(int i=0;i<1000;i++)
					{
						paint.setARGB(0, random.nextInt(), 0, 0);
						float y = (float) ((float) (Math.asin(random.nextDouble())+(Math.PI/2))/Math.PI)  * getHeight();
						float x = (float)(random.nextDouble()) * this.getWidth();
						paint.setColor(random.nextInt());
						canvas.drawPoint(x, y, paint);
					}
					try {
						Thread.sleep(REFRESH_RATE);
					}
					catch (Exception e)
					{
						
					}
					
	
					holder.unlockCanvasAndPost(canvas);
				}
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		final Random random = new Random();
		
		setContentView(new RandomView(getApplicationContext(), random));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
