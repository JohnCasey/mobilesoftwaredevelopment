package com.jcasey;

import java.util.ArrayList;
import java.util.List;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class GoogleMapsActivity extends MapActivity  {
	
	private class SimpleItemizedOverlay extends ItemizedOverlay
	{
		ArrayList<OverlayItem> overlayItems = new ArrayList<OverlayItem>();
		
		public SimpleItemizedOverlay(Drawable drawable)
		{
			super(boundCenter(drawable));
			
			overlayItems.add(new OverlayItem(new GeoPoint(-36884102, 174715592), "Mt Albert", ""));
			
			populate();
		}
		
		@Override
		protected OverlayItem createItem(int index)
		{
			return overlayItems.get(index);
		}
		
		@Override
		public int size()
		{
			return overlayItems.size();
		}
	}

	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);
        
        MapView map = (MapView) findViewById(R.id.map);
        map.setBuiltInZoomControls(true);
        
        SimpleItemizedOverlay overlay = new SimpleItemizedOverlay(getResources().getDrawable(R.drawable.ic_launcher));
        
        List <Overlay>routes = map.getOverlays();
        routes.add(overlay);
    }

	@Override
	protected boolean isRouteDisplayed()
	{
		return true;
	}
}