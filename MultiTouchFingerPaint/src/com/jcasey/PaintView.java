package com.jcasey;

import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class PaintView extends View implements OnTouchListener
{
	/**
	 * @author John Casey
	 *  
	 *  PaintView class developed by John Casey 06/03/2014 
	*/
	final Paint paint = new Paint();
	final Random random = new Random();
	
	Bitmap offScreenBitmap;
	Canvas offScreenCanvas;

	// define additional constructors so that PaintView will work with out layout file
	
	public PaintView(Context context) {
		super(context);
		
		setup();
	}

	public PaintView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		
		setup();
	}

	public PaintView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		setup();
	}

	public void setup()
	{
		setOnTouchListener(this); // define event listener and start intercepting events 
	}
	
	@Override
	protected void onDraw(Canvas canvas)
	{
		// draw the off screen bitmap
		canvas.drawBitmap(offScreenBitmap, 0, 0, null);
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {

		// get the x,y coordinates of the MotionEvent.ACTION_MOVE event
		
		int pts = event.getPointerCount(); // get the number of pts
			
		for(int i = 0; i<pts; i++)
		{
			// get the individual pts and draw on screen
			
			float x = event.getX(i);
			float y = event.getY(i);
		
			paint.setARGB(random.nextInt(256), random.nextInt(256), random.nextInt(256), random.nextInt(256));

			offScreenCanvas.drawCircle(x, y, 20, paint); // draw a red circle at the x,y coordinates specified by the user
		}
		invalidate(); // force a screen re-draw
		
		return true; // actually consume the event
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
		// create / re-create the off screen bitmap to capture the state of our drawing
		// this operation will reset the user's drawing
		
		offScreenBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		offScreenCanvas = new Canvas(offScreenBitmap);
	}	
}
