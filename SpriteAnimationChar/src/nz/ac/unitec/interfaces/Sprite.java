package nz.ac.unitec.interfaces;

import android.graphics.Canvas;
import android.graphics.Point;

public interface Sprite
{
	public void load();
	
	public void move();

	public void update();

	public void draw(Canvas canvas);
	
	public void setPos(Point pos);
}
