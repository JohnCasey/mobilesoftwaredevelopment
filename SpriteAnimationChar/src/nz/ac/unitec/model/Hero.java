package nz.ac.unitec.model;

import nz.ac.unitec.R;
import nz.ac.unitec.interfaces.Sprite;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;

public class Hero implements Sprite
{
	private final static int STAND = 0;
	private final static int WALK = 1;

	private final static int UP = 0;
	private final static int RIGHT = 1;
	private final static int DOWN = 2;
	private final static int LEFT = 3;

	private final static int SPEED = 10;

	private Resources mResources;

	private int mCurrentFrame = 0;
	private int mNumOfFrames;

	private Rect mSourceRect;
	private int mSpriteWidth;
	private int mSpriteHeight;
	private int mState;

	private Point mCurPos;
	private Point mGoalPos;
	private int mDirection;
	private boolean mKeepDir;

	private Bitmap mTexture;

	private Bitmap mWalkDown;
	private Bitmap mWalkUp;
	private Bitmap mWalkLeft;
	private Bitmap mWalkRight;

	private Bitmap mStandDown;
	private Bitmap mStandUp;
	private Bitmap mStandLeft;
	private Bitmap mStandRight;

	public Hero(Resources resources)
	{
		mSourceRect = new Rect();
		mSpriteWidth = 40;
		mSpriteHeight = 60;

		mSourceRect.top = 0;
		mSourceRect.bottom = mSpriteHeight;

		mCurPos = new Point(100, 100);

		mResources = resources;

		mState = STAND;
		mDirection = DOWN;

		mKeepDir = false;

		load();
	}

	@Override
	public void load()
	{
		mWalkDown = BitmapFactory.decodeResource(mResources,
				R.drawable.hero_walk_down);
		mWalkUp = BitmapFactory.decodeResource(mResources,
				R.drawable.hero_walk_up);
		mWalkLeft = BitmapFactory.decodeResource(mResources,
				R.drawable.hero_walk_left);
		mWalkRight = BitmapFactory.decodeResource(mResources,
				R.drawable.hero_walk_right);

		mStandDown = BitmapFactory.decodeResource(mResources,
				R.drawable.hero_down);
		mStandUp = BitmapFactory.decodeResource(mResources, R.drawable.hero_up);
		mStandLeft = BitmapFactory.decodeResource(mResources,
				R.drawable.hero_left);
		mStandRight = BitmapFactory.decodeResource(mResources,
				R.drawable.hero_right);

		mTexture = mStandDown;
	}

	@Override
	public void move()
	{
		switch (mDirection)
		{
		case UP:

			if (mState == STAND)
				mTexture = mStandUp;
			else
			{
				mTexture = mWalkUp;
				setMovement();
			}

			break;

		case RIGHT:

			if (mState == STAND)
				mTexture = mStandRight;
			else
			{
				mTexture = mWalkRight;
				setMovement();
			}

			break;

		case DOWN:

			if (mState == STAND)
				mTexture = mStandDown;
			else
			{
				mTexture = mWalkDown;
				setMovement();
			}

			break;

		case LEFT:

			if (mState == STAND)
				mTexture = mStandLeft;
			else
			{
				mTexture = mWalkLeft;
				setMovement();
			}

			break;

		default:
			break;
		}
	}

	private void setMovement()
	{
		int difX = mGoalPos.x - mCurPos.x;
		int difY = mGoalPos.y - mCurPos.y;

		if (Math.abs(difX) <= SPEED / 2 && Math.abs(difY) <= SPEED / 2)
		{
			mState = STAND;
		}
		else
		{
			if (!mKeepDir)
			{
				if (Math.abs(difX) < Math.abs(difY))
				{
					if (difY > 0)
						mDirection = DOWN;
					else
						mDirection = UP;
				}
				else
				{
					if (difX > 0)
						mDirection = RIGHT;
					else
						mDirection = LEFT;
				}

				mKeepDir = true;
			}
			else if (mDirection == UP || mDirection == DOWN)
			{
				if (Math.abs(difY) <= SPEED / 2)
				{
					mKeepDir = false;
				}
				else
				{
					if (mDirection == UP)
						mCurPos.y -= SPEED;
					else if (mDirection == DOWN)
						mCurPos.y += SPEED;

					mState = WALK;
				}
			}
			else
			{
				if (Math.abs(difX) <= SPEED / 2)
				{
					mKeepDir = false;
				}
				else
				{
					if (mDirection == LEFT)
						mCurPos.x -= SPEED;
					else if (mDirection == RIGHT)
						mCurPos.x += SPEED;

					mState = WALK;
				}
			}
		}

	}

	@Override
	public void update()
	{
		if (mState == STAND)
		{
			mNumOfFrames = 1;
		}
		else if (mState == WALK)
		{
			mNumOfFrames = 8;
		}

		mSourceRect.left = mSpriteWidth * mCurrentFrame;
		mSourceRect.right = mSourceRect.left + mSpriteWidth;

		mCurrentFrame++;

		if (mCurrentFrame >= mNumOfFrames)
		{
			mCurrentFrame = 0;
		}
	}

	@Override
	public void draw(Canvas canvas)
	{
		canvas.drawBitmap(mTexture, mSourceRect, getPosRect(), null);
	}

	private Rect getPosRect()
	{
		return new Rect(mCurPos.x - mSpriteWidth, mCurPos.y - mSpriteHeight,
				mCurPos.x + mSpriteWidth, mCurPos.y + mSpriteHeight);
	}

	@Override
	public void setPos(Point pos)
	{
		mGoalPos = pos;
		mKeepDir = false;
		mState = WALK;
	}
}