package nz.ac.unitec.model;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import nz.ac.unitec.R;
import nz.ac.unitec.interfaces.Sprite;

public class Background implements Sprite
{
	private Resources mResources;

	private Bitmap mTile;

	public Background(Resources resources)
	{
		mResources = resources;

		load();
	}

	@Override
	public void load()
	{
		mTile = BitmapFactory.decodeResource(mResources,
				R.drawable.grass_background);

	}

	@Override
	public void move()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void update()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void draw(Canvas canvas)
	{
		int width = canvas.getWidth();
		int height = canvas.getHeight();
		
		for(int i = 0; i < height; i += mTile.getHeight())
		{
			for(int j = 0; j < width; j += mTile.getWidth())
			{
				canvas.drawBitmap(mTile, j, i, null);
			}
		}		
	}

	@Override
	public void setPos(Point pos)
	{
		// TODO Auto-generated method stub

	}

}
