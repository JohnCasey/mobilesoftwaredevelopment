package nz.ac.unitec.game;

import java.util.Vector;

import nz.ac.unitec.model.Background;
import nz.ac.unitec.model.Hero;
import nz.ac.unitec.interfaces.Sprite;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;

public class GamePanel extends SurfaceView implements SurfaceHolder.Callback,
		OnTouchListener
{
	private GameLoop mGameLoop = null;
	final int NUM_OF_SPRITES = 24;

	private Vector<Sprite> mSprites;

	public GamePanel(Context context)
	{
		super(context);
		setOnTouchListener(this);

		mSprites = new Vector<Sprite>();

		// adding the callback (this) to the surface holder to intercept events
		getHolder().addCallback(this);

		mSprites.add(new Background(getResources()));
		mSprites.add(new Hero(getResources()));

		// create the game loop thread
		mGameLoop = new GameLoop(getHolder(), this);

		// make the GamePanel focusable so it can handle events
		setFocusable(true);
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height)
	{

	}

	public void surfaceCreated(SurfaceHolder holder)
	{
		mGameLoop.setRunning(true);
		mGameLoop.start();
	}

	public void surfaceDestroyed(SurfaceHolder holder)
	{
		boolean retry = true;
		while (retry)
		{
			try
			{
				mGameLoop.join();
				retry = false;
			}
			catch (InterruptedException e)
			{
				// try again shutting down the thread
			}
		}
	}

	public void render(Canvas canvas)
	{
		canvas.drawColor(Color.BLACK);

		for (Sprite sprite : mSprites)
		{
			sprite.draw(canvas);
		}

	}

	public void update()
	{
		for (Sprite sprite : mSprites)
		{
			sprite.move();
			sprite.update();
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event)
	{

		for (Sprite sprite : mSprites)
		{
			Point p =  new Point((int) event.getX(), (int) event.getY());
			
			sprite.setPos(p);
		}

		return false;
	}
}
