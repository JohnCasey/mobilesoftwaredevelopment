package nz.ac.unitec.game;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class GameLoop extends Thread
{
	private SurfaceHolder mSurfaceHolder;
	private GamePanel mGamePanel;
	private boolean mRunning;

	public GameLoop(SurfaceHolder surfaceHolder, GamePanel gamePanel)
	{
		super();
		mSurfaceHolder = surfaceHolder;
		mGamePanel = gamePanel;
	}

	@Override
	public void run()
	{
		Canvas canvas;

		while (mRunning)
		{
			canvas = null;

			try
			{
				canvas = mSurfaceHolder.lockCanvas();

				synchronized (mSurfaceHolder)
				{
					mGamePanel.update();
					mGamePanel.render(canvas);
				}
			}
			finally
			{
				if (canvas != null)
				{
					mSurfaceHolder.unlockCanvasAndPost(canvas);
				}
			}

			try
			{
				Thread.sleep((long) 100);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

	public void setRunning(boolean running)
	{
		mRunning = running;
	}
}
