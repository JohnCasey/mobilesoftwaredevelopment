package nz.ac.unitec;

import nz.ac.unitec.game.GamePanel;
import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity
{
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		GamePanel game = new GamePanel(this);

		setContentView(game);
	}
}