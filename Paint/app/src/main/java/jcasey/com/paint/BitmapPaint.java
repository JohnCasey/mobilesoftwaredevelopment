package jcasey.com.paint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by jcase on 10/08/2017.
 */

public class BitmapPaint extends View implements View.OnTouchListener{


    float x;
    float y;

    final Paint paint = new Paint();
    Bitmap buffer = null;
    Canvas pen = null;

    public BitmapPaint(Context context) {
        super(context);

        setup();
    }

    public BitmapPaint(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        setup();
    }

    public BitmapPaint(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setup();
    }

    private void setup() {
        setOnTouchListener(this);

        // buffer = Bitmap.createBitmap(getWidth(),getHeight(), Bitmap.Config.ARGB_8888); // not going to work because the layout has not been initialized yet

        // post message to the queue Runnable will be called after View has been laid out
        post(new Runnable() {
            @Override
            public void run() {
                buffer = Bitmap.createBitmap(getWidth(),getHeight(), Bitmap.Config.ARGB_8888);

                pen = new Canvas(buffer);
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(buffer,0,0,paint);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        paint.setColor(Color.RED);
        pen.drawCircle(event.getX(), event.getY(), 20, paint);

        invalidate();

        return true;
    }
}