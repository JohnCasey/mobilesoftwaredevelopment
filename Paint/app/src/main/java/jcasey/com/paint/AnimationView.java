package jcasey.com.paint;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class AnimationView extends View {

    float x = 0.0f;
    float y = 0.0f;

    float vx = 5.0f;
    float vy = 5.0f;

    Paint paint = new Paint();

    float width;
    float height;

    public AnimationView(Context context) {
        super(context);

        setup();
    }

    public AnimationView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        setup();
    }

    public AnimationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setup();
    }

    private void setup() {
        post(new Runnable() {
            @Override
            public void run() {
                width = getWidth();
                height = getHeight();
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        paint.setColor(Color.RED);

        canvas.drawCircle(x,y,20,paint);

        if(x > width || x < 0)
        {
            vx = vx * -1.0f;
        }

        if(y > height || y < 0)
        {
            vy = vy * -1.0f;
        }


        x = x + vx;
        y = y + vy;


        invalidate();
    }
}
