package jcasey.com.paint;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class SharedVariablePaint extends View implements View.OnTouchListener{



    public SharedVariablePaint(Context context) {
        super(context);

        setOnTouchListener(this);
    }

    public SharedVariablePaint(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        setOnTouchListener(this);
    }

    public SharedVariablePaint(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setOnTouchListener(this);
    }

    float x;
    float y;

    final Paint paint = new Paint();

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        paint.setColor(Color.RED);
        canvas.drawCircle(x,y,20,paint);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        x = event.getX();
        y = event.getY();

        invalidate();

        return true;
    }
}