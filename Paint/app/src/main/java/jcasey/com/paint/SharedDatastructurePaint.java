package jcasey.com.paint;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.Random;

public class SharedDatastructurePaint extends View implements View.OnTouchListener{
    ArrayList <Point> points = new ArrayList<Point>();

    class Point
    {
        float x;
        float y;

        int colour;

        public Point(float x, float y, int colour) {
            this.x = x;
            this.y = y;
            this.colour = colour;
        }
    }

    final Paint paint = new Paint();
    final Random random = new Random();

    public SharedDatastructurePaint(Context context) {
        super(context);

        setOnTouchListener(this);
    }

    public SharedDatastructurePaint(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        setOnTouchListener(this);
    }

    public SharedDatastructurePaint(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setOnTouchListener(this);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for(Point pt: points)
        {
            paint.setColor(pt.colour);
            canvas.drawCircle(pt.x,pt.y,30,paint);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        points.add(new Point(event.getX(), event.getY(), random.nextInt()));

        invalidate();

        return true;
    }
}
