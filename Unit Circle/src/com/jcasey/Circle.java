package com.jcasey;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
public class Circle extends View implements OnTouchListener
{

	float height, width;
	final float buffer = 50;

	public Circle(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		this.setOnTouchListener(this);
	}

	public Circle(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		this.setOnTouchListener(this);
	}

	public Circle(Context context) {
		super(context);

		this.setOnTouchListener(this);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

		int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
	    int parentWidth =  MeasureSpec.getSize(widthMeasureSpec);
		
	    int width = Math.min(parentHeight, parentWidth);
	    
	    super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), heightMeasureSpec);
	}
	
	float cy = height /2;
	float cx = width /2;

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {

		height = h;
		width = w;

		cy = height /2;
		cx = (width /2);

		super.onSizeChanged(w, h, oldw, oldh);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		Paint paint = new Paint();
		paint.setColor(Color.RED);
		paint.setStyle(Style.STROKE);
		paint.setAntiAlias(true);

		canvas.drawRect(0,0,width,height,paint);

		float min = Math.min(cx, cy) - buffer;

		float dx = xpos - cx;
		float dy = ypos - (height-cy);

		double angle = Math.atan2(dy, dx);

		if(angle < 0)
		{
			angle = angle + (Math.PI * 2);
		}

		Log.e("angle",""+Math.toDegrees(angle));

		canvas.setDrawFilter(new PaintFlagsDrawFilter(1, Paint.ANTI_ALIAS_FLAG));

		canvas.drawCircle(cx, cy,min,paint);

		// line for first quadrant
		canvas.drawLine(cx,cy,cx+(float)(min * Math.cos(Math.PI/2)),cy-(float)(min * Math.sin(Math.PI/2)),paint);

		// line for second quadrant
		canvas.drawLine(cx,cy,cx+(float)(min * Math.cos(Math.PI)),cy-(float)(min * Math.sin(Math.PI)),paint);

		// line for third quadrant
		canvas.drawLine(cx,cy,cx+(float)(min * Math.cos(3 * Math.PI/2)),cy-(float)(min * Math.sin(3*Math.PI/2)),paint);

		// line for forth quadrant
		canvas.drawLine(cx,cy,cx+(float)(min * Math.cos(2 * Math.PI)),cy-(float)(min * Math.sin(2*Math.PI)),paint);

		// hypotenuse
		canvas.drawLine(cx,cy,cx+(float)(min * Math.cos(angle)),cy-(float)(min * Math.sin(angle)),paint);

		// opposite
		canvas.drawLine(cx+(float)(min * Math.cos(angle)),cy,cx+(float)(min * Math.cos(angle)),cy-(float)(min * Math.sin(angle)),paint);

		//canvas.drawRect(cx+(float)(min * Math.cos(angle))-5, cy-5, cx+(float)(Math.cos(angle)), cy, paint);
		final float border = 20;
		RectF r = new RectF(cx-border,cy-border,cx+border,cy+border);
		//				canvas.drawRect(r, paint);

		float xcor = 0;
		float ycor = 0;

		if(angle >= 0 && angle < Math.PI/2)
		{
			ycor = cy -10;
			xcor = (float) (cx+(min * Math.cos(angle))-10);

			canvas.drawArc(r,(float) -Math.toDegrees(angle),(float) Math.toDegrees(angle), true, paint);

		}
		else if ( angle >= Math.PI /2 && angle <Math.PI)
		{
			ycor = cy -10;
			xcor = (float) (cx+(min * Math.cos(angle))+10);

			canvas.drawArc(r,180,180-(float) Math.toDegrees(angle), true, paint);

		}
		else if ( angle >= Math.PI && angle <3*Math.PI/2)
		{
			ycor = cy +10;
			xcor = (float) (cx+(min * Math.cos(angle))+10);

			canvas.drawArc(r,(float) -Math.toDegrees(angle),(float) Math.toDegrees(angle)-180, true, paint);
		}
		else if ( angle >= 3*Math.PI/2 && angle <2 * Math.PI)
		{
			ycor = cy +10;
			xcor = (float) (cx+(min * Math.cos(angle))-10);

			canvas.drawArc(r,360,360-(float) Math.toDegrees(angle), true, paint);
		}
		canvas.drawRect(xcor, ycor, cx+(float)(min * Math.cos(angle)), cy, paint);
	}


	float xpos, ypos;

	public boolean onTouch(View v, MotionEvent event) {
		xpos = event.getX();
		ypos = height - event.getY();

		invalidate();

		return true;
	}        	
}
