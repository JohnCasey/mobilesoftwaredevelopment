package com.jcasey;

import android.app.Activity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        
        LinearLayout layout = (LinearLayout) findViewById(R.id.layout);
        
//        layout.addView(new Circle(getApplicationContext()), LayoutParams.MATCH_PARENT);
        
//        GraphViewSeries exampleSeries = new GraphViewSeries(new GraphViewData[] {  
//        	      new GraphViewData(1, 2.0d)  
//        	      , new GraphViewData(2, 1.5d)  
//        	      , new GraphViewData(3, 2.5d)  
//        	      , new GraphViewData(4, 1.0d)  
//        	});  
//        	  
//        	GraphView graphView = new LineGraphView(  
//        	      this // context  
//        	      , "GraphViewDemo" // heading  
//        	);  
//        	graphView.addSeries(exampleSeries); // data  
//        
//        	
//        LinearLayout layout = (LinearLayout) findViewById(R.id.layout);
//        layout.addView(graphView);
//        layout.addView(new Circle(getApplicationContext()), LayoutParams.WRAP_CONTENT);
//        

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
}
