package com.jcasey;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class SimpleView extends View implements OnTouchListener
{
	Paint paint = new Paint();
	private static final int LINE_OFFSET = 200;
	private static final int SQUARE = 100;

	float angle;
	
	public SimpleView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		
		init();
	}

	public SimpleView(Context context, AttributeSet attrs)
	{
		super(context, attrs);

		init();
	}

	public SimpleView(Context context)
	{
		super(context);
		
		init();
	}
	
	public void init()
	{
		setOnTouchListener(this);
		
		paint.setAntiAlias(true);
		paint.setColor(Color.GREEN);
	}
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		canvas.save();
		
		canvas.rotate(angle,getWidth() /2, getHeight() /2); // rotate the canvas by the angle calculated in onTouch
		
		int squareTop = getHeight() / 2 - SQUARE /2;
		int squareLeft = getWidth() / 2 - SQUARE /2;
		
		// draw into the pre-rotated canvas
		
		paint.setColor(Color.GREEN);
		canvas.drawRect(squareLeft, squareTop, squareLeft + SQUARE, squareTop + SQUARE, paint);
		
		paint.setColor(Color.BLACK);
				
		canvas.drawLine(getWidth() /2, getHeight() /2, getWidth() /2, getHeight() /2 - LINE_OFFSET, paint);
		canvas.drawCircle(getWidth() /2, getHeight() /2 - LINE_OFFSET, 5, paint);
		
		canvas.restore();
	}

	
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		if(event.getActionMasked() == MotionEvent.ACTION_MOVE)
		{
			float x2 = event.getX(0);
			float y2 = event.getY(0);

			float x1 = getWidth() / 2;
			float y1 = getHeight() /2;

			if(event.getPointerCount() == 2) // get the second point if we have multi touch
			{
				x1 = event.getX(0);
				y1 = event.getY(0);
				
				x2 = event.getX(1);
				y2 = event.getY(1);
			}

			float adjacent = x2 - x1;
			float opposite = y2 - y1;
			
			// use trigonometry to work out what the angle is
			double angle = (float) Math.atan2(opposite,adjacent);
			
			if(angle < 0)
			{
				angle = (float) (angle + (Math.PI * 2)); // add 180 degrees to our angle if it is negative
			}
			
			angle = angle + Math.PI /2 ; // adjust the quadrant by 90 degrees
			
			this.angle = (float)Math.toDegrees(angle);
			
			invalidate();
			
			return true;
		}
		return true;
	}
}
