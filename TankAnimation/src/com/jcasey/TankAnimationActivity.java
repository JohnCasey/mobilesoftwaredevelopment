package com.jcasey;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;

public class TankAnimationActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        class AnimationView extends SurfaceView implements Runnable, SurfaceHolder.Callback
        {
        	Thread animation = null;
			private boolean running;
			final int MAX_SPRITES = 25;
			ArrayList <Bitmap>tankSprites = new ArrayList<Bitmap>();
			
			public AnimationView(Context context) {
				super(context);
				
				// load in our animation strip from our resrouces directory
				Bitmap animStrip = BitmapFactory.decodeResource(getResources(), R.drawable.tank_sprite);
				
				// extract individual frames of animation from our animation strip
				int x = 0;
				for(int i =0; i< MAX_SPRITES-1; i++)
				{
					tankSprites.add(Bitmap.createBitmap(animStrip, x, 0, 120, 150));
					x = x + 120;
				}
				getHolder().addCallback(this);
			}

			public void surfaceChanged(SurfaceHolder holder, int format,
					int width, int height) {
				
				
			}

			public void surfaceCreated(SurfaceHolder holder) {
				// start the animation thread once the surface has been created
				
				animation = new Thread(this);
				running = true;
				
				animation.start(); // start a new thread to handle this activities animation
			}

			public void surfaceDestroyed(SurfaceHolder holder) {
				running = false;
				if(animation != null)
				{
					try {
						animation.join();  // finish the animation thread and let the animation thread die a natural death
					} catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
				
			}

			int frame = 0;
			
			public void run() {
				while(running){
					
					
					Canvas canvas = null;
					SurfaceHolder holder = getHolder();
					synchronized(holder)
					{
						canvas = holder.lockCanvas();
						
						// draw the tank sprite frames one at a time in sequence
						canvas.scale(2, 2); // scale them up a bit
						canvas.drawBitmap(tankSprites.get(frame), 0, 0, null);
						frame = frame +1;
						
						if(frame > tankSprites.size()-1) // reset to frame zero when we reach the end
						{
							frame =0;
						}
					}
					
					try {
						Thread.sleep(10/1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					holder.unlockCanvasAndPost(canvas);
				}
			}
        	
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE); // remove the title bar

        Display display = getWindowManager().getDefaultDisplay();
        
        DisplayMetrics outMetrics = new DisplayMetrics();
		display.getMetrics(outMetrics);
        
        setContentView(new AnimationView(getApplicationContext()));
    }
}