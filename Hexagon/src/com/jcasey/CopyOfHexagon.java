package com.jcasey;


import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;


public class CopyOfHexagon extends SurfaceView implements Runnable, SurfaceHolder.Callback, OnTouchListener
{
	private Thread Hex;
	private int degrees=0;
	private int srcx=64,srcy=64;
	private int polygon=0;
	int radius=5;
	
	double tantheta;
	
	/* lookup tables of trigonometric ratios */
	
	float xcoord[]=new float[180];
	float ycoord[]=new float[180];
	
	Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
	
	int width;
	int height;
	
	boolean running = false;
	float PI = (float) Math.PI;
	
	public CopyOfHexagon(Context context)
	{		
		super(context);
		

		setOnTouchListener(this);

		
		paint.setColor(Color.RED);

		float radians = PI / 90;
		
		/* calculate trigonometric ratios and store in lookup tables */
		for(int i=0;i<180;i++)
		{
			xcoord[i] = (FloatMath.sin(i*radians));
			ycoord[i] = (FloatMath.cos(i*radians));
		}
		
		getHolder().addCallback(this);
	}

	Random random = new Random();
	
	Bitmap offscreenBuffer;
	Canvas offscreenCanvas;
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
		offscreenBuffer = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		offscreenCanvas = new Canvas(offscreenBuffer);
		
		paint.setAntiAlias(true);
		paint.setFilterBitmap(true);
		
		this.width = w;
		this.height = h;
		
		srcx = width / 2;
		srcy = height / 2;
	}

	public  void run()
	{
		while(running)
		{
			paint.setColor(Color.BLACK);
			offscreenCanvas.drawColor(Color.BLACK);
			try
			{
				Thread.sleep(1000/60);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			degrees+=1;
			
			int inner=30;
			
			paint.setColor(Color.WHITE);			
			
			if(tantheta < 0)
			{
				Log.e("test",Double.toString(Math.abs(tantheta)));
				
				drawTriangle(offscreenCanvas, inner, 150, Math.abs(tantheta));
			}
			else
			{
				Log.e("test",Double.toString((360-tantheta)));
				
				drawTriangle(offscreenCanvas, inner, 150, 360 - tantheta);
			}
			
//			drawTriangle(offscreenCanvas, inner, 150, 360-Math.abs(tantheta));
			
			offscreenCanvas.drawLine(startX, startY, finalX, finalY, paint);
			offscreenCanvas.drawLine(startX, startY, width, startY, paint);
			
			
			

			
//			polygon+=1;

						
			Canvas canvas = null;
			
			SurfaceHolder holder = getHolder();
			synchronized(holder)
			{
				canvas = holder.lockCanvas(null);
				canvas.drawColor(Color.BLACK);
				canvas.drawBitmap(offscreenBuffer, 0, 0, null);
			
				holder.unlockCanvasAndPost(canvas);
			}
		}
	}

	public void drawLines(Canvas canvas) {
		int xtemp;
		int ytemp;
		int count;
		int j = 30; // 30;
		
		for(count=0;count<360;count+=j)
		{
			xtemp = (int) (xcoord[(count+degrees) %180 ] * 150+srcx);
			ytemp = (int) (ycoord[(count+degrees) %180] * 150+srcy);
			
			/* draw lines originating at srcx,srcy */

			paint.setColor(Color.WHITE);
			canvas.drawLine(srcx,srcy,xtemp,ytemp,paint);
			
			
			

			/* draw boxes originating at the end of the lines */
			paint.setColor(Color.GREEN);
			canvas.drawRect(xtemp-10,ytemp-10,xtemp+10,ytemp+10,paint);
			
			// canvas.fillRect(xtemp-10,ytemp-10,20,20,paint);
//				canvas.setColor(255,255,255);
//				canvas.drawRect(xtemp-10,ytemp-10,20,20);

			
			/* draw circles that intersect with the lines */
//				canvas.setColor(255,0,00);
//				int xpos  = (int) (xcoord[(count+circle) %180 ]*20+srcx);
//				int ypos = (int) (ycoord[(count+circle) %180]*20+srcy);
//				canvas.fillArc(xpos-radius,ypos-radius,radius*2,radius*2,0,360);
		}
	}

	public void drawTriangle(Canvas canvas, int inner, int size, double animationStep) {
		int count;
		/* draw triangle centered at srcx, srcy */
//		int x1 = (int) xcoord[inner]*10+srcx;
//		int y1 = (int)ycoord[inner]*10+srcy;
		
		float x1=(int)(xcoord[(int) ((inner +animationStep) %180)]*size+srcx);
		float y1=(int)(ycoord[(int) ((inner +animationStep) %180)]*size+srcy);
		
		inner+=60;
		
		Path path = new Path();
		path.moveTo(x1, y1);
		/* draw triangle line by line */
		for(count=0;count<7;count++)
		{
			float x2=(int)(xcoord[(int) ((inner +animationStep) %180)]*size+srcx);
			float y2=(int)(ycoord[(int) ((inner +animationStep) %180)]*size+srcy);
			
			path.lineTo(x2, y2);
			
			inner +=60;
			
			x1=x2;
			y1=y2;
		}
		paint.setColor(Color.RED);
		paint.setStyle(Style.FILL);
		canvas.drawPath(path, paint);
		
		paint.setStyle(Style.STROKE);
		paint.setColor(Color.WHITE);
		canvas.drawPath(path, paint);
		
		paint.setStyle(Style.FILL);
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	public void surfaceCreated(SurfaceHolder holder) {
		Hex=new Thread(this,"Animation Loop");
		running = true;
		Hex.start();
		
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}

	boolean mouseDown = false;
	
	float startX;
	float startY;

	float finalX;
	float finalY;

	
	public boolean onTouch(View v, MotionEvent event)
	{
		int count = event.getPointerCount();
		if(event.getActionMasked() == MotionEvent.ACTION_DOWN)
		{
			int idx = event.getActionIndex();
			
			startX = event.getX(idx);
			startY = event.getY(idx);
			mouseDown = true;
		}
		
		if(event.getActionMasked() == MotionEvent.ACTION_MOVE)
		{
			if(event.getPointerCount() == 2)
			{
			
			startX = event.getX(0);
			startY = event.getY(0);
			
			finalX = event.getX(1);
			finalY = event.getY(1);
			
//			float slope = (finalY - startY) / (finalX - startX);
			
			tantheta = Math.toDegrees(Math.atan2((finalY - startY),(finalX - startX)));
			
			
//			Log.e("tantheta",""+(tantheta));
			}
//			Log.e("startX",""+startX);
//			Log.e("startY",""+startY);		
//
//			Log.e("finalX",""+finalX);
//			Log.e("finalY",""+finalY);	

			mouseDown = true;
		}

		if(event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN)
		{
			int idx = event.getActionIndex();
			
//			finalX = event.getX(idx);
//			finalY = event.getY(idx);
			
//			Log.e("startX",""+startX);
//			Log.e("startY",""+startY);		
//
//			Log.e("finalX",""+finalX);
//			Log.e("finalY",""+finalY);	
			float slope = (finalY - startY) / (finalX - startX);
			tantheta = Math.toDegrees(Math.atan2((finalY - startY),(finalX - startX)));
			
			
//			Log.e("tantheta",""+(tantheta));
			
			mouseDown = true;
		}
		
		
		
		return true;
	}
}
