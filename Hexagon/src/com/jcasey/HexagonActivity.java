package com.jcasey;

import android.app.Activity;
import android.os.Bundle;

public class HexagonActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new Hexagon(this.getApplicationContext()));
    }
}