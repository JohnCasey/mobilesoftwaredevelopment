package com.jcasey;


import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;


public class Hexagon extends SurfaceView implements Runnable, SurfaceHolder.Callback, OnTouchListener
{
	private Thread Hex;
	private int srcx=64,srcy=64;
	double tantheta;
	
	/* lookup tables of trigonometric ratios */
	
	float xcoord[]=new float[180];
	float ycoord[]=new float[180];
	
	Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
	
	int width;
	int height;
	
	boolean running = false;
	float PI = (float) Math.PI;
	
	public Hexagon(Context context)
	{		
		super(context);
		
		setOnTouchListener(this);		
		paint.setColor(Color.RED);

		float radians = PI / 90;
		
		/* calculate trigonometric ratios and store in lookup tables */
		for(int i=0;i<180;i++)
		{
			xcoord[i] = (FloatMath.sin(i*radians));
			ycoord[i] = (FloatMath.cos(i*radians));
		}
		
		getHolder().addCallback(this);
	}

	Random random = new Random();
	
	Bitmap offscreenBuffer;
	Canvas offscreenCanvas;
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
		offscreenBuffer = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		offscreenCanvas = new Canvas(offscreenBuffer);
		
		paint.setAntiAlias(true);
		paint.setFilterBitmap(true);
		
		this.width = w;
		this.height = h;
		
		srcx = width / 2;
		srcy = height / 2;
	}

	public  void run()
	{
		while(running)
		{
			paint.setColor(Color.BLACK);
			offscreenCanvas.drawColor(Color.BLACK);
			try
			{
				Thread.sleep(1000/60);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			int inner=30;
			
			paint.setColor(Color.WHITE);			
			
			if(tantheta < 0)
			{
				Log.e("test",Double.toString(Math.abs(tantheta)));
				drawTriangle(offscreenCanvas, inner, 150, Math.abs(tantheta));
			}
			else
			{
				Log.e("test",Double.toString((360-tantheta)));
				drawTriangle(offscreenCanvas, inner, 150, 360 - tantheta);
			}
			
			offscreenCanvas.drawLine(startX, startY, finalX, finalY, paint);
			offscreenCanvas.drawLine(startX, startY, width, startY, paint);
					
			Canvas canvas = null;
			
			SurfaceHolder holder = getHolder();
			synchronized(holder)
			{
				canvas = holder.lockCanvas(null);
				canvas.drawColor(Color.BLACK);
				canvas.drawBitmap(offscreenBuffer, 0, 0, null);
			
				holder.unlockCanvasAndPost(canvas);
			}
		}
	}

	public void drawTriangle(Canvas canvas, int inner, int size, double animationStep)
	 {
		int count;
		
		float x1=(int)(xcoord[(int) ((inner +animationStep) %180)]*size+srcx);
		float y1=(int)(ycoord[(int) ((inner +animationStep) %180)]*size+srcy);
		
		inner+=60;
		
		Path path = new Path();
		path.moveTo(x1, y1);
		
		for(count=0;count<7;count++)
		{
			float x2=(int)(xcoord[(int) ((inner +animationStep) %180)]*size+srcx);
			float y2=(int)(ycoord[(int) ((inner +animationStep) %180)]*size+srcy);
			
			path.lineTo(x2, y2);
			
			inner +=60;
			
			x1=x2;
			y1=y2;
		}
		paint.setColor(Color.RED);
		paint.setStyle(Style.FILL);
		canvas.drawPath(path, paint);
		
		paint.setStyle(Style.STROKE);
		paint.setColor(Color.WHITE);
		canvas.drawPath(path, paint);
		
		paint.setStyle(Style.FILL);
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		
	}

	public void surfaceCreated(SurfaceHolder holder) {
		Hex=new Thread(this,"Animation Loop");
		running = true;
		Hex.start();
		
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		
	}

	float startX;
	float startY;

	float finalX;
	float finalY;

	public boolean onTouch(View v, MotionEvent event)
	{
		if(event.getActionMasked() == MotionEvent.ACTION_MOVE)
		{
			if(event.getPointerCount() == 2)
			{
				startX = event.getX(0);
				startY = event.getY(0);
				
				finalX = event.getX(1);
				finalY = event.getY(1);
				
				float opposite = finalY - startY;
				float adjacent = finalX - startX;
				tantheta = Math.toDegrees(Math.atan2(opposite,adjacent));
			}
			else
			{
				startX = event.getX(0);
				startY = event.getY(0);
				
				float opposite = srcy - startY;
				float adjacent = srcx - startX;
				
				tantheta = Math.toDegrees(Math.atan2(opposite,adjacent));
			}

		}

		return true;
	}
}
