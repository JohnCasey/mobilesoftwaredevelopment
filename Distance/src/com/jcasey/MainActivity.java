package com.jcasey;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class MainActivity extends Activity {
	
	/**
	 * @author John Casey (john.casey@gmail.com)
	 * 
	 * 19/05/2014
	 * 
	 * Skeleton code for W2 activity
	 */
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// setup DrawView as our Android Activities main View using activity_main.xml
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
