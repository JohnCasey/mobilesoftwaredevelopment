package com.jcasey;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

public class DrawView extends View implements OnTouchListener
{
	/**
	 * @author John Casey (john.casey@gmail.com)
	 * 
	 * 19/05/2014
	 * 
	 * Skeleton code for W2 activity
	 */
	
	public static final int RADIUS = 5;
	
    float[] x = {50,100};
    float[] y = {50,60};

    int point = 0;
    
	int width;
	int height;

	TextView distance= null;
	TextView angle=null;
	
	public DrawView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

	}

	public DrawView(Context context, AttributeSet attrs) {
		super(context, attrs);

	}

	public DrawView(Context context)
	{
		super(context);

	}
	
	public void setup()
	{
		this.setOnTouchListener(this);
		
		if(this.getContext() instanceof Activity)
		{
			Activity activity = (Activity) this.getContext();
			
			angle = (TextView)activity.findViewById(R.id.angle);
			distance = (TextView)activity.findViewById(R.id.distance);
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas)
	{
		Paint paint = new Paint();
		paint.setColor(Color.RED);
		
		for(int i = 0; i<x.length; i++)
		{
			canvas.drawCircle(x[i], y[i], RADIUS, paint);
		}
		
		// draw a line between the two points
		canvas.drawLine(x[0], y[0], x[1], y[1], paint);

		float rise = y[1] - y[0];
		float run = x[1] - x[0];
		
		//TODO write code to calculate the distance between two points using the rise and run above





		//TODO write code to calculate the angle between the opposite (rise) and adjacent (run) sides
		if(distance !=null)
		{
			//TODO set the distance TextView widget
			distance.setText("Set this dynamically.");
		}
		
		if(angle !=null)
		{
			//TODO set the angle TextView widget
			angle.setText("Set this dynamically.");
		}
	}
	
	// use the onSizeChanged method to keep track of the width and height of the screen
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		width = w;
		height = h;
		
		setup();

		super.onSizeChanged(w, h, oldw, oldh);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {

		//TODO develop code to respond to MotionEvent.ACTION_DOWN, MotionEvent.ACTION_UP and MotionEvent.ACTION_MOVE events
		
		//TODO store the x,y co-ordinates of both points into the pre-defined x[],y[] arrays
		return true;
	}
}