package at.fhooe.mgames.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import android.content.Context;
import android.util.Log;

public final class ModelLoader
{

	/**
	 * This method is a representation of the original SetupWorld(). Here, we
	 * load the world into the structure from the original world.txt file, but
	 * in a Java way.
	 * 
	 * Please note that this is not necessarily the way to got, and definitely
	 * not the nicest way to do the reading and loading from a file. But it is
	 * near to the original and a quick solution for this example. It does not
	 * check for anything NOT part of the original file and can easily be
	 * corrupted by a changed file.
	 * 
	 * @param fileName
	 *            The file name to load from the Asset directory
	 */
	public static List<FloatBuffer> loadWorld(Context context, String fileName)
	{
		Sector sector = new Sector();

		try
		{
			// Some temporary variables
			int numtriangles = 0;
			int counter = 0;

			List<String> lines = null;
			StringTokenizer tokenizer;

			// Quick Reader for the input file
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					context.getAssets().open(fileName)));

			// Iterate over all lines
			String line = null;
			while ((line = reader.readLine()) != null)
			{
				// Skip comments and empty lines
				if (line.startsWith("//") || line.trim().equals(""))
				{
					continue;
				}

				// Read how many polygons this file contains
				if (line.startsWith("NUMPOLLIES"))
				{
					numtriangles = Integer.valueOf(line.split(" ")[1]);
					sector.numtriangles = numtriangles;
					sector.triangle = new Triangle[sector.numtriangles];

					// Read every other line
				}
				else
				{
					if (lines == null)
					{
						lines = new ArrayList<String>();
					}

					lines.add(line);
				}
			}

			// Clean up!
			reader.close();

			// Now iterate over all read lines...
			for (int loop = 0; loop < numtriangles; loop++)
			{
				// ...define triangles...
				Triangle triangle = new Triangle();

				// ...and fill these triangles with the five read data
				for (int vert = 0; vert < 3; vert++)
				{
					//
					line = lines.get(loop * 3 + vert);
					tokenizer = new StringTokenizer(line);

					//
					triangle.vertex[vert] = new Vertex();
					//
					triangle.vertex[vert].x = Float.valueOf(tokenizer
							.nextToken());
					triangle.vertex[vert].y = Float.valueOf(tokenizer
							.nextToken());
					triangle.vertex[vert].z = Float.valueOf(tokenizer
							.nextToken());
					triangle.vertex[vert].u = Float.valueOf(tokenizer
							.nextToken());
					triangle.vertex[vert].v = Float.valueOf(tokenizer
							.nextToken());
					triangle.vertex[vert].nx = Float.valueOf(tokenizer
							.nextToken());
					triangle.vertex[vert].ny = Float.valueOf(tokenizer
							.nextToken());
					triangle.vertex[vert].nz = Float.valueOf(tokenizer
							.nextToken());
				}

				// Finally, add the triangle to the sector
				sector.triangle[counter++] = triangle;
			}

			// If anything should happen, write a log and return
		}
		catch (Exception e)
		{
			Log.e("World", "Could not load the World file!", e);
			return null;
		}

		/*
		 * Now, convert the original structure of the NeHe lesson to our classic
		 * buffer structure. Could/Should be done in one step above. Kept
		 * separated to stick near and clear to the original.
		 * 
		 * This is a quick and not recommended solution. Just made to quickly
		 * present the tutorial.
		 */
		float[] vertices = new float[sector.numtriangles * 3 * 3];
		float[] texCoords = new float[sector.numtriangles * 3 * 2];
		float[] normals = new float[sector.numtriangles * 3 * 3];

		int vertCounter = 0;
		int texCounter = 0;
		int normCounter = 0;

		//
		for (Triangle triangle : sector.triangle)
		{
			//
			for (Vertex vertex : triangle.vertex)
			{
				//
				vertices[vertCounter++] = vertex.x;
				vertices[vertCounter++] = vertex.y;
				vertices[vertCounter++] = vertex.z;
				//
				texCoords[texCounter++] = vertex.u;
				texCoords[texCounter++] = vertex.v;

				normals[normCounter++] = vertex.nx;
				normals[normCounter++] = vertex.ny;
				normals[normCounter++] = vertex.nz;
			}
		}

		// Build the buffers
		List<FloatBuffer> result = new ArrayList<FloatBuffer>();

		FloatBuffer vertexBuffer = Utils.wrapDirect(vertices);
		FloatBuffer texCoordBuffer = Utils.wrapDirect(texCoords);
		FloatBuffer normalsBuffer = Utils.wrapDirect(normals);

		result.add(vertexBuffer);
		result.add(texCoordBuffer);
		result.add(normalsBuffer);

		return result;
	}

}
