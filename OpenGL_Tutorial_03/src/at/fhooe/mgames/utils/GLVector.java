package at.fhooe.mgames.utils;

public class GLVector {
	public float data[];

	private void allocate() {
		data = new float[4];
	}

	public GLVector() {
		allocate();
	}

	public GLVector(float x, float y, float z, float w) {
		data = new float[] { x, y, z, w };
	}

	public void normalize() {
		for (int i = 0; i < data.length; i++) {
			data[i] /= getLength();
		}
	}

	public double getLength() {
		return Math.sqrt(X() * X() + Y() * Y() + Z() * Z() + W() * W());
	}

	public float X() {
		return data[0];
	}

	public float Y() {
		return data[1];
	}

	public float Z() {
		return data[2];
	}

	public float W() {
		return data[3];
	}
}
