package at.fhooe.mgames.utils;

/**
 * A classic Vertex definition with texture coordinates.
 */
public class Vertex {
	public float x, y, z;
	public float u, v;
	public float nx, ny, nz;
}
