package at.fhooe.mgames.utils;

/**
 * The Sector class holding the number and all Triangles.
 */
public class Sector {
	public int numtriangles;
	public Triangle[] triangle;
}
