package at.fhooe.mgames.utils;

import android.opengl.Matrix;

public class GLMatrix {

	public float data[];

	private void allocate() {
		data = new float[16];
	}

	public GLMatrix() {
		allocate();
		identity();
	}

	public void identity() {
		Matrix.setIdentityM(data, 0);
	}

	public void assign(GLMatrix matrix) {
		data = matrix.data;
	}

	public void multiply(GLMatrix matrix) {
		Matrix.multiplyMM(data, 0, data, 0, matrix.data, 0);
	}

	public void multiply(GLVector vector) {
		Matrix.multiplyMV(vector.data, 0, data, 0, vector.data, 0);
	}

	public void premultiply(GLMatrix matrix) {
		Matrix.multiplyMM(data, 0, matrix.data, 0, data, 0);
	}

	public void translate(float dx, float dy, float dz) {
		Matrix.translateM(data, 0, dx, dy, dz);
	}

	public void rotate(float angle, float x, float y, float z) {

		Matrix.rotateM(data, 0, angle, x, y, z);
	}

	public void transpose() {
		Matrix.transposeM(data, 0, data, 0);

	}
}
