package at.fhooe.mgames.tutorial03;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import at.fhooe.mgames.entities.Cube;
import at.fhooe.mgames.entities.Light;
import at.fhooe.mgames.entities.Plateau;
import at.fhooe.mgames.entities.PlateauFloor;
import at.fhooe.mgames.entities.Skybox;

/**
 * This class is our World representation and loads the world from the textual
 * representation, and draws the World according to the read.
 * 
 * @author Florian Lettner
 */
public class Scene
{
	/** The camera direction. */
	public float mHeading;

	/** The camera orientation. */
	public float mLookupdown;

	/** A precalculated value to convert rad to degrees and vice versa (faster). */
	private static final float PI_OVER_180 = 0.0174532925f;

	/** The floor entity. */
	private PlateauFloor mFloor;

	/** The plateau entity. */
	private Plateau mPlateau;

	/** The skybox entity. */
	private Skybox mSkyBox;

	/** The cube entity. */
	private Cube mCube;

	/** The light entity. */
	private Light mLight;

	/** The position of the player. */
	private float mPosX, mPosZ;

	/** The player bouncing. */
	private float mWalkBias, mWalkBiasAngle;

	/** The player controls. */
	private boolean mForward, mBackward, mLeft, mRight;

	/**
	 * The World constructor
	 * 
	 * @param context
	 *            The application context to load files.
	 * @param objectPath
	 *            The filename of the plateau object file.
	 * @param floorPath
	 *            The filename of the floor object file.
	 */
	public Scene(Context context, String objectPath, String floorPath)
	{

		mSkyBox = new Skybox();
		mPlateau = new Plateau(context, objectPath);
		mFloor = new PlateauFloor(context, floorPath);
		mCube = new Cube();
		mLight = new Light();

		mPosZ = 4;
	}

	/**
	 * Updates the entire scene.
	 * 
	 * <p>
	 * This method is used to update the camera position based on the user
	 * input.
	 * </p>
	 * 
	 * @param dt
	 *            The time difference between the last and the current update.
	 */
	public void update(long dt)
	{
		mLight.update(dt);
		mCube.update(dt);

		float speed = 0.1f / dt;

		if (mForward)
		{
			/*
			 * STEP 25a: Calculate forward movement (x_new, z_new) = (x_old,
			 * z_old) - (sin(heading in rad) * speed, cos(heading in rad) *
			 * speed). Keep in mind that Math.toRadians is expensive. Use
			 * pre-calculated piover180 instead.
			 */
			mPosX -= Math.sin(mHeading * PI_OVER_180) * speed;
			mPosZ -= Math.cos(mHeading * PI_OVER_180) * speed;

		}
		else if (mBackward)
		{
			/*
			 * STEP 25b: Calculate backwards movement (x_new, z_new) = (x_old,
			 * z_old) + (sin(heading in rad) * speed, cos(heading in rad) *
			 * speed). Keep in mind that Math.toRadians is expensive. Use
			 * pre-calculated piover180 instead.
			 */
			mPosX += Math.sin(mHeading * PI_OVER_180) * speed;
			mPosZ += Math.cos(mHeading * PI_OVER_180) * speed;
		}

		if (mLeft)
		{
			/*
			 * STEP 26a: Calculate side movement (x_new, z_new) =
			 * (x_old-cos(heading in rad) * speed, z_old+sin(heading in rad) *
			 * speed). Keep in mind that Math.toRadians is expensive. Use
			 * pre-calculated piover180 instead.
			 */
			mPosX -= Math.cos(mHeading * PI_OVER_180) * speed;
			mPosZ += Math.sin(mHeading * PI_OVER_180) * speed;
		}
		else if (mRight)
		{
			/*
			 * STEP 26b: Calculate side movement (x_new, z_new) =
			 * (x_old+cos(heading in rad) * speed, z_old+-sin(heading in rad) *
			 * speed). Keep in mind that Math.toRadians is expensive. Use
			 * pre-calculated piover180 instead.
			 */
			mPosX += Math.cos(mHeading * PI_OVER_180) * speed;
			mPosZ -= Math.sin(mHeading * PI_OVER_180) * speed;
		}

		// apply walk bias to cause player to bounce (footsteps)
		if (mForward || mBackward || mRight || mLeft)
		{
			if (mWalkBiasAngle <= 1.0f)
			{
				mWalkBiasAngle = 359.0f;
			}
			else
			{
				mWalkBiasAngle -= 10;
			}
			mWalkBias = (float) Math.sin(mWalkBiasAngle * PI_OVER_180) / 50.0f;
		}
	}

	/**
	 * The model drawing function.
	 * 
	 * @param gl
	 *            The OpenGl render context
	 */
	public void draw(GL10 gl)
	{
		// Used for player translation
		float xtrans = -mPosX;
		// Used for player translation
		float ztrans = -mPosZ;
		// Used for bouncing motion
		float ytrans = -mWalkBias - 0.25f;
		// 360 degree angle for player direction
		float sceneroty = 360.0f - mHeading;

		/*
		 * STEP 11a: Rotate the scene to the camera head up/down orientation
		 * angle.
		 */
		gl.glRotatef(mLookupdown, 1.0f, 0.0f, 0.0f);

		/*
		 * STEP 11b: Rotate the scene to the camera head left/right orientation
		 * angle
		 */
		gl.glRotatef(sceneroty, 0.0f, 1.0f, 0.0f);

		/*
		 * STEP 22: Draw skybox and execute application!
		 */
		mSkyBox.draw(gl);

		/*
		 * STEP 11c: Translate the scene based on the player position.
		 */
		gl.glTranslatef(xtrans, ytrans, ztrans);

		/*
		 * STEP 31: Set glColorMask and glDepthMask to false. Enable the stencil
		 * test.
		 */
		gl.glColorMask(false, false, false, false);
		gl.glDepthMask(false);
		gl.glEnable(GL10.GL_STENCIL_TEST);

		/*
		 * STEP 32a: Set the stencil function to always pass. Use 1 as reference
		 * value. Set bitmask to ~0.
		 */
		gl.glStencilFunc(GL10.GL_ALWAYS, 1, ~0);

		/*
		 * STEP 32b: Set the stencil operation to GL_REPLACE to replace the
		 * current value in the stencil buffer. As the stencil test always
		 * passes the value for stencil fail does not matter.
		 */
		gl.glStencilOp(GL10.GL_REPLACE, 0, GL10.GL_REPLACE);

		/*
		 * STEP 13b: Draw the floor and execute the application!
		 */
		mFloor.draw(gl);

		/*
		 * STEP 33: Set glColorMask and glDepthMask to true and draw the floor a
		 * second time. As the stencil function has been inserted, the first
		 * call causes the floor to be drawn into the stencil buffer but not on
		 * the display anymore.
		 */
		gl.glColorMask(true, true, true, true);
		gl.glDepthMask(true);

		mFloor.draw(gl);

		/*
		 * STEP 34a: Change the stencil function to be true when the value in
		 * the stencil buffer equals 1 (GL_EQUAL 1). Set the bitmask to ~0
		 * again.
		 */
		gl.glStencilFunc(GL10.GL_EQUAL, 1, ~0);

		/*
		 * STEP 34b: Change the stencil function to always keep (GL_KEEP) the
		 * stencil buffer value in order to get overlaying shadows. If you want
		 * to have physically correct shadows use GL_INCR for the z-pass value.
		 * However, you won't see the nice cube shadow anymore as the ceiling is
		 * taken into account.
		 */
		gl.glStencilOp(GL10.GL_KEEP, GL10.GL_INCR, GL10.GL_INCR);

		gl.glPushMatrix();
		/*
		 * STEP 35: Set the glColor to black and 30% alpha. This is going to be
		 * the shadow color.
		 */
		gl.glColor4f(0.0f, 0.0f, 0.0f, 0.3f);

		/*
		 * STEP 36a: Disable texturing, lighting and depth testing.
		 */
		gl.glDisable(GL10.GL_TEXTURE);
		gl.glDisable(GL10.GL_LIGHTING);
		gl.glDisable(GL10.GL_DEPTH_TEST);

		/*
		 * STEP 36b: Disable fog if it is enabled. The current state of fog can
		 * be found in SceneState.
		 */
		if (SceneState.getInstance().isFogEnabled())
		{
			gl.glDisable(GL10.GL_FOG);
		}

		/*
		 * STEP 37: Translate 0.05f on the y-axis. This is going to be a shadow
		 * offset to avoid z-fighting.
		 */
		gl.glTranslatef(0.0f, 0.05f, 0.0f);

		/*
		 * STEP 38: Multiply the shadow matrix with the current view matrix
		 * (glMultMatrixf). The shadow matrix can be calculated using
		 * getShadowMatrix, the position stored in mLight and the normals in
		 * PlateauFloor.GROUND_NORMALS.
		 */
		float[] shadowMatrix = getShadowMatrix(mLight.getLightPosition(),
				PlateauFloor.GROUND_NORMALS);
		gl.glMultMatrixf(shadowMatrix, 0);

		/*
		 * STEP 39a: Draw the plateau. The previously set stencil function
		 * ensures that the shadow is only drawn where the stencil buffer equals
		 * 1.
		 */
		mPlateau.draw(gl);

		/*
		 * STEP 39b: Draw the cube. The previously set stencil function ensures
		 * that the shadow is only drawn where the stencil buffer equals 1.
		 */
		mCube.draw(gl);

		/*
		 * STEP 40a: Enable texturing, depth testing and lighting again.
		 */
		gl.glEnable(GL10.GL_TEXTURE);
		gl.glEnable(GL10.GL_LIGHTING);
		gl.glEnable(GL10.GL_DEPTH_TEST);

		/*
		 * STEP 40b: Enable fog if it is enabled. The current state of fog can
		 * be found in SceneState.
		 */
		if (SceneState.getInstance().isFogEnabled())
		{
			gl.glEnable(GL10.GL_FOG);
		}

		/* STEP 41: Change glColor to white and 100% alpha. */
		gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

		gl.glPopMatrix();

		/* STEP 42: Disable the stencil test. */
		gl.glDisable(GL10.GL_STENCIL_TEST);

		/*
		 * STEP 23: Draw the cube and execute application!
		 */
		mCube.draw(gl);

		/*
		 * STEP 14b: Draw the plateau and execute the application!
		 */
		mPlateau.draw(gl);

		/*
		 * STEP 17: Draw the light and execute the application!
		 */
		mLight.draw(gl);
	}

	/**
	 * Loads the textures for all entities.
	 * 
	 * <p>
	 * Sets up fog, light and loads the textures for all entities.
	 * </p>
	 * 
	 * @param gl
	 *            The OpenGL render context.
	 * @param context
	 *            The application context to load the textures.
	 */
	public void loadTextures(GL10 gl, Context context)
	{

		/*
		 * STEP 30a: Setup the fog.
		 */
		mLight.setupFog(gl);

		/*
		 * STEP 15c: Setup the light
		 */
		mLight.setupLight(gl);

		/*
		 * STEP 12: Load the textures for the skybox, the floor, the plateau and
		 * the cube.
		 */
		mSkyBox.loadTextures(gl, context);
		mFloor.loadGLTexture(gl, context);
		mPlateau.loadGLTexture(gl, context);
		mCube.loadTextures(gl, context);
	}

	/**
	 * Defines the currently active controls.
	 * 
	 * @param forward
	 *            <code>true</code> if player moves forward; <code>false</code>
	 *            otherwise.
	 * @param backward
	 *            <code>true</code> if player backward forward;
	 *            <code>false</code> otherwise.
	 * @param left
	 *            <code>true</code> if player moves left; <code>false</code>
	 *            otherwise.
	 * @param right
	 *            <code>true</code> if player moves right; <code>false</code>
	 *            otherwise.
	 */
	public void setControls(boolean forward, boolean backward, boolean left,
			boolean right)
	{
		mForward = forward;
		mBackward = backward;
		mLeft = left;
		mRight = right;
	}

	/**
	 * Computes the shadow matrix for planar shadows.
	 * 
	 * @param light
	 *            The light source that causes the projection.
	 * @param ground
	 *            The normals from the floor.
	 * 
	 * @return A 4x4 matrix containing the projection information from the
	 *         position of the light source.
	 */
	private float[] getShadowMatrix(float[] light, float[] ground)
	{
		float dot = ground[0] * light[0] + ground[1] * light[1] + ground[2]
				* light[2] + ground[3] * light[3];

		return new float[] { dot - light[0] * ground[0], -light[1] * ground[0],
				-light[2] * ground[0], -light[3] * ground[0],
				-light[0] * ground[1], dot - light[1] * ground[1],
				-light[2] * ground[1], -light[3] * ground[1],
				-light[0] * ground[2], -light[1] * ground[2],
				dot - light[2] * ground[2], -light[3] * ground[2],
				-light[0] * ground[3], -light[1] * ground[3],
				-light[2] * ground[3], dot - light[3] * ground[3] };
	}
}
