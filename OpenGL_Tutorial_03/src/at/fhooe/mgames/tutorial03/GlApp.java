package at.fhooe.mgames.tutorial03;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

/**
 * OpenGL Tutorial 03.
 * 
 * <p>
 * This tutorial is an implemenation of a tiny game engine consisting of a
 * skybox, first person controls, shadows, lighting and fog as well as moving
 * objects.
 * </p>
 * 
 * @author Florian Lettner
 */
public class GlApp extends Activity {

	/** The drawing area. */
	private GameSurfaceView mDrawingSurface;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		/*
		 * STEP 01: Create a new GLSurfaceView, which will be the OpenGL
		 * drawing area
		 */
		mDrawingSurface = new GameSurfaceView(getApplicationContext());

		SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
		Sensor s = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

		sm.registerListener(mDrawingSurface, s, SensorManager.SENSOR_DELAY_UI);

		/*
		 * STEP 02: Set the GLSurfaceView as the activity's content view
		 */
		setContentView(mDrawingSurface);
	}

	@Override
	protected void onPause() {
		super.onPause();

		/*
		 * STEP 03: Pause the drawing view to stop render cycle.
		 */
		mDrawingSurface.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();

		/*
		 * STEP 04: Resume the drawing view to continue render cycle.
		 */
		mDrawingSurface.onResume();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		/*
		 * STEP 05: Destroy the drawing view to stop render cycle.
		 */
		mDrawingSurface.onDestroy();
	}

	/**
	 * Creates the options.
	 * 
	 * <p>
	 * Adds menu items to toggle scene lighting, blending and switching texture
	 * filter modes.
	 * </p>
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 1, 0, "Toggle Fog");
		menu.add(0, 2, 0, "Toggle Flashlight");
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * Handles clicks on option menu items.
	 * 
	 * <p>
	 * Identifies the menu item and changes the scene state.
	 * </p>
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getTitle().toString().contains("Fog"))
			SceneState.getInstance().toggleFog();

		if (item.getTitle().toString().contains("Flashlight"))
			SceneState.getInstance().toggleFlashlight();

		return super.onOptionsItemSelected(item);
	}
}