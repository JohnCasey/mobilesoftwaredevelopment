package at.fhooe.mgames.tutorial03;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;

/**
 * The renderer that draws on the OpenGL surface.
 * 
 * @author Florian Lettner
 * @see Renderer
 */
public class GLRenderer implements Renderer
{

	/** A collection of existing fog modes. */
	private static final int FOG_MODE[] = { GL10.GL_EXP, GL10.GL_EXP2,
			GL10.GL_LINEAR };

	/** The application context required to load resources. */
	private Context mContext;

	/** The model to be rendered. */
	private Scene mModel;

	/** The scene settings. */
	private SceneState mState;

	/**
	 * Instantiate a new instance of GLRenderer
	 * 
	 * @param context
	 *            The application context required to load textures;
	 */
	public GLRenderer(Context context)
	{
		this.mContext = context;
		mState = SceneState.getInstance();
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		setupSurface(gl);
	}

	@Override
	public void onDrawFrame(GL10 gl)
	{

		// Clear color buffer
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT
				| GL10.GL_STENCIL_BUFFER_BIT);

		// Load model view matrix
		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity();

		if (mState.isFlashlightEnabled())
		{
			gl.glEnable(GL10.GL_LIGHT0);
		}
		else
		{
			gl.glDisable(GL10.GL_LIGHT0);
		}

		if (mState.isFogEnabled())
		{
			gl.glEnable(GL10.GL_FOG);

			/*
			 * STEP 30b: Define the fog mode and start the application. Fog
			 * filters can be switched using double clicks.
			 */
			gl.glFogxv(GL10.GL_FOG_MODE, FOG_MODE, 0);
		}
		else
		{
			gl.glDisable(GL10.GL_FOG);
		}

		// Draw model
		if (mModel != null)
		{
			mModel.draw(gl);
		}
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height)
	{
		/*
		 * STEP 10: Load the model textures
		 */
		mModel.loadTextures(gl, mContext);

		// Avoid division by zero
		if (height == 0)
		{
			height = 1;
		}

		// Define the viewport (fullscreen in this case)
		gl.glViewport(0, 0, width, height);

		// Load projection matrix
		gl.glMatrixMode(GL10.GL_PROJECTION);

		// Load identity matrix and reset projection matrix
		gl.glLoadIdentity();

		// Setup the perspective (field of view 45� - human high)
		GLU.gluPerspective(gl, 45.0f, (float) width / (float) height, 0.1f,
				100.0f);
	}

	/**
	 * Sets the current model.
	 * 
	 * @param model
	 *            The model to be rendered.
	 */
	public void setModel(Scene model)
	{
		this.mModel = model;
	}

	/**
	 * Sets up the renderer.
	 * 
	 * <p>
	 * Sets up the depth function for depth testing and the blend function.
	 * Additionally, defines some rendering hints for light and shadow.
	 * </p>
	 * 
	 * @param gl
	 *            The OpenGL render context.
	 */
	private void setupSurface(GL10 gl)
	{

		// Enable texture mapping
		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glDisable(GL10.GL_DITHER);

		// Enable smooth shading
		gl.glShadeModel(GL10.GL_SMOOTH);

		// Set background color to black, 50% transparency
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);

		// Reset depth buffer
		gl.glClearDepthf(1);
		gl.glClearStencil(0);
		gl.glDepthFunc(GL10.GL_LEQUAL);

		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);

		// Improving the rendering quality by setting a perspective correction
		// hint
		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
		gl.glHint(GL11.GL_POINT_SMOOTH_HINT, GL11.GL_NICEST);
		gl.glPointSize(5);

		gl.glEnable(GL11.GL_BLEND);
		gl.glEnable(GL11.GL_COLOR_MATERIAL);
		gl.glEnable(GL11.GL_DEPTH_TEST);
		gl.glFrontFace(GL11.GL_CW);
	}
}
