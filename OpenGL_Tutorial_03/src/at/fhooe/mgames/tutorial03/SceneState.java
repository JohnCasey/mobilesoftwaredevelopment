package at.fhooe.mgames.tutorial03;

public class SceneState
{

	private static SceneState mInstance;

	private boolean isFogEnabled = true;
	private boolean isFlashlightEnabled = false;

	private int filter;

	public static SceneState getInstance()
	{
		if (mInstance == null)
		{
			mInstance = new SceneState();
		}

		return mInstance;
	}

	private SceneState()
	{

	}

	public void switchFIlter()
	{
		filter = (filter + 1) % 3;
	}

	public void toggleFog()
	{
		isFogEnabled = !isFogEnabled;
	}

	public void toggleFlashlight()
	{
		isFlashlightEnabled = !isFlashlightEnabled;
	}

	public boolean isFogEnabled()
	{
		return isFogEnabled;
	}

	public boolean isFlashlightEnabled()
	{
		return isFlashlightEnabled;
	}

	public int getFilter()
	{
		return filter;
	}
}
