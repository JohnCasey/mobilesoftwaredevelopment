package at.fhooe.mgames.tutorial03;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Implementation of an OpenGL ES drawing area.
 * 
 * <p>
 * The drawing surface can be set as content views by activities and can handle
 * touch gestures as well as touch dragging.
 * </p>
 * 
 * @author Florian Lettner
 * @see GLSurfaceView
 */
public class GameSurfaceView extends GLSurfaceView implements
		SensorEventListener
{

	/** Filtering factor for lowpass. */
	private final float FILTERING_FACTOR = .3f;

	/** Touch scaling factor. */
	private final float TOUCH_SCALE = 0.2f;

	/** The game loop to update and render the model. */
	private GameLoop mGameLoop;

	/** The model to be drawn. */
	private Scene mModel;

	/** The renderer used for drawing OpenGL content. */
	private GLRenderer mRenderer;

	/** The time measurement from the previous update. */
	private long mLastMillis;

	/** A gesture detector to detect touch gestures. */
	private GestureDetector mGestureDetector;

	/** A boolean that indicates to quit the game. */
	private volatile boolean mQuit;

	/** Previously recorded x- and y-touch position. */
	private float mPrevX, mPrevY;

	/** Lowpass filtered accelerations. */
	private float mAccelX, mAccelY;

	/**
	 * Instantiates this instance of GameSurfaceView.
	 * 
	 * @param context
	 *            The application context.
	 */
	public GameSurfaceView(Context context)
	{
		super(context);

		/*
		 * By default, the stencil buffer size is set to zero on Android
		 * devices, so define it manually. Our stencil buffer is 8 bit large.
		 * 
		 * NOTE: Android Emulator does not have a stencil buffer, the last value
		 * has to be set to 0.
		 */
		setEGLConfigChooser(8, 8, 8, 8, 16, 8);

		/*
		 * STEP 06: Create a new renderer and assign it to the surface view
		 */
		mRenderer = new GLRenderer(context);
		setRenderer(mRenderer);

		// Disable active rendering so we can use our own game loop
		setRenderMode(RENDERMODE_WHEN_DIRTY);

		// Create a gesture detector to catch touch gestures.
		mGestureDetector = new GestureDetector(context,
				new GameSurfaceGestureListener());

		// Create our model with 50 stars
		mModel = new Scene(context, "world.txt", "floor.txt");

		// Create and start our game loop
		mGameLoop = new GameLoop();
		mGameLoop.start();
	}

	/**
	 * Stops the game loop.
	 */
	public void onDestroy()
	{
		mQuit = true;
	}

	/**
	 * Handle touch events.
	 * 
	 * <p>
	 * Reacts to screen touches and drags. A touch resets the screen to 0.0f and
	 * sets the initial touch points. Dragging on the surface calculates the
	 * distance vector from the start point and assigns the distance to the
	 * scene point.
	 * </p>
	 * <p>
	 * <strong>Touching and dragging is not a touch gesture.</strong>
	 * </p>
	 * 
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{

		// Check if touch is a gesture
		if (mGestureDetector.onTouchEvent(event))
		{
			return true;
		}

		float x = event.getX();
		float y = event.getY();

		// If a touch is moved on the screen
		if (event.getAction() == MotionEvent.ACTION_MOVE)
		{
			// Calculate the change
			float dx = mPrevX - x;
			float dy = y - mPrevY;

			// Up and down looking through touch
			mModel.mLookupdown += dy * TOUCH_SCALE;
			// Look left and right through moving on screen
			mModel.mHeading += dx * TOUCH_SCALE;
		}

		// Remember the values
		mPrevX = x;
		mPrevY = y;

		return true;
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1)
	{
	}

	@Override
	public void onSensorChanged(SensorEvent event)
	{

		// Do some lowpass filtering to get more accurate acceleration values
		mAccelX = (float) (-event.values[1] * FILTERING_FACTOR + mAccelX
				* (1.0 - FILTERING_FACTOR));
		mAccelY = (float) (event.values[0] * FILTERING_FACTOR + mAccelY
				* (1.0 - FILTERING_FACTOR));

		boolean forward, backward, left, right;
		forward = backward = left = right = false;

		/*
		 * STEP 24a: Check if acceleration is a left or right movement (x-axis).
		 */
		if (mAccelX < -1.0f)
		{
			right = true;
		}
		else if (mAccelX > 1.0f)
		{
			left = true;
		}

		/*
		 * STEP 24b: Check if acceleration is a front or back movement (y-axis).
		 */
		if (mAccelY < 4.0f)
		{
			forward = true;
		}
		else if (mAccelY > 6.0f)
		{
			backward = true;
		}

		// Set the model input
		mModel.setControls(forward, backward, left, right);
	}

	/**
	 * Render the provided model.
	 * 
	 * @param model
	 *            The model to be rendered.
	 */
	private void render(Scene model)
	{
		mRenderer.setModel(model);
		requestRender();
	}

	/**
	 * A gesture listener to detect double taps.
	 * 
	 * @author Florian Lettner
	 * @see GestureDetector.SimpleOnGestureListener
	 */
	private class GameSurfaceGestureListener extends
			GestureDetector.SimpleOnGestureListener
	{

		@Override
		public boolean onDoubleTap(MotionEvent e)
		{

			SceneState.getInstance().switchFIlter();
			return super.onDoubleTap(e);
		}
	}

	/**
	 * The game loop.
	 * 
	 * <p>
	 * The game loop calculates the difference between the single updates and
	 * uses this time to update the model, which is afterwards rendered by the
	 * renderer.
	 * <p>
	 * 
	 * @author Florian Lettner
	 * @see Thread
	 */
	private class GameLoop extends Thread
	{

		@Override
		public void run()
		{
			while (!mQuit)
			{
				/*
				 * STEP 07: Get the system time in ms and calculate the time
				 * difference since the last update.
				 */
				long curMillis = System.currentTimeMillis();
				long difference = curMillis - mLastMillis;

				/*
				 * STEP 08a: Update the model
				 */

				mModel.update(difference);

				/*
				 * STEP 08b: Render the model
				 */
				render(mModel);

				/*
				 * STEP 09: Set the last time measured to the current time.
				 */
				mLastMillis = curMillis;

				try
				{
					sleep(10);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}

			Log.d("GAME LOOP", "Game loop terminated");
		}
	}
}
