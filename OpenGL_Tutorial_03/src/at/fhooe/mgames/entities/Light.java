package at.fhooe.mgames.entities;

import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import at.fhooe.mgames.utils.GLMatrix;
import at.fhooe.mgames.utils.GLVector;
import at.fhooe.mgames.utils.Utils;

/**
 * The scene light model.
 * 
 * <p>
 * Contains the scene lighting information for a moving point light model as
 * well as a spot light.
 * </p>
 * 
 * @author Florian Lettner
 */
public class Light
{
	/** The ambient light color. */
	private static final float[] AMBIENT_COLOR = { 0.2f, 0.2f, 0.2f, 1.0f };

	/** The directional light color. */
	private static final float[] DIRECTIONAL_COLOR = { 0.5f, 0.0f, 0.0f, 1.0f };

	/** The spot light color. */
	private static final float[] SPOTLIGHT_COLOR = { 0.4f, 0.4f, 0.05f, 1.0f };

	/** The spot light direction. */
	private static final float[] SPOT_LIGHT_DIR = { 0.0f, 0.0f, -1.0f, 1.0f };

	/** The fog color. */
	private static final float[] FOG_COLOR = { 0.1f, 0.1f, 0.1f, 1.0f };

	/** The buffers to wrap the float arrays. */
	private static final FloatBuffer AMBIENT_COLOR_BUFFER,
			DIRECTIONAL_COLOR_BUFFER, SPOTLIGHT_COLOR_BUFFER, SPOT_POS_BUFFER,
			FOG_COLOR_BUFFER;

	static
	{
		AMBIENT_COLOR_BUFFER = Utils.wrapDirect(AMBIENT_COLOR);
		DIRECTIONAL_COLOR_BUFFER = Utils.wrapDirect(DIRECTIONAL_COLOR);
		SPOTLIGHT_COLOR_BUFFER = Utils.wrapDirect(SPOTLIGHT_COLOR);
		SPOT_POS_BUFFER = Utils.wrapDirect(SPOT_LIGHT_DIR);
		FOG_COLOR_BUFFER = Utils.wrapDirect(FOG_COLOR);
	}

	/** The position of the light. */
	private GLVector mLightPos;

	/**
	 * Initializes the light at its initial position.
	 */
	public Light()
	{
		mLightPos = new GLVector(0.0f, 5.0f, -6.0f, 1.0f);
	}

	/**
	 * Updates the light position.
	 * 
	 * @param dt
	 *            The time difference between the last and the current update
	 */
	public void update(long dt)
	{

		GLMatrix rotationMatrix = new GLMatrix();

		// Rotate the object on the y-axis
		rotationMatrix.rotate(0.5f, 0.0f, 1.0f, 0.0f);

		// Multiply the rotation matrix with the light position
		rotationMatrix.multiply(mLightPos);
	}

	/**
	 * The drawing function.
	 * 
	 * <p>
	 * Called from the renderer to redraw this instance with possible changes in
	 * values.
	 * </p>
	 * 
	 * @param gl
	 *            - The GL Context
	 */
	public void draw(GL10 gl)
	{
		/*
		 * STEP 16: Place the point light at the correct position.
		 */
		gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_POSITION, mLightPos.data, 0);

		// Disable lighting and texturing so the following object will always be
		// visible
		gl.glDisable(GL11.GL_TEXTURE_2D);
		gl.glDisable(GL11.GL_LIGHTING);

		// Set the object color
		gl.glColor4f(0.0f, 0.0f, 1.0f, 1.0f);

		// Bind the array buffer to draw single points
		((GL11) gl).glBindBuffer(GL11.GL_ARRAY_BUFFER, 0);

		// Set the vertex pointer to the light position
		gl.glVertexPointer(3, GL11.GL_FLOAT, 0,
				Utils.wrapDirect(mLightPos.data));

		// Draw a single vertex
		gl.glDrawArrays(GL11.GL_POINTS, 0, 1);

		// Enable lighting and texturing again
		gl.glEnable(GL11.GL_LIGHTING);
		gl.glEnable(GL11.GL_TEXTURE_2D);
	}

	/**
	 * Sets up the fog in the scene.
	 * 
	 * @param gl
	 *            The OpenGL render context.
	 */
	public void setupFog(GL10 gl)
	{
		/*
		 * STEP 27a: Set the fog color.
		 */
		gl.glFogfv(GL10.GL_FOG_COLOR, FOG_COLOR, 0);

		/*
		 * STEP 27b: Set the fog density.
		 */
		gl.glFogfv(GL10.GL_FOG_DENSITY, FOG_COLOR_BUFFER);

		/*
		 * STEP 27c: Set fog hint to GL_NICEST.
		 */
		gl.glFogf(GL10.GL_FOG_HINT, GL10.GL_NICEST);

		/*
		 * STEP 28d: Set the fog start 1.0f in front of the player.
		 */
		gl.glFogf(GL10.GL_FOG_START, 1.0f);

		/*
		 * STEP 29e: Set the fog end 5.0f in front of the player.
		 */
		gl.glFogf(GL10.GL_FOG_END, 5.0f);
	}

	/**
	 * Sets up the light for the scene.
	 * 
	 * @param gl
	 *            The OpenGL render context.
	 */
	public void setupLight(GL10 gl)
	{
		gl.glEnable(GL10.GL_LIGHTING);

		/*
		 * STEP 15a: Enable the first light source (GL_LIGHT0). This light will
		 * be a spot light - like a flashlight. Assign a spotlight color, a spot
		 * exponent and cutoff, as well as the spot direction.
		 */
		gl.glLightf(GL10.GL_LIGHT0, GL10.GL_SPOT_EXPONENT, 60);
		gl.glLightf(GL10.GL_LIGHT0, GL10.GL_SPOT_CUTOFF, 60);
		gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_DIFFUSE, SPOTLIGHT_COLOR_BUFFER);
		gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_SPOT_DIRECTION, SPOT_POS_BUFFER);
		gl.glEnable(GL10.GL_LIGHT0);

		/*
		 * STEP 15b: Enable the second light source (GL_LIGHT1). This light will
		 * be a point light - like a lantern. Assign ambient and diffuse color.
		 */
		gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_AMBIENT, AMBIENT_COLOR_BUFFER);
		gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_DIFFUSE, DIRECTIONAL_COLOR_BUFFER);
		gl.glEnable(GL10.GL_LIGHT1);

	}

	/**
	 * Provides the caller with the position of the light.
	 * 
	 * @return A float array in format x,y,z,w
	 */
	public float[] getLightPosition()
	{
		return mLightPos.data;
	}
}
