package at.fhooe.mgames.entities;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLUtils;
import at.fhooe.mgames.tutorial03.R;
import at.fhooe.mgames.utils.Utils;

/**
 * A skybox as static environment.
 * 
 * <p>
 * A skybox is basically a cube. However, the sides of this cube are rendered
 * individually and
 * </p>
 * 
 * @author Florian Lettner
 */
public class Skybox
{

	/** The front plane of the cube */
	private static final float[] VERTICES_FRONT = { -1.0f, 1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f, -1.0f,
			1.0f, 1.0f, -1.0f, 1.0f, };

	/** The back plane of the cube */
	private static final float[] VERTICES_BACK = { 1.0f, 1.0f, -1.0f, 1.0f,
			-1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f, -1.0f, };

	/** The bottom plane of the cube */
	private static final float[] VERTICES_BOTTOM = { -1.0f, -1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, -1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f, -1.0f };

	/** The top plane of the cube */
	private static final float[] VERTICES_TOP = { -1.0f, 1.0f, -1.0f, -1.0f,
			1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f,
			1.0f, 1.0f, 1.0f, 1.0f, };

	/** The right plane of the cube */
	private static final float[] VERTICES_RIGHT = { -1.0f, 1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f, 1.0f, };

	/** The left plane of the cube */
	private static final float[] VERTICES_LEFT = { 1.0f, 1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, -1.0f, -1.0f, };

	/** The front texture coordinates of the cube */
	private static final float[] TEXCOORD_FRONT = { 0.0f, 0.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, };

	/** The back texture coordinates of the cube */
	private static final float[] TEXCOORD_BACK = { 0.0f, 0.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, };

	/** The bottom texture coordinates of the cube */
	private static final float[] TEXCOORD_BOTTOM = { 0.0f, 0.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f };

	/** The top texture coordinates of the cube */
	private static final float[] TEXCOORD_TOP = { 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, };

	/** The right texture coordinates of the cube */
	private static final float[] TEXCOORD_RIGHT = { 0.0f, 0.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, };

	/** The left texture coordinates of the cube */
	private static final float[] TEXCOORD_LEFT = { 0.0f, 0.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, };

	/** The float buffers to wrap the vertex float arrays. */
	private static final FloatBuffer VERTEX_BUFFER_FRONT, VERTEX_BUFFER_BACK,
			VERTEX_BUFFER_RIGHT, VERTEX_BUFFER_LEFT, VERTEX_BUFFER_TOP,
			VERTEX_BUFFER_BOTTOM;

	/** The float buffers to wrap the texture coordinates. */
	private static final FloatBuffer TEXCOORD_BUFFER_FRONT,
			TEXCOORD_BUFFER_BACK, TEXCOORD_BUFFER_RIGHT, TEXCOORD_BUFFER_LEFT,
			TEXCOORD_BUFFER_TOP, TEXCOORD_BUFFER_BOTTOM;

	static
	{
		VERTEX_BUFFER_FRONT = Utils.wrapDirect(VERTICES_FRONT);
		TEXCOORD_BUFFER_FRONT = Utils.wrapDirect(TEXCOORD_FRONT);

		VERTEX_BUFFER_BACK = Utils.wrapDirect(VERTICES_BACK);
		TEXCOORD_BUFFER_BACK = Utils.wrapDirect(TEXCOORD_BACK);

		VERTEX_BUFFER_BOTTOM = Utils.wrapDirect(VERTICES_BOTTOM);
		TEXCOORD_BUFFER_BOTTOM = Utils.wrapDirect(TEXCOORD_BOTTOM);

		VERTEX_BUFFER_TOP = Utils.wrapDirect(VERTICES_TOP);
		TEXCOORD_BUFFER_TOP = Utils.wrapDirect(TEXCOORD_TOP);

		VERTEX_BUFFER_RIGHT = Utils.wrapDirect(VERTICES_RIGHT);
		TEXCOORD_BUFFER_RIGHT = Utils.wrapDirect(TEXCOORD_RIGHT);

		VERTEX_BUFFER_LEFT = Utils.wrapDirect(VERTICES_LEFT);
		TEXCOORD_BUFFER_LEFT = Utils.wrapDirect(TEXCOORD_LEFT);
	}

	private static final int LEFT = 0;
	private static final int RIGHT = 1;
	private static final int BOTTOM = 2;
	private static final int TOP = 3;
	private static final int BACK = 4;
	private static final int FRONT = 5;

	/** Textures buffer containing pointers to the textures. */
	private IntBuffer mTexturesBuffer;

	/**
	 * Loads the textures for all sides of the cube.
	 * 
	 * @param gl
	 *            The OpenGL render context.
	 * @param context
	 *            The application context to access the textures.
	 */
	public void loadTextures(GL10 gl, Context context)
	{

		// Allocate memory for 6 textures
		mTexturesBuffer = IntBuffer.allocate(6);
		gl.glGenTextures(6, mTexturesBuffer);

		loadTexture(gl, context, R.drawable.left, mTexturesBuffer.get(LEFT));
		loadTexture(gl, context, R.drawable.right, mTexturesBuffer.get(RIGHT));
		loadTexture(gl, context, R.drawable.bottom, mTexturesBuffer.get(BOTTOM));
		loadTexture(gl, context, R.drawable.top, mTexturesBuffer.get(TOP));
		loadTexture(gl, context, R.drawable.back, mTexturesBuffer.get(BACK));
		loadTexture(gl, context, R.drawable.front, mTexturesBuffer.get(FRONT));
	}

	/**
	 * Draws the skybox.
	 * 
	 * @param gl
	 *            The OpenGL render context.
	 */
	public void draw(GL10 gl)
	{
		gl.glPushMatrix();

		/*
		 * STEP 18: Enable texturing, disable lighting and depth testing.
		 */
		gl.glEnable(GL10.GL_TEXTURE);
		gl.glDisable(GL10.GL_LIGHTING);
		gl.glDisable(GL10.GL_DEPTH_TEST);
		gl.glDisable(GL10.GL_FOG);

		/*
		 * STEP 19: Set glColor to white and 100% alpha.
		 */
		gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

		/*
		 * STEP 20a: Bind front texture and draw front segment.
		 */
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexturesBuffer.get(FRONT));
		drawSegment(gl, VERTEX_BUFFER_FRONT, TEXCOORD_BUFFER_FRONT);

		/*
		 * STEP 20b: Bind back texture and draw back segment.
		 */
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexturesBuffer.get(BACK));
		drawSegment(gl, VERTEX_BUFFER_BACK, TEXCOORD_BUFFER_BACK);

		/*
		 * STEP 20c: Bind bottom texture and draw bottom segment.
		 */
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexturesBuffer.get(BOTTOM));
		drawSegment(gl, VERTEX_BUFFER_BOTTOM, TEXCOORD_BUFFER_BOTTOM);

		/*
		 * STEP 20d: Bind top texture and draw top segment.
		 */
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexturesBuffer.get(TOP));
		drawSegment(gl, VERTEX_BUFFER_TOP, TEXCOORD_BUFFER_TOP);

		/*
		 * STEP 20e: Bind right texture and draw right segment.
		 */
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexturesBuffer.get(RIGHT));
		drawSegment(gl, VERTEX_BUFFER_RIGHT, TEXCOORD_BUFFER_RIGHT);

		/*
		 * STEP 20f: Bind left texture and draw left segment.
		 */
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexturesBuffer.get(LEFT));
		drawSegment(gl, VERTEX_BUFFER_LEFT, TEXCOORD_BUFFER_LEFT);

		/*
		 * STEP 21: Enable depth testing and lighting again.
		 */
		gl.glEnable(GL10.GL_LIGHTING);
		gl.glEnable(GL10.GL_DEPTH_TEST);
		gl.glEnable(GL10.GL_FOG);

		gl.glPopMatrix();
	}

	/**
	 * Draws a single side of the skybox.
	 * 
	 * @param gl
	 *            The OpenGL render context.
	 * @param vertexBuffer
	 *            The vertex buffer for the side.
	 * @param textureBuffer
	 *            The texture buffer for the side.
	 */
	private void drawSegment(GL10 gl, FloatBuffer vertexBuffer,
			FloatBuffer textureBuffer)
	{
		// Enable the vertex, texture and normal state
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

		// Point to our buffers
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);

		// Draw the vertices as triangles
		gl.glDrawArrays(GL10.GL_TRIANGLES, 0, vertexBuffer.capacity() / 3);

		// Disable the client state before leaving
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
	}

	/**
	 * Loads the texture for a single side of the skybox
	 * 
	 * @param gl
	 *            The OpenGL render context.
	 * @param context
	 *            The application context to load the texture.
	 * @param resourceID
	 *            The resource id of the texture.
	 * @param bufferId
	 *            The buffer id that is associated with the texture.
	 */
	private void loadTexture(GL10 gl, Context context, int resourceID,
			int bufferId)
	{

		gl.glEnable(GL10.GL_TEXTURE_2D);

		Bitmap bitmap = Utils.getTextureFromBitmapResource(context, resourceID);

		/*
		 * Define the quality of the texture mapping. For minification
		 * GL_LINEAR_MIPMAP_LINEAR is used to provide smooth maps. For slower
		 * devices GL_LINEAR_MIPMAP_NEAREST is faster but it looks more blocky
		 * and pixelated.
		 */
		gl.glBindTexture(GL10.GL_TEXTURE_2D, bufferId);
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
				GL10.GL_LINEAR_MIPMAP_LINEAR);
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
				GL10.GL_LINEAR);
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
				GL10.GL_CLAMP_TO_EDGE);
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
				GL10.GL_CLAMP_TO_EDGE);

		if (gl instanceof GL11)
		{
			gl.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_GENERATE_MIPMAP,
					GL11.GL_TRUE);
			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
		}
		else
		{
			Utils.generateMipmapsForBoundTexture(bitmap);
		}

		/*
		 * Finally delete any ressources that have been acquired for loading the
		 * bitmap
		 */
		bitmap.recycle();
	}

}
