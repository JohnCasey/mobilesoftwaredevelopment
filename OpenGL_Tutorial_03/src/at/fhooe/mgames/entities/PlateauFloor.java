package at.fhooe.mgames.entities;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLUtils;
import at.fhooe.mgames.tutorial03.R;
import at.fhooe.mgames.utils.ModelLoader;
import at.fhooe.mgames.utils.Utils;

/**
 * A plateau entity.
 * 
 * <p>
 * The floor is handled separately for the implementation of planar shadows.
 * </p>
 * 
 * @author Florian Lettner
 */
public class PlateauFloor
{

	/** The normals of the floor plane. */
	public static final float[] GROUND_NORMALS = new float[] { 0, 1, 0, 0 };

	/** The buffer holding the vertices */
	private FloatBuffer vertexBuffer;
	/** The buffer holding the texture coordinates */
	private FloatBuffer textureBuffer;

	private FloatBuffer normalBuffer;

	/** Textures buffer containing pointers to the textures. */
	private IntBuffer mTexturesBuffer;

	/**
	 * Initializes the plateau floor.
	 * 
	 * <p>
	 * In difference to the cube entity, the vertices, normals and texture
	 * coordinates are loaded from a file using the ModelLoader.
	 * </p>
	 * 
	 * @param context
	 *            The application context used to access a file.
	 * @param objectPath
	 *            The filename of the object file.
	 * 
	 * @see at.fhooe.mgames.utils.ModelLoader
	 */
	public PlateauFloor(Context context, String objectPath)
	{
		/*
		 * STEP 13a: Have a look at the floor.txt file in the assets
		 * folder. The file contains the amount of polygons stored in the files
		 * (2 triangles for the floor). Each line represents the vertex
		 * coordinates, the corresponding normals for lighting and the texture
		 * coordinates applied to this vertex. Use the model loader to load the
		 * floor and assign the results to vertex buffer (idx = 0),
		 * textureBuffer (idx = 1) and normalBuffer (idx = 2).
		 */
		
		List<FloatBuffer> worldData = ModelLoader.loadWorld(context, objectPath);
		
		vertexBuffer = worldData.get(0);
		textureBuffer = worldData.get(1);
		normalBuffer = worldData.get(2);
	}

	/**
	 * Draws the plateau.
	 * 
	 * <p>
	 * As no index array is provided glDrawArrays() has to be used instead of
	 * glDrawElements().
	 * </p>
	 * 
	 * @param gl
	 *            The OpenGL render context.
	 */
	public void draw(GL10 gl)
	{
		// Bind the texture based on the given filter
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexturesBuffer.get(0));

		// Enable the vertex, texture and normal state
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);

		// Point to our buffers
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);
		gl.glNormalPointer(GL10.GL_FLOAT, 0, normalBuffer);

		// Draw the vertices as triangles
		gl.glDrawArrays(GL10.GL_TRIANGLES, 0, vertexBuffer.capacity() / 3);

		// Disable the client state before leaving
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glDisableClientState(GL10.GL_NORMAL_ARRAY);
	}

	/**
	 * Load the textures.
	 * 
	 * @param gl
	 *            The OpenGL render context.
	 * @param context
	 *            The application context.
	 */
	public void loadGLTexture(GL10 gl, Context context)
	{

		gl.glEnable(GL10.GL_TEXTURE_2D);
		mTexturesBuffer = IntBuffer.allocate(1);
		gl.glGenTextures(1, mTexturesBuffer);

		Bitmap bitmap = Utils.getTextureFromBitmapResource(context,
				R.drawable.door);

		// Create mipmapped textures and bind it to texture 2
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexturesBuffer.get(0));
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
				GL10.GL_LINEAR);
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
				GL10.GL_LINEAR_MIPMAP_NEAREST);
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
				GL10.GL_REPEAT);
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
				GL10.GL_REPEAT);

		if (gl instanceof GL11)
		{
			gl.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_GENERATE_MIPMAP,
					GL11.GL_TRUE);
			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
		}
		else
		{
			Utils.generateMipmapsForBoundTexture(bitmap);
		}
		// Clean up
		bitmap.recycle();
	}
}
