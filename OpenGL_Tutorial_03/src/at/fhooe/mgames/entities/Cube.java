package at.fhooe.mgames.entities;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLUtils;
import at.fhooe.mgames.tutorial03.R;
import at.fhooe.mgames.utils.Utils;

/**
 * Represents a cube
 * 
 * @author Florian Lettner
 */
public class Cube {

	/** A constant to multiply with the real angle to allow object spinning. */
	private static final float ANGLE_FACTOR = 0.35f;

	/** The initial vertex definition. */
	private static final float[] VERTICES = new float[] {
			// The front face.
			-0.2f, 0.2f, 0.2f, -0.2f, -0.2f, 0.2f, 0.2f, 0.2f, 0.2f, 0.2f, 0.2f, 0.2f, -0.2f,
			-0.2f,
			0.2f,
			0.2f,
			-0.2f,
			0.2f,

			// The left face.
			0.2f, 0.2f, 0.2f, 0.2f, -0.2f, 0.2f, 0.2f, 0.2f, -0.2f, 0.2f, 0.2f, -0.2f, 0.2f, -0.2f,
			0.2f,
			0.2f,
			-0.2f,
			-0.2f,

			// The back face.
			0.2f, 0.2f, -0.2f, 0.2f, -0.2f, -0.2f, -0.2f, 0.2f, -0.2f, -0.2f, 0.2f, -0.2f, 0.2f, -0.2f, -0.2f,
			-0.2f,
			-0.2f,
			-0.2f,

			// The right face.
			-0.2f, 0.2f, -0.2f, -0.2f, -0.2f, -0.2f, -0.2f, 0.2f, 0.2f, -0.2f, 0.2f, 0.2f, -0.2f, -0.2f, -0.2f, -0.2f,
			-0.2f,
			0.2f,

			// The top face.
			-0.2f, 0.2f, -0.2f, -0.2f, 0.2f, 0.2f, 0.2f, 0.2f, -0.2f, 0.2f, 0.2f, -0.2f, -0.2f, 0.2f, 0.2f, 0.2f, 0.2f,
			0.2f,

			// The bottom face.
			-0.2f, -0.2f, 0.2f, -0.2f, -0.2f, -0.2f, 0.2f, -0.2f, 0.2f, 0.2f, -0.2f, 0.2f, -0.2f, -0.2f, -0.2f, 0.2f,
			-0.2f, -0.2f };

	/** The initial normals definition. */
	private static final float[] NORMALS = new float[] {
			// The front face.
			0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
			0.0f,
			1.0f,

			// The left face.
			1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
			0.0f,
			0.0f,

			// The back face.
			0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f,
			0.0f,
			-1.0f,

			// The right face.
			-1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f,
			0.0f,

			// The top face.
			0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

			// The bottom face.
			0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f,
			0.0f };

	/** The initial texture coordinates definition. */
	private static final float[] TEX_COORDS = new float[] {
			// The front face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,

			// The left face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,

			// The back face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,

			// The right face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,

			// The top face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,

			// The bottom face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f };

	/** The initial faces definition. */
	private static final byte[] FACES = new byte[] { 0, 1, 2, 2, 1, 5, 6, 7, 8, 8, 7, 11, 12, 13, 14, 14, 13, 17, 18,
			19, 20, 20, 19, 23, 24, 25, 26, 26, 25, 29, 30, 31, 32, 32, 31, 35 };

	/** The buffer holding the vertices */
	private static final FloatBuffer VERTEX_BUFFER;
	/** The buffer holding the texture coordinates */
	private static final FloatBuffer TEXCOORD_BUFFER;
	/** The buffer holding the indices */
	private static final ByteBuffer INDEX_BUFFER;
	/** The buffer holding the normals */
	private static final FloatBuffer NORMALS_BUFFER;

	static {
		VERTEX_BUFFER = Utils.wrapDirect(VERTICES);
		TEXCOORD_BUFFER = Utils.wrapDirect(TEX_COORDS);
		NORMALS_BUFFER = Utils.wrapDirect(NORMALS);

		INDEX_BUFFER = ByteBuffer.allocateDirect(FACES.length);
		INDEX_BUFFER.put(FACES);
		INDEX_BUFFER.position(0);
	}

	/** Rotational distance for x- and y-axis. */
	public float mDx, mDy;

	/** Rotational speed for x- and y-axis. */
	public float mSpeedDx, mSpeedDy;

	/** Rotation angle. */
	private float mRotation;

	/** Textures buffer containing pointers to the textures. */
	private IntBuffer mTexturesBuffer;

	/**
	 * Updates the model.
	 * 
	 * @param dt The time difference between the last and the current update.
	 */
	public void update(long dt) {

		// Calculate the rotation
		mRotation = (float) Math.sqrt(mDx * mDx + mDy * mDy);

		mDx += 10.0f / dt;
		mDy += 10.0f / dt;
	}

	/**
	 * The object own drawing function. Called from the renderer to redraw this
	 * instance with possible changes in values.
	 * 
	 * @param gl - The GL Context
	 * @param filter - Which texture filter to be used
	 */
	public void draw(GL10 gl) {

		gl.glPushMatrix();

		gl.glTranslatef(0.0f, 0.5f, 0.0f);

		// Rotate the cube based on the information from touch input
		if (mRotation != 0) {
			gl.glRotatef(mRotation * ANGLE_FACTOR, -mDy / mRotation, -mDx / mRotation, 0);
		}

		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexturesBuffer.get(0));

		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);

		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, VERTEX_BUFFER);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, TEXCOORD_BUFFER);
		gl.glNormalPointer(GL10.GL_FLOAT, 0, NORMALS_BUFFER);

		gl.glDrawElements(GL10.GL_TRIANGLES, FACES.length, GL10.GL_UNSIGNED_BYTE, INDEX_BUFFER);

		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glDisableClientState(GL10.GL_NORMAL_ARRAY);

		gl.glPopMatrix();
	}

	/**
	 * Loads the texture for the cube.
	 * 
	 * @param gl The OpenGL render context.
	 * @param context The application context to load the texture.
	 */
	public void loadTextures(GL10 gl, Context context) {

		gl.glEnable(GL10.GL_TEXTURE_2D);
		mTexturesBuffer = IntBuffer.allocate(3);
		gl.glGenTextures(3, mTexturesBuffer);

		// Load texture file from resources folder
		Bitmap texture = Utils.getTextureFromBitmapResource(context, R.drawable.cube);

		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexturesBuffer.get(0));
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR_MIPMAP_LINEAR);
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);

		if (gl instanceof GL11) {
			gl.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_GENERATE_MIPMAP, GL11.GL_TRUE);
			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, texture, 0);
		} else {
			Utils.generateMipmapsForBoundTexture(texture);
		}

		// free bitmap resources as everything needed is now stored in the
		// texture buffers
		texture.recycle();
	}
}
