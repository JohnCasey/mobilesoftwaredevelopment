package nz.edu.unitec.sosgame1;

public class Constant {
	public static final int SERVER_PORT = 12345;
	public static final int EXIT = 1;
	public static final int MESSAGE = 2;
	public static final int SHOW_TILES_TO_CHOOSE = 3;
	public static final int CHOOSE_TILE = 4;
	public static final int BOARD_SIZE = 5;
	public static final int QUERY_BOARD_SIZE = 6;
	public static final int SHOW_CONNECTING_DIALOG = 100;
	public static final int CLIENT_CONNECTED = 101;
	public static final int SERVER_CONNECTED = 102;
}
