package com.group4.assignment.duckseason;
/**
 * This is a class save all static properties 
 * and methods.
 * 
 * @version 	final version 28 June 2013
 * @author 		Hui Yin
 */
public class Config {
	
	public static String INFO_TITLE_ABOUT = "About";
	public static String INFO_TITLE_HELP = "Help";
	public static String BR = "\r\n";
	public static String INFO_CONTENT_ABOUT;
	public static String INFO_CONTENT_HELP;
	
	public static boolean PLAY_SOUND;
	public static boolean USE_VIBRO;
	public static boolean ACCELEROMETER;
	public static boolean TOUCH_SCREEN;
	public static int SCREEN_WIDTH;
	public static int SCREEN_HEIGHT;
	public static int GAME_HEIGHT;
	public static int FONT_SIZE;
	
	public static int SCORE_X = 20;
	public static int SCORE_Y;
	public static String IMG_PATH_1280="images/1280/";
	public static String IMG_PATH_480="images/480/";
	public static String SOUND_PATH="sounds/";
	
	public final static int LIFE_BAR_SIZE = 5;
	
	public static int MEMORY;
	public static String IMG_PATH;
	/**
     * initialize static variables
     */
	public static void initConfigration(){
		ACCELEROMETER = true;
		TOUCH_SCREEN = false;
		if(SCREEN_WIDTH >= 700){
			MEMORY = 1024*2;
			IMG_PATH = IMG_PATH_1280;
			FONT_SIZE = 40;
		}else{
			MEMORY = 1024;
			IMG_PATH = IMG_PATH_480;
			FONT_SIZE = 20;
		}
		GAME_HEIGHT  = SCREEN_HEIGHT*3/4;
		SCORE_Y = SCREEN_HEIGHT-FONT_SIZE-10;
		initInfomation();
	}
	/**
     * initialize information for about and help pages.
     */
	private static void initInfomation(){
		INFO_CONTENT_ABOUT = " Duck Season is a light shooter android game developed by Group "+BR;
		INFO_CONTENT_ABOUT += " 4. This android application, its images, and all other features "+BR;
		INFO_CONTENT_ABOUT += " have only been created and are to be used only for assignment "+BR;
		INFO_CONTENT_ABOUT += " purposes only. This game is an extension of the original Duck "+BR;
		INFO_CONTENT_ABOUT += " Hunt from Nintendo. In the game, the player uses the crosshair "+BR;
		INFO_CONTENT_ABOUT += " using either touch screen or accelerometer to shoot the ducks that "+BR;
		INFO_CONTENT_ABOUT += " appear on the screen. The duck appears on the screen and will "+BR;
		INFO_CONTENT_ABOUT += " start inscreasing by one and speed up after every ten ducks are "+BR;
		INFO_CONTENT_ABOUT += " shot down. There is no limit to ammos for the player but there "+BR;
		INFO_CONTENT_ABOUT += " is a limit to ducks that escape the player. After the player "+BR;
		INFO_CONTENT_ABOUT += " misses five ducks, it will be game over."+BR;
		
		INFO_CONTENT_HELP = " Duck Season is a shooter game in which the objective is to shoot "+BR;
		INFO_CONTENT_HELP += " moving ducks on the screen before it flies away. You are given 10 "+BR;
		INFO_CONTENT_HELP += " seconds to shoot down each duck. 5 miss ducks will be GAME OVER. "+BR;
		INFO_CONTENT_HELP += " There are two ways to play the game: "+BR;
		INFO_CONTENT_HELP += " First is by touch screen, just tap on the duck will kill the duck. "+BR;
		INFO_CONTENT_HELP += " But your score will not be valid, touch screen is for practice "+BR;
		INFO_CONTENT_HELP += " purpose only. "+BR;
		INFO_CONTENT_HELP += " Second is by accelerometer, tilting your device in the direction "+BR;
		INFO_CONTENT_HELP += " you want the crosshair to move, this controls the crosshair and "+BR;
		INFO_CONTENT_HELP += " by tapping on the sky area on your device will fire the gun. "+BR;
	}
}
