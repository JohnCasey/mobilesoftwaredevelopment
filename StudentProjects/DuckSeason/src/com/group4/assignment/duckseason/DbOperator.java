package com.group4.assignment.duckseason;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.TextView;
/**
 * This is class extends SQLiteOpenHelper and
 * initialize tables when install game.
 * 
 * @version 	final version 28 June 2013
 * @author 		Herman Chang
 * @modified by Hui Yin
 */
public class DbOperator {
	
	public SQLiteDatabase dbHandler;
	public static String BR = "\r\n";
	public static int LOWEST_SCORE;
	public static int COUNT;
	private final int LIMIT = 3;
	public DbOperator(Context context){
		DbHelper dbHelper = new DbHelper(context);
		dbHandler = dbHelper.getWritableDatabase();
		COUNT = 0;
		LOWEST_SCORE = 0;
	}
	/**
     * get all scores from database and set on the textView.
     */
	public void setTextViewList( TextView view ){
		Cursor dbCursor = dbHandler.query(DbHelper.TABLE, null, null, null, null, null, DbHelper.PLAYER_SCORE+" DESC ");
		int i=0;
		while(dbCursor.moveToNext()){
			i++;
			String name = dbCursor.getString(1);
			int score = dbCursor.getInt(2);
			view.append(i+"  name: ["+name+"]  score: ["+score+"]"+BR);
			LOWEST_SCORE = score;
		}
		COUNT = i;
	}
	/**
     * insert users' scores to database.
     * if user name is empty, the user name will be 'Player'.
     */
	public void insertRow(String name, int score){
		if(name.equals(""))
			name = "Player";
		if(COUNT>=LIMIT){
			deleteTheLowestRow();
		}
		ContentValues values = new ContentValues();
        values.putNull(DbHelper.SID);
        values.put(DbHelper.PLAYER_NAME,name);
        values.put(DbHelper.PLAYER_SCORE,score);
        dbHandler.insert(DbHelper.TABLE, null,values);
	}
	/**
     * delete the lowest rows.
     */
	public void deleteTheLowestRow(){
		dbHandler.delete(DbHelper.TABLE, 
				DbHelper.PLAYER_SCORE+" in (select min("+DbHelper.PLAYER_SCORE+") from "+DbHelper.TABLE+")",null);
	}
	/**
     * clear all data.
     */
	public void clearAllData(){
		dbHandler.delete(DbHelper.TABLE,null,null);
	}
}
