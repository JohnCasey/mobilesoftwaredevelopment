package com.group4.assignment.duckseason.modules;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

import com.group4.assignment.duckseason.Config;
import com.group4.assignment.duckseason.Game;
import com.group4.assignment.duckseason.entites.Duck;
import com.group4.assignment.duckseason.entites.Phenix;
/**
 * This is the Animal model of the project
 * extends MModule
 * 
 * @version 	final version 28 June 2013
 * @author 		Pritesh Mistry
 * @author		Anqi Wang
 * @modified by Hui Yin
 */
public class AnimalsModule extends MyModule{

	public List<Duck> ducks;
	public List<Duck> remove;
	public Phenix phenix;
	public static final int LEVEL1 = 1;
	public static final int LEVEL2 = 2;
	public static final int LEVEL3 = 3;
	public static final int LEVEL4 = 4;
	public static float DUCK_SPEED;
	public static int FLYING_DUCK_COUNT;
	public static int CURRENT_LEVEL;
	public boolean animalRestart;
	private LifeBarModule lifeBar;
	private Random random = new Random();
	private long pauseTime;
	
	/**
	 * Constructor for duck and phoenix
	 * @param scene
	 * @param lifeBarModule
	 */
	public AnimalsModule(Scene scene,LifeBarModule lifeBarModule) {
		super(scene);
		this.lifeBar = lifeBarModule;
		ducks = new ArrayList<Duck>();
		remove = new ArrayList<Duck>();
		CURRENT_LEVEL = 0;
		pauseTime = 0;
		phenix = null;
		updateLevel(LEVEL1);
		animalRestart = false;
		addPhenix();
	}
	
	/**
	 * start the phoenix
	 */
	@Override
	public void startGame() {
		setVisable(true);
		phenix.start();
	}
	
	/**
	 * pause the game
	 */
	public void pauseGame() {
		pauseTime = System.currentTimeMillis();
	}
	
	/**
	 * resume the game
	 */
	public void resumeGame(){
		phenix.addPauseTime(System.currentTimeMillis()- pauseTime);
		for(Duck duck : ducks){
			duck.addPauseTime(System.currentTimeMillis()- pauseTime);
		}
	}
	
	/**
	 * update the duck and the phoenix when duck flies away and a new duck appears
	 * on the canvas
	 */
	public void animalsUpdate(){
		phenix.update();
		updateDucks();
		int count = FLYING_DUCK_COUNT - ducks.size();
		if(count>0){
			for(int i=0;i<count;i++){
				addDuck();
			}
		}
	}
	
	/**
	 * update ducks on the screen when duck flies away or shot down
	 */
	public void updateDucks(){
		for(Duck duck:ducks){
			if(duck.isFallen()){
				remove.add(duck);
			}else{
				duck.update(lifeBar);
			}
		}
		for(Duck duck:remove){
			ducks.remove(duck);
			this.scene.detachChild(duck);
		}
		remove.clear();
	}
	
	/**
	 * add new duck on the canvas with a set random position
	 */
	public void addDuck(){
		Duck duck;
		if(random.nextBoolean()){
			TiledTextureRegion temp = Game.DUCK_REGION.deepCopy();
			duck = new Duck(Config.SCREEN_WIDTH, random.nextInt(Config.SCREEN_HEIGHT-10), 
					temp.getTileWidth(),
					temp.getTileHeight(),temp);
			temp.setFlippedHorizontal(true);
			duck.animate(new long[]{100,100,100,100}, 0, 3, true);
			duck.setXSpeed(-DUCK_SPEED);
		}else{
			TiledTextureRegion temp = Game.DUCK_REGION.deepCopy();
			duck = new Duck(0, random.nextInt(Config.SCREEN_HEIGHT-10), 
					temp.getTileWidth(),
					temp.getTileHeight(),temp);
			temp.setFlippedHorizontal(false);
				duck.animate(new long[]{100,100,100,100}, 0, 3, true);
				duck.setXSpeed(DUCK_SPEED);
		}
		this.scene.attachChild(duck,2);
		ducks.add(duck);
	}
	
	/**
	 * show phoenix on the canvas
	 */
	public void addPhenix(){
		phenix = new Phenix(0-Game.PHENIX_REGION.getTileWidth(), random.nextInt(40),
				Game.PHENIX_REGION.getTileWidth(),
				Game.PHENIX_REGION.getTileHeight(),Game.PHENIX_REGION);
		Game.PHENIX_REGION.setFlippedHorizontal(false);
		int f = Phenix.FRAME;
		phenix.animate(new long[]{f,f,f,f,f,f}, 2, 7, true);
		this.scene.attachChild(phenix,1);
	}
	
	/**
	 * set the visibility of the duck
	 * @param flag
	 */
	public void setVisable(boolean flag){
		for(Duck duck : ducks){
			duck.setVisible(flag);
		}
	}
	
	/**
	 * updates the level of the duck when 10 ducks have been shot down
	 * @param level
	 */
	public static void updateLevel(int level){
		if(CURRENT_LEVEL == level){
			return;
		}
		CURRENT_LEVEL = level;
		switch(CURRENT_LEVEL){
			case LEVEL1:
				FLYING_DUCK_COUNT = 1;
				DUCK_SPEED = 1.0f;
				break;
			case LEVEL2:
				FLYING_DUCK_COUNT = 2;
				DUCK_SPEED = 1.3f;
				break;
			case LEVEL3:
				FLYING_DUCK_COUNT = 3;
				DUCK_SPEED = 1.5f;
				break;
			case LEVEL4:
				FLYING_DUCK_COUNT = 3;
				DUCK_SPEED = 2.0f;
				break;
		}
		
	}
	
	/**
	 * restarts the game when player choose restart
	 */
	@Override
	public void restart() {
		for(Duck duck:ducks){
			this.scene.detachChild(duck);
		}
		ducks.clear();
		remove.clear();
		pauseTime = 0;
		CURRENT_LEVEL = 0;
		updateLevel(LEVEL1);
		phenix.restart();
	}

}
