package com.group4.assignment.duckseason.modules;

import org.anddev.andengine.entity.scene.Scene;
/**
 * This is abstract class and provides two abstract methods 
 * so that modules can be easily integrated.
 * 
 * @version 	final version 28 June 2013
 * @author 		Hui Yin
 */
public abstract class MyModule {
	protected Scene scene;
	public MyModule(Scene scene){
		this.scene = scene;
	}
	/**
	 * deal with game start event.
	 */
	public abstract void startGame();
	/**
	 * deal with game restart event.
	 */
	public abstract void restart();
}
