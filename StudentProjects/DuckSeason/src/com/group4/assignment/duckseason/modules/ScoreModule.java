package com.group4.assignment.duckseason.modules;

import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.entity.text.Text;

import com.group4.assignment.duckseason.Config;
import com.group4.assignment.duckseason.Game;
/**
 * This is the Score Model of the project
 * extends MyModel
 * 
 * @version 	final version 28 June 2013
 * @author 		Herman Chang
 * @modified by Hui Yin
 */
public class ScoreModule extends MyModule{
	private Text tLeft;
	public ChangeableText tRight;
	public static int TOTAL_SCORE;
	public static int EACH_DUCK;
	public static float x;
	public static float y;
	public ScoreModule(Scene scene){
		super(scene);
		tLeft = new Text(Config.SCORE_X, Config.SCORE_Y, Game.SCORE_FONT, "SCORE:");
		tRight = new ChangeableText (tLeft.getX()+tLeft.getWidth()+5,tLeft.getY(), Game.SCORE_FONT, "000000000");
		tRight.setText("0");
		tLeft.setVisible(false);
		tRight.setVisible(false);
		TOTAL_SCORE = 0;
		EACH_DUCK = 10;
		this.scene.attachChild(tLeft,1);
		this.scene.attachChild(tRight,1);
	}
	
	/**
	 * Update score whenever a duck or phoenix is shot down
	 * @param factor
	 */
	public void updateScore(int factor){
		TOTAL_SCORE += EACH_DUCK*factor;
		tRight.setText(TOTAL_SCORE+"");
		if(TOTAL_SCORE>=100 && TOTAL_SCORE<300){
			AnimalsModule.updateLevel(AnimalsModule.LEVEL2);
		}else if(TOTAL_SCORE>=300 && TOTAL_SCORE<800){
			AnimalsModule.updateLevel(AnimalsModule.LEVEL3);
		}else if(TOTAL_SCORE>=800){
			AnimalsModule.updateLevel(AnimalsModule.LEVEL4);
		}
	}
	
	/**
	 * start game
	 */
	@Override
	public void startGame(){
		tLeft.setVisible(true);
		tRight.setVisible(true);
	}
	
	/**
	 * restart the game
	 */
	@Override
	public void restart() {
		tRight.setText("0");
		tLeft.setVisible(false);
		tRight.setVisible(false);
		TOTAL_SCORE = 0;
	}
}
