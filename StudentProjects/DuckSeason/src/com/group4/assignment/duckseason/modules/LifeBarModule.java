package com.group4.assignment.duckseason.modules;

import java.util.ArrayList;
import java.util.List;

import org.anddev.andengine.entity.scene.Scene;

import com.group4.assignment.duckseason.Config;
import com.group4.assignment.duckseason.Game;
import com.group4.assignment.duckseason.entites.MySprite;
/**
 * This is the Life Bar Model of the project
 * extends MyModel
 * 
 * @version 	final version 28 June 2013
 * @author 		Anqi Wang
 * @modified by Hui Yin
 */
public class LifeBarModule extends MyModule{
	public List<MySprite> lifeBar;
	public static int LIFE_COUNT;
	public LifeBarModule(Scene scene) {
		super(scene);
		initLifeBar();
	}
	
	/**
	 * method for showing duck life bar limit to five misses
	 */
	public void initLifeBar(){
		LIFE_COUNT = Config.LIFE_BAR_SIZE;
		lifeBar = new ArrayList<MySprite>(Config.LIFE_BAR_SIZE);
		int duckWidth = Game.DUCK_NORMAL.getWidth();
		int duckHeight = Game.DUCK_NORMAL.getHeight();
		float barX = Config.SCORE_X;
		float barY = Config.SCORE_Y-duckWidth-5;
		for(int i=0;i<LIFE_COUNT;i++){
			MySprite temp = new MySprite(barX,barY,duckWidth,duckHeight,Game.DUCK_NORMAL);
			barX+=duckWidth+0;
			temp.setVisible(false);
			this.scene.attachChild(temp,1);
			lifeBar.add(temp);
		}
		LIFE_COUNT--;
	}
	
	/**
	 * update life bar
	 */
	public void updateLifeBar(){
		if(LIFE_COUNT>=0){
			MySprite temp = lifeBar.get(LIFE_COUNT);
			temp.setColor(0.3f,0.2f,1.0f);
			//temp.setColor(1.0f,0.0f,0.0f);
		}
		LIFE_COUNT--;
	}
	
	/**
	 * set life bar to visible
	 * @param flag
	 */
	public void setLifeBarVisiable(boolean flag){
		for(MySprite temp : lifeBar){
			temp.setVisible(flag);
		}
	}
	
	/**
	 * set life bar to visible
	 */
	@Override
	public void startGame() {
		setLifeBarVisiable(true);
	}
	
	/**
	 * restart life bar count when game restart
	 */
	@Override
	public void restart() {
		LIFE_COUNT = Config.LIFE_BAR_SIZE;
		LIFE_COUNT--;
		for(MySprite temp : lifeBar){
			temp.setColor(1.0f,1.0f,1.0f);
			temp.setVisible(false);
		}
	}
}
