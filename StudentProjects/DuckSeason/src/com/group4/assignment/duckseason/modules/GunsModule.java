package com.group4.assignment.duckseason.modules;

import java.util.List;

import org.anddev.andengine.collision.BaseCollisionChecker;
import org.anddev.andengine.collision.RectangularShapeCollisionChecker;
import org.anddev.andengine.entity.scene.Scene;

import com.group4.assignment.duckseason.Config;
import com.group4.assignment.duckseason.Game;
import com.group4.assignment.duckseason.entites.Duck;
import com.group4.assignment.duckseason.entites.MySprite;
import com.group4.assignment.duckseason.entites.Phenix;
/**
 * This is the Guns Model of the project
 * extends MyModel
 * 
 * @version 	final version 28 June 2013
 * @author 		Terapong Thiankitthamrong
 * @modified by Hui Yin
 */
public class GunsModule extends MyModule{
	public MySprite crosshair,shotGun,sniperGun,currentGun,shotCrosshair,sniperCrosshair;
	private ScoreModule score;
	private float objectFactor,crosshairFactor;
	
	/**
	 * Constructor for guns
	 * @param scene
	 * @param scoreModule
	 */
	public GunsModule(Scene scene,ScoreModule scoreModule) {
		super(scene);
		this.score = scoreModule;
		shotCrosshair = new MySprite(Config.SCREEN_WIDTH/2,Config.SCREEN_HEIGHT/2,
				Game.SHOT_CROSSHAIR.getWidth(),Game.SHOT_CROSSHAIR.getHeight(),Game.SHOT_CROSSHAIR);
		sniperCrosshair = new MySprite(Config.SCREEN_WIDTH/2,Config.SCREEN_HEIGHT/2,
				Game.SNIPER_CROSSHAIR.getWidth(),Game.SNIPER_CROSSHAIR.getHeight(),Game.SNIPER_CROSSHAIR);
		shotGun = new MySprite(Config.SCREEN_WIDTH/2-(Game.SHOTGUN.getWidth()/2),
				(Config.SCREEN_HEIGHT-(Game.SHOTGUN.getHeight()/2)),
				Game.SHOTGUN.getWidth(),Game.SHOTGUN.getHeight(),Game.SHOTGUN);
		sniperGun = new  MySprite(Config.SCREEN_WIDTH/2-(Game.SNIPERGUN.getWidth()/2),
				(Config.SCREEN_HEIGHT-(Game.SNIPERGUN.getHeight()/2)),
				Game.SNIPERGUN.getWidth(),Game.SNIPERGUN.getHeight(),Game.SNIPERGUN);
		currentGun = shotGun;
		crosshair = shotCrosshair;
		this.scene.attachChild(sniperCrosshair,1);
		this.scene.attachChild(shotCrosshair,1);
		this.scene.attachChild(sniperGun,1);
		this.scene.attachChild(shotGun,1);
		sniperCrosshair.setVisible(false);
		shotCrosshair.setVisible(false);
		sniperGun.setVisible(false);
		shotGun.setVisible(false);
		objectFactor = 0.25f;
		crosshairFactor = 0.45f;
	}
	
	/**
	 * change gun option
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean changeGunByClickGun(float x, float y){
		if(RectangularShapeCollisionChecker.checkContains(currentGun, x, y)){
			changeGun();
			return true;
		}
		return false;
	}
	
	/**
	 * change gun method to switch between guns
	 */
	private void changeGun(){
		currentGun.setVisible(false);
		crosshair.setVisible(false);
		float tempX = crosshair.getX();
		float tempY = crosshair.getY();
		if(currentGun.equals(shotGun)){
			currentGun = sniperGun;
			crosshair = sniperCrosshair;
		}else{
			currentGun = shotGun;
			crosshair = shotCrosshair;
		}
		crosshair.setPosition(tempX, tempY);
		rotateGun();
		currentGun.setVisible(true);
		crosshair.setVisible(true);
	}
	
	/**
	 * method for making the gun pointing direction move where the crosshair will
	 * be located
	 */
	public void rotateGun(){
		currentGun.setPosition(currentGun.getX(), 
				Config.SCREEN_HEIGHT-sniperGun.getHeight()/2);
		float angle = (float) (Math.atan2(currentGun.getCenterY()-crosshair.getCenterY(),
				currentGun.getCenterX()-crosshair.getCenterX())*180/Math.PI-90);
		currentGun.setRotation(angle);
	}
	
	/**
	 * set the position of the cross hair
	 * @param x
	 * @param y
	 */
	public void setCrosshairPosition(float x,float y){
		crosshair.setCenterPosition(x, y);
	}
	
	/**
	 * set the position of the cross hair including a factor
	 * @param x
	 * @param y
	 * @param factor
	 */
	public void setCrosshairPosition(float x,float y,int factor){
		float px = crosshair.getX()+(x*factor);
		float py = crosshair.getY()+(y*factor);
		if(px>=(Config.SCREEN_WIDTH-crosshair.getWidth())){
			px = (Config.SCREEN_WIDTH-crosshair.getWidth());
		}else if(px<=0){
			px = 0;
		}
		if(py>=(Config.GAME_HEIGHT-crosshair.getHeight())){
			py = (Config.GAME_HEIGHT-crosshair.getHeight());
		}else if(py<=0){
			py=0;
		}
		crosshair.setPosition(px,py);
	}
	
	/**
	 * method for shooting down ducks and phoenix where the phoenix can only be
	 * shot down by a sniper gun
	 * @param ducks
	 * @param phenix
	 */
	public void shoot(List<Duck> ducks,Phenix phenix){
		if(Config.PLAY_SOUND){
			playGunSound();
		}
		if(currentGun.equals(sniperGun)){
			if(isPhenixShotted(phenix))
			{
				score.updateScore(5);
				phenix.setFalling(true);
				phenix.stopAnimation(1);
			}
		}
		for( Duck duck : ducks ){
			if(duck.isFlying() && isShotted(duck))
			{
				score.updateScore(1);
				duck.setFalling(true);
				duck.stopAnimation(12);
			}
		}
		
	}
	
	/**
	 * if duck is located where the crosshair is duck will be shot
	 * @param duck
	 * @return
	 */
	public boolean isShotted(Duck duck){
		float duckWidth = duck.getWidth()*objectFactor;
		float duckHeight = duck.getHeight()*objectFactor;
		float crosshairWidth = crosshair.getWidth()*crosshairFactor;
		float crosshairHeight = crosshair.getHeight()*crosshairFactor;
		if(BaseCollisionChecker.checkAxisAlignedRectangleCollision(
				duck.getCenterX()-duckWidth,
				duck.getCenterY()-duckHeight, 
				duck.getCenterX()+duckWidth, 
				duck.getCenterY()+duckHeight,
				crosshair.getCenterX()-crosshairWidth, 
				crosshair.getCenterY()-crosshairHeight, 
				crosshair.getCenterX()+crosshairWidth, 
				crosshair.getCenterY()+crosshairHeight))
		{
			return true;
		}
		return false;
	}
	
	/**
	 * shoot phoenix when sniper cross hair is on top of the phoenix
	 * @param phenix
	 * @return
	 */
	public boolean isPhenixShotted(Phenix phenix){
		float duckWidth = phenix.getWidth()*objectFactor;
		float duckHeight = phenix.getHeight()*objectFactor;
		float crosshairWidth = crosshair.getWidth()*crosshairFactor;
		float crosshairHeight = crosshair.getHeight()*crosshairFactor;
		if(BaseCollisionChecker.checkAxisAlignedRectangleCollision(
				phenix.getCenterX()-duckWidth,
				phenix.getCenterY()-duckHeight, 
				phenix.getCenterX()+duckWidth, 
				phenix.getCenterY()+duckHeight,
				crosshair.getCenterX()-crosshairWidth, 
				crosshair.getCenterY()-crosshairHeight, 
				crosshair.getCenterX()+crosshairWidth, 
				crosshair.getCenterY()+crosshairHeight))
		{
			return true;
		}
		return false;
	}

	/**
	 * makes the cross hair and the cureent gun visible when game is started
	 */
	@Override
	public void startGame() {
		currentGun.setVisible(true);
		crosshair.setVisible(true);
	}
	
	/**
	 * method for playing sound of either shot gun or sniper gun
	 */
	private void playGunSound(){
		if(currentGun.equals(shotGun)){
			Game.SOUND_SHOTGUN.play();
		}else{
			Game.SOUND_SNIPERGUN.play();
		}
	}
	
	/**
	 * restart method to restart game
	 */
	@Override
	public void restart() {
		sniperCrosshair.setVisible(false);
		shotCrosshair.setVisible(false);
		sniperGun.setVisible(false);
		shotGun.setVisible(false);
		currentGun = shotGun;
		crosshair = shotCrosshair;
	}
	
}
