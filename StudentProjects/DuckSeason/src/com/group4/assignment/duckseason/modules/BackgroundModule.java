package com.group4.assignment.duckseason.modules;
import org.anddev.andengine.entity.scene.Scene;
import com.group4.assignment.duckseason.Config;
import com.group4.assignment.duckseason.Game;
import com.group4.assignment.duckseason.entites.MySprite;
/**
 * This is the Background Model of the project
 * extends MyModel
 * 
 * @version 	final version 28 June 2013
 * @author 		Terapong Thiankitthamrong
 * @modified by Hui Yin
 */
public class BackgroundModule extends MyModule{
	
	public MySprite start,settings,exit,menu,bg,lDuck,lSeason,logo,gameOver;
	
	/**
	 * Constructor for background
	 * @param scene
	 */
	public BackgroundModule(Scene scene) {
		super(scene);
		bg = new MySprite(0,0,Config.SCREEN_WIDTH,Config.SCREEN_HEIGHT,Game.BG);
		this.scene.attachChild(bg,0);
		int w = Config.SCREEN_WIDTH/2;
		int h = Config.SCREEN_HEIGHT/2;
		int x = w-(Game.LOGO_SEASON.getWidth()/2);
		int y = h-(Game.LOGO_SEASON.getHeight()/2);
		lSeason = new MySprite(x,y,Game.LOGO_SEASON.getWidth(),Game.LOGO_SEASON.getHeight(),Game.LOGO_SEASON);
		this.scene.attachChild(lSeason,1);
		lDuck = new MySprite(x,y-Game.LOGO_DUCK.getHeight()-10,Game.LOGO_DUCK.getWidth(),Game.LOGO_DUCK.getHeight(),Game.LOGO_DUCK);
		this.scene.attachChild(lDuck,1);
		x = x+Game.LOGO_DUCK.getWidth()+20;
		y = y-20-(Game.LOGO.getHeight()/2);
		logo = new MySprite(x,y,Game.LOGO.getWidth(),Game.LOGO.getHeight(),Game.LOGO);
		this.scene.attachChild(logo,1);
		
		h = Config.SCREEN_HEIGHT*2/3;
		start = new MySprite(w-Game.START.getWidth()-(Game.SETTINGS.getWidth()/2)-5,
				h,Game.START.getWidth(),Game.START.getHeight(),Game.START);
		this.scene.attachChild(start,1);
		this.scene.registerTouchArea(start);
		settings = new MySprite(w-(Game.SETTINGS.getWidth()/2),h,Game.SETTINGS.getWidth(),
				Game.SETTINGS.getHeight(),Game.SETTINGS);
		this.scene.attachChild(settings,1);
		this.scene.registerTouchArea(settings);
		exit = new MySprite(w+(Game.EXIT.getWidth()/2)+5,h,
				Game.EXIT.getWidth(),Game.EXIT.getHeight(),Game.EXIT);
		this.scene.attachChild(exit,1);
		this.scene.registerTouchArea(exit);
		
		menu = new MySprite(Config.SCREEN_WIDTH-(Game.MENU.getWidth()+5),Config.SCREEN_HEIGHT-(Game.MENU.getHeight()+5),
				Game.MENU.getWidth(),Game.MENU.getHeight(),Game.MENU);
		menu.setVisible(false);
		this.scene.attachChild(menu,1);
		this.scene.registerTouchArea(menu);
		
		gameOver = new MySprite(w-(Game.T_GAME_OVER.getWidth()/2),(Config.SCREEN_HEIGHT/2)-(Game.T_GAME_OVER.getHeight()/2),
				Game.T_GAME_OVER.getWidth(),Game.T_GAME_OVER.getHeight(),Game.T_GAME_OVER);
		this.scene.attachChild(gameOver,1);
		gameOver.setVisible(false);
	}
	
	/**
	 * Starts the game
	 */
	@Override
	public void startGame() {
		start.setVisible(false);
		settings.setVisible(false);
		lDuck.setVisible(false);
		lSeason.setVisible(false);
		logo.setVisible(false);
		exit.setVisible(false);
		menu.setVisible(true);
		start.setPosition(0, Config.SCREEN_HEIGHT+start.getHeight());
		settings.setPosition(0,Config.SCREEN_HEIGHT+settings.getHeight());
		exit.setPosition(0,Config.SCREEN_HEIGHT+exit.getHeight());
	}
	
	/**
	 * restart method to restart game when player restart game
	 */
	@Override
	public void restart() {
		start.setVisible(true);
		settings.setVisible(true);
		exit.setVisible(true);
		lDuck.setVisible(true);
		lSeason.setVisible(true);
		logo.setVisible(true);
		menu.setVisible(false);
		exit.initXY();
		start.initXY();
		settings.initXY();
		gameOver.setVisible(false);
	}
	
	/**
	 * makes Game Over image visible when the game is over
	 */
	public void showGameOver(){
		gameOver.setVisible(true);
	}

}
