package com.group4.assignment.duckseason;

import java.io.IOException;

import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.sensor.accelerometer.AccelerometerData;
import org.anddev.andengine.sensor.accelerometer.IAccelerometerListener;
import org.anddev.andengine.ui.activity.LayoutGameActivity;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.group4.assignment.duckseason.modules.ScoreModule;
/**
 * This is the main activity of project
 * extends LayoutGameActivity
 * 
 * @version 	final version 28 June 2013
 * @author 		Hui Yin
 */
public class MainActivity extends LayoutGameActivity implements IAccelerometerListener,OnClickListener{

	private Camera camera;
	private BitmapTextureAtlas initTexture,gameTexture,fontTexture;
	private String path;
	private Game game;
	private LinearLayout settings,buttons,saveScore,scoreButtons,information;
	private RelativeLayout gameMenu;
	public DbOperator dbOperator;
	/**
     * load all buttons,layout from activity_main.xml.
     * Create database operator.
     * @param savedInstanceState
     */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dbOperator = new DbOperator(this.getApplicationContext());
		settings =  (LinearLayout)this.findViewById(R.id.settings);
		buttons = (LinearLayout)settings.findViewById(R.id.buttons);
		Button saveBtn = (Button)buttons.findViewById(R.id.saveBtn);
		Button closeBtn = (Button)buttons.findViewById(R.id.closeBtn);
		saveBtn.setOnClickListener(this);
		closeBtn.setOnClickListener(this);
		
		gameMenu = (RelativeLayout)this.findViewById(R.id.gameMenu);
		
		Button menuAbout = (Button)gameMenu.findViewById(R.id.btnAbout);
		Button menuHelp = (Button)gameMenu.findViewById(R.id.btnHelp);
		
		Button menuResume = (Button)gameMenu.findViewById(R.id.menuCloseBtn);
		Button menuRestart = (Button)gameMenu.findViewById(R.id.menuRestart);
		
		Button menuSaveScore = (Button)gameMenu.findViewById(R.id.menuSaveScore);
		Button mainBtn = (Button)gameMenu.findViewById(R.id.menuExitBtn);
		
		menuSaveScore.setOnClickListener(this);
		menuResume.setOnClickListener(this);
		mainBtn.setOnClickListener(this);
		menuAbout.setOnClickListener(this);
		menuHelp.setOnClickListener(this);
		menuRestart.setOnClickListener(this);
		
		
		saveScore =  (LinearLayout)this.findViewById(R.id.saveScore);
		scoreButtons = (LinearLayout)saveScore.findViewById(R.id.scoreButtons);
		Button scoreSaveBtn = (Button)scoreButtons.findViewById(R.id.scoreSaveBtn);
		Button scoreCloseBtn = (Button)scoreButtons.findViewById(R.id.scoreCloseBtn);
		scoreSaveBtn.setOnClickListener(this);
		scoreCloseBtn.setOnClickListener(this);
		
		information = (LinearLayout)this.findViewById(R.id.information);
		Button infoClose = (Button)information.findViewById(R.id.info_close);
		infoClose.setOnClickListener(this);
		
	}
	@Override
	public void onLoadComplete() {
	}
	/**
     * load engine and initialize screen size.
     */
	@Override
	public Engine onLoadEngine() {
		DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
		Config.SCREEN_WIDTH = displayMetrics.widthPixels;
		Config.SCREEN_HEIGHT = displayMetrics.heightPixels;
		Config.initConfigration();
		path = Config.IMG_PATH;
		this.camera = new Camera(0,0,Config.SCREEN_WIDTH,Config.SCREEN_HEIGHT);
		RatioResolutionPolicy resolution = new RatioResolutionPolicy(
				Config.SCREEN_WIDTH,Config.SCREEN_HEIGHT);
		EngineOptions options = new EngineOptions(true,ScreenOrientation.LANDSCAPE,resolution,this.camera);
		options.setNeedsSound(true);
		options.setNeedsMusic(true);
		return new Engine(options);
	}
	/**
     * load all Textures and sounds resources from folder assets.
     */
	@Override
	public void onLoadResources() {
		int gap = 0;
		this.initTexture = new BitmapTextureAtlas(Config.MEMORY,Config.MEMORY,TextureOptions.BILINEAR);
		int postionY = 0;
		Game.BG = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.initTexture, this, path+"bg.png", 0, postionY);
		postionY+=Game.BG.getHeight()+gap;
		Game.LOGO_DUCK = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.initTexture, this, path+"duck.png", 0, postionY);
		postionY+=Game.LOGO_DUCK.getHeight()+gap;
		Game.LOGO_SEASON = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.initTexture, this, path+"season.png", 0, postionY);
		postionY+=Game.LOGO_SEASON.getHeight()+gap;
		Game.LOGO = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.initTexture, this, path+"logo.png", 0, postionY);
		postionY+=Game.LOGO.getHeight()+gap;
		Game.START = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.initTexture, this, path+"btnStart.png", 0, postionY);
		postionY+=Game.START.getHeight()+gap;
		Game.SETTINGS = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.initTexture, this, path+"btnSetting.png", 0, postionY);
		postionY+=Game.SETTINGS.getHeight()+gap;
		Game.EXIT = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.initTexture, this, path+"btnExit.png", 0, postionY);
		Game.MENU = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.initTexture, this, path+"btnMenu.png", Game.EXIT.getWidth()+gap, postionY);
		postionY+=Game.EXIT.getHeight()+gap;
		Game.T_GAME_OVER = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.initTexture, this, path+"gameover.png", 0, postionY);
		postionY+=Game.T_GAME_OVER.getHeight()+gap;
		Game.DUCK_MISS = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.initTexture, this, path+"duck_grey.png", 0, postionY);
		Game.DUCK_NORMAL = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.initTexture, this, path+"duck_yeelow.png", Game.DUCK_MISS.getWidth()+gap, postionY);
		
		this.gameTexture = new BitmapTextureAtlas(Config.MEMORY,Config.MEMORY,TextureOptions.BILINEAR);
		postionY = 0;
		Game.SHOT_CROSSHAIR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.gameTexture, this, path+"crosshair.png", 0, postionY);
		Game.SNIPER_CROSSHAIR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.gameTexture, this, path+"crosshair_sniper.png", Game.SHOT_CROSSHAIR.getWidth()+gap, postionY);
		postionY += Game.SHOT_CROSSHAIR.getHeight()+gap;
		Game.DUCK_REGION = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				this.gameTexture, this, path+"duckYellow.png", 0, postionY,4,4);
		postionY += Game.DUCK_REGION.getHeight()+gap;
		Game.SHOTGUN = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.gameTexture, this, path+"gun.png", 0, postionY);
		postionY += Game.SHOTGUN.getHeight()+gap;
		Game.SNIPERGUN = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.gameTexture, this, path+"sniper.png", 0, postionY);
		postionY += Game.SNIPERGUN.getHeight()+gap;
		Game.PHENIX_REGION = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				this.gameTexture, this, path+"phoenix.png", 0, postionY,8,1);
		
		
		fontTexture = new BitmapTextureAtlas(256,256,TextureOptions.BILINEAR);
		Game.SCORE_FONT = new Font(this.fontTexture, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 
				Config.FONT_SIZE, true, Color.WHITE);
		
		this.mEngine.getTextureManager().loadTexture(fontTexture);
		this.mEngine.getFontManager().loadFont(Game.SCORE_FONT);
		
		this.mEngine.getTextureManager().loadTexture(initTexture);
		this.mEngine.getTextureManager().loadTexture(gameTexture);
		
		try {
			SoundFactory.setAssetBasePath(Config.SOUND_PATH);
			Game.SOUND_SHOTGUN = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "shotgun.wav");
			Game.SOUND_SNIPERGUN = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "snipergun.wav");
			Game.SOUND_DUCK = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "duck.ogg");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
     * create scene and game and registerUpdateHandler.
     */
	@Override
	public Scene onLoadScene() {
		final Scene scene = new Scene();
		game = new Game(scene,this);
		scene.registerUpdateHandler(new IUpdateHandler()
		{
			public void onUpdate(float secondsElapsed) {
				game.update();
			}
			public void reset() {
				
			}			
		});
		this.mEngine.enableAccelerometerSensor(this, this);
		return scene;
	}
	/**
     * set name of xml file.
     */
	@Override
	protected int getLayoutID() {
		return R.layout.activity_main;
	}
	/**
     * set name of surfaceView.
     */
	@Override
	protected int getRenderSurfaceViewID() {
		return R.id.andengine_surface;
	}
	/**
     * pass Accelerometer Data to class game.
     */
	@Override
	public void onAccelerometerChanged(AccelerometerData accData) {
		if(game!=null)
			game.onAccelerometerChanged(accData);
	}
	/**
     * Deal with all button click event of menus.
     */
	@Override
	public void onClick(View view) {
		//for main menu
		if( view.getId() == R.id.saveBtn ){
			Config.PLAY_SOUND = ((CheckBox)findViewById(R.id.settSoundEnable)).isChecked();
			Config.ACCELEROMETER = ((RadioButton)findViewById(R.id.useAccelerometer)).isChecked();
			Config.TOUCH_SCREEN = ((RadioButton)findViewById(R.id.useTouchScreen)).isChecked();
			settings.setVisibility(View.GONE);
		}
		if( view.getId() == R.id.closeBtn ){
			settings.setVisibility(View.GONE);
		}
		
		//for game menu
		if( view.getId() == R.id.btnAbout ){
			gameMenu.setVisibility(View.GONE);
			setInformation(Config.INFO_TITLE_ABOUT);
			information.setVisibility(View.VISIBLE);
		}
		if( view.getId() == R.id.btnHelp ){
			gameMenu.setVisibility(View.GONE);
			setInformation(Config.INFO_TITLE_HELP);
			information.setVisibility(View.VISIBLE);
		}
		//resume
		if( view.getId() == R.id.menuCloseBtn ){
			gameMenu.setVisibility(View.GONE);
			game.resumeGame();
		}
		//restart
		if( view.getId() == R.id.menuRestart ){
			gameMenu.setVisibility(View.GONE);
			game.restart(true);
		}
		//save score
		if( view.getId() == R.id.menuSaveScore ){
			gameMenu.setVisibility(View.GONE);
			game.showSaveScore();
		}
		//back to main menu
		if( view.getId() == R.id.menuExitBtn ){
			gameMenu.setVisibility(View.GONE);
			game.restart(false);
		}
		
		
		
		
		
		// for save score
		if(view.getId() == R.id.scoreSaveBtn){
			EditText name = ((EditText)findViewById(R.id.playerName));
			dbOperator.insertRow(name.getText().toString(),ScoreModule.TOTAL_SCORE);
			saveScore.setVisibility(View.GONE);
			gameMenu.setVisibility(View.VISIBLE);
		}
		if(view.getId() == R.id.scoreCloseBtn){
			saveScore.setVisibility(View.GONE);
			gameMenu.setVisibility(View.VISIBLE);
		}
		
		// for close about or help page
		if(view.getId() == R.id.info_close){
			information.setVisibility(View.GONE);
			gameMenu.setVisibility(View.VISIBLE);
		}
		
	}
	/**
     * set information to layout after clicking about and help buttons
     */
	private void setInformation(String title){
		TextView t = ((TextView)information.findViewById(R.id.info_title));
		TextView content = ((TextView)information.findViewById(R.id.info_content));
		if(title.equals(Config.INFO_TITLE_ABOUT)){
			t.setText(Config.INFO_TITLE_ABOUT);
			content.setText(Config.INFO_CONTENT_ABOUT);
		}else{
			t.setText(Config.INFO_TITLE_HELP);
			content.setText(Config.INFO_CONTENT_HELP);
		}
	}

}
