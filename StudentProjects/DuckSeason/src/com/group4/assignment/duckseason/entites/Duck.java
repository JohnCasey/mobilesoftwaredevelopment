package com.group4.assignment.duckseason.entites;
import java.util.Random;

import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

import com.group4.assignment.duckseason.Config;
import com.group4.assignment.duckseason.Game;
import com.group4.assignment.duckseason.modules.LifeBarModule;
/**
 * This is the duck entity of the project
 * extends AnimalsModule
 * 
 * @version 	final version 28 June 2013
 * @author 		Pritesh Mistry
 * @modified by Hui Yin
 */
public class Duck extends AnimatedSprite{
	private boolean falling = false;
	private boolean fallen = false;
	private boolean flying = true;
	private boolean flyAway = false;
	
	private int deadRotation = 0;
	private float xSpeed = 1, ySpeed = 0;
	private int speed = 4;
	private Random r = new Random();
	private final long creationTime;
	private long pauseTime=0;
	private final long lifeTime = 10*1000;//10 secs
	
	/**
	 * Constructor for duck
	 * @param px
	 * @param py
	 * @param width
	 * @param height
	 * @param textureRegion
	 */
	public Duck(float px, float py, float width, float height,
			TiledTextureRegion textureRegion) {
		super(px, py, width, height, textureRegion);
		creationTime = System.currentTimeMillis();
		
		if(r.nextBoolean())
		{
			ySpeed = 1;
		}
		else
		{
			ySpeed = -1;
		}
	}
	
	/**
	 * get the speed of the duck
	 * @return
	 */
	public float getXSpeed() {
		return xSpeed;
	}
	
	/**
	 * set the speed of the duck
	 * @param speed
	 */
	public void setXSpeed(float speed) {
		xSpeed = speed;
	}
	
	/**
	 * updates the life bar when the duck flies away and status
	 * @param lifeBar
	 */
	public void update(LifeBarModule lifeBar)
	{
		float currX = getX();
		float currY = getY();

		if(System.currentTimeMillis() - creationTime-pauseTime > lifeTime && isFlying())
		{
			setFlyAway(true);
			lifeBar.updateLifeBar();
		}
		
		if(flying)
		{
			currX = currX + speed * xSpeed;
			if(currX<0)
			{
				currX = 0;
				getTextureRegion().setFlippedHorizontal(!getTextureRegion().isFlippedHorizontal());
				xSpeed=-xSpeed;		
			}
			else if(currX+getWidth()>Config.SCREEN_WIDTH) 
			{
				currX = Config.SCREEN_WIDTH-getWidth();
				getTextureRegion().setFlippedHorizontal(!getTextureRegion().isFlippedHorizontal());
				xSpeed=-xSpeed;
			}
			
			currY = currY + speed * ySpeed;
			if(currY<0)
			{
				currY = 0;
				ySpeed=-ySpeed;		
			}
			else if(currY+getHeight()>Config.GAME_HEIGHT) 
			{
				currY = Config.GAME_HEIGHT-getWidth();
				ySpeed=-ySpeed;
			}
			
			int rotCoeff = 1;
			
			if(xSpeed>0)
			{
				rotCoeff = 1;
			}
			else
			{
				rotCoeff = -1;
			}
			
			if(ySpeed>0)
			{
				setRotation(15*rotCoeff);
			}
			else
			{
				setRotation(-15*rotCoeff);
			}
			
			
			setPosition(currX, currY);
		}
		else if(flyAway)
		{
			ySpeed =Math.abs(ySpeed)*-1;
			
			currX = currX + speed * xSpeed*2;
			currY = currY + speed * ySpeed*2;
			
			if(currX+getWidth()<0)
			{
				fallen = true;		
			}else if(currX>Config.SCREEN_WIDTH) 
			{
				fallen = true;
			}
			
			if(currY+getHeight()<0)
			{
				fallen = true;
			}
			
			int rotCoeff = 1;
			if(xSpeed>0)
			{
				rotCoeff = 1;
			}
			else
			{
				rotCoeff = -1;
			}
			setRotation(-15*rotCoeff);
			setPosition(currX, currY);
		}
		else if (falling)
		{
			currX = currX + 2 * xSpeed;
			if(currX+getWidth()<0)
			{
				currX = Config.SCREEN_WIDTH;			
			}else if(currX>Config.SCREEN_WIDTH) 
			{
				currX = 0-getWidth();
			}
			
			currY +=10;
			
			if(currY > Config.SCREEN_HEIGHT*1.4f)
			{
				currY = Config.SCREEN_HEIGHT*1.4f;
				
				fallen = true;
				falling = false;
			}
			
			setPosition(currX, currY);
			deadRotation += 5*xSpeed;
			setRotation(deadRotation);
		}
		
	}
	
	/**
	 * check if duck is falling
	 * @return
	 */
	public boolean isFalling() {
		return falling;
	}

	/**
	 * set duck falling
	 * @param falling
	 */
	public void setFalling(boolean falling) {
		this.falling = falling;
		
		if(falling)
		{
			flying = false;
		}
	}

	/**
	 * gets the fallen duck
	 * @return
	 */
	public boolean isFallen() {
		return fallen;
	}

	/**
	 * set fallen duck
	 * @param fallen
	 */
	public void setFallen(boolean fallen) {
		this.fallen = fallen;
	}

	/**
	 * gets duck is flying
	 * @return
	 */
	public boolean isFlying() {
		return flying;
	}

	/**
	 * set duck flying
	 * @param flying
	 */
	public void setFlying(boolean flying) {
		this.flying = flying;
	}

	/**
	 * gets duck flying away
	 * @param flyAway
	 */
	public void setFlyAway(boolean flyAway) {
		this.flyAway = flyAway;
		
		if(flyAway)
		{
			animate(new long[]{100,100,100,100}, 4, 7, true);
			if(Config.PLAY_SOUND)Game.SOUND_DUCK.play();
			
			flying = false;
		}
	}
	
	/**
	 * gets time 10 second
	 * @return
	 */
	public float timeLeft() {
		return (System.currentTimeMillis() - creationTime-pauseTime)/1000f;
	}
	
	/**
	 * set the time to 10 seconds
	 * @return
	 */
	public float getLifeTime() {
		return lifeTime/1000f;
	}

	/**
	 * get the time when the game is paused
	 * @return
	 */
	public long getPauseTime() {
		return pauseTime;
	}

	/**
	 * set the time when the game is paused
	 * @param pauseTime
	 */
	public void setPauseTime(long pauseTime) {
		this.pauseTime = pauseTime;
	}
	
	/**
	 * adds pause time
	 * @param pauseTime
	 */
	public void addPauseTime(long pauseTime)
	{
		this.pauseTime+=pauseTime;
	}
	
	/**
	 * gets the duck center point horizontal
	 * @return
	 */
	public float getCenterX(){
		return this.getX()+this.getWidth()/2;
	}
	
	/**
	 * gets the duck center point vertical
	 * @return
	 */
	public float getCenterY(){
		return this.getY()+this.getHeight()/2;
	}

}
