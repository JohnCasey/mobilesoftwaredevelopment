package com.group4.assignment.duckseason.entites;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
/**
 * This class extends Sprite and 
 * add some methods which are frequently used.
 * 
 * @version 	final version 28 June 2013
 * @author 		Hui Yin
 */
public class MySprite extends Sprite{
	protected float initX;
	protected float initY;
	public MySprite(float pX, float pY, float pWidth, float pHeight,
			TextureRegion pTextureRegion) {
		super(pX, pY, pWidth, pHeight, pTextureRegion);
		initX = pX;
		initY = pY;
	}
	/**
     * get center X value of object 
     */
	public float getCenterX(){
		return this.getX()+this.getWidth()/2;
	}
	/**
     * get center Y value of object 
     */
	public float getCenterY(){
		return this.getY()+this.getHeight()/2;
	}
	/**
     * set center x and y value.
     */
	public void setCenterPosition(float x,float y){
		this.setPosition(x-this.getWidth()/2,
				y-this.getHeight()/2);
	}
	/**
     * initialize X and y.
     */
	public void initXY(){
		this.setPosition(initX, initY);
	}
}
