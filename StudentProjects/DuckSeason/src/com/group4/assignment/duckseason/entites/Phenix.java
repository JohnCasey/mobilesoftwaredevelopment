package com.group4.assignment.duckseason.entites;

import java.util.Random;

import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

import com.group4.assignment.duckseason.Config;
import com.group4.assignment.duckseason.Game;
/**
 * This is the phoenix entity of the project
 * extends AnimalsModule
 * 
 * @version 	final version 28 June 2013
 * @author 		Anqi Wang
 * @modified by Hui Yin
 */
public class Phenix extends AnimatedSprite{
	public static int FRAME = 200;
	private boolean falling = false;
	private boolean fallen = true;
	private boolean flying = false;
	private boolean flyAway = false;
	private boolean isReady = false;
	private long showTime;
	private float xSpeed = 1;
	private int speed = 6;
	private int deadRotation = 0;
	private Random random = new Random();
	private long creationTime;
	private long pauseTime=0;
	
	/**
	 * Constructor for phoenix
	 * @param pX
	 * @param pY
	 * @param pTileWidth
	 * @param pTileHeight
	 * @param pTiledTextureRegion
	 */
	public Phenix(float pX, float pY, float pTileWidth, float pTileHeight,
			TiledTextureRegion pTiledTextureRegion) {
		super(pX, pY, pTileWidth, pTileHeight, pTiledTextureRegion);
	}
	
	/**
	 * update position of phoenix
	 */
	public void update(){
		float currX = getX();
		float currY = getY();
		if(flying){
			currX = currX + speed * xSpeed;
			setPosition(currX,currY);
			if( currX>Config.SCREEN_WIDTH ){
				this.flyAway = true;
				flying = false;
			}
		}else if(falling){
			currX = currX + 2 * xSpeed;
			currY +=10;
			if(currY > Config.SCREEN_HEIGHT*1.4f)
			{
				currY = Config.SCREEN_HEIGHT*1.4f;
				
				flyAway = true;
				falling = false;
			}
			
			setPosition(currX, currY);
			deadRotation += 5*xSpeed;
			setRotation(deadRotation);
		}else if(flyAway){
			setShowTime();
			creationTime = System.currentTimeMillis();
			flyAway = false;
			fallen = true;
		}else if(fallen && isReady){
			if(System.currentTimeMillis() - creationTime-pauseTime>showTime)
			{
				startFly();
			}
		}
	}
	
	/**
	 * method for to start the phoenix fly on the canvas
	 */
	public void startFly(){
		if(fallen){
			flying = true;
			fallen = false;
			falling = false;
			flyAway = false;
			setPosition(0-Game.PHENIX_REGION.getTileWidth(),random.nextInt(40));
			int f = Phenix.FRAME;
			animate(new long[]{f,f,f,f,f,f}, 2, 7, true);
			deadRotation = 0;
			setRotation(deadRotation);
		}
	}
	
	/**
	 * reset the position of the phoenix
	 */
	public void restart(){
		flying = false;
		fallen = true;
		falling = false;
		flyAway = false;
		setPosition(0-Game.PHENIX_REGION.getTileWidth(),random.nextInt(40));
		int f = Phenix.FRAME;
		animate(new long[]{f,f,f,f,f,f}, 2, 7, true);
		deadRotation = 0;
		setRotation(deadRotation);
		isReady = false;
	}
	
	/**
	 * start the time
	 */
	public void start(){
		setShowTime();
		creationTime = System.currentTimeMillis();
		isReady = true;
	}
	
	/**
	 * returns phoenix if fallen
	 * @return
	 */
	public boolean isFallen(){
		return this.fallen;
	}
	
	/**
	 * returns the center horizontal point of the phoenix
	 * @return
	 */
	public float getCenterX(){
		return this.getX()+this.getWidth()/2;
	}
	
	/**
	 * return vertical point of the phoenix
	 * @return
	 */
	public float getCenterY(){
		return this.getY()+this.getHeight()/2;
	}
	
	/**
	 * sets the speed of the phoenix
	 * @param speed
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	/**
	 * set falling point of the phoenix
	 * @param falling
	 */
	public void setFalling(boolean falling) {
		this.falling = falling;
		
		if(falling)
		{
			flying = false;
		}
	}
	
	/**
	 * pause the phoenix when the game is paused
	 * @param pauseTime
	 */
	public void addPauseTime(long pauseTime)
	{
		this.pauseTime+=pauseTime;
	}
	
	/**
	 * set time of the phoenix randomly between 60 - 90 seconds
	 */
	public void setShowTime(){
		showTime = 10*1000*(random.nextInt(4)+6);
	}

}
