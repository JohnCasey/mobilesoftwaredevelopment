package com.group4.assignment.duckseason;

import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.Scene.IOnAreaTouchListener;
import org.anddev.andengine.entity.scene.Scene.IOnSceneTouchListener;
import org.anddev.andengine.entity.scene.Scene.ITouchArea;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.sensor.accelerometer.AccelerometerData;

import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.group4.assignment.duckseason.modules.AnimalsModule;
import com.group4.assignment.duckseason.modules.BackgroundModule;
import com.group4.assignment.duckseason.modules.GunsModule;
import com.group4.assignment.duckseason.modules.LifeBarModule;
import com.group4.assignment.duckseason.modules.ScoreModule;
/**
 * This is a class to call relative methods of individual modules
 * so that response users actions.
 * @version 	final version 28 June 2013
 * @author 		Hui Yin
 * @modified by	all
 */
public class Game implements IOnAreaTouchListener,IOnSceneTouchListener{
	private Scene scene;
	public static TextureRegion START,SETTINGS,EXIT,MENU,T_GAME_OVER,BG,LOGO_DUCK,LOGO_SEASON,LOGO,SHOT_CROSSHAIR,SNIPER_CROSSHAIR,SHOTGUN,SNIPERGUN;
	public static TextureRegion DUCK_MISS,DUCK_NORMAL;
	public static Sound SOUND_SHOTGUN,SOUND_SNIPERGUN,SOUND_DUCK;
	public static boolean GAME_START;
	public static boolean GAME_OVER;
	public static Font SCORE_FONT;
	private GunsModule gunsModule;
	private AnimalsModule animalsModule;
	private BackgroundModule bgModule;
	private ScoreModule scoreModule;
	private LifeBarModule lifeBarModule;
	private MainActivity activity;
	public static TiledTextureRegion DUCK_REGION,PHENIX_REGION;
	private int factor=3;
	public Game(final Scene scene,MainActivity activity){
		this.scene = scene;
		GAME_START = false;
		GAME_OVER = false;
		Config.PLAY_SOUND = true;
		this.scene.setOnSceneTouchListener(this);
		this.activity = activity;
		bgModule = new BackgroundModule(this.scene);
		scoreModule = new ScoreModule(this.scene);
		lifeBarModule = new LifeBarModule(this.scene);
		gunsModule = new GunsModule(this.scene,scoreModule);
		animalsModule = new AnimalsModule(this.scene,lifeBarModule);
		this.scene.setOnAreaTouchListener(this);
	}
	/**
     * deal with all buttons from screen.
     */
	@Override
	public boolean onAreaTouched(TouchEvent event, ITouchArea tArea, float x,
			float y) {
		if(event.getAction() == MotionEvent.ACTION_DOWN){
			if( tArea.equals(bgModule.start) ){
				startGame();
			}else if( tArea.equals(bgModule.settings) ){
				activity.findViewById(R.id.settings).setVisibility(View.VISIBLE);
			}else if(tArea.equals(bgModule.menu)){
				if( Config.TOUCH_SCREEN ){
					activity.findViewById(R.id.menuSaveScore).setEnabled(false);
				}else{
					activity.findViewById(R.id.menuSaveScore).setEnabled(true);
				}
				pauseGame();
				activity.findViewById(R.id.gameMenu).setVisibility(View.VISIBLE);
			}else{
				activity.finish();
			}
		}
		return true;
	}
	/**
     * when click start button from screen call this method
     */
	public void startGame(){
		bgModule.startGame();
		gunsModule.startGame();
		animalsModule.startGame();
		lifeBarModule.startGame();
		scoreModule.startGame();
		GAME_START = true;
	}
	/**
     * when menu button from screen that pause game, call this method
     */
	public void pauseGame(){
		animalsModule.pauseGame();
		GAME_START = false;
	}
	/**
     * resume game from pause game.
     */
	public void resumeGame(){
		animalsModule.resumeGame();
		GAME_START = true;
	}
	/**
     * when game is over, call this method.
     */
	public void gameOver(){
		this.bgModule.showGameOver();
		pauseGame();
	}
	/**
     * when flag = true, the game will restart,
     * when flag = false, the game screen will turn to main screen.
     */
	public void restart(boolean flag){
		GAME_OVER = false;
		bgModule.restart();
		gunsModule.restart();
		animalsModule.animalRestart = true;
		lifeBarModule.restart();
		scoreModule.restart();
		if(flag){
			this.startGame();
		}
	}
	/**
     * deal with accelerometer data changes.
     */
	public void onAccelerometerChanged(AccelerometerData data){
		if(Config.ACCELEROMETER && GAME_START){
			float x = data.getX();
			float y = data.getY();
			gunsModule.setCrosshairPosition(x,y,factor);
		}
	}
	/**
     * deal with shoot event and change gun event.
     */
	@Override
	public boolean onSceneTouchEvent(Scene arg0, TouchEvent event) {
		float x = event.getX();
		float y = event.getY();
		if(y>=Config.GAME_HEIGHT && event.getAction()==MotionEvent.ACTION_DOWN){
			return gunsModule.changeGunByClickGun(x,y);
		}
		if(event.getAction()==MotionEvent.ACTION_DOWN){
			if(!GAME_START){
				return false;
			}
			if(Config.TOUCH_SCREEN){
				gunsModule.setCrosshairPosition(x, y);
				gunsModule.shoot(animalsModule.ducks,animalsModule.phenix);
				return true;
			}else if(Config.ACCELEROMETER){
				gunsModule.shoot(animalsModule.ducks,animalsModule.phenix);
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	/**
     * redraw all objects in drawing thread.
     */
	public void update(){
		if(animalsModule.animalRestart){
			animalsModule.restart();
			animalsModule.animalRestart = false;
		}
		if(GAME_OVER){
			gameOver();
		}else if(GAME_START){
			if( LifeBarModule.LIFE_COUNT < 0 ){
				GAME_OVER = true;
			}
			gunsModule.rotateGun();
			animalsModule.animalsUpdate();
		}
	}
	/**
     * show save score layout to users 
     * so that users can put their score to the SQLLite.
     */
	public void showSaveScore(){
		if(!activity.findViewById(R.id.scoreSaveBtn).isEnabled()){
			activity.findViewById(R.id.scoreSaveBtn).setEnabled(true);
		}
		TextView view = ((TextView)activity.findViewById(R.id.scoreTitle));
		view.setText("Score List: "+DbOperator.BR);
		activity.dbOperator.setTextViewList(view);
		view.append(DbOperator.BR);
		view.append("Your score: ["+ScoreModule.TOTAL_SCORE+"] ");
		if(ScoreModule.TOTAL_SCORE<=DbOperator.LOWEST_SCORE){
			view.append(" is too low to save !");
			((Button)activity.findViewById(R.id.scoreSaveBtn)).setEnabled(false);
		}
		activity.findViewById(R.id.saveScore).setVisibility(View.VISIBLE);
	}
}
