package com.group4.assignment.duckseason;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
/**
 * This is class extends SQLiteOpenHelper and
 * initialize tables when install game.
 * 
 * @version 	final version 28 June 2013
 * @author 		Herman Chang
 * @modified by Hui Yin
 */
public class DbHelper extends SQLiteOpenHelper{
	
	public final static int VERSION = 1;
    public final static String NAME = "duckSeason.db";
    public final static String TABLE = "SCORE";
    public final static String SID ="SID";
    public final static String PLAYER_NAME="player_name";
    public final static String PLAYER_SCORE="player_score";
    
    public DbHelper(Context context) {
		super(context, NAME, null, VERSION);
	}
    /**
     * initialize table.
     */
	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "create table " +TABLE +" ("+SID+" integer primary key, "+PLAYER_NAME+" text,"+PLAYER_SCORE+" integer);";
        db.execSQL(sql);
	}
	/**
     * update table.
     */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		 db.execSQL("drop table if exists "+TABLE);
	     onCreate(db);
	}

}
