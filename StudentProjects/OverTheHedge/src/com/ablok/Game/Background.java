package com.ablok.Game;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;

import com.ablok.R;

public class Background {

	private final double TILEWIDTH;
	private final double TILEHEIGHT;

	private Resources resources;

	private Bitmap bgTexture;
	private int bgWidth;
	private int bgHeight;
	private Point[][] bgPoints; // background tile positions
	private double[][] xCord;
	private double[][] yCord;

	public Background(Resources resource, int sWidth, int sHeight) {
		this.resources = resource;
		bgTexture = BitmapFactory.decodeResource(resources,
				R.drawable.game_background);
		TILEWIDTH = bgTexture.getWidth();
		TILEHEIGHT = bgTexture.getHeight();
		init(sWidth, sHeight);
	}

	private void init(int sWidth, int sHeight) {
		int numWidthTile = (int) Math.ceil((sWidth / TILEWIDTH) + 1);
		int numHeightTile = (int) Math.ceil((sHeight / TILEHEIGHT) + 1);
		xCord = new double[numHeightTile][numWidthTile];
		yCord = new double[numHeightTile][numWidthTile];
		Point point;
		int y = 0;
		// th: tile height, tw: tile width
		for (int th = 0; th < numHeightTile; th++) {
			int x = 0;
			for (int tw = 0; tw < numWidthTile; tw++) {
				xCord[th][tw] = x;
				yCord[th][tw] = y;
				x += TILEWIDTH;
			}
			y += TILEHEIGHT;
		}
	}

	public void update(double xVelo, double yVelo, int sWidth, int sHeight) {

		// if -ve Moving to left
		if (xVelo < 0) {
			for (int y = 0; y < xCord.length; y++) {

				for (int x = 0; x < xCord[y].length; x++) {
					xCord[y][x] += xVelo;
					if (xCord[y][x] <= (sWidth
							- ((xCord[y].length - 1) * TILEWIDTH) - TILEWIDTH)) {
						double extra = (double) (xCord[y][x] + TILEWIDTH
								+ ((xCord[y].length - 1) * TILEWIDTH) - sWidth);
						xCord[y][x] = sWidth + extra;
					}
				}
			}

			// +ve Moving to right
		} else if (xVelo > 0) {
			for (int y = 0; y < xCord.length; y++) {
				for (int x = 0; x < xCord[y].length; x++) {
					xCord[y][x] += xVelo;
					if (xCord[y][x] >= ((xCord[y].length - 1) * TILEWIDTH)) {
						double extra = (double) (xCord[y][x] - ((xCord[y].length - 1) * TILEWIDTH));
						xCord[y][x] = (double) (-TILEWIDTH + extra);

					}
				}
			}

		}

		if (yVelo < 0) {
			for (int y = 0; y < yCord.length; y++) {

				for (int x = 0; x < yCord[y].length; x++) {
					yCord[y][x] += yVelo;
					if (yCord[y][x] <= (sHeight
							- ((yCord.length - 1) * TILEHEIGHT) - TILEHEIGHT)) {
						double extra = (double) (yCord[y][x] + TILEHEIGHT
								+ ((yCord.length - 1) * TILEHEIGHT) - sHeight);
						yCord[y][x] = sHeight + extra;
					}
				}
			}

		}
		
		else if (yVelo > 0) {
			for (int y = 0; y < yCord.length; y++) {
				for (int x = 0; x < yCord[y].length; x++) {
					yCord[y][x] += yVelo;
					if (yCord[y][x] >= ((yCord.length - 1) * TILEHEIGHT)) {
						double extra = (double) (yCord[y][x] - ((yCord.length - 1) * TILEHEIGHT));
						yCord[y][x] = (double) (-TILEHEIGHT + extra);

					}
				}
			}
		}

	}

	public void render(Canvas canvas) {
		for (int y = 0; y < yCord.length; y++) {

			for (int x = 0; x < yCord[y].length; x++) {
				canvas.drawBitmap(bgTexture, (float) xCord[y][x],
						(float) yCord[y][x], null);
			}
		}
	}

}
