package com.ablok.Game;

import com.ablok.Game.Tile.TileType;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class Tile {

	public enum TileType {
		Safe, UnSafe
	}

	public static final int TILE_SIZE = 50;
	private TileType tileType;

	private double x;
	private double y;

	public Tile(int x, int y, TileType tt) {
		this.x = x;
		this.y = y;
		tileType = tt;
	}
	
	
	public void render(Canvas canvas, double xOffset, double yOffset, Bitmap bmpGravel) {
		Paint p = new Paint();
		p.setColor(Color.WHITE);
		//canvas.drawRect(getBounds(xOffset, yOffset), p);
		canvas.drawBitmap(bmpGravel, (float)(xOffset+x),
				(float)(yOffset+y), null);
//		canvas.drawBitmap(bmpGravel, getBounds(xOffset, yOffset), null);
//		canvas.drawBitmap(bmpGravel, getBounds(xOffset, yOffset), getBounds(xOffset, yOffset), null);
		//canvas.dra
	
		
	}
	
	public Rect getBounds(double xOffset, double yOffset) {
		Rect rect = new Rect();
		rect.left = (int) (x + xOffset);
		rect.right = (int) (x + TILE_SIZE + xOffset);
		rect.top = (int) (y + yOffset);
		rect.bottom = (int) (y + TILE_SIZE + yOffset);
		return rect;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	
	
	

}
