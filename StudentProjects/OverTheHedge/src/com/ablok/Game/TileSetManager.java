package com.ablok.Game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.ablok.Game.Tile.TileType;

public class TileSetManager {

	private Tile[][] tiles;
	private String testLevel = "##########\n" + 
                               "  ###   # \n" + 
                               "   #    # \n" + 
                               "   #   ## \n" + 
                               " ###  ### \n" + 
                               " ###  ##  \n" +
                               " ###  ####\n" + 
                               "   #     #\n" + 
                               " ###     #\n" + 
                               " ###     #\n" + 
                               "     # ###\n" + 
                               "     ### #";
	
	public TileSetManager(Resources resources, String tileSet, Context context) {
		loadTileSet(resources, tileSet, context);
	}

	// need context? getPackageName check
	private void loadTileSet(Resources resources, String tileSet, Context context) {
		Log.d("tileset", tileSet);
		int rId = resources.getIdentifier("raw/"+tileSet, null, context.getPackageName());
		InputStream inputStream = resources.openRawResource(rId);
		
		InputStreamReader inputReader = new InputStreamReader(inputStream);
		BufferedReader bufferedReader = new BufferedReader(inputReader);
		String line;
		StringBuilder text = new StringBuilder();
		
		try {
			while((line = bufferedReader.readLine())!=null) {
				text.append(line);
				text.append('\n');
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		processTileset(text.toString());
		//processTileset(testLevel);
	}

	private void processTileset(String levelString) {
		String[] lines = levelString
				.split(System.getProperty("line.separator")); // process level
																// into array of
																// lines
		// get max string width
		int sWidth = 0;
		for (int i = 0; i < lines.length; i++) {
			if (lines[i].length() > sWidth)
				sWidth = lines[i].length();
		}
		tiles = new Tile[lines.length][sWidth]; // init tileset

		// init tiles
		int y = 0;
		for (String s : lines) {
			for (int x = 0; x < s.length(); x++) {
				TileType tt = getCharacterType(String.valueOf(s.charAt(x)));
				if (tt != null) { // create tile and add to tileset
					Tile tile = new Tile(x * Tile.TILE_SIZE, y
							* Tile.TILE_SIZE, tt);
					Log.d("generate tile", y+ " " + x);
					tiles[y][x] = tile;
				}
			}
			y++;
		}
		Log.d("generate tile", "finish");

	}

	private TileType getCharacterType(String sTile) {
		TileType tt = null;
		if (sTile.equals("#"))
			tt = TileType.UnSafe;
		else if (sTile.equals("S"))
			tt = TileType.Safe;
		return tt;
	}
	
	public Tile[][] getTiles() {
		return tiles;
	}
		
	

}
