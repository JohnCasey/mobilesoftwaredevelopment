package com.ablok.Game;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.util.Log;

import com.ablok.GameActivity;
import com.ablok.GameSurfaceView;
import com.ablok.R;
import com.ablok.Database.DbSource;
import com.ablok.Database.Dm_GameObject;
import com.ablok.Database.Dm_Level;
import com.ablok.Database.Keys;
import com.ablok.GameObject.Finish;
import com.ablok.GameObject.GameObject;
import com.ablok.GameObject.GameObject.GameObjectType;
import com.ablok.GameObject.Obstacle;
import com.ablok.GameObject.Obstacle.PathType;
import com.ablok.GameObject.Player;
import com.ablok.GameObject.Pt;
import com.ablok.GameObject.Start;
import com.ablok.GameObject.Wall;
import com.ablok.GameObject.Wall.WallType;

public class Level {

	// private static final float ZOOM_MAX = 2.5f;
	// private static final float ZOOM_MIN = 0.5f;
	// private float zoom = 1; // scale
	private static final int STARTTEXT_LENGTH = 4; // start text display length
	private static final int FINISHTEXT_LENGTH = 5; // finish text display
													// length
	// ____TEXT_DISPLAY___
	private boolean startText = true; // display start text
	private boolean finishText = false; // display finish text
	private int textOpacity = 225;
	private int textOpacityTick = 2; // opacity tick decrement
	private int displayCount;
	private String levelName;
	private int levelId;

	// ______OBJECT_______
	private Player player;
	private List<Wall> walls = new ArrayList<Wall>();
	private List<Obstacle> obstacles = new ArrayList<Obstacle>();
	private List<Pt> pts = new ArrayList<Pt>(); // game points
	private TileSetManager tsm;
	private Background background;
	private Start start;
	private Finish finish;

	// ________RES_________
	private Resources resources;
	private Context context;
	private GameSurfaceView gsv;
	private Bitmap bmp_gameTile; // bitmap used for tile
	private Bitmap bmp_gameWall; // bitmap used for wall
	private Bitmap bmp_gameScore;
	private Bitmap bmp_gameTrapOpen;
	private Bitmap bmp_gameTrapClosed;
	private Bitmap bmp_gameBlobRight;
	private Bitmap bmp_gameBlobRightOpen;
	private Bitmap bmp_gameBlobLeft;
	private Bitmap bmp_gameBlobLeftOpen;

	// _____TIME/SCORE_____
	private Timer timer;
	private int seconds; // game time seconds
	private int minutes; // game time minutes
	private int score = 0; // ###
	private int parSeconds;
	private int parMinutes;
	private String time; // display time string
	private int tickDecrement = 2; // score tick decrement
	private int parPoint = 200;

	// ________MISC________
	private int sWidth; // screen width
	private int sHeight; // screen height
	// add offset to tileset and gameobjects in updates and render
	private double xOffset; // amount to offset the screen by
	private double yOffset;
	private boolean finished = false;

	public Level(Context context, int id, int sWidth, int sHeight,
			Resources resources, GameSurfaceView gsv) {
		this.sWidth = sWidth;
		this.sHeight = sHeight;
		this.resources = resources;
		this.context = context;
		this.gsv = gsv;
		levelId = id;
		loadLevel();
	}

	private void loadLevel() {
		DbSource db = new DbSource(context);
		db.open();
		Dm_Level db_level = db.getLevel(levelId);
		db.close();
		// set par time
		levelName = db_level.getName();
		String[] par = db_level.getPar().split(":");
		parMinutes = Integer.parseInt(par[0]);
		parSeconds = Integer.parseInt(par[1]);
		tsm = new TileSetManager(resources, db_level.getTileset(), context);
		loadBitmaps();
		loadGameObjects();
		initTimer();
		generateWalls();
	}

	private void loadGameObjects() {
		DbSource db = new DbSource(context);
		db.open();
		List<Dm_GameObject> gameObjects = db.getLevelGameObjects(levelId);
		db.close();

		for (Dm_GameObject go : gameObjects) {
			// SEEKABLE TYPES
			if (go.getType() == Keys.GO_STATIONARY
					|| go.getType() == Keys.GO_SET
					|| go.getType() == Keys.GO_RANDOM) {
				// STATIONARY
				if (go.getType() == Keys.GO_STATIONARY) {
					Obstacle obstacle = new Obstacle(go.getX(), go.getY(),
							go.getWidth(), go.getHeight(), bmp_gameTrapOpen,
							bmp_gameTrapClosed);
					obstacles.add(obstacle);
				}
				// SET
				else if (go.getType() == Keys.GO_SET) {
					PathType pathType = null;
					if (go.getParam1() == Keys.SET_P1_BACKANDFOWARDS) {
						pathType = PathType.BackandForwards;
					} else if (go.getParam1() == Keys.SET_P1_ROTATION) {
						pathType = PathType.Rotation;
					}
					// create points from param2 string
					List<Point> points = new ArrayList<Point>();
					String[] temp = go.getParam2().split("\\)\\(");
					for (int i = 0; i < temp.length; i++) {
						temp[i] = temp[i].replace("(", "");
						temp[i] = temp[i].replace(")", "");
						temp[i] = temp[i].replace(" ", "");
					}
					for (int i = 0; i < temp.length; i++) {
						String[] cords = temp[i].split(",");
						for (int c = 0; c < cords.length; c++) {
							points.add(new Point(Integer.parseInt(cords[c]),
									Integer.parseInt(cords[c + 1])));
							c++;
						}
					}
					Obstacle obstacle = new Obstacle(points, go.getWidth(),
							go.getHeight(), go.getxVelo(), go.getyVelo(),
							pathType, bmp_gameBlobRight, bmp_gameBlobRightOpen,
							bmp_gameBlobLeft, bmp_gameBlobLeftOpen);
					obstacles.add(obstacle);
				}
				// RANDOM
				else if (go.getType() == Keys.GO_RANDOM) {
					// random not implemented yet
				}

			}
			// PLAYER
			else if (go.getType() == Keys.GO_PLAYER) {
				player = new Player(sWidth / 2, sHeight / 2, resources);
			}
			// START
			else if (go.getType() == Keys.GO_START) {
				int x = go.getX();
				int y = go.getY();
				start = new Start(x, y);
				setStart();
			}
			// FINISH
			else if (go.getType() == Keys.GO_FINISH) {
				int x = go.getX();
				int y = go.getY();
				finish = new Finish(x, y, resources);
			}
			// POINT
			else if (go.getType() == Keys.GO_POINT) {
				int x = go.getX();
				int y = go.getY();
				int point = go.getParam1();
				Pt pt = new Pt(x, y, point, bmp_gameScore);
				pts.add(pt);
			}
		}
		background = new Background(resources, sWidth, sHeight);
	}

	private void initTimer() {
		timer = new Timer();
		// create and set score timer
		timer.schedule(new TimerTask() {
			public void run() {
				if (minutes != 0) {
					time = String.format("%d:%02d", minutes, seconds);
				} else {
					time = String.format("0:%02d", seconds);
				}
				seconds++;
				if (seconds >= 60) {
					minutes++;
					seconds = 0;
				}
				score -= tickDecrement;
				if (score < 0) {
					score = 0;
				}
			}
		}, 0, 1000);
		// create and set start text timer
		Timer startTimer = new Timer();
		displayCount = 0;
		startTimer.schedule(new TimerTask() {
			public void run() {
				displayCount++;
				if (displayCount >= STARTTEXT_LENGTH) {
					this.cancel();
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {
						public void run() {
							textOpacity -= textOpacityTick;
							if (textOpacity <= 0) {
								this.cancel();
								textOpacity = 0;
								startText = false;
							}
						}
					}, 0, 1);
				}
			}
		}, 0, 1000);
	}

	// intialize / reset timer/score
	public void setTimer() {
		seconds = 0;
		minutes = 0;
		score = 0;
		score += parPoint;
		score += (parMinutes * 60) * tickDecrement;
		score += parSeconds * tickDecrement;
	}

	public Player getPlayer() {
		return player;
	}

	public Timer getTimer() {
		return timer;
	}

	// images that only need to be loaded once opposed to be loaded for each
	// object
	private void loadBitmaps() {
		Bitmap bmp = BitmapFactory.decodeResource(resources,
				R.drawable.game_grass);
		bmp_gameTile = Bitmap.createScaledBitmap(bmp, 50, 50, false);
		bmp = BitmapFactory.decodeResource(resources, R.drawable.game_hedge);
		bmp_gameWall = Bitmap.createScaledBitmap(bmp, 15, 15, false);
		bmp = BitmapFactory.decodeResource(resources,
				R.drawable.game_peanutbutter);
		bmp_gameScore = Bitmap.createScaledBitmap(bmp, 40, 40, false);
		bmp = BitmapFactory.decodeResource(resources, R.drawable.game_trapopen);
		bmp_gameTrapOpen = Bitmap.createScaledBitmap(bmp, 35, 35, false);
		bmp = BitmapFactory.decodeResource(resources,
				R.drawable.game_trapclosed);
		bmp_gameTrapClosed = Bitmap.createScaledBitmap(bmp, 35, 35, false);

		bmp = BitmapFactory
				.decodeResource(resources, R.drawable.game_blobright);
		bmp_gameBlobRight = Bitmap.createScaledBitmap(bmp, 35, 35, false);
		bmp = BitmapFactory.decodeResource(resources,
				R.drawable.game_blobrightopen);
		bmp_gameBlobRightOpen = Bitmap.createScaledBitmap(bmp, 35, 35, false);

		bmp = BitmapFactory.decodeResource(resources, R.drawable.game_blobleft);
		bmp_gameBlobLeft = Bitmap.createScaledBitmap(bmp, 35, 35, false);
		bmp = BitmapFactory.decodeResource(resources,
				R.drawable.game_blobleftopen);
		bmp_gameBlobLeftOpen = Bitmap.createScaledBitmap(bmp, 35, 35, false);
	}

	public void render(Canvas canvas) {
		canvas.drawColor(Color.BLACK);
		background.render(canvas);
		Tile[][] tiles = tsm.getTiles();
		// render tiles
		for (int y = 0; y < tiles.length; y++) {

			for (int x = 0; x < tiles[y].length; x++) {
				if (tiles[y][x] != null) {
					tiles[y][x].render(canvas, xOffset, yOffset, bmp_gameTile);
				}
			}
		}
		// render walls
		for (Wall wall : walls) {
			wall.render(canvas, xOffset, yOffset);
		}
		for (Pt pt : pts) {
			pt.render(canvas, xOffset, yOffset);
		}
		for (Obstacle obs : obstacles) {
			obs.render(canvas, xOffset, yOffset);
		}
		finish.render(canvas, xOffset, yOffset);
		player.render(canvas);

		// LEVEL TEXT
		Paint p = new Paint();
		Typeface font = Typeface.createFromAsset(context.getAssets(),
				"SelenesHandwriting1.ttf");
		p.setTypeface(font);
		p.setTextSize(40);
		p.setColor(Color.YELLOW);
		p.setTextAlign(Align.LEFT);

		canvas.drawText("SCORE:" + score, 10, 40, p);
		p.setTextAlign(Align.RIGHT);
		canvas.drawText("TIME:" + time, sWidth - 10, 40, p);
		if (startText) {
			p.setAlpha(textOpacity);
			p.setTextAlign(Align.CENTER);
			canvas.drawText("Level" + levelId, (sWidth / 2),
					(sHeight / 2) - 230, p);
			canvas.drawText("AKA: " + levelName, (sWidth / 2),
					(sHeight / 2) - 180, p);
		} else if (finishText) {
			p.setTextAlign(Align.CENTER);
			canvas.drawText("Level Complete", (sWidth / 2),
					(sHeight / 2) - 230, p);
		}
	}

	public void update() {
		player.update();
		updateGO(); // Game Object - WALLS, FP
		updateMGO(); // Moving Game Object
		if (finished) {
			setFinish();
		}
		// MGO unessecery to check collision with player
	}

	// update obstacle locations. check obstacle collisions
	private void updateMGO() {
		boolean hit = false;
		for (Obstacle obs : obstacles) {
			obs.update(xOffset, yOffset, player.getBounds(), walls);
			if (player.getBounds().intersect(obs.getBounds(xOffset, yOffset))) {
				if (obs.getAlignment() == com.ablok.GameObject.Obstacle.Alignment.Negative) {
					hit = true;
					if (obs.getGameObjectType() == GameObjectType.Stationary
							|| obs.getGameObjectType() == GameObjectType.Set) {
						obs.triggerTrap();
					}
					// break;
				}
			}
		}
		for (Pt pt : pts) {
			if (player.getBounds().intersect(pt.getBounds(xOffset, yOffset))) {
				if (pt.isActive()) {
					// pt.setActive(false);
					score += pt.getPts();
				}
			}
		}
		if (hit) {
			player.setColor(Color.YELLOW);
			// fade player out, fade in at start position, reset score timer and
			// point items
			player.fadeOut(this, true);
		} else {
			player.setColor(Color.GREEN);
		}
		if (player.getBounds().intersect(finish.getBounds(xOffset, yOffset))) {
			Timer finishTimer = new Timer();
			finishText = true;
			displayCount = 0;
			player.fadeOut(this, false);
			finishTimer.schedule(new TimerTask() {
				public void run() {
					displayCount++;
					if (displayCount >= FINISHTEXT_LENGTH) {
						this.cancel();
						finished = true;
					}
				}
			}, 0, 1000);
		}

	}

	// set player to start and fade in player & point items
	public void setStart() {
		// reposition level to start
		int x = (int) start.getX();
		int y = (int) start.getY();
		xOffset = getDifference(sWidth / 2, x);
		yOffset = getDifference(sHeight / 2, y);
		// reset point items
		player.fadeIn(this);
		for (Pt pt : pts) {
			pt.fadeIn();
		}
		// reset trap images
		for (Obstacle obs : obstacles) {
			if (obs.getGameObjectType() == GameObjectType.Stationary
					|| obs.getGameObjectType() == GameObjectType.Set) {
				obs.armTrap();
			}
		}
	}

	// finish level
	private void setFinish() {
		// set data to send to next activity and finish current activity
		GameActivity.setFinished(true);
		GameActivity.setScore(score);
		gsv.gsvFinish();
	}

	// update tile and wall location. check wall collisions
	private void updateGO() {

		boolean colT = false, colB = false, colL = false, colR = false;
		double oldX = xOffset; // stores old offset
		double oldY = yOffset;
		processInput();
		// can't have collision with top and bottom wall at same time ?
		// set collision type
		for (Wall wall : walls) {
			// Rect oldBounds = wall.getBounds(oldX, oldY);
			if (player.getBounds().intersect(wall.getBounds(xOffset, yOffset))) {
				// player does not collide with outer corners
				// WALL COLLISIONS
				// TOP
				if (wall.getWallType() == WallType.Top) {
					// if collision with top wall. center screen to wall.bottom
					oldY = getDifference(sHeight / 2,
							(wall.getY() + Wall.WALL_THICKNESS));
					colT = true;
				} else if (wall.getWallType() == WallType.Bottom) {
					oldY = yOffset = getDifference(sHeight / 2, wall.getY()
							- Player.PLAYER_HEIGHT);
					colB = true;
				} else if (wall.getWallType() == WallType.Left) {
					oldX = xOffset = getDifference(sWidth / 2, wall.getX()
							+ Wall.WALL_THICKNESS);
					colL = true;
				} else if (wall.getWallType() == WallType.Right) {
					colR = true;
					oldX = xOffset = getDifference(sWidth / 2, wall.getX()
							- Player.PLAYER_WIDTH);
				} // INNER CORNER COLLISIONS
					// TOP LEFT
				else if (wall.getWallType() == WallType.CornerTLI) {
					// player can't collide with 2 sides of corner at the same
					// time.
					if (oldY <= (getDifference(sHeight / 2, (wall.getY()
							+ Wall.WALL_THICKNESS - 1)))) {
						oldY = yOffset = getDifference(sHeight / 2,
								(wall.getY() + Wall.WALL_THICKNESS));
						colT = true;
					} // else if?
					if (oldX <= (getDifference(sWidth / 2, wall.getX()
							+ Wall.WALL_THICKNESS - 1))) {
						oldX = xOffset = getDifference(sWidth / 2, wall.getX()
								+ Wall.WALL_THICKNESS);
						colL = true;
					}
				}
				// TOP RIGHT
				else if (wall.getWallType() == WallType.CornerTRI) {
					if (oldY <= (getDifference(sHeight / 2, (wall.getY()
							+ Wall.WALL_THICKNESS - 1)))) {
						oldY = yOffset = getDifference(sHeight / 2,
								(wall.getY() + Wall.WALL_THICKNESS));
						colT = true;
					}
					if (oldX >= (getDifference(sWidth / 2, wall.getX()
							- Player.PLAYER_WIDTH))) {
						oldX = xOffset = getDifference(sWidth / 2, wall.getX()
								- Player.PLAYER_WIDTH);
						colR = true;
					}
				}
				// BOTTOM LEFT
				else if (wall.getWallType() == WallType.CornerBLI) {

					if (oldY >= (getDifference(sHeight / 2, wall.getY()
							- Player.PLAYER_HEIGHT))) {
						oldY = yOffset = getDifference(sHeight / 2, wall.getY()
								- Player.PLAYER_HEIGHT);
						colB = true;
					}
					if (oldX <= (getDifference(sWidth / 2, wall.getX()
							+ Wall.WALL_THICKNESS - 1))) {
						oldX = xOffset = getDifference(sWidth / 2, wall.getX()
								+ Wall.WALL_THICKNESS);
						colL = true;
					}
				}
				// BOTTOM RIGHT
				else if (wall.getWallType() == WallType.CornerBRI) {
					if (oldY >= (getDifference(sHeight / 2, wall.getY()
							- Player.PLAYER_HEIGHT))) {
						oldY = yOffset = getDifference(sHeight / 2, wall.getY()
								- Player.PLAYER_HEIGHT);
						colB = true;
					}
					if (oldX >= (getDifference(sWidth / 2, wall.getX()
							- Player.PLAYER_WIDTH))) {
						oldX = xOffset = getDifference(sWidth / 2, wall.getX()
								- Player.PLAYER_WIDTH);
						colR = true;
					}
				}
			}
		}

		// process collision type
		if (colT) {
			yOffset = oldY;
			player.setyVelo(0);
		} else if (colB) {
			yOffset = oldY;
			player.setyVelo(0);
		}
		if (colL) {
			player.setxVelo(0);
			xOffset = oldX;
		} else if (colR) {
			xOffset = oldX;
			player.setxVelo(0);
		}
		background
				.update(player.getxVelo(), player.getyVelo(), sWidth, sHeight);
	}

	// NOTE: tried to get some form of pixel perfect collision working but-
	// unsuccessful

	// public boolean checkPixelCol(Rect rect1, Rect rect2, Bitmap sprite1,
	// Bitmap sprite2) {
	// Rect colArea = getCollisionBounds(rect1, rect2);
	// // HORIZONTAL PIXELS
	// for (int h = colArea.left; h < colArea.right-1; h++) {
	// // VERTICAL PIXELS
	// for (int v = colArea.top; v < colArea.bottom-1; v++) {
	// Log.d("dskljfds","X:"+String.valueOf(h
	// -rect1.left)+" Y:"+String.valueOf(v - rect1.top));
	// int sprite1Pixel = sprite1.getPixel( v - rect1.top,h -rect1.left);
	// int sprite2Pixel = sprite2.getPixel( v - rect2.top,h -rect2.left);
	//
	// if (isFilled(sprite1Pixel) == true && isFilled(sprite2Pixel) == true) {
	// return true;
	// }
	// }
	// }
	// return false;
	// }
	//
	// // Get intersection rectangle
	// public Rect getCollisionBounds(Rect rect1, Rect rect2) {
	// Rect rectReturn = rect1;
	// return rectReturn;
	// }
	//
	// private boolean isFilled(int pixel) {
	// if (pixel == Color.TRANSPARENT || pixel == 0) {
	// Log.d("PIXEL", "transperant");
	// return false;
	// } else {
	// return true;
	// }
	// }

	public void processInput(double xVelo, double yVelo, int angle) {
		player.setxVelo(xVelo);
		player.setyVelo(yVelo);
		player.setAngle(angle);
	}

	private void processInput() {
		xOffset += player.getxVelo();
		yOffset += player.getyVelo();
	}

	// get screen offset for location
	private double getDifference(double mid, double pos) {
		if (mid > pos) {
			return Math.abs(mid - pos);
		} else {
			return -Math.abs(mid - pos);
		}
	}

	// create walls around the perimeter of the tileset.
	private void generateWalls() {
		Wall wall;
		Tile[][] tiles = tsm.getTiles();
		for (int y = 0; y < tiles.length; y++) {

			for (int x = 0; x < tiles[y].length; x++) {
				if (tiles[y][x] != null) {
					Log.d("generate wall", y + " " + x);
					// WALLS
					// Top
					// if tile is on top row or has no tile above create top
					// wall
					if (y == 0) {
						wall = new Wall(WallType.Top, tiles[y][x].getX(),
								tiles[y][x].getY(), bmp_gameWall);
						walls.add(wall); // not || statement because if tile is
											// in 0 position. calling [0-1]
											// would case an error.
					} else if (tiles[y - 1][x] == null) {
						wall = new Wall(WallType.Top, tiles[y][x].getX(),
								tiles[y][x].getY(), bmp_gameWall);
						walls.add(wall);
					}
					// Bottom
					// if tile is on bottom row or has no tile below create
					// bottom wall
					if (y == tiles.length - 1) {
						wall = new Wall(WallType.Bottom, tiles[y][x].getX(),
								tiles[y][x].getY(), bmp_gameWall);
						walls.add(wall);
					} else if (tiles[y + 1][x] == null) {
						wall = new Wall(WallType.Bottom, tiles[y][x].getX(),
								tiles[y][x].getY(), bmp_gameWall);
						walls.add(wall);
					}
					// Left
					// if tile is on left row or has no tile on left create left
					// wall
					if (x == 0) {
						wall = new Wall(WallType.Left, tiles[y][x].getX(),
								tiles[y][x].getY(), bmp_gameWall);
						walls.add(wall);
					} else if (tiles[y][x - 1] == null) {
						wall = new Wall(WallType.Left, tiles[y][x].getX(),
								tiles[y][x].getY(), bmp_gameWall);
						walls.add(wall);
					}
					// Right
					// if tile is on right row or has no tile on right create
					// right wall
					if (x == tiles[y].length - 1) {
						wall = new Wall(WallType.Right, tiles[y][x].getX(),
								tiles[y][x].getY(), bmp_gameWall);
						walls.add(wall);
					} else if (tiles[y][x + 1] == null) {
						wall = new Wall(WallType.Right, tiles[y][x].getX(),
								tiles[y][x].getY(), bmp_gameWall);
						walls.add(wall);
					}

					// OUTER CORNERS
					// Outer Top Left
					if (y == 0) {
						// can put if else block in wall if
						if (x == 0) {
							wall = new Wall(WallType.CornerTLO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						} else if (tiles[y][x - 1] == null) {
							wall = new Wall(WallType.CornerTLO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						}
					} else if (tiles[y - 1][x] == null) {
						if (x == 0) {
							wall = new Wall(WallType.CornerTLO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						} else if (tiles[y][x - 1] == null) {
							wall = new Wall(WallType.CornerTLO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						}
					}
					// Outer Top Right
					if (y == 0) {
						if (x == tiles[y].length - 1) {
							wall = new Wall(WallType.CornerTRO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						} else if (tiles[y][x + 1] == null) {
							wall = new Wall(WallType.CornerTRO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						}
					} else if (tiles[y - 1][x] == null) {
						if (x == tiles[y].length - 1) {
							wall = new Wall(WallType.CornerTRO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						} else if (tiles[y][x + 1] == null) {
							wall = new Wall(WallType.CornerTRO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						}
					}
					// Outer Bottom Left
					if (y == tiles.length - 1) {
						if (x == 0) {
							wall = new Wall(WallType.CornerBLO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						} else if (tiles[y][x - 1] == null) {
							wall = new Wall(WallType.CornerBLO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						}
					} else if (tiles[y + 1][x] == null) {
						if (x == 0) {
							wall = new Wall(WallType.CornerBLO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						} else if (tiles[y][x - 1] == null) {
							wall = new Wall(WallType.CornerBLO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						}
					}
					// Outer Bottom Right
					if (y == tiles.length - 1) {
						if (x == tiles[y].length - 1) {
							wall = new Wall(WallType.CornerBRO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						} else if (tiles[y][x + 1] == null) {
							wall = new Wall(WallType.CornerBRO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						}
					} else if (tiles[y + 1][x] == null) {
						if (x == tiles[y].length - 1) {
							wall = new Wall(WallType.CornerBRO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						} else if (tiles[y][x + 1] == null) {
							wall = new Wall(WallType.CornerBRO,
									tiles[y][x].getX(), tiles[y][x].getY(),
									bmp_gameWall);
							walls.add(wall);
						}
					}
				}
			}
			Log.d("generate wall", "next line");
		}
		Log.d("generate wall", "finish");

		// INNER CORNERS
		// adjust walls where intersect and replace with inner corner
		double adjust;
		for (int i = 0; i < walls.size() - 1; i++) {
			Wall wall1 = walls.get(i);
			for (int ii = 0; ii < walls.size() - 1; ii++) {
				Wall wall2 = walls.get(ii);
				if (wall1.getBounds(0, 0).intersect(wall2.getBounds(0, 0))) {
					// TOP RIGHT
					if (wall1.getWallType() == WallType.Right
							&& wall2.getWallType() == WallType.Top) {
						// adjust position and height of walls
						adjust = wall2.getX() + Wall.WALL_THICKNESS;
						wall2.setX(adjust);
						adjust = wall2.getWidth() - Wall.WALL_THICKNESS;
						wall2.setWidth((int) adjust);
						adjust = wall1.getHeight() - Wall.WALL_THICKNESS;
						// replace adjusted space with inner corner
						wall1.setHeight((int) adjust);
						wall = new Wall(WallType.CornerTRI, wall1.getX(),
								wall1.getY() + wall1.getHeight(), bmp_gameWall);
						walls.add(wall);
					}
					// TOP LEFT
					else if (wall1.getWallType() == WallType.Left
							&& wall2.getWallType() == WallType.Top) {
						adjust = wall2.getWidth() - Wall.WALL_THICKNESS;
						wall2.setWidth((int) adjust);
						adjust = wall1.getHeight() - Wall.WALL_THICKNESS;
						wall1.setHeight((int) adjust);
						wall = new Wall(WallType.CornerTLI, wall1.getX(),
								wall1.getY() + wall1.getHeight(), bmp_gameWall);
						walls.add(wall);
					}
					// BOTTOM LEFT
					else if (wall1.getWallType() == WallType.Left
							&& wall2.getWallType() == WallType.Bottom) {
						adjust = wall2.getWidth() - Wall.WALL_THICKNESS;
						wall2.setWidth((int) adjust);
						adjust = wall1.getHeight() - Wall.WALL_THICKNESS;
						wall1.setHeight((int) adjust);
						adjust = wall1.getY() + Wall.WALL_THICKNESS;
						wall1.setY(adjust);
						wall = new Wall(WallType.CornerBLI, wall1.getX(),
								wall1.getY() - Wall.WALL_THICKNESS,
								bmp_gameWall);
						walls.add(wall);
					}
					// BOTTOM RIGHT
					else if (wall1.getWallType() == WallType.Right
							&& wall2.getWallType() == WallType.Bottom) {
						adjust = wall2.getX() + Wall.WALL_THICKNESS;
						wall2.setX(adjust);
						adjust = wall2.getWidth() - Wall.WALL_THICKNESS;
						wall2.setWidth((int) adjust);
						adjust = wall1.getHeight() - Wall.WALL_THICKNESS;
						wall1.setHeight((int) adjust);
						adjust = wall1.getY() + Wall.WALL_THICKNESS;
						wall1.setY(adjust);
						wall = new Wall(WallType.CornerBRI, wall1.getX(),
								wall1.getY() - Wall.WALL_THICKNESS,
								bmp_gameWall);
						walls.add(wall);
					}
				}
			}
			wall1.createBitmapEx();
		}

	}

}
