package com.ablok.Game;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

import com.ablok.GameSurfaceView;

// game loop thread
public class GameThread extends Thread {

	private boolean running;
	private GameSurfaceView gameSurfaceView;
	private SurfaceHolder surfaceHolder;
	private Level level;

	public GameThread(GameSurfaceView gameSurfaceView,
			SurfaceHolder surfaceHolder, Level level) {
		this.gameSurfaceView = gameSurfaceView;
		this.surfaceHolder = surfaceHolder;
		this.level = level;
	}

	public void run() {
		Canvas canvas = null;
		while (running) {
			try {
				canvas = surfaceHolder.lockCanvas();
				synchronized (surfaceHolder) {
					level.processInput(gameSurfaceView.getxVelo(),
							gameSurfaceView.getyVelo(),gameSurfaceView.getAngle());
					level.update();
					level.render(canvas);
				}
			} finally {

				if (canvas != null) {
					surfaceHolder.unlockCanvasAndPost(canvas);
				}
			}

		}
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

}
