package com.ablok;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

public class MenuObjectives extends Activity {

	private static final int MAX_VIEW_WIDTH = 480;
	private static final int MAX_VIEW_HEIGHT = 800;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_objectives);
		// CONFIGIRE MAX SCREEN SIZE FOR MENU
				LinearLayout vLayout = (LinearLayout) findViewById(R.id.mo_vLayout);
				WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
				Display display = wm.getDefaultDisplay();
				int width = display.getWidth();
				int height = display.getHeight();
				if (width > MAX_VIEW_WIDTH && height > MAX_VIEW_HEIGHT) {
					vLayout.setLayoutParams(new LinearLayout.LayoutParams(
							MAX_VIEW_WIDTH, MAX_VIEW_HEIGHT));
				} else if (width > MAX_VIEW_WIDTH) {
					vLayout.setLayoutParams(new LinearLayout.LayoutParams(
							MAX_VIEW_WIDTH, LayoutParams.WRAP_CONTENT));
				} else if (height > MAX_VIEW_HEIGHT) {
					vLayout.setLayoutParams(new LinearLayout.LayoutParams(
							LayoutParams.FILL_PARENT, MAX_VIEW_HEIGHT));
				} // __________________________
	}

	
	
	
	
}
