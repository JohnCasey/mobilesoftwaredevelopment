package com.ablok;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

public class MenuMain extends Activity implements OnClickListener {

	private static final int MAX_VIEW_WIDTH = 480;
	private static final int MAX_VIEW_HEIGHT = 800;

	private Button btnStart;
	private Button btnScore;
	private Button btnObjective;
	private Button btnExit;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_main);
		init();
	}

	private void init() {
		// CONFIGIRE MAX SCREEN SIZE FOR MENU
		LinearLayout vLayout = (LinearLayout) findViewById(R.id.mm_vLayout);
		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();
		if (width > MAX_VIEW_WIDTH && height > MAX_VIEW_HEIGHT) {
			vLayout.setLayoutParams(new LinearLayout.LayoutParams(
					MAX_VIEW_WIDTH, MAX_VIEW_HEIGHT));
		} else if (width > MAX_VIEW_WIDTH) {
			vLayout.setLayoutParams(new LinearLayout.LayoutParams(
					MAX_VIEW_WIDTH, LayoutParams.WRAP_CONTENT));
		} else if (height > MAX_VIEW_HEIGHT) {
			vLayout.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, MAX_VIEW_HEIGHT));
		} // __________________________

		btnStart = (Button) findViewById(R.id.mm_btnStart);
		btnScore = (Button) findViewById(R.id.mm_btnScore);
		btnObjective = (Button) findViewById(R.id.mm_btnObjective);
		btnExit = (Button) findViewById(R.id.mm_btnExit);
		btnStart.setOnClickListener(this);
		btnScore.setOnClickListener(this);
		btnObjective.setOnClickListener(this);
		btnExit.setOnClickListener(this);
		
		btnStart.setTextSize(20);
		btnScore.setTextSize(20);
		btnObjective.setTextSize(20);
		btnExit.setTextSize(20);
	}

	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.mm_btnStart: {
			Intent intent = new Intent(this, MenuSelect.class);
			intent.putExtra("selectfor", MenuSelect.SEL_GAME);
			startActivity(intent);
			break;
		}
		case R.id.mm_btnScore: {
			Intent intent = new Intent(this, MenuSelect.class);
			intent.putExtra("selectfor", MenuSelect.SEL_SCORE);
			startActivity(intent);
			break;
		}
		case R.id.mm_btnObjective: {
			Intent intent = new Intent(this, MenuObjectives.class);
			startActivity(intent);
			break;
		}
		case R.id.mm_btnExit: {
			finish();
			break;
		}

		}

	}

}
