package com.ablok;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.ablok.Database.DbSource;
import com.ablok.Database.Dm_Score;

// activity not added to stack
public class MenuScore extends Activity {

	private static final int TEXT_COLOR = Color.WHITE;
	private static final int MAX_VIEW_WIDTH = 480;
	private static final int MAX_VIEW_HEIGHT = 800;
	private static final int MAX_SHOWN_SCORES = 10; // amount of scores shown on
													// high score screen
	private static final int MAX_KEPT_SCORES = 10; // total amount of scores
													// stored in database per
													// level
	public static final int FROM_GAME = 0;
	public static final int FROM_MENU = 1;
	private int from;

	private TextView[] tvNames;
	private TextView[] tvScores;
	private TableLayout tableLayout;
	private DbSource db;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_score);
		init();
	}

	private void init() {

		// CONFIGIRE MAX SCREEN SIZE FOR MENU
		LinearLayout vLayout = (LinearLayout) findViewById(R.id.msco_vLayout);
		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();
		if (width > MAX_VIEW_WIDTH && height > MAX_VIEW_HEIGHT) {
			vLayout.setLayoutParams(new LinearLayout.LayoutParams(
					MAX_VIEW_WIDTH, MAX_VIEW_HEIGHT));
		} else if (width > MAX_VIEW_WIDTH) {
			vLayout.setLayoutParams(new LinearLayout.LayoutParams(
					MAX_VIEW_WIDTH, LayoutParams.WRAP_CONTENT));
		} else if (height > MAX_VIEW_HEIGHT) {
			vLayout.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, MAX_VIEW_HEIGHT));
		} // __________________________

		Bundle bundle = getIntent().getExtras();
		from = bundle.getInt("from");
		final int levelid = bundle.getInt("levelid");
		final int score = bundle.getInt("score");
		db = new DbSource(this);
		db.open();
		List<Dm_Score> scores = db.getLevelScores(levelid, MAX_SHOWN_SCORES);

		TextView tvTitle = (TextView) findViewById(R.id.msco_lblTitle);
		tvTitle.setText("High Score : Level " + levelid);
		tvTitle.setTextSize(40);

		setTable(scores);

		if (from == MenuScore.FROM_GAME) {

			// if not max scores reached or if new high score
			if (db.getScoreCount(levelid) < MAX_KEPT_SCORES
					|| score > db.getLowestScore(levelid)) {
				db.close();

				LayoutInflater li = LayoutInflater.from(this);
				final View inputDialog = li.inflate(R.layout.input, null);

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						this);
				alertDialogBuilder.setView(inputDialog);

				final EditText userInput = (EditText) inputDialog
						.findViewById(R.id.ip_txtInput);

				final TextView scoreTxt = (TextView) inputDialog
						.findViewById(R.id.ip_lblScore);
				scoreTxt.setText("New High Score : " + String.valueOf(score));

				// set dialog message
				alertDialogBuilder
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										addScore(
												userInput.getText().toString(),
												score, levelid);
									}
								})
						.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
			LinearLayout buttonLayout = new LinearLayout(this);
			Button btnBack = new Button(this);
			btnBack.setText("Back");
			btnBack.setTextSize(20);
			btnBack.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, .60f));
			Button btnRetry = new Button(this);
			btnRetry.setText("Retry");
			btnRetry.setTextSize(20);
			btnRetry.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, .60f));
			Button btnNextLevel = new Button(this);
			btnNextLevel.setText("Next Level");
			btnNextLevel.setTextSize(20);
			btnNextLevel.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, .60f));

			btnNextLevel.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
					DbSource db = new DbSource(getApplicationContext());
					db.open();
					int id = db.getNextLevel(levelid);
					db.close();
					Intent intent = new Intent(getApplicationContext(),
							GameActivity.class);
					// ### if -1 returned show finish screen
					// finish screen not implemented
					if (id == -1) {
						id = 1;
					}
					intent.putExtra("levelid", id);
					startActivity(intent);
				}
			});
			btnRetry.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
					Intent intent = new Intent(getApplicationContext(),
							GameActivity.class);
					intent.putExtra("levelid", levelid);
					startActivity(intent);
				}
			});
			btnBack.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
					finish();
				}
			});

			buttonLayout.addView(btnBack);
			buttonLayout.addView(btnRetry);
			buttonLayout.addView(btnNextLevel);

			LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.FILL_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			layoutParam.leftMargin = 35;
			layoutParam.rightMargin = 35;

			vLayout.addView(buttonLayout, layoutParam);
		} else {
			Button btnBack = new Button(this);
			btnBack.setTextSize(20);
			btnBack.setText("Back");
			LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.FILL_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			layoutParam.leftMargin = 35;
			layoutParam.rightMargin = 35;
			vLayout.addView(btnBack, layoutParam);
			btnBack.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
					finish();
				}
			});
		}
		db.close();
	}

	private void setTable(List<Dm_Score> scores) {
		tvNames = new TextView[scores.size()];
		tvScores = new TextView[scores.size()];
		tableLayout = (TableLayout) findViewById(R.id.msco_TBL);
		tableLayout.removeAllViews();

		// HEADER ROW
		TableRow tableRow = new TableRow(this);
		TextView headerPos = new TextView(this);
		headerPos.setText("Pos");
		headerPos.setGravity(Gravity.CENTER);
		headerPos.setTextSize(30);
		TextView headerName = new TextView(this);
		headerName.setText("Name");
		headerName.setTextSize(30);
		TextView headerScore = new TextView(this);
		headerScore.setText("Score");
		headerScore.setTextSize(30);
		headerPos.setTextColor(TEXT_COLOR);
		headerName.setTextColor(TEXT_COLOR);
		headerScore.setTextColor(TEXT_COLOR);
		tableRow.addView(headerPos);
		tableRow.addView(headerName);
		tableRow.addView(headerScore);
		tableLayout.addView(tableRow);

		int i;
		for (i = 0; i < scores.size(); i++) {
			tableRow = new TableRow(this);
			TextView tvPos = new TextView(this);
			tvPos.setText(String.valueOf(i + 1));
			tvPos.setLayoutParams(new TableRow.LayoutParams(60,
					LayoutParams.WRAP_CONTENT));
			tvPos.setGravity(Gravity.CENTER);
			tvPos.setTextSize(20);
			TextView tvName = new TextView(this);
			tvName.setText(scores.get(i).getName());
			tvName.setTextSize(20);
			TextView tvScore = new TextView(this);
			tvScore.setText(String.valueOf(scores.get(i).getScore()));
			tvScore.setTextSize(20);
			tvPos.setTextColor(TEXT_COLOR);
			tvName.setTextColor(TEXT_COLOR);
			tvScore.setTextColor(TEXT_COLOR);
			tableRow.addView(tvPos);
			tableRow.addView(tvName);
			tableRow.addView(tvScore);
			tableLayout.addView(tableRow);
		}
		// blank score templates
		for (int ii = i + 1; i < MAX_SHOWN_SCORES; i++) {
			tableRow = new TableRow(this);
			TextView tvPos = new TextView(this);
			tvPos.setText(String.valueOf(i + 1));
			tvPos.setLayoutParams(new TableRow.LayoutParams(60,
					LayoutParams.WRAP_CONTENT));
			tvPos.setGravity(Gravity.CENTER);
			tvPos.setTextSize(20);

			TextView tvName = new TextView(this);
			tvName.setText("---");
			tvName.setTextSize(20);
			TextView tvScore = new TextView(this);
			tvScore.setText("---");
			tvScore.setTextSize(20);
			tvPos.setTextColor(TEXT_COLOR);
			tvName.setTextColor(TEXT_COLOR);
			tvScore.setTextColor(TEXT_COLOR);
			tableRow.addView(tvPos);
			tableRow.addView(tvName);
			tableRow.addView(tvScore);
			tableLayout.addView(tableRow);
		}

	}

	private void addScore(String name, int score, int level_id) {
		DbSource db = new DbSource(this);
		db.open();
		db.addScore(name, score, level_id, MAX_KEPT_SCORES);
		List<Dm_Score> scores = db.getLevelScores(level_id, MAX_SHOWN_SCORES);
		setTable(scores);
		db.close();
	}

}
