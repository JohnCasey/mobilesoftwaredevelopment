package com.ablok.Database;

public class Dm_Level {

	private long id;
	private String name;
	private String tileset;
	private int complete;
	private String par;
	
	public Dm_Level(long id, String name, int complete, String tileset, String par) {
		this.id = id;
		this.name = name;
		this.tileset = tileset;
		this.complete = complete;
		this.par = par;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getTileset() {
		return tileset;
	}

	public int getComplete() {
		return complete;
	}
	
	public String getPar() {
		return par;
	}
	
	
	
}
