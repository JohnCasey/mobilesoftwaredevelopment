package com.ablok.Database;

public class Dm_GameObject {

	private long id;
	private int level_id;
	private int x;
	private int y;
	private int width;
	private int height;
	private int xVelo; 
	private int yVelo;
	private int type;
	private int param1;
	private String param2;
	private int seek_id;

	public Dm_GameObject(long id, int level_id, int x, int y, int width,
			int height, int xVelo, int yVelo, int type, int param1,
			String param2, int seek_id) {
		this.id = id;
		this.level_id = level_id;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.xVelo = xVelo;
		this.yVelo = yVelo;
		this.type = type;
		this.param1 = param1;
		this.param2 = param2;
		this.seek_id = seek_id;
	}

	public long getId() {
		return id;
	}

	public int getLevel_id() {
		return level_id;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getxVelo() {
		return xVelo;
	}

	public int getyVelo() {
		return yVelo;
	}

	public int getType() {
		return type;
	}

	public int getParam1() {
		return param1;
	}

	public String getParam2() {
		return param2;
	}

	public int getSeek_id() {
		return seek_id;
	}

}
