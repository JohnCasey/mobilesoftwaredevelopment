package com.ablok.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

// check all db work
public class DbHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "database";
	private static final int DATABASE_VERSION = 1;

	// ______________TABLE GAMEOBJECT____________
	public static final String TABLE_GAMEOBJECT = "gameobject";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_LEVELID = "level_id";
	public static final String COLUMN_X = "x";
	public static final String COLUMN_Y = "y";
	public static final String COLUMN_WIDTH = "width";
	public static final String COLUMN_HEIGHT = "height";
	public static final String COLUMN_XVELO = "x_velo";
	public static final String COLUMN_YVELO = "y_velo";
	public static final String COLUMN_TYPE = "type";
	public static final String COLUMN_PARAM1 = "param1";
	public static final String COLUMN_PARAM2 = "param2";
	public static final String COLUMN_SEEKID = "seek_id";
	private static final String CREATE_TABLE_GAMEOBJECT = "create table "
			+ TABLE_GAMEOBJECT + " (" + COLUMN_ID
			+ " integer primary key autoincrement," + COLUMN_LEVELID
			+ " integer," + COLUMN_X + " integer," + COLUMN_Y + " integer,"
			+ COLUMN_WIDTH + " integer," + COLUMN_HEIGHT + " integer,"
			+ COLUMN_XVELO + " integer," + COLUMN_YVELO + " integer,"
			+ COLUMN_TYPE + " integer," + COLUMN_PARAM1 + " integer,"
			+ COLUMN_PARAM2 + " text," + COLUMN_SEEKID + " integer);";

	// ______________TABLE LEVEL_____________
	public static final String TABLE_LEVEL = "level";
	// COLUMN_ID
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_COMPLETE = "complete";
	public static final String COLUMN_TILESET = "tileset";
	public static final String COLUMN_PAR = "par";
	private static final String CREATE_TABLE_LEVEL = "create table "
			+ TABLE_LEVEL + " (" + COLUMN_ID
			+ " integer primary key autoincrement," + COLUMN_NAME + " text,"
			+ COLUMN_COMPLETE + " integer," + COLUMN_TILESET + " text,"
			+ COLUMN_PAR + " text);";

	// ______________TABLE SCORE_____________
	public static final String TABLE_SCORE = "score";
	// COLUMN_ID
	// COLUMN_LEVEL_ID
	// COLUMN_NAME
	public static final String COLUMN_SCORE = "score";
	private static final String CREATE_TABLE_SCORE = "create table "
			+ TABLE_SCORE + " (" + COLUMN_ID
			+ " integer primary key autoincrement," + COLUMN_LEVELID
			+ " integer," + COLUMN_NAME + " text," + COLUMN_SCORE
			+ " integer);";

	// _______________TABLE SEEK_______________
	public static final String TABLE_SEEK = "seek";
	// COLUMN_ID
	public static final String COLUMN_TOP = "top";
	public static final String COLUMN_BOTTOM = "bottom";
	public static final String COLUMN_LEFT = "left";
	public static final String COLUMN_RIGHT = "right";
	// COLUMN_XVELO
	// COLUMN_YVELO
	public static final String COLUMN_SEEK_HORIZONTAL = "seek_horizontal";
	public static final String COLUMN_SEEK_VERTICAL = "seek_vertical";
	private static final String CREATE_TABLE_SEEK = "create table "
			+ TABLE_SEEK + " (" + COLUMN_ID
			+ " integer primary key autoincrement," + COLUMN_TOP + " integer,"
			+ COLUMN_BOTTOM + " integer," + COLUMN_LEFT + " integer,"
			+ COLUMN_RIGHT + " integer," + COLUMN_XVELO + " integer,"
			+ COLUMN_YVELO + " integer," + COLUMN_SEEK_HORIZONTAL + " integer,"
			+ COLUMN_SEEK_VERTICAL + " integer);";

	public DbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_LEVEL);
		db.execSQL(CREATE_TABLE_GAMEOBJECT);
		db.execSQL(CREATE_TABLE_SCORE);
		db.execSQL(CREATE_TABLE_SEEK);
		for (String s : RecordSet.levels()) {
			db.execSQL("INSERT INTO " + TABLE_LEVEL + " (" + COLUMN_NAME + ", "
					+ COLUMN_COMPLETE + ", " + COLUMN_TILESET + ", "
					+ COLUMN_PAR + ") VALUES (" + s + ");");
		}
		for (String s : RecordSet.gameObjects()) {
			db.execSQL("INSERT INTO " + TABLE_GAMEOBJECT + " ("
					+ COLUMN_LEVELID + ", " + COLUMN_X + ", " + COLUMN_Y + ", "
					+ COLUMN_WIDTH + ", " + COLUMN_HEIGHT + ", " + COLUMN_XVELO
					+ ", " + COLUMN_YVELO + ", " + COLUMN_TYPE + ", "
					+ COLUMN_PARAM1 + ", " + COLUMN_PARAM2 + ", "
					+ COLUMN_SEEKID + ") VALUES (" + s + ");");
		}
		for (String s : RecordSet.scores()) {
			db.execSQL("INSERT INTO " + TABLE_SCORE + " (" + COLUMN_LEVELID
					+ ", " + COLUMN_NAME + ", " + COLUMN_SCORE + ") VALUES ("
					+ s + ");");
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DbHelper.class.getName(), "Upgrading database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LEVEL);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_GAMEOBJECT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCORE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEEK);
		onCreate(db);
	}

}
