package com.ablok.Database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DbSource {

	private SQLiteDatabase database;
	private DbHelper dbHelper;

	// _______COLUMNS______ // used for querys
	private String[] levelColumns = { DbHelper.COLUMN_ID, DbHelper.COLUMN_NAME,
			DbHelper.COLUMN_COMPLETE, DbHelper.COLUMN_TILESET,
			DbHelper.COLUMN_PAR };
	private String[] gameObjectColumns = { DbHelper.COLUMN_ID,
			DbHelper.COLUMN_LEVELID, DbHelper.COLUMN_X, DbHelper.COLUMN_Y,
			DbHelper.COLUMN_WIDTH, DbHelper.COLUMN_HEIGHT,
			DbHelper.COLUMN_XVELO, DbHelper.COLUMN_YVELO, DbHelper.COLUMN_TYPE,
			DbHelper.COLUMN_PARAM1, DbHelper.COLUMN_PARAM2,
			DbHelper.COLUMN_SEEKID };
	private String[] scoreColumns = { DbHelper.COLUMN_ID,
			DbHelper.COLUMN_LEVELID, DbHelper.COLUMN_NAME,
			DbHelper.COLUMN_SCORE };
	private String[] seekColumns = { DbHelper.COLUMN_ID, DbHelper.COLUMN_TOP,
			DbHelper.COLUMN_BOTTOM, DbHelper.COLUMN_LEFT,
			DbHelper.COLUMN_RIGHT, DbHelper.COLUMN_XVELO,
			DbHelper.COLUMN_YVELO, DbHelper.COLUMN_SEEK_HORIZONTAL,
			DbHelper.COLUMN_SEEK_VERTICAL };

	// ____________________

	public DbSource(Context context) {
		dbHelper = new DbHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		database.close();
	}

	public List<Dm_Level> getLevels() {
		List<Dm_Level> levels = new ArrayList<Dm_Level>();
		Cursor cursor = database.query(DbHelper.TABLE_LEVEL, levelColumns,
				null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Dm_Level level = new Dm_Level(cursor.getLong(0),
					cursor.getString(1), cursor.getInt(2), cursor.getString(3),
					cursor.getString(4));
			levels.add(level);
			cursor.moveToNext();
		}
		cursor.close();
		return levels;
	}

	public Dm_Level getLevel(long id) {
		Cursor cursor = database.query(DbHelper.TABLE_LEVEL, levelColumns,
				DbHelper.COLUMN_ID + " = " + id, null, null, null, null);
		cursor.moveToFirst();
		Dm_Level level = new Dm_Level(cursor.getLong(0), cursor.getString(1),
				cursor.getInt(2), cursor.getString(3), cursor.getString(4));
		cursor.close();
		return level;
	}

	public List<Dm_Score> getLevelScores(int id, int maxShown) {
		Log.d("dbSource", "Dbsource start");
		List<Dm_Score> scores = new ArrayList<Dm_Score>();
		Cursor cursor = database.query(DbHelper.TABLE_SCORE, scoreColumns,
				DbHelper.COLUMN_LEVELID + " = " + id, null, null, null,
				DbHelper.COLUMN_SCORE + " DESC");
		cursor.moveToFirst(); // add order by
		int count = 0;
		while (!cursor.isAfterLast() && maxShown > count) {
			Dm_Score score = new Dm_Score(cursor.getLong(0), cursor.getInt(1),
					cursor.getString(2), cursor.getInt(3));
			scores.add(score);
			cursor.moveToNext();
			count++;
		}
		cursor.close();
		Log.d("dbSource", "Dbsource end");
		return scores;
	}

	public List<Dm_GameObject> getLevelGameObjects(int id) {
		List<Dm_GameObject> gameObjects = new ArrayList<Dm_GameObject>();
		Cursor cursor = database.query(DbHelper.TABLE_GAMEOBJECT,
				gameObjectColumns, DbHelper.COLUMN_LEVELID + " = " + id, null,
				null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Dm_GameObject gameObject = new Dm_GameObject(cursor.getLong(0),
					cursor.getInt(1), cursor.getInt(2), cursor.getInt(3),
					cursor.getInt(4), cursor.getInt(5), cursor.getInt(6),
					cursor.getInt(7), cursor.getInt(8), cursor.getInt(9),
					cursor.getString(10), cursor.getInt(11));
			gameObjects.add(gameObject);
			cursor.moveToNext();
		}
		cursor.close();
		Log.d("dbsource", "returing game objects");
		return gameObjects;
	}

	public Dm_Seek getSeek(long id) {
		Dm_Seek seek = new Dm_Seek(id, 0, 0, 0, 0, 0, 0, 0, 0);

		return seek;
	}

	// use sqlite min?
	public int getLowestScore(int id) {
		Cursor cursor = database.query(DbHelper.TABLE_SCORE, scoreColumns,
				DbHelper.COLUMN_LEVELID + " = " + id, null, null, null,
				DbHelper.COLUMN_SCORE + " DESC");
		// last row is lowest score
		cursor.moveToLast();
		int score = cursor.getInt(3); // get score
		cursor.close();
		return score;
	}

	// use sqlite count?
	public int getScoreCount(int id) {
		Cursor cursor = database.query(DbHelper.TABLE_SCORE, scoreColumns,
				DbHelper.COLUMN_LEVELID + " = " + id, null, null, null,
				DbHelper.COLUMN_SCORE + " DESC");
		int count = cursor.getCount();
		cursor.close();
		return count;
	}

	public void addScore(String name, int score, int level_id, int maxKept) {
		// add new score
		ContentValues values = new ContentValues();
		values.put(DbHelper.COLUMN_LEVELID, level_id);
		values.put(DbHelper.COLUMN_NAME, name);
		values.put(DbHelper.COLUMN_SCORE, score);
		database.insert(DbHelper.TABLE_SCORE, null, values);

		// if over maxKept value remove loweset score
		if (getScoreCount(level_id) > maxKept) {
			Cursor cursor = database.query(DbHelper.TABLE_SCORE, scoreColumns,
					DbHelper.COLUMN_LEVELID + " = " + level_id, null, null,
					null, DbHelper.COLUMN_SCORE + " DESC");
			cursor.moveToLast();
			int id = cursor.getInt(0);
			cursor.close();
			deleteScore(id);
		}
	}

	public void deleteScore(int id) {
		database.delete(DbHelper.TABLE_SCORE, DbHelper.COLUMN_ID + " = " + id,
				null);
	}

	// if -1 returned display finish activity
	public int getNextLevel(int currentLevel) {
		Cursor cursor = database.query(DbHelper.TABLE_LEVEL, levelColumns,
				null, null, null, null, null);
		int count = cursor.getCount();
		cursor.close();
		if (currentLevel == count) {
			return -1;
		} else {
			int result = currentLevel + 1;
			return result;
		}
	}

}
