package com.ablok.Database;

public class Dm_Score {

	private long id;
	private long level_id;
	private String name;
	private int score;
	
	public Dm_Score(long id, long level_id, String name, int score) {
		this.id = id;
		this.level_id = level_id;
		this.name = name;
		this.score = score;
	}

	public long getId() {
		return id;
	}

	public long getLevel_id() {
		return level_id;
	}

	public String getName() {
		return name;
	}

	public int getScore() {
		return score;
	}
	
}
