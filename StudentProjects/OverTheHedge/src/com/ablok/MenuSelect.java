package com.ablok;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;

import com.ablok.Database.DbSource;
import com.ablok.Database.Dm_Level;

// Activity used for selecting a level for a game or score
// Retrieves and lists all levels from database
public class MenuSelect extends Activity implements OnClickListener {

	private static final int MAX_VIEW_WIDTH = 480;
	private static final int MAX_VIEW_HEIGHT = 800;
	public static final int SEL_GAME = 0;
	public static final int SEL_SCORE = 1;
	private int selectfor;

	private static final int COLS = 3; // Columns per row

	private Button btnSelect;
	private Button btnBack;
	private RadioButton[] rbLevels;
	private int[] rbLevelIds;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_select);
		init();
	}

	private void init() {
		// CONFIGIRE MAX SCREEN SIZE FOR MENU
		LinearLayout vLayout = (LinearLayout) findViewById(R.id.msel_vLayout);
		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();
		if (width > MAX_VIEW_WIDTH && height > MAX_VIEW_HEIGHT) {
			vLayout.setLayoutParams(new LinearLayout.LayoutParams(
					MAX_VIEW_WIDTH, MAX_VIEW_HEIGHT));
		} else if (width > MAX_VIEW_WIDTH) {
			vLayout.setLayoutParams(new LinearLayout.LayoutParams(
					MAX_VIEW_WIDTH, LayoutParams.WRAP_CONTENT));
		} else if (height > MAX_VIEW_HEIGHT) {
			vLayout.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, MAX_VIEW_HEIGHT));
		} // __________________________

		btnBack = (Button) findViewById(R.id.msel_btnBack);
		btnBack.setOnClickListener(this);
		btnSelect = (Button) findViewById(R.id.msel_btnSelect);
		btnSelect.setOnClickListener(this);
		TableLayout tbLevel = (TableLayout) findViewById(R.id.msel_TBL);
		TextView tvTitle = (TextView) findViewById(R.id.msel_titleBanner);

		Bundle bundle = getIntent().getExtras();
		selectfor = bundle.getInt("selectfor");

	
		tvTitle.setTextSize(40);
		btnSelect.setTextSize(20);
		btnBack.setTextSize(20);
		// set text banners and buttons
		if (selectfor == SEL_GAME) {
			tvTitle.setText("Select Level");
		} else if (selectfor == SEL_SCORE) {
			tvTitle.setText("Show Level Scores");
		}
		btnSelect.setText("Select");

		DbSource db = new DbSource(this);
		db.open();
		List<Dm_Level> levels = db.getLevels();
		db.close();

		rbLevels = new RadioButton[levels.size()];
		rbLevelIds = new int[levels.size()];

		TableRow tableRow = null;
		int columnCount = 0;
		for (int i = 0; i < levels.size(); i++) {

			// START ROW
			if ((i) % COLS == 0) {
				tableRow = new TableRow(this);
			}

			LinearLayout colLayout = new LinearLayout(this);
			colLayout.setOrientation(LinearLayout.VERTICAL);
			colLayout.setGravity(Gravity.CENTER);

			TextView colText = new TextView(this);
			RadioButton colRadio = new RadioButton(this);
			colRadio.setId(0000 + i);
			colRadio.setOnClickListener(this);
			// rbLevel.setText(levels.get(i).getName());
			colRadio.setText("");

			// StateListDrawable states = new StateListDrawable();
			// states.addState(new int[] {android.R.attr.state_checked},
			// getResources().getDrawable(R.drawable.radio_clear));
			// colRadio.setButtonDrawable(states);
			colText.setText("Level " + levels.get(i).getId());
			colText.setTextColor(Color.WHITE);
			colText.setTextSize(20);
			colText.setPadding(0, 0, 0, 20);
			rbLevels[i] = colRadio;
			rbLevelIds[i] = (int) levels.get(i).getId();

			DisplayMetrics metrics = getResources().getDisplayMetrics();
			float dp = 60f;
			float fpixels = metrics.density * dp;
			int pixels = (int) (metrics.density * dp + 0.5f);

			colRadio.setLayoutParams(new LinearLayout.LayoutParams(pixels,
					LayoutParams.WRAP_CONTENT));
			colText.setGravity(Gravity.CENTER);
			colLayout.addView(colRadio);
			colLayout.addView(colText);
			tableRow.addView(colLayout);

			// END ROW
			if ((i + 1) % COLS == 0) {
				tbLevel.addView(tableRow);
			}

			columnCount = i + 1;
		}
		// END UNFINISHED ROW
		if (!(columnCount % COLS == 0)) {
			tbLevel.addView(tableRow);
		}
		rbLevels[0].setChecked(true);
		setRadioImages();
		//rbLevels[0]
		// rbLevels[1].setEnabled(false);
	}

	public void onClick(View view) {
		// SELECT BUTTON
		if (view.getId() == btnSelect.getId()) {
			int sLevel = -1;
			for (int i = 0; i < rbLevels.length; i++) {
				if (rbLevels[i].isChecked()) {
					sLevel = rbLevelIds[i];
					break;
				}
			}
			// GAME
			if (selectfor == SEL_GAME && sLevel != -1) {
				Intent intent = new Intent(this, GameActivity.class);
				intent.putExtra("levelid", sLevel);
				startActivity(intent);
			}
			// SCORE
			else if (selectfor == SEL_SCORE && sLevel != -1) {
				Intent intent = new Intent(this, MenuScore.class);
				intent.putExtra("levelid", sLevel);
				intent.putExtra("from", MenuScore.FROM_MENU);
				startActivity(intent);
			}
		}
		// BACK BUTTON
		else if (view.getId() == btnBack.getId()) {
			finish();
		}
		// RADIO BUTTONS
		// radio group could not be used because of table format for radio
		// button layout
		else {
			for (int i = 0; i < rbLevels.length; i++) {
				if (rbLevels[i].getId() == view.getId()) {
					rbLevels[i].setChecked(true);
				} else {
					rbLevels[i].setChecked(false);
				}
			}
			setRadioImages();
		}

	}

	private void setRadioImages() {
		for (int i = 0; i < rbLevels.length; i++) {
			if (rbLevels[i].isChecked() == true) {
				StateListDrawable states = new StateListDrawable();
				states.addState(new int[] { android.R.attr.state_checked },
						getResources().getDrawable(R.drawable.radio_ticked));
				rbLevels[i].setButtonDrawable(states);
			} else {
				StateListDrawable states = new StateListDrawable();
				states.addState(new int[] { android.R.attr.state_enabled },
						getResources().getDrawable(R.drawable.radio_clear));
				rbLevels[i].setButtonDrawable(states);
			}
		}
	}

}
