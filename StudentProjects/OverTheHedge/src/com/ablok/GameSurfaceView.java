package com.ablok;

import java.text.DecimalFormat;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import com.ablok.Game.GameThread;
import com.ablok.Game.Level;

public class GameSurfaceView extends SurfaceView implements
		SurfaceHolder.Callback, SensorEventListener {

	private static final float FILTER = 0.1f;

	GameThread thread;
	Level level;
	private double xVelo;
	private double yVelo;
	private int angle;
	private double oldX = 0;
	private double oldY = 0;

	public GameSurfaceView(Context context, int levelid) {
		super(context);
		getHolder().addCallback(this);
		init(levelid);
		thread = new GameThread(this, getHolder(), level);
		setFocusable(true);
	}

	private void init(int levelid) {
		// get passed game id from intent
		// get screen size
		WindowManager wm = (WindowManager) getContext().getSystemService(
				Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		int width = display.getWidth(); // this method has been depreciated
										// API13
		int height = display.getHeight();

		level = new Level(getContext(), levelid, width, height, getResources(),
				this);

	}

	public double getxVelo() {
		return xVelo;
	}

	public double getyVelo() {
		return yVelo;
	}

	public int getAngle() {
		return angle;
	}

	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	public void surfaceCreated(SurfaceHolder arg0) {
		thread.setRunning(true);
		thread.start();
	}

	public void surfaceDestroyed(SurfaceHolder arg0) {
		level.getTimer().cancel();
		thread.setRunning(false);
		boolean retry = true;
		while (retry) {
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {

			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
			yVelo = -4;
		} else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
			yVelo = 4;
		} else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
			xVelo = -4;
		} else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
			xVelo = 4;
		} else if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			gsvFinish();

		}

		// TODO Auto-generated method stub
		// return super.onKeyDown(keyCode, event);
		return true;
	}

	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	public void onSensorChanged(SensorEvent event) {

		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

			// final float alpha = 0.8f;
			//
			// float gravity[] = new float[3];
			// float linear_acceleration[] = new float[3];
			//
			// gravity[0] = alpha * gravity[0] + (1-alpha) * event.values[0];
			// gravity[1] = alpha * gravity[1] + (1-alpha) * event.values[1];
			// gravity[2] = alpha * gravity[2] + (1-alpha) * event.values[2];
			// linear_acceleration[0] = event.values[0] - gravity[0];
			// linear_acceleration[1] = event.values[1] - gravity[1];
			// linear_acceleration[2] = event.values[2] - gravity[2];

			// assign directions
			double x = event.values[0];
			double y = event.values[1];

			x = x - (x * FILTER) + oldX * (1.0 - FILTER);
			oldX = x;
			y = y - (y * FILTER) + oldY * (1.0 - FILTER);
			oldY = y;
			
			xVelo = x/3;
			yVelo = (y * -1)/3;

			// GET & SET ANGLE USING TRIGONOMETRY
			// used for rotating player to corresponding accelerometer direction
			angle = 0;
			double xValue = x, yValue = y * -1;
			double nonNegX = xValue, nonNegY = yValue;
			if (xValue < 0) {
				nonNegX = (xValue * -1);
			}
			if (yValue < 0) {
				nonNegY = (yValue * -1);
			}
			if (xValue < 0 && yValue < 0) {
				angle = 0;
				angle += (int) Math.toDegrees((Math.atan2(nonNegY, nonNegX)));
			} else if (xValue > 0 && yValue < 0) {
				angle = 90;
				angle += (int) Math.toDegrees((Math.atan2(nonNegX, nonNegY)));
			} else if (xValue > 0 && yValue > 0) {
				angle = 180;
				angle += (int) Math.toDegrees((Math.atan2(nonNegY, nonNegX)));
			} else if (xValue < 0 && yValue > 0) {
				angle = 270;
				angle += (int) Math.toDegrees((Math.atan2(nonNegX, nonNegY)));
			}
		}
	}

	public void gsvFinish() {
		thread.setRunning(false);
		((Activity) this.getContext()).finish();
	}

}
