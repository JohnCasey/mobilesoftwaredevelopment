package com.ablok;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class MenuFinish extends Activity {

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TextView textView = new TextView(this);
		textView.setText("Finish");
		setContentView(textView);
	}

}
