package com.ablok;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

public class GameActivity extends Activity {

	private static boolean finished;
	private static int score;
	private int level_id;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		Bundle bundle = getIntent().getExtras();
		level_id = bundle.getInt("levelid");
		finished = false;
		GameSurfaceView gsv = new GameSurfaceView(this, level_id);
		setContentView(gsv);

		// accelerometer sensor
		SensorManager sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sm.registerListener(gsv,
				sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_GAME);

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

	}

	@Override
	public void finish() {
		super.finish();
		// if finished show score screen
		if (finished) {
			Intent intent = new Intent(this, MenuScore.class);
			intent.putExtra("levelid", level_id);
			intent.putExtra("from", MenuScore.FROM_GAME);
			intent.putExtra("score", score);
			startActivity(intent);
		}

	}

	public static void setFinished(boolean f) {
		finished = f;
	}

	public static void setScore(int s) {
		score = s;
	}

}