package com.ablok.GameObject;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;

import com.ablok.Game.Level;
import com.ablok.GameObject.Wall.WallType;

// obstacle use to have subclasses but became a problem when
// all obstacles needed to be able to be of the seeking subclass
public class Obstacle extends GameObject {

	// alignment can be added later

	private static final int DEFAULT_WIDTH = 20;
	private static final int DEFAULT_HEIGHT = 20;
	private static final int DIRECTION_LEFT = 1;
	private static final int DIRECTION_RIGHT = 2;

	// _________SEEKING_____________
	private boolean seeker = false;
	private boolean seekHorizontal;
	private boolean seekVertical;
	private double seekXVelo;
	private double seekYVelo;
	// seeking rectangle lengths
	private int seekTop;
	private int seekBottom;
	private int seekLeft;
	private int seekRight;

	private boolean seeking;
	private boolean returning;
	private double returnX; // original x
	private double returnY;
	// _____________________________

	// _________SET_PATH____________
	private List<Point> paths = new ArrayList<Point>();
	private Point nextPath;
	private boolean increment;
	private PathType pathType;
	private int direction = DIRECTION_RIGHT;

	public enum PathType {
		Rotation, BackandForwards;
	};

	// _____________________________

	public enum Alignment {
		Negative, Neutral;
	}

	private Alignment alignment;
	private Bitmap bitmap1; // trap open
	private Bitmap bitmap2; // trap closed
	private Bitmap bitmap3;
	private Bitmap bitmap4;
	private boolean hit = false;

	// STATIONARY Obstacle
	public Obstacle(double x, double y, int width, int height, Bitmap bitmap1,
			Bitmap bitmap2) {
		super.x = x;
		super.y = y;
		super.width = width;
		super.height = height;
		super.gameObjectType = GameObjectType.Stationary;
		this.bitmap1 = bitmap1;
		this.bitmap2 = bitmap2;
		alignment = Alignment.Negative;
	}

	// PATH Set Obstacle
	// NOTE: need at least 2 points to function
	public Obstacle(List<Point> paths, int width, int height, double xVelo,
			double yVelo, PathType pathType, Bitmap bitmap1, Bitmap bitmap2,
			Bitmap bitmap3, Bitmap bitmap4) {
		super.x = paths.get(0).x;
		super.y = paths.get(0).y;
		super.width = width;
		super.height = height;
		super.xVelo = xVelo;
		super.yVelo = yVelo;
		super.gameObjectType = GameObjectType.Set;
		this.pathType = pathType;
		this.paths = paths;
		nextPath = paths.get(1);
		this.bitmap1 = bitmap1;
		this.bitmap2 = bitmap2;
		this.bitmap3 = bitmap3;
		this.bitmap4 = bitmap4;
		alignment = Alignment.Negative;
	}

	public void update(double xOffset, double yOffset, Rect playerBounds,
			List<Wall> walls) {

		if (seeker) {
			// 3 States: Seeking, Returning, Normal Update
			double offsetX = x + xOffset;
			double offsetY = y + yOffset;
			double oldX = x; // used for corner collision
			double oldY = y;
			Rect offsetSeekBounds = getSeekBounds(xOffset, yOffset);
			// SEEK
			if (playerBounds.intersect(offsetSeekBounds)) {
				seeking = true;
				// SEEK HORIZONTAL
				if (seekHorizontal) {
					int pX = playerBounds.centerX();
					// player is to left
					if (offsetX < pX) {
						offsetX = offsetX + seekXVelo;
						if (offsetX > pX) {
							// xWos = pX;
						}
					} else if (offsetX > pX) {
						// point is to right
						offsetX = offsetX - seekXVelo;
						if (offsetX < pX) {
							// xWos = pX;
						}
					}
					x = offsetX - xOffset;
				}
				// SEEK VERTICAL
				if (seekVertical) {
					int pY = playerBounds.centerY();
					// if point is on top
					if (offsetY < pY) {
						offsetY = offsetY + seekYVelo;
						if (offsetY > pY) {
							// yWos = pY;
						}
					} else if (offsetY > pY) {
						// point is below
						offsetY = offsetY - seekYVelo;
						if (offsetY < pY) {
							// yWos = pY;
						}
					}
					y = offsetY - yOffset;
				}
				handleCollision(xOffset, yOffset, oldX, oldY, walls);
				returning = true;
				// collision with player is handled in level method

			} else {
				// RETURNING
				seeking = false;
				if (returning) {
					if (gameObjectType == GameObjectType.Set) {
						// return to nextPath
						returnX = nextPath.x;
						returnY = nextPath.y;
					}

					if (x < returnX) {
						x = x + seekXVelo;
						if (x > returnX) {
							x = returnX;
						}
					} else if (x > returnX) {
						x = x - seekXVelo;
						if (x < returnX) {
							x = returnX;
						}
					}
					if (y < returnY) {
						y = y + seekYVelo;
						if (y > returnY) {
							y = returnY;
						}
					} else if (y > returnY) {
						y = y - seekYVelo;
						if (y < returnY) {
							y = returnY;
						}
					}
					if (x == returnX && y == returnY) {
						returning = false;
					}
					handleCollision(xOffset, yOffset, oldX, oldY, walls);
				}
				// NORMAL UPDATE
				else {
					if (gameObjectType == GameObjectType.Stationary) {

					} else if (gameObjectType == GameObjectType.Set) {
						updateSetPath(xOffset, yOffset);
					} else if (gameObjectType == GameObjectType.Random) {

					}
				}

			}

		} else if (gameObjectType == GameObjectType.Stationary) {
			// stationary obstacles don't need update: remove?
		} else if (gameObjectType == GameObjectType.Set) {
			updateSetPath(xOffset, yOffset);
		} else if (gameObjectType == GameObjectType.Random) {

		}

	}

	public Rect getBounds(double xOffset, double yOffset) {
		Rect rect = new Rect();
		rect.left = (int) (x + xOffset);
		rect.right = (int) (x + width + xOffset);
		rect.top = (int) (y + yOffset);
		rect.bottom = (int) (y + height + yOffset);
		return rect;
	}

	public void render(Canvas canvas, double xOffset, double yOffset) {
		Paint p = new Paint();
		// if(alignment == Alignment.Negative) {
		// p.setColor(Color.RED);
		// } else {
		//
		// }

		if (seeker) {
			p.setColor(Color.MAGENTA);
			if (seeking) {
				p.setColor(Color.BLUE);
			}
		} else {
			p.setColor(Color.RED);
		}

		if (gameObjectType == GameObjectType.Stationary) {

			if (hit) {
				canvas.drawBitmap(bitmap2,
						getBounds(xOffset, yOffset).left - 5,
						getBounds(xOffset, yOffset).top - 5, p);
			} else {
				canvas.drawBitmap(bitmap1,
						getBounds(xOffset, yOffset).left - 5,
						getBounds(xOffset, yOffset).top - 5, p);
			}

		} else if (gameObjectType == GameObjectType.Set) {
			if (direction == DIRECTION_LEFT) {
				if (hit) {
					canvas.drawBitmap(bitmap2,
							getBounds(xOffset, yOffset).left - 5,
							getBounds(xOffset, yOffset).top - 5, p);
				} else {
					canvas.drawBitmap(bitmap1,
							getBounds(xOffset, yOffset).left - 5,
							getBounds(xOffset, yOffset).top - 5, p);
				}
			} else if (direction == DIRECTION_RIGHT) {
				if (hit) {
					canvas.drawBitmap(bitmap4,
							getBounds(xOffset, yOffset).left - 5,
							getBounds(xOffset, yOffset).top - 5, p);
				} else {
					canvas.drawBitmap(bitmap3,
							getBounds(xOffset, yOffset).left - 5,
							getBounds(xOffset, yOffset).top - 5, p);
				}
			}
		}

	}

	public Rect getSeekBounds(double xOffset, double yOffset) {
		Rect rect = new Rect();
		rect.top = (int) (y - seekTop + yOffset);
		rect.bottom = (int) (y + seekBottom + yOffset);
		rect.left = (int) (x - seekLeft + xOffset);
		rect.right = (int) (x + seekRight + xOffset);
		return rect;
	}

	// handle seek or return collision with walls
	private void handleCollision(double xOffset, double yOffset, double oldX,
			double oldY, List<Wall> walls) {
		for (Wall wall : walls) {
			if (getBounds(xOffset, yOffset).intersect(
					wall.getBounds(xOffset, yOffset))) {
				if (wall.getWallType() == WallType.Top) {
					// if collision with top wall. center screen to wall.bottom
					y = wall.getY() + Wall.WALL_THICKNESS;
				} else if (wall.getWallType() == WallType.Bottom) {
					y = wall.getY() - height;
				} else if (wall.getWallType() == WallType.Left) {
					x = wall.getX() + Wall.WALL_THICKNESS;
				} else if (wall.getWallType() == WallType.Right) {
					x = wall.getX() - width;
				} // INNER CORNER COLLISIONS
					// TOP LEFT
				else if (wall.getWallType() == WallType.CornerTLI) {
					// obstacle can't collide with 2 sides of corner at the same
					// time.
					if (oldY >= wall.getY() + Wall.WALL_THICKNESS) {
						y = wall.getY() + Wall.WALL_THICKNESS;
					} // else if?
					if (oldX >= wall.getX() + Wall.WALL_THICKNESS) {
						x = wall.getX() + Wall.WALL_THICKNESS;
					}
				}
				// TOP RIGHT
				else if (wall.getWallType() == WallType.CornerTRI) {
					if (oldY >= wall.getY() + Wall.WALL_THICKNESS) {
						y = wall.getY() + Wall.WALL_THICKNESS;
					}
					if (oldX <= wall.getX() - width) {
						x = wall.getX() - width;
					}
				}
				// BOTTOM LEFT
				else if (wall.getWallType() == WallType.CornerBLI) {
					if (oldY <= wall.getY() - height) {
						y = wall.getY() - height;
					}
					if (oldX >= wall.getX() + Wall.WALL_THICKNESS) {
						x = wall.getX() + Wall.WALL_THICKNESS;
					}
				}
				// BOTTOM RIGHT
				else if (wall.getWallType() == WallType.CornerBRI) {
					if (oldY <= wall.getY() - height) {
						y = wall.getY() - height;
					}
					if (oldX <= wall.getX() - width) {
						x = wall.getX() - width;
					}
				}

			}
		}
	}

	private void updateSetPath(double xOffset, double yOffset) {
		// if point is to left
		if (x < nextPath.x) {
			direction = DIRECTION_LEFT;
			x = x + xVelo;
			if (x > nextPath.x) {
				x = nextPath.x;
			}
		} else if (x > nextPath.x) {
			// point is to right
			direction = DIRECTION_RIGHT;
			x = x - xVelo;
			if (x < nextPath.x) {
				x = nextPath.x;
			}
		}
		// if point is on top
		if (y < nextPath.y) {
			y = y + yVelo;
			if (y > nextPath.y) {
				y = nextPath.y;
			}
		} else if (y > nextPath.y) {
			// point is below
			y = y - yVelo;
			if (y < nextPath.y) {
				y = nextPath.y;
			}
		}
		// set next point
		if (x == nextPath.x && y == nextPath.y) {

			if (pathType == PathType.BackandForwards) {
				int ind;
				if (increment) {
					ind = paths.indexOf(nextPath) + 1;
					Log.d("Current Index", String.valueOf(ind));
					Log.d("Points List Size", String.valueOf(paths.size()));
					if (ind > paths.size() - 1) { // reached end
						ind -= 2;
						Log.d("New Index", String.valueOf(ind));
						increment = !increment; // toggle inc
					}
				} else {
					ind = paths.indexOf(nextPath) - 1;
					if (ind < 0) {
						ind += 2;
						increment = !increment;
					}
				}
				nextPath = paths.get(ind);

			} else if (pathType == PathType.Rotation) {
				int ind = paths.indexOf(nextPath) + 1;
				if (ind > paths.size() - 1) {
					ind = 0;
				}
				nextPath = paths.get(ind);
			}
		}
	}

	private void updateRandom(int xOffset, int yOffset, Rect rect) {

	}

	public Alignment getAlignment() {
		return alignment;
	}

	public void setSeek(int top, int bottom, int left, int right,
			int seekXVelo, int seekYVelo, boolean horizontal, boolean vertical) {
		seeker = true;
		seekHorizontal = horizontal;
		seekVertical = vertical;
		seekTop = top;
		seekBottom = bottom;
		seekLeft = left;
		seekRight = right;
		this.seekXVelo = seekXVelo;
		this.seekYVelo = seekYVelo;
		returnX = x;
		returnY = y;
	}

	public void triggerTrap() {
		hit = true;
	}

	public void armTrap() {
		hit = false;
	}

}
