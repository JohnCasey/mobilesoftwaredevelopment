package com.ablok.GameObject;

import com.ablok.R;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class Finish extends GameObject {

	private static final int FINISH_HEIGHT = 50;
	private static final int FINISH_WIDTH = 50;
	private static final int FINISH_INNER_HEIGHT = 25;
	private static final int FINISH_INNER_WIDTH = 25;

	private Bitmap bitmap;

	public Finish(int x, int y, Resources resources) {
		super(x, y, 0, 0, FINISH_HEIGHT, FINISH_WIDTH);
		Bitmap bmp = BitmapFactory.decodeResource(resources,
				R.drawable.game_exit);
		bitmap = Bitmap.createScaledBitmap(bmp, 50, 49, false);
	}

	public Rect getBounds(double xOffset, double yOffset) {
		Rect rect = new Rect();
		rect.left = (int) (x + xOffset);
		rect.right = (int) (x + width + xOffset);
		rect.top = (int) (y + yOffset);
		rect.bottom = (int) (y + height + yOffset);
		return rect;
	}

	public void render(Canvas canvas, double xOffset, double yOffset) {
		Paint p = new Paint();
		// p.setColor(Color.CYAN);
		// canvas.drawRect(getBounds(xOffset, yOffset), p);
		canvas.drawBitmap(bitmap, getBounds(xOffset, yOffset).left,
				getBounds(xOffset, yOffset).top, p);
	}

}
