package com.ablok.GameObject;

import java.util.Timer;
import java.util.TimerTask;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;

import com.ablok.R;
import com.ablok.Game.Level;

public class Player extends GameObject {

	private static final int MAX_XVELO = 4;
	private static final int MIN_XVELO = -4;
	private static final int MAX_YVELO = 4;
	private static final int MIN_YVELO = -4;

	public static final int PLAYER_WIDTH = 35;
	public static final int PLAYER_HEIGHT = 35;
	// public static final int MAX_XVELO = 4;
	// public static final int MAX_YVELO = 4;

	private static final int MIN_OPACITY = 0;
	private static final int MAX_OPACITY = 255;
	private static final int FADE_TICK = 1;
	private Bitmap bitmap;
	private Bitmap bitmapC; // current bitmap with rotation applied

	// temp color for debugging
	private int color;
	private int opacity = MAX_OPACITY;
	private boolean fading = false;
	private int angle = 0;

	public Player(int x, int y, Resources resources) {
		super(x = x + (PLAYER_WIDTH / 2), y = y + (PLAYER_HEIGHT / 2), 0, 0,
				PLAYER_WIDTH, PLAYER_HEIGHT);
		Bitmap bmp = BitmapFactory.decodeResource(resources,
				R.drawable.game_hedgehog);
		bitmap = Bitmap.createScaledBitmap(bmp, 35, 35, false);
		
	}

	public void render(Canvas canvas) {
		Paint p = new Paint();
		// p.setColor(color);
		p.setAlpha(opacity);
		
		canvas.drawBitmap(bitmapC, getBounds().left, getBounds().top, p);
	}

	public Rect getBounds() {
		Rect rect = new Rect();
		rect.left = (int) (x - (PLAYER_WIDTH / 2));
		rect.right = (int) (x + (PLAYER_WIDTH / 2));
		rect.top = (int) (y - (PLAYER_HEIGHT / 2));
		rect.bottom = (int) (y + (PLAYER_HEIGHT / 2));
		return rect;
	}

	public void setColor(int colorCode) {
		color = colorCode;
	}
	
	// player needs rotate update to check pixel collisions
	public void update() {
		Matrix mt = new Matrix();
		mt.postRotate(angle);
		bitmapC = Bitmap.createBitmap(bitmap, 0, 0, PLAYER_WIDTH,
				PLAYER_HEIGHT, mt, true);
	}

	public void setxVelo(double xVelo) {
		if (!fading) {
			if (xVelo > MAX_XVELO) {
				super.xVelo = MAX_XVELO;
			} else if (xVelo < MIN_XVELO) {
				super.xVelo = MIN_XVELO;
			} else {
				super.xVelo = xVelo;
			}
		}
	}

	public void setyVelo(double yVelo) {
		if (!fading) {
			if (yVelo > MAX_YVELO) {
				super.yVelo = MAX_YVELO;
			} else if (yVelo < MIN_YVELO) {
				super.yVelo = MIN_YVELO;
			} else {
				super.yVelo = yVelo;
			}
		}
	}

	public void fadeOut(final Level level, final boolean restart) {
		if (!fading) {
			fading = true;
			this.xVelo = 0;
			this.yVelo = 0;
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				public void run() {
					opacity -= FADE_TICK;
					if (opacity <= MIN_OPACITY) {
						this.cancel();
						opacity = 0;
						if (restart) {
						level.setStart();
						}
					}
				}
			}, 0, 1);
		}
	}

	public void fadeIn(final Level level) {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				opacity += FADE_TICK;
				if (opacity >= MAX_OPACITY) {
					this.cancel();
					opacity = 255;
					level.setTimer();
					fading = false;
					xVelo = 0;
					yVelo = 0;
				}
			}
		}, 0, 1);
	}

	public void setAngle(int angle) {
		this.angle = angle;
	}
	
	public Bitmap getBitmap() {
		return bitmap;
	}

}
