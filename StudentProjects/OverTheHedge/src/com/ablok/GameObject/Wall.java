package com.ablok.GameObject;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.StateListDrawable;
import android.util.Log;

import com.ablok.Game.Tile;

public class Wall extends GameObject {

	public enum WallType {
		Top, Bottom, Left, Right, CornerTRO, // top right outer
		CornerTLO, // top left outer
		CornerBRO, // bottom right outer
		CornerBLO, // bottom left outer
		CornerTRI, // top right inner
		CornerTLI, // top left inner
		CornerBRI, // bottom right inner
		CornerBLI; // bottom left inner
	}

	// TILE_THICKNESS cannot be more than half TILE_SIZE
	public static final int WALL_THICKNESS = 15;
	private WallType wallType;
	private Bitmap bitmap;

	public Wall(WallType wt, double tX, double tY, Bitmap bitmap) {
		super.xVelo = 0;
		super.yVelo = 0;
		wallType = wt;
		this.bitmap = bitmap;
		createWall(tX, tY);
	}

	private void createWall(double tX, double tY) {
		// change to switch case

		// WALL***
		if (wallType == WallType.Top) {
			super.x = tX;
			super.y = tY - WALL_THICKNESS;
			super.width = Tile.TILE_SIZE;
			super.height = WALL_THICKNESS;
		} else if (wallType == WallType.Bottom) {
			super.x = tX;
			super.y = tY + Tile.TILE_SIZE;
			super.width = Tile.TILE_SIZE;
			super.height = WALL_THICKNESS;
		} else if (wallType == WallType.Left) {
			super.x = tX - WALL_THICKNESS;
			super.y = tY;
			super.width = WALL_THICKNESS;
			super.height = Tile.TILE_SIZE;
		} else if (wallType == WallType.Right) {
			super.x = tX + Tile.TILE_SIZE;
			super.y = tY;
			super.width = WALL_THICKNESS;
			super.height = Tile.TILE_SIZE;
		}
		// OUTER CORNER ***
		else if (wallType == WallType.CornerTLO) {
			super.x = tX - WALL_THICKNESS;
			super.y = tY - WALL_THICKNESS;
			super.width = WALL_THICKNESS;
			super.height = WALL_THICKNESS;
		} else if (wallType == WallType.CornerTRO) {
			super.x = tX + Tile.TILE_SIZE;
			super.y = tY - WALL_THICKNESS;
			super.width = WALL_THICKNESS;
			super.height = WALL_THICKNESS;
		} else if (wallType == WallType.CornerBLO) {
			super.x = tX - WALL_THICKNESS;
			super.y = tY + Tile.TILE_SIZE;
			super.width = WALL_THICKNESS;
			super.height = WALL_THICKNESS;
		} else if (wallType == WallType.CornerBRO) {
			super.x = tX + Tile.TILE_SIZE;
			super.y = tY + Tile.TILE_SIZE;
			super.width = WALL_THICKNESS;
			super.height = WALL_THICKNESS;
		}
		// INNER CORNER
		else if (wallType == WallType.CornerTLI
				|| wallType == WallType.CornerTRI
				|| wallType == WallType.CornerBLI
				|| wallType == WallType.CornerBRI) {
			super.x = tX;
			super.y = tY;
			super.width = Wall.WALL_THICKNESS;
			super.height = Wall.WALL_THICKNESS;
		}
	}

	public void render(Canvas canvas, double xOffset, double yOffset) {
		Paint p = new Paint();
		p.setColor(Color.GRAY);
		// canvas.drawRect(getBounds(xOffset, yOffset), p);

		// walls do not tesilate perfectly as 50 is not divisible by 15
		// was not worth fixing at current time as barley visible

		//
		canvas.drawBitmap(bitmap, (float) (xOffset + x), (float) (yOffset + y),
				null);

	}

	public void createBitmapEx() {

		if (wallType == WallType.Top || wallType == WallType.Bottom
				|| wallType == WallType.Left || wallType == WallType.Right) {
			// walls do not tesilate perfectly as 50 is not divisible by 15
			// was not worth fixing at current time as barley visible
			//Bitmap exBmp = bitmap;
			
			//Config conf = Bitmap.Config.ARGB_8888;
			Bitmap newBmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
			Log.d("WALL", "width:"+String.valueOf(width)+" height:" + String.valueOf(height));

			if (wallType == WallType.Top || wallType == WallType.Bottom) {
				int xx = 0;
				while (xx < width) {
					newBmp = overlay(newBmp, bitmap, xx, 0);
					//newBmp = exBmp;
					xx += WALL_THICKNESS;
				}
			} else if (wallType == WallType.Left || wallType == WallType.Right) {
				int yy = 0;
				while (yy < height) {
					newBmp = overlay(newBmp, bitmap, 0, yy);
					yy += WALL_THICKNESS;
				}
			}
			bitmap = newBmp;
		}
	}

	public Rect getBounds(double xOffset, double yOffset) {
		Rect rect = new Rect();
		rect.left = (int) (x + xOffset);
		rect.right = (int) (x + width + xOffset);
		rect.top = (int) (y + yOffset);
		rect.bottom = (int) (y + height + yOffset);
		return rect;
	}

	public WallType getWallType() {
		return wallType;
	}

	public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2, int daX, int daY) {
		Log.d("WALL", "x:" + String.valueOf(daX) + " y:" + String.valueOf(daY));
		Bitmap bmpOverlay = Bitmap.createBitmap(bmp1.getWidth(),
				bmp1.getHeight(), bmp1.getConfig());
		Canvas canvas = new Canvas(bmpOverlay);
		canvas.drawBitmap(bmp1, 0,0, null);
		canvas.drawBitmap(bmp2, daX, daY, null);
		//canvas.save();
		return bmpOverlay;
	}

}
