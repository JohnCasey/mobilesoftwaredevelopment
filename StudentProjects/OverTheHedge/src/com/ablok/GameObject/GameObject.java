package com.ablok.GameObject;

// basic template for gameobject holding common attributes.
public abstract class GameObject {

	public enum GameObjectType {
		Stationary, // stationary obstacle
		Set, // obstacle with set path
		Random, // obstacle with random path
	}

	protected GameObjectType gameObjectType;
	protected double x;
	protected double y;
	protected double xVelo;
	protected double yVelo;
	protected int width;
	protected int height;

	// bitmap

	public GameObject(int x, int y, int xVelo, int yVelo, int width, int height) {
		this.x = x;
		this.y = y;
		this.xVelo = xVelo;
		this.yVelo = yVelo;
		this.width = width;
		this.height = height;
	}

	public GameObject() {

	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getxVelo() {
		return xVelo;
	}

	public void setxVelo(double xVelo) {
		this.xVelo = xVelo;
	}

	public double getyVelo() {
		return yVelo;
	}

	public void setyVelo(double yVelo) {
		this.yVelo = yVelo;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
	public GameObjectType getGameObjectType() {
		return gameObjectType;
	}

	// render method for when all subclass are using bitmap?

}
