package com.ablok.GameObject;

import java.util.Timer;
import java.util.TimerTask;

import com.ablok.R;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class Pt extends GameObject {

	private static final int DEFAULT_POINT_WIDTH = 20;
	private static final int DEFAULT_POINT_HEIGHT = 20;

	private static final int MIN_OPACITY = 0;
	private static final int MAX_OPACITY = 225;
	private static final int FADE_TICK = 1;

	private int pts; // point amount
	private boolean active;
	private int opacity = MAX_OPACITY;
	private Bitmap bitmap;

	public Pt(int x, int y, int pts, Bitmap bitmap) {
		super.x = x;
		super.y = y;
		this.pts = pts;
		active = true;
		this.bitmap = bitmap;
	}

	public Rect getBounds(double xOffset, double yOffset) {
		Rect rect = new Rect();
		rect.left = (int) (x + xOffset);
		rect.right = (int) (x + DEFAULT_POINT_WIDTH + xOffset);
		rect.top = (int) (y + yOffset);
		rect.bottom = (int) (y + DEFAULT_POINT_HEIGHT + yOffset);
		return rect;
	}

	public void render(Canvas canvas, double xOffset, double yOffset) {
		Paint p = new Paint();
		//p.setColor(Color.YELLOW);
		p.setAlpha(opacity);
		//canvas.drawRect(getBounds(xOffset, yOffset), p);
		canvas.drawBitmap(bitmap, getBounds(xOffset, yOffset).left-10, getBounds(xOffset, yOffset).top-10, p);
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getPts() {
		setActive(false);
		fadeOut();
		return pts;
	}

	public void fadeOut() {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				opacity -= FADE_TICK;
				if (opacity <= MIN_OPACITY) {
					opacity = 0;
					this.cancel();
				}
			}
		}, 0, 1);
	}

	public void fadeIn() {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				opacity += FADE_TICK;
				if (opacity >= MAX_OPACITY) {
					opacity = 255;
					setActive(true);
					this.cancel();
				}
			}
		}, 0, 1);
	}

}
