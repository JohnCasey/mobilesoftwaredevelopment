package com.unitec.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;

public class HoroscopeActivity extends Activity 
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); 
        setContentView(R.layout.menu);

        Button btn_start = (Button) findViewById(R.id.btn_start);
        
        btn_start.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {				
				Intent game_activity = new Intent();
				game_activity.setAction("com.unitec.OPEN_GAME_ACTIVITY");
				startActivity(game_activity);
			}
		});
        Button about = (Button) findViewById(R.id.btn_about);
        
        about.setOnClickListener(new OnClickListener() 
        {
			public void onClick(View v) {				
				Intent about_activity = new Intent();
				about_activity.setAction("com.unitec.OPEN_ABOUT_ACTIVITY");
				startActivity(about_activity);
			}
		});
    }
}