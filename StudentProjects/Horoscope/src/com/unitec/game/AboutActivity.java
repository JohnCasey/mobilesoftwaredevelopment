package com.unitec.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;

public class AboutActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); 
        setContentView(R.layout.about);

        Button back = (Button) findViewById(R.id.btn_about_back);
        
        back.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {				
				Intent message = new Intent();
				message.setAction("com.unitec.OPEN_MENU_ACTIVITY");
				startActivity(message);
			}
		});
        
    }

}
