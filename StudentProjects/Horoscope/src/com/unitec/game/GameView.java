package com.unitec.game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class GameView extends View implements DialogInterface.OnClickListener, OnTouchListener
{
	private final float RADIUS = 3;         //radius for little circles
	private final float LINE_WIDTH = 2; 	//width of lines
	private final float BIG_TEXT_SIZE = 35; //size for labels
	private final int SHOW_EXAMPLE_DELAY = 3000;//time which example shows to user

	private Point[] points;
	private byte[][] edgesMatrix;           //Adjacency matrix made by user
	private byte[][] tmpMatrixTemplate;   	//Temporary Adjacency matrix
	Constellation currentTemplate;			//Correct Adjacency constellation
	private Vector<Constellation> matrixTemplates;
	private int N;                			// Number of points
	private int constellationNum;   					//Number of constellation
	private int LEVEL;
	private int levelNum;
	private int edgesNumber;

	private final Point edgesNumberPoint = new Point(440,40);  //Point where number of edges shows
	private Point fingerPoint = null;       //Point that user pressed
	private Point firstEdgePoint = null;
	private int firstEdgePointId = -1;
	private boolean startDrag;				//Flag, true if user is dragging
	private Paint paintStar;		
	private Paint paintEdge;		
	private Paint paintEdgesNumber;		
	private boolean showTemplate;
	private Timer showTemplateTimer;
	private boolean win;
	private boolean lose;
	private Context context;
	
	public GameView(Context context, AttributeSet attributeSet)
	{
		super(context,attributeSet);
		this.context = context;
		init();
	}
	
	public void init()
	{
		this.setOnTouchListener(this);
		
		LEVEL = 1;
		levelNum = 2;	
		//setBackgroundColor(Color.BLACK);
		//setBackgroundResource(R.drawable.bg);
		
		paintStar = new Paint();
		paintStar.setStrokeWidth(RADIUS);
		paintStar.setColor(Color.YELLOW);

		paintEdge = new Paint();
		paintEdge.setStrokeWidth(LINE_WIDTH);
		paintEdge.setColor(Color.YELLOW);

		paintEdgesNumber = new Paint();
		paintEdgesNumber.setStrokeWidth(RADIUS);
		paintEdgesNumber.setColor(Color.WHITE);
		paintEdgesNumber.setTextSize(BIG_TEXT_SIZE);

		try {
			initialize(context);
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void initialize(Context context) throws XmlPullParserException, IOException
	{
		edgesNumber = 0;
		startDrag = false;
		win = false;
		lose = false;
		showTemplate = true;
		constellationNum=0;
	
		matrixTemplates = new Vector<Constellation>();
		final String CONSTELLATION_TAG = "constellation";
		final String POINT_TAG = "point";
		final String EDGES_TAG = "edges";
		final String EDGE_TAG = "edge";
		final String NAME_TAG = "name";
		final String X_ATTR = "x";
		final String Y_ATTR = "y";
		final String P1_ATTR = "p1";
		final String P2_ATTR = "p2";
		ArrayList<Point> plist = new ArrayList<Point>();
		
		int numRandomPoints;
		Resources res = context.getResources();
		XmlResourceParser xpp = null;
		if(LEVEL == 1) 
		{	
			xpp = res.getXml(R.xml.level1);
			numRandomPoints = 7;
		}
		else
		{
			xpp = res.getXml(R.xml.level2);
			numRandomPoints = 10;
		}
		xpp.next();
		
		int eventType = xpp.getEventType();
		String cur_tag_name="";
		Point cur_point=null;
		int edgesCount=0;
		String name = null;
		
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			if(eventType == XmlPullParser.START_DOCUMENT)
		    {
				//stringBuffer.append("--- Start XML ---");
		    }
		    else if(eventType == XmlPullParser.START_TAG)
		    {
		    	cur_tag_name = xpp.getName();
		    	if(cur_tag_name.equals(POINT_TAG)) 
		    	{
		    		cur_point = new Point();
		    		cur_point.x = xpp.getAttributeIntValue(null, X_ATTR, 0);
		    		cur_point.y = xpp.getAttributeIntValue(null, Y_ATTR, 0);
		    	} else if(cur_tag_name.equals(EDGES_TAG)) 
		    	{
		    		tmpMatrixTemplate = new byte[plist.size()+numRandomPoints][plist.size()+numRandomPoints];
		    	} else if(cur_tag_name.equals(EDGE_TAG)) 
		    	{
		    		int i = xpp.getAttributeIntValue(null, P1_ATTR, -1);
		    		int j = xpp.getAttributeIntValue(null, P2_ATTR, -1);
		    		if(i!=-1 && j!=-1 && i<plist.size() && j<plist.size()) {
		    			edgesCount++;
		    			tmpMatrixTemplate[i][j] = 1;
		    			tmpMatrixTemplate[j][i] = 1;
		    		}
		    	} else if(cur_tag_name.equals(CONSTELLATION_TAG)) 
			    {
			    	name = xpp.getAttributeValue(null, NAME_TAG);
			    }
		    }
		  
		    else if(eventType == XmlPullParser.END_TAG)
		    {
		    	if(xpp.getName().equals(CONSTELLATION_TAG))
		    	{
		    		
		    		points = new Point[plist.size() + numRandomPoints];
					int i=0;
					for(Point p:plist)
						points[i++] = p;
					for(int j=0; j<numRandomPoints;j++)
					{
						Random randomGenerator = new Random();
			    		Point tmp = new Point(); 
						tmp.x = randomGenerator.nextInt(350)+30;
						tmp.y = randomGenerator.nextInt(450)+30;
						points[i++] = tmp;
					}
					matrixTemplates.add(new Constellation(points, tmpMatrixTemplate, edgesCount, name));
					edgesCount = 0;
					plist.clear();
		    	}

				cur_tag_name = null;
		    	cur_point = null;
		    }
		    else if(eventType == XmlPullParser.TEXT)
		    {
		    	if(cur_tag_name.equals(POINT_TAG)) {
		    		int index = Integer.parseInt(xpp.getText());
		    		plist.add(index, cur_point);
		    	}
		    }
		    eventType = xpp.next();
		}
		loadNextConstellation();
	}
	
	private void loadNextConstellation() 
	{
		if(constellationNum<=(matrixTemplates.size()-1)) 
		{
			showTemplate = true;
			currentTemplate = matrixTemplates.get(constellationNum++);
			tmpMatrixTemplate = currentTemplate.getEdgesMatrix();
			points = currentTemplate.getPoints();
			N = points.length;
			edgesMatrix = new byte[N][N];
			edgesNumber = currentTemplate.getEdgesCount();
			invalidate();
		} else {//all constellations completed
			new AlertDialog.Builder(context).setTitle("You completed all constellations").setMessage("Congratulations!")
			   .setNeutralButton("Next Level", new DialogInterface.OnClickListener() {
			       public void onClick(DialogInterface dialog, int which) {
			    	   LEVEL++;
			    	   loadNextLevel();
			        }
			   }).show();
			
		}
	}
	private void loadNextLevel() 
	{
		if(LEVEL<=levelNum) 
		{
			try {
				initialize(context);
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			invalidate();
		} else 
		{//all levels completed
			new AlertDialog.Builder(context).setTitle("You completed all levels").setMessage("Congratulations!").setNeutralButton("Exit", this).show();
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas)
	{
		//canvas.drawColor(Color.WHITE);
		for(int i=0;i<N;i++) 
		{
			drawStar(canvas, points[i]);
		}
		//Log.d(VIEW_LOG_TAG, "start draw edges");
		if(showTemplate) 
		{
			showTemplate = false;
			drawEdges(canvas, tmpMatrixTemplate);
			showTemplateTimer = new Timer();
			showTemplateTimer.schedule(new TimerTask()
			{
				@Override
				public void run()
				{
					postInvalidate();
				}
			}, SHOW_EXAMPLE_DELAY);
		} else 
		{
			drawEdgesNumber(canvas, edgesNumberPoint);
			drawEdges(canvas, edgesMatrix);
		}
		drawDragEdge(canvas);
		if(win)
		{
			win = false;
			// custom dialog
			final Dialog dialog = new Dialog(context);
			dialog.setContentView(R.layout.win);
			dialog.setTitle("You win");
			 
			// set the custom dialog components - text, image and button
			TextView text = (TextView) dialog.findViewById(R.id.text);
			String name = currentTemplate.getName();
			text.setText(name);
			ImageView image=(ImageView) dialog.findViewById(R.id.image);
		
			if(name.equals("crab")) 
				image.setImageResource(R.drawable.crab); 
			else if(name.equals("gemini")) 
				image.setImageResource(R.drawable.gemini); 
			else if(name.equals("leo")) 
				image.setImageResource(R.drawable.leo); 
			else if(name.equals("libra")) 
				image.setImageResource(R.drawable.libra); 
			else if(name.equals("taurus")) 
				image.setImageResource(R.drawable.taurus); 
			else 
				image.setImageResource(R.drawable.virgo); 
		
			Button dialogButton = (Button) dialog.findViewById(R.id.btn_next);
			// if button is clicked, close the custom dialog
			dialogButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					loadNextConstellation();
				}
			});
			 
			dialog.show();
			
		}
		if(lose)
		{
			lose = false;
			new AlertDialog.Builder(context).setTitle("").setMessage("Not correct!").setNeutralButton("Try again", new DialogInterface.OnClickListener() {
			       public void onClick(DialogInterface dialog, int which) {
			    	   constellationNum--;
			    	   loadNextConstellation();
			       }
			   }).show();
			
			//show lose message
			//load same template
		}
	}
	
	
	private void drawEdges(Canvas canvas, byte[][] matrix)  //draws all edges according to Adjacency matrix
	{
		for(int i=0;i<N;i++)
		{
			for(int j=0;j<N;j++) 
			{
				if(matrix[i][j] == 1)                   //There is an edge between points i and j
					drawEdge(canvas, points[i], points[j]);        // => draw this edge
			}
		}
	}
	
	private void drawEdgesNumber(Canvas canvas, Point p)
	{
		canvas.drawText(""+edgesNumber, p.x, p.y, paintEdgesNumber);
	}
	
	private void drawStar(Canvas canvas, Point p)
	{
		if(null!=firstEdgePoint && p.equals(firstEdgePoint))
		{
			canvas.drawCircle(p.x, p.y, 2*RADIUS, paintStar);   //Clicked point is two times bigger
		} else 
		{			
			canvas.drawCircle(p.x, p.y, RADIUS, paintStar);     //Ordinary point
		}
	}
	
	private void drawEdge(Canvas canvas, Point pStart, Point pEnd) 
	{
		//Log.d(VIEW_LOG_TAG, "draw edge from "+pStart.x+","+pStart.y+"to "+pEnd.x+","+pEnd.y);
		
		if(!pStart.equals(pEnd)) 
		{
			canvas.drawLine(pStart.x, pStart.y, pEnd.x, pEnd.y, paintEdge);
		}
	}

	private void drawDragEdge(Canvas canvas) //drawing edge that is being dragged at the moment
	{
		if(startDrag) 
		{
			canvas.drawLine(firstEdgePoint.x, firstEdgePoint.y, fingerPoint.x, fingerPoint.y, paintEdge);
		}
	}


	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		final int action = event.getAction();
    	float x = event.getX();
		float y = event.getY();
	   
		switch (action)
	    {
		    case MotionEvent.ACTION_DOWN:
		    {
		    	firstEdgePointId = getNearPointId(x,y);
		    	if(-1!=firstEdgePointId) firstEdgePoint = points[firstEdgePointId];
		        break;
		    }
		    case MotionEvent.ACTION_MOVE: 
		    {
		    	if(firstEdgePoint != null)
		    	{
					startDrag = true;
			    	fingerPoint = new Point((int)x, (int)y);
		    	}
		        break;
		    }
		    case MotionEvent.ACTION_UP: 
		    {
		    	int endEdgePoint = getNearPointId(x,y);
		    	if(endEdgePoint != -1) 
		    	{
		    		addNewEdge(edgesMatrix, firstEdgePointId, endEdgePoint);
		    	}
		    	startDrag = false;  // User stopped dragging
		    	fingerPoint = null;
		    	firstEdgePoint = null;
		        break;
		    }		    
	    }
	    invalidate(); //redraw all
		return true;
	}
	
	private void addNewEdge(byte[][] matrix, int from, int to) //add new edge to  Adjacency matrix
	{
		if((from>=0) && (from<N) && (to>=0) && (to<N)) 
		{
			matrix[from][to] = (byte)((matrix[from][to]==0)?1:0);
			matrix[to][from] = (byte)((matrix[to][from]==0)?1:0);
			edgesNumber += (matrix[from][to]==0)?1:-1;
		}
		if(compareMatrix()) 
			win = true;
		else if(edgesNumber==0) lose = true;
	}
	private int getNearPointId(float x, float y) 
	{
		final float DIST_RANGE = 10;  
		for(int i=0;i<N;i++)
		{
			if(points[i]!=firstEdgePoint)
				if(Math.abs(x-points[i].x)<DIST_RANGE && Math.abs(y-points[i].y)<DIST_RANGE) //User dropped near point i
					return i;
		}
		return -1;
	}
	
	private boolean compareMatrix()
	{
		for(int i=0; i<N; i++)
			for(int j=0; j<N; j++)
				if(tmpMatrixTemplate[i][j] != edgesMatrix[i][j])
					return false;
		
		return true;
	}
	@Override
	public void onClick(DialogInterface dialog, int which) {
		Intent main_menu = new Intent();
		main_menu.setAction("com.unitec.OPEN_MENU_ACTIVITY");
		context.startActivity(main_menu);
	}
	
}
