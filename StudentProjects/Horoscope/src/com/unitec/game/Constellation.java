package com.unitec.game;

import android.graphics.Point;

public class Constellation {
	private Point[] points;					//Star points
	private byte[][] edgesMatrix;           //correct Adjacency matrix 
	private int edgesCount;
	private String name;

	public Constellation(Point[] points, byte[][] edges, int edgesCount, String name) {
		this.points = points;
		this.edgesMatrix = edges;
		this.edgesCount = edgesCount;
		this.name = name;
	}
	public Point[] getPoints() {
		return points;
	}
	public byte[][] getEdgesMatrix() {
		return edgesMatrix;
	}
	public int getEdgesCount() {
		return edgesCount;
	}
	public String getName() {
		return name;
	}
}
