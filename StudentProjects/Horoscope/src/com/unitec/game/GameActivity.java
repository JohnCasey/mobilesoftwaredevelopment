package com.unitec.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;

public class GameActivity extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); 
        setContentView(R.layout.game);

        Button back = (Button) findViewById(R.id.btn_back);
        
        back.setOnClickListener(new OnClickListener() 
        {
			public void onClick(View v) {				
				Intent main_menu = new Intent();
				main_menu.setAction("com.unitec.OPEN_MENU_ACTIVITY");
				startActivity(main_menu);
			}
		});
        
    }
}
