package com.groupone.animate;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;

/**
 * @author Jamie Crawley
 * Date Created: 29/10/2012
 * Description: Main activity, sets the buttons...etc
 * History:		Jamie Crawley	30/10/2012	(Modified)
 * 				Jamie Crawley	 2/11/2012	(Modified)
 * 				Jamie Crawley	 3/11/2012	(Modified)
 */
public class AnimateActivity extends Activity {
	public static RelativeLayout mainLayout;
	public static TextView charText;
	public static Display display;
	
	private SlidingDrawer selectDrawer;
	private AnimateView mView;
	private Button bearBtn;
	private Button catBtn;
	private Button dogBtn;
	private Button parrotBtn;
	private Button pirateBtn;
	private Button rabbitBtn;
	private Button sadParrotBtn;
	private Button tweetyBtn;
	private EditText text;
	private AlertDialog alertDialog;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		display = getWindowManager().getDefaultDisplay();
		// so screen will stay on
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		mView = new AnimateView(this);
        setContentView(R.layout.activity_animate);
        mView.setLayoutParams(new RelativeLayout.LayoutParams(
        		RelativeLayout.LayoutParams.MATCH_PARENT,
        		RelativeLayout.LayoutParams.MATCH_PARENT));
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.relative1);
        relativeLayout.addView(mView);
        mainLayout = (RelativeLayout) findViewById(R.id.RelativeLayout1);
        
        charText = (TextView) findViewById(R.id.characterText);
        
        Button undoBtn = (Button) findViewById(R.id.btnBack);
        undoBtn.bringToFront();
        undoBtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		if (!mView.mRunning) mView.Undo();
        	}
        });
        
        Button resetBtn = (Button) findViewById(R.id.btnStart);
        resetBtn.bringToFront();
        resetBtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		if (!mView.mRunning) mView.Reset();
        	}
        });
        
        Button playBtn = (Button) findViewById(R.id.btnPlay);
        playBtn.bringToFront();
        playBtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		if (!mView.mRunning
        				&& mView.numCharacters != 0
        				&& mView.timeline.size() > 1) {
        			mainLayout.setVisibility(4); // invisible
        			mView.playAnimations();
        		}
        	}
        });
        
        text = new EditText(this);
        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setView(text);
        alertDialog.setMessage("Enter A Message");
        alertDialog.setButton("Cancel", new DialogInterface.OnClickListener() {
        	public void onClick(DialogInterface dialog, int which) {
        		text.getText().clear();
        		return;
        	}
        });
        
        alertDialog.setButton2("OK", new DialogInterface.OnClickListener() {
        	public void onClick(DialogInterface dialog, int which) {
        		String speechText = text.getText().toString();
        		mView.setSpeechText(speechText);
        		text.getText().clear();
        		return;
        	}
        });
        
        Button speechBtn = (Button) findViewById(R.id.btnSpeech);
        speechBtn.bringToFront();
        speechBtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
                if (mView.numCharacters != 0 && !mView.mRunning) 
                	alertDialog.show();
        	}
        });
        
        selectDrawer = (SlidingDrawer) findViewById(R.id.slidingDrawer1);
        selectDrawer.bringToFront();
        
        // SlidingDrawer character buttons from here down
        bearBtn = (Button) findViewById(R.id.btnBear);
        bearBtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		mView.selectCharacter("bear");
        		selectDrawer.animateClose();
        	}
        });
        
        catBtn = (Button) findViewById(R.id.btnCat);
        catBtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		mView.selectCharacter("cat");
        		selectDrawer.animateClose();
        	}
        });
        
        dogBtn = (Button) findViewById(R.id.btnDog);
        dogBtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		mView.selectCharacter("dog");
        		selectDrawer.animateClose();
        	}
        });
        
        parrotBtn = (Button) findViewById(R.id.btnParrot);
        parrotBtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		mView.selectCharacter("parrot");
        		selectDrawer.animateClose();
        	}
        });
        
        pirateBtn = (Button) findViewById(R.id.btnPirate);
        pirateBtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		mView.selectCharacter("pirate");
        		selectDrawer.animateClose();
        	}
        });
        
        rabbitBtn = (Button) findViewById(R.id.btnRabbit);
        rabbitBtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		mView.selectCharacter("rabbit");
        		selectDrawer.animateClose();
        	}
        });
        
        sadParrotBtn = (Button) findViewById(R.id.btnSadParrot);
        sadParrotBtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		mView.selectCharacter("sadParrot");
        		selectDrawer.animateClose();
        	}
        });
        
        tweetyBtn = (Button) findViewById(R.id.btnTweety);
        tweetyBtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		mView.selectCharacter("tweety");
        		selectDrawer.animateClose();
        	}
        });
    }
    
    @Override
	public void onBackPressed() {
		finish(); // stops it from crashing, for now
	}

    @Override
    protected void onDestroy()
    {
    	super.onDestroy();
    	if (mView.mRunning) mView.stopAnimating();
    	mView.soundPool.release(); // release resources
    	mView.myTTS.stop(); // stop TTS & shut it down
    	mView.myTTS.shutdown();
    }
}