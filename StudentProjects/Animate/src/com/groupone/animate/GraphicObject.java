package com.groupone.animate;

/**
 * @author Jamie Crawley
 * Date Created: 29/10/2012
 * Description:	Contains x coordinate & y coordinate, speech message, 
 * 				task (animate or speech) & string of character
 * 				selected (character1Bmp or character2Bmp)
 * History:		Jamie Crawley	30/10/2012 (Modified)
 */
public class GraphicObject {

	private String _task;
	private String _character;
	private String _message;
	private int _x;
	private int _y;

    public GraphicObject(String task, String character, String message, int x, int y) {
    	_task = task;
    	_character = character;
    	_message = message;
    	_x = x;
    	_y = y;
    }
    
    public String getTask() {
    	return _task;
    }
    
    public String getCharacter() {
    	return _character;
    }
    public String getMessage() {
    	return _message;
    }
    
    public int getX() {
    	return _x;
    }
    
    public int getY() {
    	return _y;
    }
}