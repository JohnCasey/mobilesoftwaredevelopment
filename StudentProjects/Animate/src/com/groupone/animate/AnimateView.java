package com.groupone.animate;

import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * @author Jamie Crawley
 * Date Created: 29/10/2012
 * Description: Main view controls touch events, animations,
 * 				music & text-to-speech(TTS)
 * History:		Jamie Crawley	30/10/2012	(Modified)
 * 				Jamie Crawley	 2/11/2012	(Modified)
 * 				Jamie Crawley	 3/11/2012	(Modified)
 */
public class AnimateView extends View implements OnInitListener {
    public SoundPool soundPool;
	public TextToSpeech myTTS;
	public boolean mRunning = false;
	public int numCharacters = 0;

	private boolean started = false;
	private float mX,  mY;		// character1 XY coordinates
	private float mX2, mY2;		// character2 XY coordinates
    private float volume;
    private int musicSoundID;
	private int timelineIndex = 0;
	private int lowestY;
	private int canvasW;
	private int canvasH;
	private String selectedBmp = "character1Bmp";
	private String character1Name;
	private String character2Name;
	private String task;
	private String message;
	private String character;
	private String startingCharacter;
	private Canvas mCanvas;
	private Rect npdBounds;
	private Bitmap character1Bmp, character2Bmp, canvasBmp;
	private NinePatchDrawable textLeftNpd, textRightNpd;
	public ArrayList<GraphicObject> timeline = new ArrayList<GraphicObject>();
	
	public AnimateView(Context context) {
		super(context);
        myTTS = new TextToSpeech(context, this);
		canvasW = AnimateActivity.display.getWidth();
		canvasH = AnimateActivity.display.getHeight();
		canvasBmp = Bitmap.createBitmap(canvasW, canvasH, Bitmap.Config.ARGB_8888);
		mCanvas = new Canvas(canvasBmp);
		mX = 10;
		mY = canvasH - 175;
		mX2 = 100;
		mY2 = canvasH - 175;
        npdBounds = new Rect();
        textLeftNpd = (NinePatchDrawable) getResources().getDrawable(R.drawable.lefttextbubble);
        textRightNpd = (NinePatchDrawable) getResources().getDrawable(R.drawable.righttextbubble);
        
        AudioManager aManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        float actualVolume = (float) aManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) aManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        volume = actualVolume / maxVolume;
     	soundPool = new SoundPool(12, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
            	soundPool.play(musicSoundID, volume, volume, 1, -1, 1f);
            	soundPool.autoPause(); // music is ready
            }
        });
     	musicSoundID = soundPool.load(context, R.raw.music, 1);
	}
	
	/** 
	 * Draws the images onto the canvas.
	 * invalidate() calls this method 
	 **/
	public void onDraw(Canvas canvas) {
		if (numCharacters == 2) {
			if (timelineIndex == 1) {
				if (character == "character1Bmp") {
					startingCharacter = "character1Bmp";
					canvas.drawBitmap(character1Bmp, mX, mY, null);
				}
				else if (character == "character2Bmp") {
					startingCharacter = "character2Bmp";
					canvas.drawBitmap(character2Bmp, mX2, mY2, null);
				}
			}
			else if (timelineIndex > 1) {
				if (character == startingCharacter && !started
						&& character == "character1Bmp")
					canvas.drawBitmap(character1Bmp, mX, mY, null);
				else if (character == startingCharacter && !started
						&& character == "character2Bmp")
					canvas.drawBitmap(character2Bmp, mX2, mY2, null);
				else if (character != startingCharacter && !started) {
					started = true;
					if (startingCharacter == "character1Bmp") 
						canvas.drawBitmap(character1Bmp, mX, mY, null);
					else if (startingCharacter == "character2Bmp") 
						canvas.drawBitmap(character2Bmp, mX2, mY2, null);
				}
				else if (started) {
					canvas.drawBitmap(character1Bmp, mX,  mY,  null);
					canvas.drawBitmap(character2Bmp, mX2, mY2, null);
				}
			}
			else if (timelineIndex == 0 && !mRunning) {
				canvas.drawBitmap(character1Bmp, mX, mY, null);
				canvas.drawBitmap(character2Bmp, mX2, mY2, null);
			}
			else if (timelineIndex == 0 && mRunning && character == "character1Bmp") {
				canvas.drawBitmap(character1Bmp, mX, mY, null);
			}
			else if (timelineIndex == 0 && mRunning && character == "character2Bmp") {
				canvas.drawBitmap(character2Bmp, mX2, mY2, null);
			}
		}
		else if (numCharacters != 0) canvas.drawBitmap(character1Bmp, mX,  mY,  null);
		
		if (task != "speech") {
			AnimateActivity.charText.setText("");
		}
		else if (task == "speech") {
			mCanvas = canvas;
			if (mY <= (canvasH / 5) * 2
					|| mY2 <= (canvasH / 5) * 2) {
				lowestY = (canvasH / 5) * 2;
			}
			else if (mY > mY2) lowestY = (int) mY2;
	        else if (mY <= mY2) lowestY = (int) mY;
			
	        npdBounds.set(20, 40, canvasW - 20, lowestY);
	        
	        if (character == "character1Bmp") {
	        	if (mX + character1Bmp.getWidth() / 2 <= canvasW / 2) {
	        		textLeftNpd.setBounds(npdBounds);
	        		textLeftNpd.draw(mCanvas);
	        	}
	        	else if (mX + character1Bmp.getWidth() > canvasW / 2) {
	        		textRightNpd.setBounds(npdBounds);
	        		textRightNpd.draw(mCanvas);
	        	}
	        }
	        else if (character == "character2Bmp") {
	        	if (mX2 + character1Bmp.getWidth() <= canvasW / 2) {
	        		textLeftNpd.setBounds(npdBounds);
	        		textLeftNpd.draw(mCanvas);
	        	}
	        	else if (mX2 + character1Bmp.getWidth() > canvasW / 2) {
	        		textRightNpd.setBounds(npdBounds);
	        		textRightNpd.draw(mCanvas);
	        	}
	        }
			canvas.drawBitmap(canvasBmp, 0, 0, null);
		}
	}
	
	/** 
	 * Handles onTouchEvent. Touch down on the screen
	 * selects character. Touch move stores GraphicObject
	 * into the arraylist(timeline).
	 **/
	public boolean onTouchEvent(MotionEvent event) {
		if (numCharacters != 0
				&& !mRunning)
		{
			float x = event.getX();
			float y = event.getY();
			int action = event.getAction();
			
			if (action == MotionEvent.ACTION_DOWN) {
				if ((x >= mX && x <= mX + character1Bmp.getWidth()) && 
						(y >= mY && y <= mY + character1Bmp.getHeight())) {
					selectedBmp = "character1Bmp";
				}
				else if (character2Bmp != null &&
						(x >= mX2 && x <= mX2 + character2Bmp.getWidth()) && 
						(y >= mY2 && y <= mY2 + character2Bmp.getHeight())) {
					selectedBmp = "character2Bmp";
				}
			}
			else if (action == MotionEvent.ACTION_MOVE) {
				GraphicObject g;
				if (selectedBmp == "character1Bmp") {
					mX = x - character1Bmp.getWidth() / 2;
					mY = y - character1Bmp.getHeight() / 2;
					
					g = new GraphicObject("animate", selectedBmp, "", (int) mX, (int) mY);
				}
				else {
					mX2 = x - character2Bmp.getWidth() / 2;
					mY2 = y - character2Bmp.getHeight() / 2;
					
					g = new GraphicObject("animate", selectedBmp, "", (int) mX2, (int) mY2);
				}
				timeline.add(g);
				invalidate();
			}
		}
		return true;
	}

	/**
	 * 	This will run when the play button is clicked
	 * 	Stops when the timeline arraylist has finished
	 **/
	public Runnable mUpdateState = new Runnable() {
		public void run() {
			if (timelineIndex < timeline.size()) {
				mRunning = true;
				GraphicObject g1 = timeline.get(timelineIndex);
				task = g1.getTask();
				character = g1.getCharacter();
				
				if (task == "animate" &&
						character == "character1Bmp") {
					mX = g1.getX();
					mY = g1.getY();
					timelineIndex++;
					invalidate();
					getHandler().postDelayed(this, 15);
				}
				else if (task == "animate" &&
						character == "character2Bmp") {
					mX2 = g1.getX();
					mY2 = g1.getY();
					timelineIndex++;
					invalidate();
					getHandler().postDelayed(this, 15);
				}
				else if (task == "speech" &&
						character == "character1Bmp") {
					message = g1.getMessage();
					AnimateActivity.charText.setText(character1Name + ": " + message);
					AnimateActivity.charText.bringToFront();
					invalidate();
					stopAnimating();
					myTTS.setSpeechRate(1.0f);
					myTTS.setPitch(1.9f);
					myTTS.speak(message, TextToSpeech.QUEUE_FLUSH, null);
					while (myTTS.isSpeaking()) {} // Pause while TTS is speaking
					timelineIndex++;
					getHandler().postDelayed(this, 1800);
				}
				else if (task == "speech" &&
						character == "character2Bmp") {
					message = g1.getMessage();
					AnimateActivity.charText.setText(character2Name + ": " + message);
					AnimateActivity.charText.bringToFront();
					invalidate();
					stopAnimating();
					myTTS.setPitch(0.7f);
					myTTS.setSpeechRate(0.9f);
					myTTS.speak(message, TextToSpeech.QUEUE_FLUSH, null);
					while (myTTS.isSpeaking()) {} // Pause while TTS is speaking
					timelineIndex++;
					getHandler().postDelayed(this, 1800);
				}
			}
			else {
				soundPool.autoPause();
				AnimateActivity.mainLayout.setVisibility(VISIBLE);
				stopAnimating();
				mRunning = false;
				started = false;
				task = "";
				timelineIndex = 0;
			}
		}
	};

	/** Sets the text for text-to-speech (TTS) */
	public void setSpeechText(String message) {
		GraphicObject g = new GraphicObject("speech", selectedBmp, message, 0, 0);
		timeline.add(g);
	}
	
	/** Starts the animation process */
	public void playAnimations() {
		if (timeline.size() != 0) {
			getHandler().removeCallbacks(mUpdateState);
			getHandler().post(mUpdateState);
			soundPool.autoResume();
		}
	}

	/** Stops the animation process */
	public void stopAnimating() {
		if (mUpdateState != null) {
			getHandler().removeCallbacks(mUpdateState);
			invalidate();
		}	
	}
	
	/** Resets program */
	public void Reset() {
		if (timeline.size() != 0) {
			timelineIndex = 0;
			character1Bmp = null;
			character2Bmp = null;
			numCharacters = 0;
			selectedBmp = "";
			timeline.clear();
			invalidate();
		}
	}
	
	/** Undo button deletes 10 arraylist GraphicObjects
	 * unless it has been undone down to the previous
	 * character */
	public void Undo() {
		if (numCharacters != 0) {
			timelineIndex = timeline.size() - 1;
			int count = 0;
			
			while (timelineIndex > 0 && count != 10) {
				GraphicObject g = timeline.get(timelineIndex);
				String c = g.getCharacter();
				timeline.remove(timelineIndex);
				timelineIndex--;
				g = timeline.get(timelineIndex);
				
				if (c == g.getCharacter()) count++;
				else count = 10;
			}
			timelineIndex = 0;
			invalidate();
		}
	}
	
	/** Selects character graphic, stores name of the character
	 * for speech bubbles and sets the new character as the
	 * selected character. If there is 2 characters, this will
	 * only change the selected character's graphic */
	public void selectCharacter(String s) {
		if (s == "bear") {
			if (numCharacters == 0) {
				character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bear);
				selectedBmp = "character1Bmp";
				character1Name = "BEAR";
				numCharacters++;
			}
			else if (numCharacters == 1) {
				character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bear);
				selectedBmp = "character2Bmp";
				character2Name = "BEAR";
				numCharacters++;
			}
			else if (numCharacters == 2) {
				if (selectedBmp == "character2Bmp") {
					character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bear);
					character2Name = "BEAR";
				}
				else if (selectedBmp == "character1Bmp") {
					character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bear);
					character1Name = "BEAR";
				}
			}
		}
		else if (s == "cat") {
			if (numCharacters == 0) {
				character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.cat);
				selectedBmp = "character1Bmp";
				character1Name = "CAT";
				numCharacters++;
			}
			else if (numCharacters == 1) {
				character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.cat);
				selectedBmp = "character2Bmp";
				character2Name = "CAT";
				numCharacters++;
			}
			else if (numCharacters == 2) {
				if (selectedBmp == "character2Bmp") {
					character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.cat);
					character2Name = "CAT";
				}
				else if (selectedBmp == "character1Bmp") {
					character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.cat);
					character1Name = "CAT";
				}
			}
		}
		else if (s == "dog") {
			if (numCharacters == 0) {
				character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.dog);
				selectedBmp = "character1Bmp";
				character1Name = "DOG";
				numCharacters++;
			}
			else if (numCharacters == 1) {
				character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.dog);
				selectedBmp = "character2Bmp";
				character2Name = "DOG";
				numCharacters++;
			}
			else if (numCharacters == 2) {
				if (selectedBmp == "character2Bmp") {
					character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.dog);
					character2Name = "DOG";
				}
				else if (selectedBmp == "character1Bmp") {
					character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.dog);
					character1Name = "DOG";
				}
			}
		}
		else if (s == "parrot") {
			if (numCharacters == 0) {
				character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.parrot);
				selectedBmp = "character1Bmp";
				character1Name = "PARROT";
				numCharacters++;
			}
			else if (numCharacters == 1) {
				character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.parrot);
				selectedBmp = "character2Bmp";
				character2Name = "PARROT";
				numCharacters++;
			}
			else if (numCharacters == 2) {
				if (selectedBmp == "character2Bmp") {
					character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.parrot);
					character2Name = "PARROT";
				}
				else if (selectedBmp == "character1Bmp") {
					character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.parrot);
					character1Name = "PARROT";
				}
			}
		}
		else if (s == "pirate") {
			if (numCharacters == 0) {
				character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.pirate);
				selectedBmp = "character1Bmp";
				character1Name = "PIRATE";
				numCharacters++;
			}
			else if (numCharacters == 1) {
				character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.pirate);
				selectedBmp = "character2Bmp";
				character2Name = "PIRATE";
				numCharacters++;
			}
			else if (numCharacters == 2) {
				if (selectedBmp == "character2Bmp") {
					character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.pirate);
					character2Name = "PIRATE";
				}
				else if (selectedBmp == "character1Bmp") {
					character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.pirate);
					character1Name = "PIRATE";
				}
			}
		}
		else if (s == "rabbit") {
			if (numCharacters == 0) {
				character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.rabbit);
				selectedBmp = "character1Bmp";
				character1Name = "RABBIT";
				numCharacters++;
			}
			else if (numCharacters == 1) {
				character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.rabbit);
				selectedBmp = "character2Bmp";
				character2Name = "RABBIT";
				numCharacters++;
			}
			else if (numCharacters == 2) {
				if (selectedBmp == "character2Bmp") {
					character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.rabbit);
					character2Name = "RABBIT";
				}
				else if (selectedBmp == "character1Bmp") {
					character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.rabbit);
					character1Name = "RABBIT";
				}
			}
		}
		else if (s == "sadParrot") {
			if (numCharacters == 0) {
				character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.sad_parrot);
				selectedBmp = "character1Bmp";
				character1Name = "SAD PARROT";
				numCharacters++;
			}
			else if (numCharacters == 1) {
				character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.sad_parrot);
				selectedBmp = "character2Bmp";
				character2Name = "SAD PARROT";
				numCharacters++;
			}
			else if (numCharacters == 2) {
				if (selectedBmp == "character2Bmp") {
					character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.sad_parrot);
					character2Name = "SAD PARROT";
				}
				else if (selectedBmp == "character1Bmp") {
					character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.sad_parrot);
					character1Name = "SAD PARROT";
				}
			}
		}
		else if (s == "tweety") {
			if (numCharacters == 0) {
				character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.tweety);
				selectedBmp = "character1Bmp";
				character1Name = "TWEETY";
				numCharacters++;
			}
			else if (numCharacters == 1) {
				character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.tweety);
				selectedBmp = "character2Bmp";
				character2Name = "TWEETY";
				numCharacters++;
			}
			else if (numCharacters == 2) {
				if (selectedBmp == "character2Bmp") {
					character2Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.tweety);
					character2Name = "TWEETY";
				}
				else if (selectedBmp == "character1Bmp") {
					character1Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.tweety);
					character1Name = "TWEETY";
				}
			}
		}
	}

	//check for successful instantiation
	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			if(myTTS.isLanguageAvailable(Locale.UK) == TextToSpeech.LANG_AVAILABLE)
				myTTS.setLanguage(Locale.UK);
		}
		else if (status == TextToSpeech.ERROR) {
			Log.e("TTS", "Text To Speech error");
		}
	}
}