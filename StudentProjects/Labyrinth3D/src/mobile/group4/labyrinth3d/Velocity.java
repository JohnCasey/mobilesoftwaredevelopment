package mobile.group4.labyrinth3d;
/**
 * Contains 2D velocity data
 * @author Dmitry Shuvaev
 */
public class Velocity {
	private float _xV;
	private float _yV;
	
	public Velocity (float xV, float yV)
	{
		_xV = xV;
		_yV = yV;
	}

	public float getXVelocity()
	{
		return _xV;
	}
	
	public float getYVelocity()
	{
		return _yV;
	}
}
