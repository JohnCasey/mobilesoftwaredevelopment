package mobile.group4.labyrinth3d;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBConnector extends SQLiteOpenHelper {

	public final static int VERSION = 1;
	public final static String NAME = "labyrinth3d1.db";
	public final static String TABLE = "CONFIGURATION";
	public final static String CID = "CID";
	public final static String LEVEL ="LEVEL";
	public final static String SOUNDS ="SOUNDS";
	public final static String GRAVITY_SCALE ="GSCALE";
	
	public final static int DEFAULT_CID = 1;
	public final static int DEFAULT_LEVEL = 0;
	public final static int DEFAULT_SOUNDS = 1; // 1 - yes; 0 - no;
	public final static int DEFAULT_GRAVITYSCALE = 10;
	
	private static DBConnector _instance = null;
	
	public static DBConnector getInstance(Context context)
	{
		if (_instance == null)
		{
			_instance = new DBConnector(context);
		}
		return _instance;
	}
	
	private DBConnector(Context context)
	{
		super(context, DBConnector.NAME, null, DBConnector.VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// Create table
		String sqlCommand = "create table " + DBConnector.TABLE + " (" +
				DBConnector.CID + " integer primary key, " + 
				DBConnector.LEVEL + " integer, " +
				DBConnector.SOUNDS + " integer, " +
				DBConnector.GRAVITY_SCALE + " integer);";
		
		db.execSQL(sqlCommand);
		
		//Fill default values
		ContentValues defaultValues = new ContentValues();
		defaultValues.put(DBConnector.CID, DBConnector.DEFAULT_CID);
		defaultValues.put(DBConnector.LEVEL, DBConnector.DEFAULT_LEVEL);
		defaultValues.put(DBConnector.SOUNDS, DBConnector.DEFAULT_SOUNDS);
		defaultValues.put(DBConnector.GRAVITY_SCALE, DBConnector.DEFAULT_GRAVITYSCALE);
		db.insert(DBConnector.TABLE, null, defaultValues);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		db.execSQL("DROP TABLE IF EXISTS " + DBConnector.TABLE);
	}

	public boolean getSounds()
	{
		boolean result = false;
		Cursor configuration = this.getWritableDatabase().query(DBConnector.TABLE, null, null, null, null, null, null);
		
		if (configuration.moveToFirst())
		{
			int soundsIndex = 0;
			
			String[] columnNames = configuration.getColumnNames();
			
			for (int columnIndex = 0; columnIndex < columnNames.length; columnIndex++)
			{
				if (columnNames[columnIndex].equalsIgnoreCase(DBConnector.SOUNDS ))
				{
					soundsIndex = columnIndex;
				}
			}
			
			result = configuration.getInt(soundsIndex) == 1? true : false;
		}
		
		return result;
		
	}

	public void setSounds(boolean areSoundsOn)
	{
		int sounds = areSoundsOn ? 1: 0;
		
		ContentValues newValues = new ContentValues();
		newValues.put(DBConnector.SOUNDS, sounds);
		String whereClause = DBConnector.CID + " = " + DBConnector.DEFAULT_CID;
		this.getWritableDatabase().update(DBConnector.TABLE, newValues, whereClause, null);

	}
	
	public int getLevel()
	{
		int result = DBConnector.DEFAULT_LEVEL;
		Cursor configuration = this.getWritableDatabase().query(DBConnector.TABLE, null, null, null, null, null, null);
		
		if (configuration.moveToFirst())
		{
			int levelIndex = 0;
			
			String[] columnNames = configuration.getColumnNames();
			
			for (int columnIndex = 0; columnIndex < columnNames.length; columnIndex++)
			{
				if (columnNames[columnIndex].equalsIgnoreCase(DBConnector.LEVEL ))
				{
					levelIndex = columnIndex;
				}
			}
			
			result = configuration.getInt(levelIndex);
		}
		
		return result;
		
	}
	
	public void setLevel(int levelIndex)
	{
		ContentValues newValues = new ContentValues();
		newValues.put(DBConnector.LEVEL, levelIndex);
		String whereClause = DBConnector.CID + " = " + DBConnector.DEFAULT_CID;
		this.getWritableDatabase().update(DBConnector.TABLE, newValues, whereClause, null);
	}
	
	public int getGravityScale()
	{
		int result = DBConnector.DEFAULT_GRAVITYSCALE;
		Cursor configuration = this.getWritableDatabase().query(DBConnector.TABLE, null, null, null, null, null, null);
		
		if (configuration.moveToFirst())
		{
			int gravityScaleIndex = 0;
			
			String[] columnNames = configuration.getColumnNames();
			
			for (int columnIndex = 0; columnIndex < columnNames.length; columnIndex++)
			{
				if (columnNames[columnIndex].equalsIgnoreCase(DBConnector.GRAVITY_SCALE ))
				{
					gravityScaleIndex = columnIndex;
				}
			}
			
			result = configuration.getInt(gravityScaleIndex);
		}
		
		return result;
		
	}
	
	public void setGravityScale(int gravityScale)
	{
		ContentValues newValues = new ContentValues();
		newValues.put(DBConnector.GRAVITY_SCALE, gravityScale);
		String whereClause = DBConnector.CID + " = " + DBConnector.DEFAULT_CID;
		this.getWritableDatabase().update(DBConnector.TABLE, newValues, whereClause, null);
	}
	
}
