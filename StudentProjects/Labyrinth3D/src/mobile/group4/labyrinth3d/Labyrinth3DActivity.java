package mobile.group4.labyrinth3d;

import java.util.ArrayList;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
/**
 * Describes application. Reads accelerometer data, reads initial data, defines labyrinth
 * @author Dmitry Shuvaev
 */
public class Labyrinth3DActivity extends Activity implements SensorEventListener, OnClickListener, OnLevelChangeListener {

	//Sensor
	private SensorManager _sensorManager;
	
	
	private MazeView _maze;
	
	private float _cmInInch = 2.54f;

	private ArrayList<TextView> _levelIndicators = new ArrayList<TextView>(4);
	private int _currentLabyrinthLevel = 0; 
	
	private Context _appContext;
	private float _gravityScale = 1;
	private TextView _txtVictory;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_labyrinth);

        _appContext = this.getApplicationContext();
        
		TextView tvLevel1 = (TextView) findViewById(R.id.level1);
		TextView tvLevel2 = (TextView) findViewById(R.id.level2);
		TextView tvLevel3 = (TextView) findViewById(R.id.level3);
		TextView tvLevel4 = (TextView) findViewById(R.id.level4);

		_levelIndicators.add(tvLevel1);
		_levelIndicators.add(tvLevel2);
		_levelIndicators.add(tvLevel3);
		_levelIndicators.add(tvLevel4);
        
        Button btExit = (Button)(this.findViewById(R.id.btMazeExit));
        
        btExit.setOnClickListener(this);
        
        _maze = (MazeView)findViewById(R.id.mazeView);
        
        
        _sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);       
        
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		float xMetrToPixel = dm.xdpi/_cmInInch*100;
		float yMetrToPixel = dm.ydpi/_cmInInch*100;

		//Get current level from database
		DBConnector db = DBConnector.getInstance(this.getApplicationContext());
		_gravityScale = 0.1f + db.getGravityScale()*0.1f;
		_currentLabyrinthLevel = db.getLevel();
		boolean areSoundsOn = db.getSounds();
		
		Labyrinth labyrinth = this.createLabyrinth();
		_maze.setOnLevelChangeListener(this	);
		_maze.setSoundsStatus(areSoundsOn);
		_maze.setXYConversionToPixels(xMetrToPixel, yMetrToPixel);
		_maze.setLabyrinth(labyrinth);
		_maze.setCurrentLevel(_currentLabyrinthLevel);
		
		_txtVictory = (TextView)findViewById(R.id.txtVictory);	
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.activity_gyroscope_prototype, menu);
        return true;
    }
    
    @Override
    protected void onResume() {
        super.onResume();
       
        _sensorManager.registerListener(
          this,
          _sensorManager.getDefaultSensor(
            Sensor.TYPE_ACCELEROMETER
          ),
            SensorManager.SENSOR_DELAY_UI);
    }
    @Override
    protected void onPause() {
        super.onPause();
        _sensorManager.unregisterListener(this);
    }    
    
    @Override
    protected void onStop() {
        super.onStop();
        
        this.stopActivity();
        
    }
    
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		
	}


	@Override
	public void onSensorChanged(SensorEvent event) {
		synchronized (this) {
			
			if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
				
				float gX = ((float)(Math.round(event.values[0]*10)))/10*_gravityScale;
				float gY = ((float)(Math.round(event.values[1]*10)))/10*_gravityScale;
				float gZ = ((float)(Math.round(event.values[2]*10)))/10*_gravityScale;
				
				Display display = getWindowManager().getDefaultDisplay();
				
				int rotation = display.getRotation();
				
				switch (rotation)
				{
					case Surface.ROTATION_0:
						_maze.setXYZAcceleration(-gX, gY, gZ);
						break;
					case Surface.ROTATION_90:
						_maze.setXYZAcceleration(gY, gX, gZ);
						break;
					case Surface.ROTATION_180:
						_maze.setXYZAcceleration(gX, -gY, gZ);
						break;
					case Surface.ROTATION_270:
						_maze.setXYZAcceleration(-gY, -gX, gZ);
						break;
				}
			
			}
		}
	}
	
	private Labyrinth createLabyrinth()
	{

		MazeLevel[] levels = new MazeLevel[4];
		
		//level 1
		Barier[] bariers1 = new Barier[13];
		
		bariers1[0] = new Barier(0.15f, 0.025f, 0.2f, 0.025f, BarierOrientation.Vertical);
		bariers1[1] = new Barier(0.0f, 0.35f, 0.3f, 0.03f, BarierOrientation.Horizontal);
		bariers1[2] = new Barier(0.3f, 0.15f, 0.23f, 0.025f, BarierOrientation.Vertical);
		bariers1[3] = new Barier(0.45f, 0.025f, 0.35f, 0.025f, BarierOrientation.Vertical);
		bariers1[4] = new Barier(0.45f, 0.21f, 0.15f, 0.03f, BarierOrientation.Horizontal);
		bariers1[5] = new Barier(0.7f, 0.025f, 0.65f, 0.025f, BarierOrientation.Vertical);
		bariers1[6] = new Barier(0.25f, 0.52f, 0.35f, 0.03f, BarierOrientation.Horizontal);
		bariers1[7] = new Barier(0.0f, 0.65f, 0.15f, 0.03f, BarierOrientation.Horizontal);
		bariers1[8] = new Barier(0.27f, 0.67f, 0.2f, 0.03f, BarierOrientation.Horizontal);
		bariers1[9] = new Barier(0.27f, 0.67f, 0.33f, 0.025f, BarierOrientation.Vertical);
		bariers1[10] = new Barier(0.6f, 0.67f, 0.33f, 0.025f, BarierOrientation.Vertical);
		bariers1[11] = new Barier(0.7f, 0.65f, 0.2f, 0.03f, BarierOrientation.Horizontal);
		bariers1[12] = new Barier(0.8f, 0.4f, 0.2f, 0.03f, BarierOrientation.Horizontal);

		
		Hole[] holes1 = new Hole[8];
		
		holes1[0] = new Hole(0.05f, 0.43f, 0.035f, HoleDirection.Down);
		//holes1[0] = new Hole(0.2f, 0.45f, 0.035f, HoleDirection.Down);
		
		holes1[1] = new Hole(0.5f, 0.09f, 0.035f, HoleDirection.Down);
		holes1[2] = new Hole(0.5f, 0.28f, 0.035f, HoleDirection.Down);
		holes1[3] = new Hole(0.05f, 0.73f, 0.035f, HoleDirection.Down);
		holes1[4] = new Hole(0.35f, 0.75f, 0.035f, HoleDirection.Down);
		holes1[5] = new Hole(0.95f, 0.9f, 0.035f, HoleDirection.Down);
		holes1[6] = new Hole(0.9f, 0.35f, 0.035f, HoleDirection.Down);
		holes1[7] = new Hole(0.67f, 0.09f, 0.035f, HoleDirection.Down);

		levels[0] = new MazeLevel(bariers1, holes1);
		
		//level 2
		Barier[] bariers2 = new Barier[11];

		bariers2[0] = new Barier(0.0f, 0.15f, 0.54f, 0.03f, BarierOrientation.Horizontal);
		bariers2[1] = new Barier(0.54f, 0.0f, 0.55f, 0.025f, BarierOrientation.Vertical);
		bariers2[2] = new Barier(0.54f, 0.2f, 0.46f, 0.03f, BarierOrientation.Horizontal);
		bariers2[3] = new Barier(0.43f, 0.15f, 0.55f, 0.025f, BarierOrientation.Vertical);
		bariers2[4] = new Barier(0.45f, 0.375f, 0.2f, 0.03f, BarierOrientation.Horizontal);
		bariers2[5] = new Barier(0.7f, 0.52f, 0.3f, 0.03f, BarierOrientation.Horizontal);
		bariers2[6] = new Barier(0.3f, 0.68f, 0.7f, 0.03f, BarierOrientation.Horizontal);
		bariers2[7] = new Barier(0.3f, 0.7f, 0.3f, 0.025f, BarierOrientation.Vertical);
		bariers2[8] = new Barier(0.15f, 0.375f, 0.4f, 0.025f, BarierOrientation.Vertical);
		bariers2[9] = new Barier(0.15f, 0.5f, 0.15f, 0.03f, BarierOrientation.Horizontal);
		bariers2[10] = new Barier(0.0f, 0.6f, 0.15f, 0.03f, BarierOrientation.Horizontal);
		
		Hole[] holes2 = new Hole[14];
		
		holes2[0] = new Hole(0.05f, 0.43f, 0.035f, HoleDirection.Up);
		holes2[1] = new Hole(0.5f, 0.09f, 0.035f, HoleDirection.Up);
		holes2[2] = new Hole(0.5f, 0.28f, 0.035f, HoleDirection.Up);
		holes2[3] = new Hole(0.05f, 0.73f, 0.035f, HoleDirection.Up);
		holes2[4] = new Hole(0.35f, 0.75f, 0.035f, HoleDirection.Up);
		holes2[5] = new Hole(0.95f, 0.9f, 0.035f, HoleDirection.Up);
		holes2[6] = new Hole(0.9f, 0.35f, 0.035f, HoleDirection.Up);
		holes2[7] = new Hole(0.67f, 0.09f, 0.035f, HoleDirection.Up);
		holes2[8] = new Hole(0.05f, 0.09f, 0.035f, HoleDirection.Down);
		holes2[9] = new Hole(0.95f, 0.09f, 0.035f, HoleDirection.Down);
		holes2[10] = new Hole(0.4f, 0.25f, 0.035f, HoleDirection.Down);
		holes2[11] = new Hole(0.2f, 0.6f, 0.035f, HoleDirection.Down);
		holes2[12] = new Hole(0.95f, 0.62f, 0.035f, HoleDirection.Down);
		holes2[13] = new Hole(0.6f, 0.9f, 0.035f, HoleDirection.Down);

		levels[1] = new MazeLevel(bariers2, holes2);
		
		//level 3
		Barier[] bariers3 = new Barier[9];
		
		bariers3[0] = new Barier(0.1f, 0.0f, 0.7f, 0.025f, BarierOrientation.Vertical);
		bariers3[1] = new Barier(0.5f, 0.0f, 1f, 0.025f, BarierOrientation.Vertical);
		bariers3[2] = new Barier(0.75f, 0.0f, 0.3f, 0.025f, BarierOrientation.Vertical);
		bariers3[3] = new Barier(0.8f, 0.5f, 0.5f, 0.025f, BarierOrientation.Vertical);
		bariers3[4] = new Barier(0.3f, 0.15f, 0.3f, 0.03f, BarierOrientation.Vertical);
		bariers3[5] = new Barier(0.1f, 0.15f, 0.2f, 0.03f, BarierOrientation.Horizontal);
		bariers3[6] = new Barier(0.3f, 0.44f, 0.2f, 0.03f, BarierOrientation.Horizontal);
		bariers3[7] = new Barier(0.5f, 0.5f, 0.5f, 0.03f, BarierOrientation.Horizontal);
		bariers3[8] = new Barier(0.1f, 0.7f, 0.25f, 0.03f, BarierOrientation.Horizontal);
		
		Hole[] holes3 = new Hole[10];
		
		holes3[0] = new Hole(0.05f, 0.09f, 0.035f, HoleDirection.Up);
		holes3[1] = new Hole(0.95f, 0.09f, 0.035f, HoleDirection.Up);
		holes3[2] = new Hole(0.4f, 0.25f, 0.035f, HoleDirection.Up);
		holes3[3] = new Hole(0.2f, 0.6f, 0.035f, HoleDirection.Up);
		holes3[4] = new Hole(0.95f, 0.62f, 0.035f, HoleDirection.Up);
		holes3[5] = new Hole(0.6f, 0.9f, 0.035f, HoleDirection.Up);
		holes3[6] = new Hole(0.18f, 0.09f, 0.035f, HoleDirection.Down);
		holes3[7] = new Hole(0.7f, 0.09f, 0.035f, HoleDirection.Down);
		holes3[8] = new Hole(0.15f, 0.78f, 0.035f, HoleDirection.Down);
		holes3[9] = new Hole(0.87f, 0.9f, 0.035f, HoleDirection.Down);
		
		levels[2] = new MazeLevel(bariers3, holes3);
		
		//level 4
		Barier[] bariers4 = new Barier[11];
		
		bariers4[0] = new Barier(0.25f, 0.0f, 0.25f, 0.025f, BarierOrientation.Vertical);
		bariers4[1] = new Barier(0.55f, 0.0f, 0.6f, 0.025f, BarierOrientation.Vertical);
		bariers4[2] = new Barier(0.15f, 0.25f, 0.25f, 0.025f, BarierOrientation.Vertical);
		bariers4[3] = new Barier(0.25f, 0.5f, 0.5f, 0.025f, BarierOrientation.Vertical);
		bariers4[4] = new Barier(0.5f, 0.6f, 0.2f, 0.025f, BarierOrientation.Vertical);
		bariers4[5] = new Barier(0.8f, 0.8f, 0.2f, 0.025f, BarierOrientation.Vertical);
		bariers4[6] = new Barier(0.15f, 0.25f, 0.125f, 0.03f, BarierOrientation.Horizontal);
		bariers4[7] = new Barier(0.55f, 0.2f, 0.3f, 0.03f, BarierOrientation.Horizontal);
		bariers4[8] = new Barier(0.0f, 0.5f, 0.25f, 0.03f, BarierOrientation.Horizontal);
		bariers4[9] = new Barier(0.4f, 0.6f, 0.6f, 0.03f, BarierOrientation.Horizontal);
		bariers4[10] = new Barier(0.8f, 0.8f, 0.1f, 0.03f, BarierOrientation.Horizontal);
		
		Hole[] holes4 = new Hole[5];
		
		holes4[0] = new Hole(0.18f, 0.09f, 0.035f, HoleDirection.Up);
		holes4[1] = new Hole(0.7f, 0.09f, 0.035f, HoleDirection.Up);
		holes4[2] = new Hole(0.15f, 0.78f, 0.035f, HoleDirection.Up);
		holes4[3] = new Hole(0.87f, 0.9f, 0.035f, HoleDirection.Up);
		holes4[4] = new Hole(0.5f, 0.09f, 0.035f, HoleDirection.Down);
		
		levels[3] = new MazeLevel(bariers4, holes4);
	
		Labyrinth result = new Labyrinth(levels);

		return result;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
			case R.id.btMazeExit:

				this.stopActivity();
				
				break;
		}
		
	}
	
	@Override
	public void onBackPressed()
	{
		this.stopActivity();
	}
	
	
	private void stopActivity()
	{
		try 
		{
			_maze.surfaceDestroyed(null);
		}
		catch (Exception e)
		{
			Log.w("Labyrinth3DActivity", "Attempt to close MazeView is unsuccessful. " + e.getMessage());
		}
		finally {
			this.finish();	
		}
	}

	@Override
	public void onLevelChanged(int levelIndex) {

		_currentLabyrinthLevel = levelIndex;
		 runOnUiThread(new Runnable() {
             public void run() {
         		for(TextView tv : _levelIndicators)
        		{
        			tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.back));
        		}
        		_levelIndicators.get(_currentLabyrinthLevel).setBackgroundDrawable(getResources().getDrawable(R.drawable.currentlevel));

        		DBConnector db = DBConnector.getInstance(_appContext);
         		db.setLevel(_currentLabyrinthLevel);
        		
             }
         });
		 

	}
	
	public void setVictoryText(final String txt)
	{
		this.runOnUiThread(new Runnable() {     
	        public void run() {         
	            _txtVictory.setText(txt);     
	        } 
	     });
	}
	
	
}

