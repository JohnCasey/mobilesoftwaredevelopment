package mobile.group4.labyrinth3d;

/**
 * Contains 2D point
 * @author Dmitry Shuvaev
 */
public class Point {

	private float _x;
	private float _y;
	
	public Point (float x, float y)
	{
		_x = x;
		_y = y;
	}

	public float getX()
	{
		return _x;
	}
	
	public float getY()
	{
		return _y;
	}
	
	
}
