package mobile.group4.labyrinth3d;
/**
 * Contains Barrier definition
 * @author Dmitry Shuvaev
 */
public class Barier {

	private float _xLeft = 0;
	private float _yUpper = 0;
	private float _length = 0;
	private float _width = 0;
	private BarierOrientation _orientation = BarierOrientation.Horizontal;
	
	public float getXLeft()
	{
		return _xLeft;
	}
	
	public float getYUpper()
	{
		return _yUpper;
	}
	
	public float getXRight()
	{
		float xRight = 0;
		switch(_orientation)
		{
			case  Horizontal:
				xRight = _xLeft + _length;
				break;
			case Vertical:
				xRight = _xLeft + _width;
				break;
			default:
				xRight = _xLeft + _length;
				break;
		}
		return xRight;
	}

	public float getYBottom()
	{
		float yBottom = 0;
		switch(_orientation)
		{
			case  Horizontal:
				yBottom = _yUpper + _width;
				break;
			case Vertical:
				yBottom = _yUpper + _length;
				break;
			default:
				yBottom = _yUpper + _width;
				break;
		}
		return yBottom;
	}
	
	
	public Barier(float xLeft, float yUpper, float length, float width, BarierOrientation orientation)
	{
		_xLeft = xLeft;
		_yUpper = yUpper;
		_length = length;
		_width = width;
		_orientation = orientation;
	}
	
}
