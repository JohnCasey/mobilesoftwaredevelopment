package mobile.group4.labyrinth3d;

/**
 * Listens for changing current level in a labyrinth
 * @author Dmitry Shuvaev
 *
 */
public interface OnLevelChangeListener {
	void onLevelChanged(int levelIndex);
	
}
