package mobile.group4.labyrinth3d;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.media.MediaPlayer;


/**
 * Represents ball and its movement in gravitational field
 * @author Dmitry Shuvaev
 */
public class Ball {
	
	private Bitmap _ball;
	private float _ballRadius;
	private float _time;
	
	private float _xMetrToPixel;
	private float _yMetrToPixel;
	
	private Point _startPoint;
	private Velocity _startVelocity = new Velocity(0,0);
	private Acceleration _gravity = new Acceleration(0,0);
	private float _zAcceleration = 0;
	private float _zLimitAcceleration = 11.8f; // m/s^2
	//Decrement of speed after collision
	private float _viscosity = 0.5f;
		
	private int _recursionCount = 0;
	
	private enum XCollisionState {No, Yes}
	private enum XCollisionDirection {Left, Right}
	
	private enum YCollisionState {No, Yes}
	private enum YCollisionDirection {Top, Bottom}
	
	public static final int MAX_RECURSION = 3;
	
	private MediaPlayer _collisionSound;
	private MediaPlayer _holeSound;
	
	private boolean _isSoundsOn = false;
	
	private boolean _isCollisionSoundPlayed = false;
	
	public Ball(Bitmap ball, float xMetrToPixel, float yMetrToPixel, float xStart, float yStart
			, boolean isSoundsOn
			, MediaPlayer collisionSound
			, MediaPlayer holeSound
			)
	{
		_ball = ball;
		
		_xMetrToPixel = xMetrToPixel;
		_yMetrToPixel = yMetrToPixel;
		
		_ballRadius = _ball.getWidth()/2; 
		_startPoint = new Point(xStart, yStart);
		
		_isSoundsOn = isSoundsOn;
		_collisionSound = collisionSound;
		_holeSound = holeSound;
		
		
		_time = 0;
	}
	
	
	
	public BallStatus drawBall(float time, float timeStep, float newXGravity, float newYGravity, Canvas canvas, Bitmap background)
	{
		_isCollisionSoundPlayed = false;
		BallStatus result = BallStatus.Move;
		
		float canvasWidth = canvas.getWidth();
		float canvasHeight = canvas.getHeight();
		
		boolean xStopped = false;
		boolean yStopped = false;
		
		//Calculate current coordinate
		Point currentCoordinate = GravityMovement.getCoordinate(time - _time, _startPoint, _startVelocity, _gravity);

		Point ballPoint = new Point(
				currentCoordinate.getX()*_xMetrToPixel - _ballRadius
				, currentCoordinate.getY()*_yMetrToPixel - _ballRadius
				);
		
		int x = (int)(currentCoordinate.getX()*_xMetrToPixel);
		int y = (int)(currentCoordinate.getY()*_yMetrToPixel);
		
		//Apply new value only for nextStep;
		this.setGravity(time, newXGravity, newYGravity);

		if (_gravity.getXAcceleration() > 0)
		{
			int checkX = (int)(_startPoint.getX()*_xMetrToPixel+_ballRadius+1);
			int checkY = (int)( _startPoint.getY()*_yMetrToPixel);
			
			if (checkX >= 0 && checkX < canvasWidth && checkY >=0 && checkY < canvasHeight)
			{

				int currentColor = background.getPixel(checkX, checkY);
				if (currentColor == Labyrinth.BARIER_COLOR && _startVelocity.getXVelocity() == 0)
				{
					xStopped = true;
					x = (int)(_startPoint.getX()*_xMetrToPixel);
				}
			}
		}

		if (_gravity.getXAcceleration() < 0)
		{
			int checkX = (int)(_startPoint.getX()*_xMetrToPixel-_ballRadius-1);
			int checkY = (int)( _startPoint.getY()*_yMetrToPixel);
			if (checkX >= 0 && checkX < canvasWidth && checkY >=0 && checkY < canvasHeight)
			{
				int currentColor = background.getPixel(checkX, checkY);
				if (currentColor == Labyrinth.BARIER_COLOR && _startVelocity.getXVelocity() == 0)
				{
					xStopped = true;
					x = (int)(_startPoint.getX()*_xMetrToPixel);
				}
			}
		}
		
		if (_gravity.getYAcceleration() > 0)
		{
			int checkX = (int)( _startPoint.getX()*_xMetrToPixel);
			int checkY = (int)(_startPoint.getY()*_yMetrToPixel+_ballRadius+1);
			if (checkX >= 0 && checkX < canvasWidth && checkY >=0 && checkY < canvasHeight)
			{

				int	currentColor = background.getPixel(checkX,checkY);
				if (currentColor == Labyrinth.BARIER_COLOR && _startVelocity.getYVelocity() == 0)
				{
					yStopped = true;
					y = (int)(_startPoint.getY()*_yMetrToPixel);
				}
			}
		}

		if (_gravity.getYAcceleration() < 0)
		{
			int checkX = (int)( _startPoint.getX()*_xMetrToPixel);
			int checkY = (int)(_startPoint.getY()*_yMetrToPixel+_ballRadius+1);
			if (checkX >= 0 && checkX < canvasWidth && checkY >=0 && checkY < canvasHeight)
			{

				int currentColor = background.getPixel(checkX,checkY);
				if (currentColor == Labyrinth.BARIER_COLOR && _startVelocity.getYVelocity() == 0)
				{
					yStopped = true;
					y = (int)(_startPoint.getY()*_yMetrToPixel);
				}
			}
		}
			
		float xCorrected = x;
		float yCorrected = y;
		
		XCollisionState xCollisionState = XCollisionState.No;
		XCollisionDirection xColDir = XCollisionDirection.Right;
		
		YCollisionState yCollisionState = YCollisionState.No;
		YCollisionDirection yColDir = YCollisionDirection.Top;
		
		
		//Check collision
		if (ballPoint.getX() >= 0 && ballPoint.getX() + 2*_ballRadius < canvasWidth 
				&& 
			ballPoint.getY() >= 0 && ballPoint.getY() + 2*_ballRadius < canvasHeight)
		{
			//for (int i = (int)_ballRadius + 1; i >=0; i--)
			for (int i = 1; i <= _ballRadius + 1 ; i++)
			{
				
				int collisionsCount = 0;

				if (!xStopped)
				{
					
					int currentXColorRight = background.getPixel(x + i, y);
					if(currentXColorRight == Labyrinth.BARIER_COLOR) 
					{
						xCollisionState = XCollisionState.Yes;
						xColDir = XCollisionDirection.Right;
						
						if (i <= _ballRadius)
						{
							xCorrected = x + i - _ballRadius;
							this.setCoordinates(xCorrected/_xMetrToPixel, yCorrected/_yMetrToPixel, time);
						}
						
						collisionsCount++;
					}
	
					int currentXColorLeft = background.getPixel(x - i, y);
					if (currentXColorLeft == Labyrinth.BARIER_COLOR)
					{
						xCollisionState = XCollisionState.Yes;
						xColDir = XCollisionDirection.Left;
						if (i <= _ballRadius)
						{
							xCorrected = x - i + _ballRadius;
							this.setCoordinates(xCorrected/_xMetrToPixel, yCorrected/_yMetrToPixel, time);
						}
						
						collisionsCount++;
					}
				}
				if (!yStopped)
				{
					int currentYColorBelow = background.getPixel(x, y + i);
					if (currentYColorBelow == Labyrinth.BARIER_COLOR)
					{
						yCollisionState = YCollisionState.Yes;
						yColDir = YCollisionDirection.Bottom;
						
						if (i <= _ballRadius)
						{
							yCorrected = y + i - _ballRadius;
							this.setCoordinates(xCorrected/_xMetrToPixel, yCorrected/_yMetrToPixel, time);
						}					
						
						collisionsCount++;
					}
	
					int currentYColorUpper = background.getPixel(x, y - i);
					if (currentYColorUpper == Labyrinth.BARIER_COLOR)
					{
						yCollisionState = YCollisionState.Yes;
					    yColDir = YCollisionDirection.Top;
						if (i <= _ballRadius)
						{
							yCorrected = y - i + _ballRadius;
							this.setCoordinates(xCorrected/_xMetrToPixel, yCorrected/_yMetrToPixel, time);
						}					
						
						collisionsCount++;
					}
				}
				if (collisionsCount > 0)
				{
					break;
				}
				

				
			}
			
			// Falling to low level
			int currentColorCenter = background.getPixel(x, y);
			if (currentColorCenter == Labyrinth.DOWN_HOLE_COLOR)
			{
				result = BallStatus.Fall;
				this.playHoleSound();
			}
			
			// Jumping on level up
			if (currentColorCenter == Labyrinth.UP_HOLE_COLOR)
			{
				if (_zAcceleration > _zLimitAcceleration)
				{
					result = BallStatus.Jump;
					
					Velocity currentVelocity = GravityMovement.getVelocity(time - _time, _startVelocity, _gravity);
					
					float xNewLevel = 100;
					float yNewLevel = 100;
					
					if (currentVelocity.getXVelocity() >=0)
					{
						xNewLevel = xCorrected + _ballRadius + 1;
					}
					else
					{
						xNewLevel = xCorrected - _ballRadius - 1;
					}
					
					if (currentVelocity.getYVelocity() >=0)
					{
						yNewLevel = yCorrected + _ballRadius + 1;
					}
					else
					{
						yNewLevel = yCorrected - _ballRadius - 1;
					}
					
					this.setCoordinates(xNewLevel/_xMetrToPixel, yNewLevel/_yMetrToPixel, time);
					
					this.playHoleSound();
				}
			}
			
			//X collision detected
			if (xCollisionState != XCollisionState.No)
			{
				this.setXCollision(time, xColDir);
			}
			//Y collision detected
			if (yCollisionState != YCollisionState.No)
			{
				this.setYCollision(time, yColDir);
			}
			
			canvas.drawBitmap(_ball, xCorrected - _ballRadius, yCorrected - _ballRadius, null);
			
		}
		else
		{
			// Exceptional case. Must be prevented.
		}
		
		
		Velocity currentVelocity = GravityMovement.getVelocity(time - _time, _startVelocity, _gravity);

		float currentVx = currentVelocity.getXVelocity();
		float currentVy = currentVelocity.getYVelocity();

		
		if (Math.abs( currentVelocity.getXVelocity()) <= GravityMovement.MINIMIM_VELOCITY)
		{
			currentVx = 0;
		}
		
		if (Math.abs( currentVelocity.getYVelocity()) <= GravityMovement.MINIMIM_VELOCITY)
		{
			currentVy = 0;
		}
		
		this.setVelocity(currentVx, currentVy, time);
		
		if (Math.abs( currentVelocity.getXVelocity() ) <= GravityMovement.MINIMIM_VELOCITY
				&&
			Math.abs(currentVelocity.getYVelocity()) <= GravityMovement.MINIMIM_VELOCITY)
		{
			return result;
		}
		
		_recursionCount = 0;
		//Calculate coordinate on a next time step. It handles cases when the ball covers during one time step 
		//distance to a nearest barrier. This calculation already takes into account possible collisions on current state
		this.FuturePoint(time, timeStep, time + timeStep, xStopped, yStopped, canvas, background);
		
		return result;
		
	}
	
	
	private void FuturePoint(float time, float timeStep, float timeEnd, boolean xStopped, boolean yStopped, Canvas canvas, Bitmap background)
	{
		
		float canvasWidth = canvas.getWidth();
		float canvasHeight = canvas.getHeight();

		// Two points - current and future
		Point currentCoordinate = GravityMovement.getCoordinate(time - _time, _startPoint, _startVelocity, _gravity);
		Point futureCoordinate = GravityMovement.getCoordinate(time + timeStep - _time, _startPoint, _startVelocity, _gravity);

		float x0 = currentCoordinate.getX()*_xMetrToPixel;
		float y0 = currentCoordinate.getY()*_yMetrToPixel;
	
		float x1 = futureCoordinate.getX()*_xMetrToPixel;
		float y1 = futureCoordinate.getY()*_yMetrToPixel;
		
		
		//if there is a barrier on a line between these two points then it is needed to calculate new future coordinate that is to be used on next step 
		// y = a*x + b
		// a = (y1 - y0)/(x1-x0)
		// b = y0 - (y1 - y0)/(x1-x0)*x0
		//It is important to take into account ball size when checking for collision

		if ((x0 == x1 && y0 == y1) || (xStopped && yStopped))
		{
			return;
		}
		
		
		boolean xUnchanged = false;
		if (x0 == x1)
		{
			// Check only y
			xUnchanged = true;
		}
		boolean yUnchanged = false;
		if (y0 == y1)
		{
			// Check only x
			yUnchanged = true;
		}
		
		
		
		//Check in the same direction as ball moves
		//Pixel by pixel
		int stepX = 1;
		int stepY = 1;
		
		if (x0 > x1) //from right to left
		{
			stepX = -1;
		} 
		else //from left to right - x0 == x1 - exceptional case, must be considered above
		{
			stepX = 1;
		}
		
		if (y0 > y1) // from Bottom to Top 
		{
			stepY = -1;
		}
		else // from Top to Bottom - y0 == y1 - exceptional case, must be considered above 
		{
			stepY = 1;
		}
		
		// x1 must not be equal to x0!
		float a = 0;
		if (x1 != x0)
		{
			a = (y1 - y0)/(x1 - x0);
		}
		
		float b = y0 - a*x0;
		
		
		float y = a*(x0 + stepX) + b;
		
		XCollisionState xCollisionState = XCollisionState.No;
		YCollisionState yCollisionState = YCollisionState.No;

		float collisionTime = 0;
		float yC = y0;
		float xC = x0;
		
		// Changing x strongly affects y - go by y way
		if (Math.abs(y - y0) > Math.abs(stepY) || xUnchanged)
		{
			while (Math.abs(yC - y1) > Math.abs(stepY))
			{
				yC += stepY;

				
				xC = xUnchanged ? x0 : (yC - b)/a;

				if (!xStopped)
				{
					int checkX = (int)(xC + stepX*(_ballRadius+1) );
					int checkY = (int)(yC);
					
					if (checkX >= 0 && checkX < canvasWidth && checkY >=0 && checkY < canvasHeight)
					{
					
						int currentXColor = background.getPixel(checkX, checkY);
						
						if (currentXColor == Labyrinth.BARIER_COLOR)
						{
							xCollisionState = XCollisionState.Yes;
						}
					}
					else
					{
						xCollisionState = XCollisionState.Yes;
					}
				}
				
				if (!yStopped)
				{
					int checkX = (int)(xC);
					int checkY = (int)(yC + stepY*(_ballRadius+1));
					
					if (checkX >= 0 && checkX < canvasWidth && checkY >=0 && checkY < canvasHeight)
					{
						int currentYColor = background.getPixel(checkX, checkY);
						
						if (currentYColor == Labyrinth.BARIER_COLOR)
						{
							yCollisionState = YCollisionState.Yes;
						}
					}
					else
					{
						yCollisionState = YCollisionState.Yes;
					}
				}
				if (xCollisionState == XCollisionState.Yes || yCollisionState == YCollisionState.Yes)
				{
					// Calculate time
					// t = (-v0 +- sqrt(v0^2 - 2*g * (y0 - yC)))/g
					float v0 = _startVelocity.getYVelocity();
					float g = _gravity.getYAcceleration();
					
					if (g == 0)
					{
						collisionTime = (yC - y0)/(_yMetrToPixel*v0) + _time;
					}
					else
					{

					
						float collisionTime1 = (-v0 - android.util.FloatMath.sqrt(v0*v0 - 2*g*(y0-yC)/_yMetrToPixel))/g + _time;
						float collisionTime2 = (-v0 + android.util.FloatMath.sqrt(v0*v0 - 2*g*(y0-yC)/_yMetrToPixel))/g + _time;
						
						if (v0 < 0)
						{
							collisionTime = collisionTime1;
						}
						else
						{
							collisionTime = collisionTime2;
						}
					}
					
					//collisionTime = (-v0 + android.util.FloatMath.sqrt(v0*v0 - 2*g*(y0-yC)/_yMetrToPixel))/g + _time;

					break;
				}
				
			}
		}
		else // go by x way
		{
			while (Math.abs(xC - x1) > Math.abs(stepX))
			{
				xC += stepX;
				
				yC = yUnchanged ? y0 : a*xC + b;

				if (!xStopped)
				{
					int checkX = (int)(xC + stepX*(_ballRadius+1) );
					int checkY = (int)(yC);
					
					if (checkX >= 0 && checkX < canvasWidth && checkY >=0 && checkY < canvasHeight)
					{
					
						int currentXColor = background.getPixel(checkX, checkY);
						
						if (currentXColor == Labyrinth.BARIER_COLOR)
						{
							xCollisionState = XCollisionState.Yes;
						}
					}
					else
					{
						xCollisionState = XCollisionState.Yes;
					}
				}
				
				if (!yStopped)
				{
					int checkX = (int)(xC);
					int checkY = (int)(yC + stepY*(_ballRadius+1));
					
					if (checkX >= 0 && checkX < canvasWidth && checkY >=0 && checkY < canvasHeight)
					{
						int currentYColor = background.getPixel(checkX, checkY);
						
						if (currentYColor == Labyrinth.BARIER_COLOR)
						{
							yCollisionState = YCollisionState.Yes;
						}
					}
					else
					{
						yCollisionState = YCollisionState.Yes;
					}
				}
				if (xCollisionState == XCollisionState.Yes || yCollisionState == YCollisionState.Yes)
				{
					// Calculate time
					// t = (-v0 +- sqrt(v0^2 - 2*g * (y0 - yC)))/g
					float v0 = _startVelocity.getXVelocity();
					float g = _gravity.getXAcceleration();
					
					if (g == 0)
					{
						collisionTime = (xC - x0)/(_xMetrToPixel*v0) + _time;
					}
					else
					{
						float collisionTime1 = (-v0 - android.util.FloatMath.sqrt(v0*v0 - 2*g*(x0-xC)/_xMetrToPixel))/g;
						float collisionTime2 = (-v0 + android.util.FloatMath.sqrt(v0*v0 - 2*g*(x0-xC)/_xMetrToPixel))/g;
						
						if (v0 < 0)
						{
							collisionTime = collisionTime1 + _time;
							if (collisionTime1 < 0)
							{
								collisionTime = collisionTime2 + _time;
							}
						}
						else
						{
							collisionTime = collisionTime2 + _time;
							
							if (collisionTime2 < 0)
							{
								collisionTime = collisionTime1 + _time;
							}
							
						}
					}

					
					//collisionTime = (-v0 + android.util.FloatMath.sqrt(v0*v0 - 2*g*(x0-xC)/_xMetrToPixel))/g + _time;

					
					
					break;
				}
				
			}
			
		}
		
		if (xCollisionState == XCollisionState.Yes || yCollisionState == YCollisionState.Yes)
		{
			if (xCollisionState == XCollisionState.Yes)
			{
				this.setXCollision(collisionTime);
			}
			
			if (yCollisionState == YCollisionState.Yes)
			{
				this.setYCollision(collisionTime);
			}

			//It is still possible that there are several barriers, thus some recursion is needed
			
			float currentTimeStep = timeEnd - collisionTime;
			if (currentTimeStep > 0)
			{
				_recursionCount++;
				if (_recursionCount < Ball.MAX_RECURSION)
				{
					this.FuturePoint(collisionTime, currentTimeStep,  timeEnd,xStopped, yStopped, canvas, background);
				}
			}
		}
		
		
	}
	
	
	private void setXCollision(float time)
	{
		_startPoint =  GravityMovement.getCoordinate(time - _time, _startPoint, _startVelocity, _gravity);
		_startVelocity = new Velocity(-_viscosity*_startVelocity.getXVelocity(), _startVelocity.getYVelocity());
		_time = time;
		this.playCollisionSound();
	}
	
	private void setYCollision(float time)
	{

		_startPoint =  GravityMovement.getCoordinate(time - _time, _startPoint, _startVelocity, _gravity);
		_startVelocity = new Velocity(_startVelocity.getXVelocity(), -_viscosity*_startVelocity.getYVelocity());

		_time = time;
		this.playCollisionSound();
	}

	private void setXCollision(float time, XCollisionDirection xColDir)
	{
		if (xColDir == XCollisionDirection.Left)
		{
			if (_startVelocity.getXVelocity() > 0)
			{
				return;
			}

		}
		else
		{
			if (_startVelocity.getXVelocity() < 0)
			{
				return;
			}
		}
		_startPoint =  GravityMovement.getCoordinate(time - _time, _startPoint, _startVelocity, _gravity);
		_startVelocity = new Velocity(-_viscosity*_startVelocity.getXVelocity(), _startVelocity.getYVelocity());
		_time = time;
	}
	
	private void setYCollision(float time, YCollisionDirection yColDir)
	{
		if (yColDir == YCollisionDirection.Top)
		{
			if (_startVelocity.getYVelocity() > 0)
			{
				return;
			}
		}
		else
		{
			if (_startVelocity.getYVelocity() < 0)
			{
				return;
			}
		}
		
		_startPoint =  GravityMovement.getCoordinate(time - _time, _startPoint, _startVelocity, _gravity);
		_startVelocity = new Velocity(_startVelocity.getXVelocity(), -_viscosity*_startVelocity.getYVelocity());

		_time = time;
	}

	
	
	private void setGravity(float time, float xG, float yG)
	{
		_startPoint =  GravityMovement.getCoordinate(time - _time, _startPoint, _startVelocity, _gravity);
		_startVelocity = GravityMovement.getVelocity(time - _time,_startVelocity, _gravity);
		
		_gravity = new Acceleration(xG, yG);
		_time = time;
	}
	
	
	private void setCoordinates(float x, float y, float time)
	{
		_startPoint = new Point (x,y);
		_startVelocity = GravityMovement.getVelocity(time - _time,_startVelocity, _gravity);
		
		_time = time;
	}
	
	private void setVelocity(float xV, float yV, float time)
	{
		_startPoint =  GravityMovement.getCoordinate(time - _time, _startPoint, _startVelocity, _gravity);
		_startVelocity = new Velocity(xV, yV);
		
		_time = time;
	}

	
	public void setZAcceleration(float zG)
	{
		_zAcceleration = zG;
	}

	private void playCollisionSound()
	{
		if (_isSoundsOn && !_isCollisionSoundPlayed)
		{
			_isCollisionSoundPlayed = true;
			_collisionSound.start();
		}
	}

	private void playHoleSound()
	{
		if (_isSoundsOn)
		{
			_holeSound.start();
		}
	}

	
}

