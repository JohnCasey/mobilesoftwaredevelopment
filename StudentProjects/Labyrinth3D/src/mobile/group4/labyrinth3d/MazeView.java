/**
 * 
 */
package mobile.group4.labyrinth3d;


import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Draws labyrinth, ball movements. Registers collisions and falling (jumping) into holes;
 * @author Dmitry Shuvaev
 */
public class MazeView extends SurfaceView implements Runnable, SurfaceHolder.Callback
{

	//Animation
	private Thread _animationThread; 
	private boolean _isRunning = false;  
	
	//Draw
	private Bitmap _buferBitmap;		
		
	
	//Ball movement
	
	float _xStart = 50;
	float _yStart = 150;
	float _time = 0; //s
	float _timeStep = 3; // ms
	float _zAcceleration = 0; 
	float _zDefaultAcceleration = 9.8f; // m/s^2
	
	//Conversion from meter to pixels
	
	private float _xMetrToPixel = 7000;
	private float _yMetrToPixel = 7000;

	//Maze
	private boolean _isNewLevel = true;
	private boolean _isMazeDefined = false;
	
	private Labyrinth _labyrinth;
	private int _currentLevelIndex = 0;
	
	
	private Ball _ballToDraw;
	private float _xGravity;
	private float _yGravity;
	
	//Setup
	private boolean _areSoundsOn = true;
	
	//Event
	private ArrayList<OnLevelChangeListener> _listeners = new ArrayList<OnLevelChangeListener> ();
	
	
	public void setXYConversionToPixels(float xMetrToPixel, float yMetrToPixel)
	{
		_xMetrToPixel = xMetrToPixel;
		_yMetrToPixel = yMetrToPixel;
		
		MediaPlayer collisionSound = MediaPlayer.create(this.getContext(), mobile.group4.labyrinth3d.R.raw.collision);
		MediaPlayer holeSound = MediaPlayer.create(this.getContext(), mobile.group4.labyrinth3d.R.raw.disapear);
		
		//TODO: ball just taken from the Internet. Possibly it is wise to draw out own
		Bitmap ball = BitmapFactory.decodeResource(getResources(), mobile.group4.labyrinth3d.R.drawable.ball);

		float screenWidth = this.getWidth();
		float scale = 1;
		
		if (screenWidth <= 320)
		{
			scale = 0.7f;
		} else if (screenWidth > 320 && screenWidth <= 480)
		{
			scale = 1f;
		} else if (screenWidth > 480 && screenWidth <= 800)
		{
			scale = 1.2f;
		} else if (screenWidth > 800 && screenWidth <= 1024)
		{
			scale = 1.4f;
		} else if (screenWidth > 1024 && screenWidth <= 1280)
		{
			scale = 1.6f;
		} else
		{
			scale = 1.7f;
		}
		

		Bitmap scaledBall = Bitmap.createScaledBitmap(ball,
			Math.round(ball.getWidth()*scale), Math.round(ball.getHeight()*scale),
			false);
		
		_ballToDraw = new Ball(scaledBall, _xMetrToPixel, _yMetrToPixel, _xStart/_xMetrToPixel, _yStart/_yMetrToPixel
				, _areSoundsOn
				, collisionSound
				, holeSound
				);
	}
	
	public void setXYZAcceleration(float xG, float yG, float zG)
	{
		_xGravity = xG;
		_yGravity = yG;
		
		_zAcceleration = zG;
		_ballToDraw.setZAcceleration(zG);
	}
	
	public void setLabyrinth(Labyrinth labyrinth)
	{
		_labyrinth = labyrinth;
		_isMazeDefined = true;
	}
	
	public void setCurrentLevel(int currentLevelIndex)
	{
		if (_labyrinth == null)
		{
			return;
		}
		if (currentLevelIndex < _labyrinth.getLevels().length && currentLevelIndex >= 0)
		{
			_currentLevelIndex = currentLevelIndex;
			
			for (OnLevelChangeListener listener : _listeners) 
			{
			    listener.onLevelChanged(_currentLevelIndex);
			}
		}
	}
	public void setSoundsStatus(boolean areSoundsOn)
	{
		_areSoundsOn = areSoundsOn;
	}
	
	public MazeView(Context context, AttributeSet set)
	{
		super(context, set);
		
		SurfaceHolder holder = this.getHolder();
		holder.addCallback(this);
		
	}	
		
	
	
	public void setOnLevelChangeListener (OnLevelChangeListener listener) 
    {
		_listeners.add(listener);
    }
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {		
		super.onSizeChanged(w, h, oldw, oldh);
		
		_buferBitmap = Bitmap.createBitmap(w, h, Config.ARGB_8888); 
		Canvas bufferCanvas = new Canvas(_buferBitmap);
		bufferCanvas.drawColor(Color.WHITE);
	}
	
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {

	}

	public void surfaceCreated(SurfaceHolder arg0) {
	
		_animationThread = new Thread(this);
		_isRunning = true;
		_animationThread.start();
	}

	public void surfaceDestroyed(SurfaceHolder arg0) {
		
		_isRunning = false;
		
		if (_animationThread != null)
		{
			try 
			{
				_animationThread.join();  
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}	
	

	@Override
	public void run() {
		while(_isRunning){
			Canvas canvas = null;
			SurfaceHolder holder = this.getHolder();
			synchronized(holder)
			{
				if (_isMazeDefined)
				{
					canvas = holder.lockCanvas();
					canvas.save();
					
					if (_buferBitmap == null)
					{
						_buferBitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Config.ARGB_8888);
					}
					

					if (_isNewLevel)
					{
						_buferBitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Config.ARGB_8888);
						Canvas bufferCanvas = new Canvas(_buferBitmap);
						bufferCanvas.drawColor(Color.WHITE);
						
						this.drawMaze();
						_isNewLevel = false;
					}
					
					canvas.drawBitmap(_buferBitmap, 0, 0, null);
	
					_time += _timeStep/1000;
	
					BallStatus currentBallStatus = _ballToDraw.drawBall(_time, _timeStep/1000, _xGravity, _yGravity, canvas, _buferBitmap);

					this.checkBallStatus(currentBallStatus, _buferBitmap);

					canvas.restore();
				}
			}

			try {
				Thread.sleep((int)_timeStep);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			holder.unlockCanvasAndPost(canvas);
		}
	}

	//Draws current level of labyrinth
	private void drawMaze()
	{
		Canvas intermCanvas = new Canvas(_buferBitmap);
		Paint p = new Paint();
		p.setColor(Labyrinth.BARIER_COLOR);

		//Border (always needed)
		p.setStyle(Paint.Style.STROKE);
		p.setStrokeWidth(60);
		intermCanvas.drawRect(0, 0, this.getWidth(),this.getHeight(),p);

		
		//Maze
		p.setStrokeWidth(1);
		p.setStyle(Paint.Style.FILL);
	
		int canvasHeight = intermCanvas.getHeight();
		int canvasWidth = intermCanvas.getWidth();
		
		MazeLevel currentLevel = _labyrinth.getLevel(_currentLevelIndex);
		Barier[] bariers = currentLevel.getBariers();
		
		
		
		//Bariers

		for(int barierIndex = 0; barierIndex < bariers.length; barierIndex++)
		{
			Barier barier = bariers[barierIndex];
			
			int xLeft = (int)(barier.getXLeft()*canvasWidth);
			int xRight = (int)(barier.getXRight()*canvasWidth);
			int yUpper = (int)(barier.getYUpper()*canvasHeight);
			int yBottom = (int)(barier.getYBottom()*canvasHeight);
			
			intermCanvas.drawRect(xLeft, yUpper, xRight, yBottom, p);
		}
		
	
		// Holes
		
		Hole[] holes = currentLevel.getHoles();
		for (int holeIndex = 0; holeIndex < holes.length; holeIndex++)
		{
			Hole hole = holes[holeIndex];
			float x = hole.getX()*canvasWidth;
			float y = hole.getY()*canvasHeight;
			float radius = hole.getRadius()*canvasHeight;
			HoleDirection direction = hole.getDirection();
			switch (direction)
			{
				case Up:
					p.setColor(Labyrinth.UP_HOLE_COLOR);
					break;
				case Down:
					p.setColor(Labyrinth.DOWN_HOLE_COLOR);
					break;
			}
			intermCanvas.drawCircle(x, y, radius, p);
		}
		

		
		
	}
	
	// Checks the status of a ball and reacts by changing current level of the labyrinth
	// raises event of level change
	private void checkBallStatus(BallStatus currentStatus, Bitmap background)
	{
		switch (currentStatus)
		{
			case Fall:
				if (_labyrinth.getLevels().length == _currentLevelIndex + 1)
				{
					((Labyrinth3DActivity) getContext()).setVictoryText("Labyrinth Solved!");					
				}
				else
				{
					_currentLevelIndex++;
					_isNewLevel = true;
				
					
					for (OnLevelChangeListener listener : _listeners) 
					{
					    listener.onLevelChanged(_currentLevelIndex);
					}
				}
			break;
			
			case Jump:
				if (_currentLevelIndex == 0)
				{
					//It is exceptional case. There must not be UP holes on the first level
					//Do nothing
				}
				else
				{
					_currentLevelIndex--;
					_isNewLevel = true;
					
					for (OnLevelChangeListener listener : _listeners) 
					{
					    listener.onLevelChanged(_currentLevelIndex);
					}
				}

				break;
			default:
				break;
		}

		
	}



}
