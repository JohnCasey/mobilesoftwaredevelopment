package mobile.group4.labyrinth3d;
/**
 * Describes falling movement in gravitational field.
 * If a falling object achieves a bottom level it will be 
 * thrown up with certain decrement 
 * @author Dmitry Shuvaev
 */
public class GravityMovement {

	public static final float MINIMIM_VELOCITY = 0.005f;
	public static final float MAXIMUM_GRAVITY_SCALE = 1;
	public static final float MINIMUM_GRAVITY_SCALE = 0.1f;
	
	public static Point getCoordinate(float time, Point startPoint, Velocity startVelocity, Acceleration a)
	{
		float x = startPoint.getX() + startVelocity.getXVelocity()*time + a.getXAcceleration()*time*time/2;
		float y = startPoint.getY() + startVelocity.getYVelocity()*time + a.getYAcceleration()*time*time/2;
		
		Point result = new Point (x,y);

		return result;
	}
	
	public static Velocity getVelocity(float time, Velocity startVelocity, Acceleration a)
	{
		float xV = startVelocity.getXVelocity() + a.getXAcceleration()*time;
		float yV = startVelocity.getYVelocity() + a.getYAcceleration()*time;
		
		Velocity result = new Velocity(xV,yV);
		return result;
	}


	
}
