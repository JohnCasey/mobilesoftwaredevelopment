package mobile.group4.labyrinth3d;
/**
 * Contains 2D acceleration data
 * @author Dmitry Shuvaev
 */
public class Acceleration {
	private float _xA;
	private float _yA;
	
	public Acceleration (float xA, float yA)
	{
		_xA = xA;
		_yA = yA;
	}

	public float getXAcceleration()
	{
		return _xA;
	}
	
	public float getYAcceleration()
	{
		return _yA;
	}
}
