package mobile.group4.labyrinth3d;
/**
 * Contains Hole definition
 * @author Dmitry Shuvaev
 */
public class Hole {
	private float _x = 0;
	private float _y = 0;
	private float _radius = 0;
	
	private HoleDirection _direction = HoleDirection.Down;
	
	public float getX()
	{
		return _x;
	}
	
	public float getY()
	{
		return _y;
	}

	public float getRadius()
	{
		return _radius;
	}
	
	public HoleDirection getDirection()
	{
		return _direction;
	}
	
	
	public Hole(float x, float y, float radius, HoleDirection direction)
	{
		_x = x;
		_y = y;
		_radius = radius;
		_direction = direction;
	}
	
	
	
}
