package mobile.group4.labyrinth3d;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
/**
 * Set options activity
 * @author Dmitry Shuvaev
 *
 */
public class OptionsActivity extends Activity implements
		OnCheckedChangeListener, OnClickListener, OnSeekBarChangeListener {

	private final static int MAX_GRAVITY_CONTROL = 10;
	
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.options_layout);

    	Button btOptionsExit = (Button)( this.findViewById(R.id.btOptionsExit) );
    	btOptionsExit.setOnClickListener(this);
   	
    	CheckBox chbSounds = (CheckBox)( this.findViewById(R.id.chbSounds) );
    	
    	
    	// get checked from database
    	
		DBConnector db = DBConnector.getInstance(this.getApplicationContext());
    	boolean checked = db.getSounds();
    	
    	chbSounds.setChecked(checked);
    	chbSounds.setOnCheckedChangeListener(this);
    	
    	SeekBar sbGravityControl = (SeekBar) (this.findViewById(R.id.sbGravityControl) );
    
    	sbGravityControl.setMax(MAX_GRAVITY_CONTROL);
    	
    	// get last scale from database;
    	
    	sbGravityControl.setProgress(db.getGravityScale());
    	sbGravityControl.setOnSeekBarChangeListener(this);
    	
    }

	
	@Override
	public void onCheckedChanged(CompoundButton v, boolean checkedState) {

		
		switch(v.getId())
		{
			case R.id.chbSounds:
				boolean areSoundsOn = checkedState;

				// Write to database
				
				DBConnector db = DBConnector.getInstance(this.getApplicationContext());
				db.setSounds(areSoundsOn);
				
				break;
		}
	}
	
	@Override
	public void onClick(View v) {

		switch(v.getId())
		{
		case R.id.btOptionsExit:
			finish();
			break;		
			
		}

	}


	@Override
	public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
	}

	@Override
	public void onStartTrackingTouch(SeekBar arg0) {
	}


	@Override
	public void onStopTrackingTouch(SeekBar control) {

		int currentProgress = control.getProgress();
		
		DBConnector db = DBConnector.getInstance(this.getApplicationContext());
		db.setGravityScale(currentProgress);
	
	}

}
