package mobile.group4.labyrinth3d;

/**
 * Contains single Maze level definition
 * @author Dmitry Shuvaev
 */
public class MazeLevel {

	private Barier[] _bariers;
	private Hole[] _holes;

	public Barier[] getBariers()
	{
		return _bariers;
	}
	
	public Hole[] getHoles()
	{
		return _holes;
	}
	
	public MazeLevel(Barier[] bariers, Hole[] holes)
	{
		_bariers = bariers;
		_holes = holes;
	}
}
