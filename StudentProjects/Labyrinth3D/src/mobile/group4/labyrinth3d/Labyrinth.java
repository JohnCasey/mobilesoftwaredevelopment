package mobile.group4.labyrinth3d;

import android.graphics.Color;
/**
 * Contains Labyrinth definition
 * @author Dmitry Shuvaev
 */
public class Labyrinth {

	public final static int BARIER_COLOR = Color.BLACK;
	public final static int DOWN_HOLE_COLOR = Color.BLUE;
	public final static int UP_HOLE_COLOR = Color.RED;
	
	private MazeLevel[] _levels;
	
	public MazeLevel[] getLevels()
	{
		return _levels;
	}
	
	public MazeLevel getLevel(int levelIndex)
	{
		return _levels[levelIndex];
	}
	
	public Labyrinth(MazeLevel[] levels)
	{
		_levels = levels;
	}
}
