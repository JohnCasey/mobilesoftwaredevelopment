/**
 * 
 */
package mobile.group4.labyrinth3d;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * Entry point to the application. Contains menu allowing starting new game, load old game, and set options
 * @author Dmitry Shuvaev
 *
 */
public class EntryPoint extends Activity implements OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.entry_point_layout);

    	
    	Button btNewGame = (Button)( this.findViewById(R.id.btNewGame) );
    	btNewGame.setOnClickListener(this);
    	
    	Button btLoadGame = (Button)( this.findViewById(R.id.btLoadGame) );
    	btLoadGame.setOnClickListener(this);

    	Button btOptions = (Button)( this.findViewById(R.id.btOptions) );
    	btOptions.setOnClickListener(this);

    	Button btExit = (Button)( this.findViewById(R.id.btExit) );
    	btExit.setOnClickListener(this);

    
    	
    }
	
	@Override
	public void onClick(View v) {

		switch(v.getId())
		{
		case R.id.btNewGame:
			// Write current level to database
	 		DBConnector db = DBConnector.getInstance(this.getApplicationContext());
	 		db.setLevel(DBConnector.DEFAULT_LEVEL);

			Intent newGame = new Intent (this, Labyrinth3DActivity.class );
			startActivity(newGame);
			break;
		case R.id.btLoadGame:
			// Do nothing - level is already in the database
			Intent loadGame = new Intent (this, Labyrinth3DActivity.class );
			startActivity(loadGame);
			break;
		
		case R.id.btOptions:
			
			Intent options = new Intent (this,OptionsActivity.class);
			startActivity(options);
			break;
			
		case R.id.btExit:
			finish();
			break;		
			
		}

		
	}

}
